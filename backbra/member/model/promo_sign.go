package model

import (
	"database/sql"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"
	"member/contrib/helper"
	ryrpc "member/rpc"
	"time"
)

// 签到活动配置
func PromoSignConfigList() ([]PromoSignConfig, error) {

	var data []PromoSignConfig
	query, _, _ := dialect.From("tbl_promo_sign_config").
		Select(colsPromoSignConfig...).Where(g.Ex{}).Order(g.C("vip").Asc()).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}

// 签到记录
func PromoSignMemberRecord(mb ryrpc.TblMemberBase) (PromoSignRecord, error) {

	data := PromoSignRecord{}
	query, _, _ := dialect.From("tbl_promo_sign_record").Select(colsPromoSignRecord...).Where(g.Ex{"uid": mb.Uid}).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&data, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		data.UID = mb.Uid
		data.Username = mb.Username
		data.Vip = mb.Vip
		data.Sign1 = "0"
		data.Sign2 = "0"
		data.Sign3 = "0"
		data.Sign4 = "0"
		data.Sign5 = "0"
		data.Sign6 = "0"
		data.Sign7 = "0"
		data.Sign8 = "0"
		data.Sign9 = "0"
		data.Sign10 = "0"
		data.Sign11 = "0"
		data.Sign12 = "0"
		data.Sign13 = "0"
		data.Sign14 = "0"
		data.Sign15 = "0"
		data.Sign16 = "0"
		data.Sign17 = "0"
		data.Sign18 = "0"
		data.Sign19 = "0"
		data.Sign20 = "0"
		data.Sign21 = "0"
		data.Sign22 = "0"
		data.Sign23 = "0"
		data.Sign24 = "0"
		data.Sign25 = "0"
		data.Sign26 = "0"
		data.Sign27 = "0"
		data.Sign28 = "0"
		data.Sign29 = "0"
		data.Sign30 = "0"
		data.SignWeek1 = "0"
		data.SignWeek2 = "0"
		data.SignWeek3 = "0"
		data.SignWeek4 = "0"
		data.SignWeek5 = "0"
		data.SignWeek6 = "0"
		data.SignWeek7 = "0"
		data.LastSign = "0"
	}

	return data, nil
}

// 签到记录
func PromoSignMemberRewardRecord(uid string) ([]PromoSignRewardRecord, error) {

	var data []PromoSignRewardRecord
	query, _, _ := dialect.From("tbl_promo_sign_reward_record").Select(colsPromoSignRewardRecord...).Where(g.Ex{"uid": uid}).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}

// 签到记录
func PromoSign(mb ryrpc.TblMemberBase, weekDays, monthDays int) error {

	// 获取会员签到信息
	data := PromoSignRecord{}
	query, _, _ := dialect.From("tbl_promo_sign_record").Select(colsPromoSignRecord...).Where(g.Ex{"uid": mb.Uid}).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&data, query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	ts := time.Now()
	today := ts.Format("2006-01-02")
	if data.LastSign == today {
		return errors.New(helper.SignAlreadyToday)
	}
	date, _ := time.ParseInLocation(time.DateOnly, data.LastSign, time.Local)
	todayDate, _ := time.ParseInLocation(time.DateOnly, today, time.Local)
	nextDay := date.AddDate(0, 0, 1)
	t := todayDate.Compare(nextDay)
	// 执行签到流程
	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}
	// 增加会员余额
	var (
		weekAmount  = decimal.Zero
		monthAmount = decimal.Zero
		amount      = decimal.Zero
		signRecord  = g.Record{
			"last_sign": today,
		}
	)
	cfg := PromoSignConfig{}
	query, _, _ = dialect.From("tbl_promo_sign_config").Select(colsPromoSignConfig...).Where(g.Ex{"vip": mb.Vip}).Limit(1).ToSQL()
	fmt.Println(query)
	err = meta.MerchantDB.Get(&cfg, query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	b, err := MemberBalanceFindOne(mb.Uid)
	if err != nil {
		return err
	}

	if weekDays > 0 {
		//周签到重置
		if data.SignWeek7 == "1" && t >= 0 {
			weekRecord := g.Record{
				"sign_week_1": "0",
				"sign_week_2": "0",
				"sign_week_3": "0",
				"sign_week_4": "0",
				"sign_week_5": "0",
				"sign_week_6": "0",
				"sign_week_7": "0",
			}
			query, _, _ = dialect.Update("tbl_promo_sign_record").Set(weekRecord).Where(g.Ex{"uid": mb.Uid}).ToSQL()
			fmt.Println(query)
			_, err = tx.Exec(query)
			if err != nil {
				_ = tx.Rollback()
				return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
			}
		}
		// 校验是否合法签到
		switch weekDays {
		case 1: //第一天
			if data.SignWeek1 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}
			signRecord["sign_week_1"] = "1"
			weekAmount, _ = decimal.NewFromString(cfg.Sign1Amount)
		case 2: //第二天
			if data.SignWeek2 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.SignWeek1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign_week_2"] = "1"
			weekAmount, _ = decimal.NewFromString(cfg.Sign2Amount)
		case 3: //第三天
			if data.SignWeek3 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.SignWeek1 == "0" || data.SignWeek2 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign_week_3"] = "1"
			weekAmount, _ = decimal.NewFromString(cfg.Sign3Amount)
		case 4: //第四天
			if data.SignWeek4 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.SignWeek3 == "0" || data.SignWeek2 == "0" || data.SignWeek1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign_week_4"] = "1"
			weekAmount, _ = decimal.NewFromString(cfg.Sign4Amount)
		case 5: //第五天
			if data.SignWeek5 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.SignWeek4 == "0" || data.SignWeek3 == "0" || data.SignWeek2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign_week_5"] = "1"
			weekAmount, _ = decimal.NewFromString(cfg.Sign5Amount)
		case 6: //第六天
			if data.SignWeek6 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.SignWeek5 == "0" || data.SignWeek4 == "0" || data.SignWeek3 == "0" || data.SignWeek2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign_week_6"] = "1"
			weekAmount, _ = decimal.NewFromString(cfg.Sign6Amount)
		case 7: //第七天
			if data.SignWeek7 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.SignWeek6 == "0" || data.SignWeek5 == "0" || data.SignWeek4 == "0" || data.SignWeek3 == "0" || data.SignWeek2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign_week_7"] = "1"
			weekAmount, _ = decimal.NewFromString(cfg.Sign7Amount + cfg.SignWeekAmount)
		default:
			return errors.New(helper.ParamErr)
		}
	}

	if monthDays > 0 {
		//月签到重置
		if data.Sign30 == "1" && t >= 0 {
			monthRecord := g.Record{
				"sign1":  "0",
				"sign2":  "0",
				"sign3":  "0",
				"sign4":  "0",
				"sign5":  "0",
				"sign6":  "0",
				"sign7":  "0",
				"sign8":  "0",
				"sign9":  "0",
				"sign10": "0",
				"sign11": "0",
				"sign12": "0",
				"sign13": "0",
				"sign14": "0",
				"sign15": "0",
				"sign16": "0",
				"sign17": "0",
				"sign18": "0",
				"sign19": "0",
				"sign20": "0",
				"sign21": "0",
				"sign22": "0",
				"sign23": "0",
				"sign24": "0",
				"sign25": "0",
				"sign26": "0",
				"sign27": "0",
				"sign28": "0",
				"sign29": "0",
				"sign30": "0",
			}
			query, _, _ = dialect.Update("tbl_promo_sign_record").Set(monthRecord).Where(g.Ex{"uid": mb.Uid}).ToSQL()
			fmt.Println(query)
			_, err = tx.Exec(query)
			if err != nil {
				_ = tx.Rollback()
				return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
			}
		}
		// 校验是否合法签到
		switch monthDays {
		case 1: //第一天
			if data.Sign1 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}
			signRecord["sign1"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign1Amount)
		case 2: //第二天
			if data.Sign2 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign2"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign2Amount)
		case 3: //第三天
			if data.Sign3 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign3"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign3Amount)
		case 4: //第四天
			if data.Sign4 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign4"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign4Amount)
		case 5: //第五天
			if data.Sign5 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign5"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign5Amount)
		case 6: //第六天
			if data.Sign6 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign5 == "0" || data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign6"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign6Amount)
		case 7: //第七天
			if data.Sign7 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign6 == "0" || data.Sign5 == "0" || data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign7"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign7Amount)
		case 8: //第八天
			if data.Sign8 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign7 == "0" || data.Sign6 == "0" || data.Sign5 == "0" || data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign8"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign1Amount)
		case 9: //第九天
			if data.Sign9 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign8 == "0" || data.Sign7 == "0" || data.Sign6 == "0" || data.Sign5 == "0" || data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign9"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign2Amount)
		case 10: //第十天
			if data.Sign10 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign9 == "0" || data.Sign8 == "0" || data.Sign7 == "0" || data.Sign6 == "0" || data.Sign5 == "0" || data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign10"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign3Amount)
		case 11: //第十一天
			if data.Sign11 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign10 == "0" || data.Sign9 == "0" || data.Sign8 == "0" || data.Sign7 == "0" || data.Sign6 == "0" || data.Sign5 == "0" || data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign11"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign4Amount)
		case 12: //第十二天
			if data.Sign12 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign11 == "0" || data.Sign10 == "0" || data.Sign9 == "0" || data.Sign8 == "0" || data.Sign7 == "0" || data.Sign6 == "0" || data.Sign5 == "0" || data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign12"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign5Amount)
		case 13: //第十三天
			if data.Sign13 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign12 == "0" || data.Sign11 == "0" || data.Sign10 == "0" || data.Sign9 == "0" || data.Sign8 == "0" || data.Sign7 == "0" || data.Sign6 == "0" || data.Sign5 == "0" || data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign13"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign6Amount)
		case 14: //第十四天
			if data.Sign14 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign13 == "0" || data.Sign12 == "0" || data.Sign11 == "0" || data.Sign10 == "0" || data.Sign9 == "0" || data.Sign8 == "0" || data.Sign7 == "0" || data.Sign6 == "0" || data.Sign5 == "0" || data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign14"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign7Amount)
		case 15: //第十五天
			if data.Sign15 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign14 == "0" || data.Sign13 == "0" || data.Sign12 == "0" || data.Sign11 == "0" || data.Sign10 == "0" || data.Sign9 == "0" || data.Sign8 == "0" || data.Sign7 == "0" || data.Sign6 == "0" || data.Sign5 == "0" || data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign15"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign1Amount)
		case 16: //第十六天
			if data.Sign16 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign15 == "0" || data.Sign14 == "0" || data.Sign13 == "0" || data.Sign12 == "0" || data.Sign11 == "0" || data.Sign10 == "0" || data.Sign9 == "0" || data.Sign8 == "0" || data.Sign7 == "0" || data.Sign6 == "0" || data.Sign5 == "0" || data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign16"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign2Amount)
		case 17: //第十七天
			if data.Sign17 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign16 == "0" || data.Sign15 == "0" || data.Sign14 == "0" || data.Sign13 == "0" || data.Sign12 == "0" || data.Sign11 == "0" || data.Sign10 == "0" || data.Sign9 == "0" || data.Sign8 == "0" || data.Sign7 == "0" || data.Sign6 == "0" || data.Sign5 == "0" || data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign17"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign3Amount)
		case 18: //第十八天
			if data.Sign18 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign17 == "0" || data.Sign16 == "0" || data.Sign15 == "0" || data.Sign14 == "0" || data.Sign13 == "0" || data.Sign12 == "0" || data.Sign11 == "0" || data.Sign10 == "0" || data.Sign9 == "0" || data.Sign8 == "0" || data.Sign7 == "0" || data.Sign6 == "0" || data.Sign5 == "0" || data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign18"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign4Amount)
		case 19: //第十九天
			if data.Sign19 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign18 == "0" || data.Sign17 == "0" || data.Sign16 == "0" || data.Sign15 == "0" || data.Sign14 == "0" || data.Sign13 == "0" || data.Sign12 == "0" || data.Sign11 == "0" || data.Sign10 == "0" || data.Sign9 == "0" || data.Sign8 == "0" || data.Sign7 == "0" || data.Sign6 == "0" || data.Sign5 == "0" || data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign19"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign5Amount)
		case 20: //第二十天
			if data.Sign20 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign19 == "0" || data.Sign18 == "0" || data.Sign17 == "0" || data.Sign16 == "0" || data.Sign15 == "0" || data.Sign14 == "0" || data.Sign13 == "0" || data.Sign12 == "0" || data.Sign11 == "0" || data.Sign10 == "0" || data.Sign9 == "0" || data.Sign8 == "0" || data.Sign7 == "0" || data.Sign6 == "0" || data.Sign5 == "0" || data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign20"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign6Amount)
		case 21: //第二十一天
			if data.Sign21 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign20 == "0" || data.Sign19 == "0" || data.Sign18 == "0" || data.Sign17 == "0" || data.Sign16 == "0" || data.Sign15 == "0" || data.Sign14 == "0" || data.Sign13 == "0" || data.Sign12 == "0" || data.Sign11 == "0" || data.Sign10 == "0" || data.Sign9 == "0" || data.Sign8 == "0" || data.Sign7 == "0" || data.Sign6 == "0" || data.Sign5 == "0" || data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign21"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign7Amount)
		case 22: //第二十二天
			if data.Sign22 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign21 == "0" || data.Sign20 == "0" || data.Sign19 == "0" || data.Sign18 == "0" || data.Sign17 == "0" || data.Sign16 == "0" || data.Sign15 == "0" || data.Sign14 == "0" || data.Sign13 == "0" || data.Sign12 == "0" || data.Sign11 == "0" || data.Sign10 == "0" || data.Sign9 == "0" || data.Sign8 == "0" || data.Sign7 == "0" || data.Sign6 == "0" || data.Sign5 == "0" || data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign22"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign1Amount)
		case 23: //第二十三天
			if data.Sign23 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign22 == "0" || data.Sign21 == "0" || data.Sign20 == "0" || data.Sign19 == "0" || data.Sign18 == "0" || data.Sign17 == "0" || data.Sign16 == "0" || data.Sign15 == "0" || data.Sign14 == "0" || data.Sign13 == "0" || data.Sign12 == "0" || data.Sign11 == "0" || data.Sign10 == "0" || data.Sign9 == "0" || data.Sign8 == "0" || data.Sign7 == "0" || data.Sign6 == "0" || data.Sign5 == "0" || data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign23"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign2Amount)
		case 24: //第二十四天
			if data.Sign24 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign23 == "0" || data.Sign22 == "0" || data.Sign21 == "0" || data.Sign20 == "0" || data.Sign19 == "0" || data.Sign18 == "0" || data.Sign17 == "0" || data.Sign16 == "0" || data.Sign15 == "0" || data.Sign14 == "0" || data.Sign13 == "0" || data.Sign12 == "0" || data.Sign11 == "0" || data.Sign10 == "0" || data.Sign9 == "0" || data.Sign8 == "0" || data.Sign7 == "0" || data.Sign6 == "0" || data.Sign5 == "0" || data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign24"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign3Amount)
		case 25: //第二十五天
			if data.Sign25 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign24 == "0" || data.Sign23 == "0" || data.Sign22 == "0" || data.Sign21 == "0" || data.Sign20 == "0" || data.Sign19 == "0" || data.Sign18 == "0" || data.Sign17 == "0" || data.Sign16 == "0" || data.Sign15 == "0" || data.Sign14 == "0" || data.Sign13 == "0" || data.Sign12 == "0" || data.Sign11 == "0" || data.Sign10 == "0" || data.Sign9 == "0" || data.Sign8 == "0" || data.Sign7 == "0" || data.Sign6 == "0" || data.Sign5 == "0" || data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign25"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign4Amount)
		case 26: //第二十六天
			if data.Sign26 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}
			if data.Sign25 == "0" || data.Sign24 == "0" || data.Sign23 == "0" || data.Sign22 == "0" || data.Sign21 == "0" || data.Sign20 == "0" || data.Sign19 == "0" || data.Sign18 == "0" || data.Sign17 == "0" || data.Sign16 == "0" || data.Sign15 == "0" || data.Sign14 == "0" || data.Sign13 == "0" || data.Sign12 == "0" || data.Sign11 == "0" || data.Sign10 == "0" || data.Sign9 == "0" || data.Sign8 == "0" || data.Sign7 == "0" || data.Sign6 == "0" || data.Sign5 == "0" || data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign26"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign5Amount)
		case 27: //第二十七天
			if data.Sign27 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign26 == "0" || data.Sign25 == "0" || data.Sign24 == "0" || data.Sign23 == "0" || data.Sign22 == "0" || data.Sign21 == "0" || data.Sign20 == "0" || data.Sign19 == "0" || data.Sign18 == "0" || data.Sign17 == "0" || data.Sign16 == "0" || data.Sign15 == "0" || data.Sign14 == "0" || data.Sign13 == "0" || data.Sign12 == "0" || data.Sign11 == "0" || data.Sign10 == "0" || data.Sign9 == "0" || data.Sign8 == "0" || data.Sign7 == "0" || data.Sign6 == "0" || data.Sign5 == "0" || data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign27"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign6Amount)
		case 28: //第二十八天
			if data.Sign28 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign27 == "0" || data.Sign26 == "0" || data.Sign25 == "0" || data.Sign24 == "0" || data.Sign23 == "0" || data.Sign22 == "0" || data.Sign21 == "0" || data.Sign20 == "0" || data.Sign19 == "0" || data.Sign18 == "0" || data.Sign17 == "0" || data.Sign16 == "0" || data.Sign15 == "0" || data.Sign14 == "0" || data.Sign13 == "0" || data.Sign12 == "0" || data.Sign11 == "0" || data.Sign10 == "0" || data.Sign9 == "0" || data.Sign8 == "0" || data.Sign7 == "0" || data.Sign6 == "0" || data.Sign5 == "0" || data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign28"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign7Amount)
		case 29: //第二十九天
			if data.Sign29 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign28 == "0" || data.Sign27 == "0" || data.Sign26 == "0" || data.Sign25 == "0" || data.Sign24 == "0" || data.Sign23 == "0" || data.Sign22 == "0" || data.Sign21 == "0" || data.Sign20 == "0" || data.Sign19 == "0" || data.Sign18 == "0" || data.Sign17 == "0" || data.Sign16 == "0" || data.Sign15 == "0" || data.Sign14 == "0" || data.Sign13 == "0" || data.Sign12 == "0" || data.Sign11 == "0" || data.Sign10 == "0" || data.Sign9 == "0" || data.Sign8 == "0" || data.Sign7 == "0" || data.Sign6 == "0" || data.Sign5 == "0" || data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign29"] = "1"
			//monthAmount, _ = decimal.NewFromString(cfg.Sign1Amount)
		case 30: //第三十天
			if data.Sign30 == "1" {
				return errors.New(helper.SignAlreadyToday)
			}

			if data.Sign29 == "0" || data.Sign28 == "0" || data.Sign27 == "0" || data.Sign26 == "0" || data.Sign25 == "0" || data.Sign24 == "0" || data.Sign23 == "0" || data.Sign22 == "0" || data.Sign21 == "0" || data.Sign20 == "0" || data.Sign19 == "0" || data.Sign18 == "0" || data.Sign17 == "0" || data.Sign16 == "0" || data.Sign15 == "0" || data.Sign14 == "0" || data.Sign13 == "0" || data.Sign12 == "0" || data.Sign11 == "0" || data.Sign10 == "0" || data.Sign9 == "0" || data.Sign8 == "0" || data.Sign7 == "0" || data.Sign6 == "0" || data.Sign5 == "0" || data.Sign4 == "0" || data.Sign3 == "0" || data.Sign2 == "0" || data.Sign1 == "0" {
				return errors.New(helper.ParamErr)
			}
			signRecord["sign30"] = "1"
			monthAmount, _ = decimal.NewFromString(cfg.SignMonthAmount)
		default:
			return errors.New(helper.ParamErr)
		}
	}

	amount = weekAmount.Add(monthAmount)
	balance := decimal.NewFromFloat(b.Brl)
	balanceAfter := balance.Add(amount)
	id := helper.GenId()
	trans := MemberTransaction{
		AfterAmount:  balanceAfter.String(),
		Amount:       amount.String(),
		BeforeAmount: balance.String(),
		BillNo:       id,
		CreatedAt:    ts.UnixMilli(),
		ID:           id,
		CashType:     helper.TransactionSignDividend,
		UID:          mb.Uid,
		Username:     mb.Username,
		Remark:       fmt.Sprintf("vip%d, %ddays sign bonus", mb.Vip, monthDays),
	}
	query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(&trans).ToSQL()
	_, err = tx.Exec(query)
	fmt.Println(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	reward := PromoSignRewardRecord{
		ID:        id,
		UID:       mb.Uid,
		Username:  mb.Username,
		Vip:       mb.Vip,
		Day:       weekDays,
		MonthDay:  monthDays,
		Amount:    amount.Abs().String(),
		CreatedAt: ts.Unix(),
	}
	query, _, _ = dialect.Insert("tbl_promo_sign_reward_record").Rows(&reward).ToSQL()
	_, err = tx.Exec(query)
	fmt.Println(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	// 中心钱包上下分
	record := g.Record{
		"brl": g.L(fmt.Sprintf("brl+%s", amount.Abs().String())),
	}
	query, _, _ = dialect.Update("tbl_member_balance").Set(record).Where(g.Ex{"uid": mb.Uid}).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	query, _, _ = dialect.Update("tbl_promo_sign_record").Set(signRecord).Where(g.Ex{"uid": mb.Uid}).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	err = tx.Commit()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	return nil
}
