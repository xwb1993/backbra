package model

import (
	"database/sql"
	"errors"
	"fmt"
	"member/contrib/helper"
	"member/contrib/session"
	ryrpc "member/rpc"
	"strings"
	"time"

	"github.com/shopspring/decimal"

	g "github.com/doug-martin/goqu/v9"
	"github.com/go-redis/redis/v8"
	"github.com/speps/go-hashids"
	"github.com/valyala/fasthttp"
)

func MemberLogout(fctx *fasthttp.RequestCtx) {

	session.Destroy(fctx)
}

// 返回用户信息，会员端使用
func MemberInfo(fctx *fasthttp.RequestCtx) (MemberInfosData, error) {

	var err error
	var res MemberInfosData
	res.Contate = meta.Contate
	res.TblMemberBase, err = MemberCache(fctx, "")
	if err != nil {
		return res, errors.New(helper.AccessTokenExpires)
	}

	if res.TblMemberBase.PayPassword != "0" {
		res.TblMemberBase.PayPassword = "1"
	}

	vip, err := getVip(res.TblMemberBase.Vip + 1)
	if err == nil {
		res.NextDeposit = fmt.Sprintf(`%d`, vip.DepositAmount)
		res.NextValidamount = fmt.Sprintf(`%d`, vip.Flow)
		res.NowDeposit = fmt.Sprintf(`%f`, res.TblMemberBase.DepositAmount)

		now := decimal.NewFromFloat(res.TblMemberBase.DepositAmount + res.TblMemberBase.Score)
		next := decimal.NewFromInt(vip.DepositAmount + vip.Flow)
		if now.LessThanOrEqual(next) {
			res.Rate = now.Div(next).String()
		} else {
			res.Rate = "1"
		}
		res.NowValidAmount = fmt.Sprintf(`%f`, res.TblMemberBase.Score)
	} else {
		res.NowDeposit = fmt.Sprintf(`%f`, res.TblMemberBase.DepositAmount)
		res.Rate = "1"
		res.NowValidAmount = fmt.Sprintf(`%f`, res.TblMemberBase.Score)
	}

	return res, nil
}

// 通过用户名获取用户在redis中的数据
func MemberCache(fctx *fasthttp.RequestCtx, name string) (ryrpc.TblMemberBase, error) {

	m := ryrpc.TblMemberBase{}
	if name == "" {
		value := fctx.UserValue("token")
		if value == nil {
			return m, errors.New(helper.UsernameErr)
		}
		name = string(value.([]byte))
		if name == "" {
			return m, errors.New(helper.UsernameErr)
		}
	}

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMember...).Where(g.Ex{"username": name}).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&m, query)
	if err != nil && err != sql.ErrNoRows {
		return m, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return m, errors.New(helper.UsernameErr)
	}

	if m.InviteCode == "" {
		mycode, err := shortURLGen(m.Uid)
		if err == nil {
			// 更新会员信息
			query, _, _ = dialect.Update("tbl_member_base").Set(g.Record{"invite_code": mycode}).Where(g.Ex{"uid": m.Uid}).ToSQL()
			_, err = meta.MerchantDB.Exec(query)
		}
	}

	return m, nil
}

func MemberAmount(fctx *fasthttp.RequestCtx) (TblMemberBalance, error) {

	mb := TblMemberBalance{}

	m, err := MemberCache(fctx, "")
	if err != nil {
		return mb, pushLog(err, helper.AccessTokenExpires)
	}

	t := dialect.From("tbl_member_balance")
	query, _, _ := t.Select(colsMemberBalance...).Where(g.Ex{"uid": m.Uid}).Limit(1).ToSQL()
	err = meta.MerchantDB.Get(&mb, query)
	if err != nil && err == sql.ErrNoRows {
		return mb, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return mb, pushLog(err, helper.AccessTokenExpires)
	}

	mb.DepositBalance = mb.Brl - mb.AgencyAmount - mb.DepositLockAmount
	mb.AgencyBalance = mb.AgencyAmount - mb.AgencyLockAmount
	mb.DepositAmount = mb.Brl - mb.AgencyAmount
	//mb.BrlAmount = mb.Brl - mb.DepositLockAmount - mb.AgencyLockAmount
	if mb.Brl <= mb.UnlockAmount {
		mb.UnlockAmount = mb.Brl
	}
	mb.BrlAmount = mb.UnlockAmount
	if mb.DepositBalance < 0 {
		mb.DepositBalance = 0
	}
	if mb.AgencyBalance < 0 {
		mb.AgencyBalance = 0
	}
	if mb.DepositAmount < 0 {
		mb.DepositAmount = 0
	}
	if mb.BrlAmount < 0 {
		mb.BrlAmount = 0
	}

	return mb, nil
}

func MemberLogin(fctx *fasthttp.RequestCtx, prefix, vid, code, phone, email, password, ip, device, deviceNo string) (string, error) {

	username := fmt.Sprintf("%s%s", phone, email)
	key := "merchant:ip_blacklist"
	if meta.MerchantRedis.SIsMember(ctx, key, ip).Val() {
		return "", errors.New(helper.IpBanErr)
	}

	// web/h5不检查设备号黑名单
	if device != "24" && device != "25" {

		// 检查设备号黑名单
		key = "merchant:device_blacklist"
		if meta.MerchantRedis.SIsMember(ctx, key, deviceNo).Val() {
			return "", errors.New(helper.DeviceBanErr)
		}
	}

	mb, err := MemberFindOne(phone, email)
	if err != nil {
		return "", errors.New(helper.UserNotExist)
	}

	if mb.Uid == "" {
		return "", errors.New(helper.UserNotExist)
	}
	if mb.State == 2 {
		return "", errors.New(helper.Blocked)
	}
	//pwd := fmt.Sprintf("%d", MurmurHash(password, mb.CreatedAt))
	if password != mb.Password {
		return "", errors.New(helper.PasswordErr)
	}

	if mb.Pid == "0" {
		record := g.Record{
			"pid": helper.GenId(),
		}
		query, _, _ := dialect.Update("tbl_member_base").Set(record).Where(g.Ex{"uid": mb.Uid}).ToSQL()
		fmt.Println(query)
		_, err := meta.MerchantDB.Exec(query)
		if err != nil {
			_ = pushLog(err, helper.DBErr)
		}
	}

	sid, err := session.Set([]byte(username), mb.Uid)
	if err != nil {
		return "", errors.New(helper.SessionErr)
	}

	err = meta.MerchantRedis.Unlink(ctx, key).Err()
	if err != nil {
		return "", pushLog(err, helper.RedisErr)
	}
	MemberFlush([]string{mb.Uid})
	//ryrpc.MemberFlushByUid(mb.Uid)

	return sid, nil
}

func MemberVerify(id, code string) bool {

	code = fmt.Sprintf("cap:code:%s", code)
	id = fmt.Sprintf("cap:id:%s", id)
	cmd := meta.MerchantRedis.Get(ctx, id)
	fmt.Println(cmd.String())
	val, err := cmd.Result()
	if err != nil || err == redis.Nil {
		return false
	}

	fmt.Println(id, code, val)
	code = strings.ToLower(code)
	if val != code {
		return false
	}

	_, _ = meta.MerchantRedis.Unlink(ctx, id).Result()

	return true
}

// 查询用户单条数据
func MemberFindOne(phone, email string) (ryrpc.TblMemberBase, error) {

	m := ryrpc.TblMemberBase{}

	ex := g.Ex{}
	if phone != "" {
		ex["phone"] = phone
	}
	if email != "" {
		ex["email"] = email
	}
	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMember...).Where(ex).Limit(1).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&m, query)
	if err != nil && err != sql.ErrNoRows {
		return m, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return m, errors.New(helper.UsernameErr)
	}

	return m, nil
}

func MemberFindOneByUid(uid string) (ryrpc.TblMemberBase, error) {

	m := ryrpc.TblMemberBase{}

	ex := g.Ex{}
	if uid != "" {
		ex["uid"] = uid
	}

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMember...).Where(ex).Limit(1).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&m, query)
	if err != nil && err != sql.ErrNoRows {
		return m, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return m, errors.New(helper.UsernameErr)
	}

	return m, nil
}

func MemberReg(device int, prefix, captchaId, email, password, ip, deviceNo, regUrl, linkID, phone, ts, proxyId string, createdAt uint32) (string, error) {

	username := fmt.Sprintf("%s%s", phone, email)
	// 检查ip黑名单

	if meta.MerchantRedis.SIsMember(ctx, "merchant:ip_blacklist", ip).Val() {
		return "", errors.New(helper.IpBanErr)
	}

	if phone != "" {
		if meta.MerchantRedis.SIsMember(ctx, "phoneExist", phone).Val() {
			return "", errors.New(helper.PhoneExist)
		}
	}
	if email != "" {
		if meta.MerchantRedis.SIsMember(ctx, "emailExist", email).Val() {
			return "", errors.New(helper.EmailExist)
		}
	}

	if !helper.CtypeDigit(ts) {
		return "", errors.New(helper.ParamErr)
	}

	// web/h5不检查设备号黑名单
	if _, ok := WebDevices[device]; !ok {

		// 检查设备号黑名单
		key1 := "merchant:device_blacklist"
		if meta.MerchantRedis.SIsMember(ctx, key1, deviceNo).Val() {
			return "", errors.New(helper.DeviceBanErr)
		}
	}

	userName := strings.ToLower(username)
	if MemberExist(userName) {
		return "", errors.New(helper.UsernameExist)
	}

	ex := g.Ex{}
	if phone != "" {
		ex["phone"] = phone
		if MemberBindCheck(ex) {
			return "", errors.New(helper.PhoneExist)
		}
	}
	if email != "" {
		ex["email"] = email
		if MemberBindCheck(ex) {
			return "", errors.New(helper.EmailExist)
		}
	}
	var operatorId string
	var err error
	if proxyId != "" {
		rec := g.Ex{}
		rec["proxy_id"] = proxyId
		operatorId, err = ProxyIdCheck(rec)
		if err != nil {
			return "", errors.New(helper.ParamErr)
		}
	} else {
		operatorId = "0"
		proxyId = "0"
	}

	sid, err := meta.MerchantRedis.Incr(ctx, "id").Result()
	if err != nil {
		return "", pushLog(err, helper.RedisErr)
	}

	uid := fmt.Sprintf("%d", sid)
	mycode, err := shortURLGen(uid)
	if err != nil {
		return "", errors.New(helper.ServerErr)
	}
	parent := ryrpc.TblMemberBase{
		ParentID: "0",
		GrandID:  "0",
		Uid:      "0",
		TopID:    "0",
	}
	// 邀请链接注册，不成功注册在默认代理root下
	if linkID != "" {
		parent, err = regLink(linkID)
		if err != nil {
			fmt.Println("reg link get uid is error")
		}
	} else {
		fmt.Println("parent:", parent)
	}
	m := ryrpc.TblMemberBase{
		Uid:            uid,
		Pid:            helper.GenId(),
		Username:       userName,
		Phone:          phone,
		Email:          email,
		TopID:          parent.TopID,
		TopName:        parent.TopName,
		GreatGrandID:   parent.GrandID,
		GreatGrandName: parent.GrandName,
		GrandID:        parent.ParentID,
		GrandName:      parent.ParentName,
		ParentID:       parent.Uid,
		ParentName:     parent.Username,
		//Password:       fmt.Sprintf("%d", MurmurHash(password, createdAt)),
		Password:         password,
		CreatedIp:        ip,
		Vip:              1,
		Score:            0,
		CreatedAt:        createdAt,
		InviteCode:       mycode,
		Tester:           1,
		PayPassword:      "0",
		Telegram:         "",
		CanBonus:         1,
		CanRebate:        1,
		Prefix:           prefix,
		OperatorId:       operatorId,
		ProxyId:          proxyId,
		WinFlowMultiple:  "0",
		LoseFlowMultiple: "0",
	}
	if phone != "" {
		m.PhoneVerify = 1
		m.EmailVerify = 0
	}
	if email != "" {
		m.PhoneVerify = 0
		m.EmailVerify = 1
	}
	tx, err := meta.MerchantDB.Begin() // 开启事务
	if err != nil {
		return "", pushLog(err, helper.DBErr)
	}

	query, _, _ := dialect.Insert("tbl_member_base").Rows(&m).ToSQL()
	_, err = tx.Exec(query)
	if err != nil {

		fmt.Println("1 query = ", query)
		fmt.Println("1 err = ", err.Error())
		_ = tx.Rollback()
		return "", pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	b := TblMemberBalance{
		Prefix:            prefix,
		Uid:               uid,
		Brl:               0,
		LockAmount:        0,
		UnlockAmount:      0,
		AgencyAmount:      0,
		DepositLockAmount: 0,
		AgencyLockAmount:  0,
	}
	query, _, _ = dialect.Insert("tbl_member_balance").Rows(&b).ToSQL()
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return "", pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	sign := PromoSignRecord{
		UID:       uid,
		Username:  userName,
		Vip:       0,
		Sign1:     "0",
		Sign2:     "0",
		Sign3:     "0",
		Sign4:     "0",
		Sign5:     "0",
		Sign6:     "0",
		Sign7:     "0",
		Sign8:     "0",
		Sign9:     "0",
		Sign10:    "0",
		Sign11:    "0",
		Sign12:    "0",
		Sign13:    "0",
		Sign14:    "0",
		Sign15:    "0",
		Sign16:    "0",
		Sign17:    "0",
		Sign18:    "0",
		Sign19:    "0",
		Sign20:    "0",
		Sign21:    "0",
		Sign22:    "0",
		Sign23:    "0",
		Sign24:    "0",
		Sign25:    "0",
		Sign26:    "0",
		Sign27:    "0",
		Sign28:    "0",
		Sign29:    "0",
		Sign30:    "0",
		SignWeek1: "0",
		SignWeek2: "0",
		SignWeek3: "0",
		SignWeek4: "0",
		SignWeek5: "0",
		SignWeek6: "0",
		SignWeek7: "0",
	}
	query, _, _ = dialect.Insert("tbl_promo_sign_record").Rows(&sign).ToSQL()
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return "", pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	query = MemberClosureInsert(m.Uid, m.ParentID)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return "", pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if parent.Uid != "" && parent.Uid != "0" {

		inviteRecord := tblPromoInviteRecord{
			Id:             helper.GenId(),
			Uid:            parent.Uid,
			Username:       parent.Username,
			Lvl:            1,
			ChildUid:       uid,
			ChildUsername:  userName,
			FirstDepositAt: 0,
			DepositAmount:  0,
			BonusAmount:    0,
			CreatedAt:      createdAt,
			State:          1,
		}
		queryInvite, _, _ := dialect.Insert("tbl_promo_invite_record").Rows(&inviteRecord).ToSQL()

		_, err = tx.Exec(queryInvite)
		if err != nil {
			_ = tx.Rollback()
			return "", pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}
		pt := g.Record{
			"invite_register_num": g.L("invite_register_num+1"),
		}
		query, _, _ = dialect.Update("tbl_member_base").Set(pt).Where(g.Ex{"uid": parent.Uid}).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			_ = tx.Rollback()
			return "", pushLog(err, helper.DBErr)
		}

		ut := g.Record{
			"invited": 1,
		}
		update, _, _ := dialect.Update("tbl_member_base").Set(ut).Where(g.Ex{"uid": uid}).ToSQL()
		fmt.Println(update)
		_, err = tx.Exec(update)
		if err != nil {
			_ = tx.Rollback()
			return "", pushLog(err, helper.DBErr)
		}
	}

	_ = tx.Commit()

	id, err := session.Set([]byte(m.Username), m.Uid)
	if err != nil {
		return "", errors.New(helper.SessionErr)
	}

	if phone != "" {
		_ = meta.MerchantRedis.SAdd(ctx, "phoneExist", phone).Err()
	}
	if email != "" {
		_ = meta.MerchantRedis.SAdd(ctx, "emailExist", email).Err()
	}
	//ryrpc.MemberFlushByUid(uid)

	//vv := fmt.Sprintf("captchaId=%s&uid=%s", captchaId, uid)
	//meta.MerchantRedis.LPush(ctx, "bg:member:reg", vv).Err()

	body := fmt.Sprintf("t=2&id=%s&uid=%s", captchaId, uid)
	meta.MerchantBean.Put("smslog", []byte(body), 0, 0, 0)

	uids := []string{uid}
	MemberFlush(uids)
	return id, nil
}

// 检测会员账号是否已存在
func MemberExist(username string) bool {

	key := "member:" + username
	exists := meta.MerchantRedis.Exists(ctx, key).Val()
	if exists == 0 {
		return false
	}

	return true
}

func MemberBindCheck(ex g.Ex) bool {

	var id string

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select("uid").Where(ex).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&id, query)
	if err == sql.ErrNoRows {
		return false
	}

	return true
}

func MemberClosureInsert(nodeID, targetID string) string {

	if targetID == "0" {
		targetID = nodeID
	}
	t := "SELECT ancestor, " + nodeID + ",lvl+1 FROM tbl_members_tree WHERE descendant = " + targetID + " UNION SELECT " + nodeID + "," + nodeID + ",0"
	query := "INSERT INTO tbl_members_tree (ancestor, descendant,lvl) (" + t + ")"

	return query
}

func MemberUpdateAvatar(fctx *fasthttp.RequestCtx, id int) error {

	m, err := MemberCache(fctx, "")
	if err != nil {
		return pushLog(err, helper.AccessTokenExpires)
	}

	ex := g.Ex{
		"uid": m.Uid,
	}
	record := g.Record{"avatar": fmt.Sprintf("%d", id)}

	// 更新会员信息
	query, _, _ := dialect.Update("tbl_member_base").Set(record).Where(ex).ToSQL()
	_, err = meta.MerchantDB.Exec(query)

	uids := []string{m.Uid}
	MemberFlush(uids)

	return err
}

func MemberFlush(uids []string) error {

	var data []ryrpc.TblMemberBase

	ex := g.Ex{
		"uid": uids,
	}

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMember...).Where(ex).ToSQL()
	err := meta.MerchantDB.Select(&data, query)
	if err != nil {
		fmt.Println("MemberFlush err = ", err.Error())
		return err
	}

	b, err := helper.JsonMarshal(data)
	if err != nil {
		fmt.Println("MemberFlush JsonMarshal err = ", err.Error())
		return err
	}

	index := meta.Meili.Index("members")
	index.DeleteDocuments(uids)
	_, err = index.AddDocuments(b, "uid")
	if err != nil {
		fmt.Println("MemberFlush AddDocuments err = ", err.Error())
		return err
	}
	return nil
}

func MemberNav() ([]byte, error) {

	res, err := meta.MerchantRedis.Get(ctx, "nav").Bytes()
	if err != nil {
		return res, pushLog(err, helper.RedisErr)
	}

	return res, nil
}

// 会员忘记密码
func MemberForgetPwd(pwd, phone, email, ip, sid, code, ts string) error {

	mb, err := MemberCache(nil, phone+email)
	if err != nil {
		return err
	}

	if phone != "" {
		err = CheckSmsCaptcha(ip, sid, "55"+phone, code)
		if err != nil {
			return err
		}
	}
	if email != "" {
		err = CheckEmailCaptcha(ip, sid, email, code)
		if err != nil {
			return err
		}
	}

	record := g.Record{
		//"password": fmt.Sprintf("%d", MurmurHash(pwd, mb.CreatedAt)),
		"password": pwd,
	}
	ex := g.Ex{
		"uid": mb.Uid,
	}
	// 更新会员信息
	query, _, _ := dialect.Update("tbl_member_base").Set(record).Where(ex).ToSQL()
	_, err = meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	key := fmt.Sprintf("MPE:%s", mb.Username)
	err = meta.MerchantRedis.Unlink(ctx, key).Err()
	if err != nil {
		return pushLog(err, "redis")
	}
	//fmt.Println("==== ForgotPWD mongodb Update ====")

	uids := []string{mb.Uid}
	MemberFlush(uids)
	return nil
}

// shortURLGen uri 原始url,返回短url
func shortURLGen(uri string) (string, error) {

	//fmt.Println("uri:", uri)
	for i := 0; i < 100; i++ {

		shortCode := shortUrlGenCode()
		key := fmt.Sprintf("shortcode:%s", shortCode)

		err := meta.MerchantRedis.SetNX(ctx, key, uri, 0).Err()
		if err != nil {
			fmt.Println(err)
			continue
			//return "", errors.New(helper.RedisErr)
		}

		return shortCode, nil
	}
	return "", errors.New(helper.RedisErr)
}

// shortUrlGenCode 传入ID生成短码
func shortUrlGenCode() string {

	hd := hashids.NewData()
	hd.Salt = helper.GenId()
	hd.MinLength = 6
	h, _ := hashids.NewWithData(hd)
	shortCode, _ := h.EncodeInt64([]int64{time.Now().UnixMilli()})
	return shortCode
}

func regLink(linkID string) (ryrpc.TblMemberBase, error) {

	m := ryrpc.TblMemberBase{}

	key := fmt.Sprintf("shortcode:%s", linkID)
	val, err := meta.MerchantRedis.Get(ctx, key).Result()
	if err != nil && err != redis.Nil {
		return m, errors.New(helper.RedisErr)
	}
	m, err = MemberFindOneByUid(val)
	if err != nil {
		return m, err
	}

	return m, nil
}

// 更新用户密码
func MemberPasswordUpdate(old, password string, fctx *fasthttp.RequestCtx) error {

	mb, err := MemberCache(fctx, "")
	if err != nil {
		return errors.New(helper.AccessTokenExpires)
	}

	//pwd := fmt.Sprintf("%d", MurmurHash(password, mb.CreatedAt))
	if password == mb.Password {
		return errors.New(helper.PasswordConsistent)
	}
	//oldpwd := fmt.Sprintf("%d", MurmurHash(old, mb.CreatedAt))

	if old != mb.Password {
		return errors.New(helper.OldPasswordErr)
	}
	// 更新会员信息
	record := g.Record{
		"password": password,
	}
	query, _, _ := dialect.Update("tbl_member_base").Set(record).Where(g.Ex{"uid": mb.Uid}).ToSQL()
	_, err = meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func MemberPayPasswordUpdate(ty int, sid, code, password, ts, old string, fctx *fasthttp.RequestCtx) error {

	mb, err := MemberCache(fctx, "")
	if err != nil {
		return errors.New(helper.AccessTokenExpires)
	}
	ip := helper.FromRequest(fctx)

	if mb.PayPassword != "0" && old == "" {
		return errors.New(helper.OldPasswordErr)
	}
	if old != "" {
		//oldpwd := fmt.Sprintf("%d", MurmurHash(old, mb.CreatedAt))
		if old != mb.PayPassword {
			return errors.New(helper.OldPasswordErr)
		}
	}

	// 手机修改
	if ty == 1 {
		if !helper.CtypeDigit(sid) {
			return errors.New(helper.ParamErr)
		}
		if !helper.CtypeDigit(ts) {
			return errors.New(helper.ParamErr)
		}
		if !helper.CtypeDigit(code) {
			return errors.New(helper.ParamErr)
		}
		err = CheckSmsCaptcha(ip, sid, "55"+mb.Phone, code)
		if err != nil {
			return err
		}
	}
	if ty == 2 {
		if !helper.CtypeDigit(sid) {
			return errors.New(helper.ParamErr)
		}
		if !helper.CtypeDigit(ts) {
			return errors.New(helper.ParamErr)
		}
		if !helper.CtypeDigit(code) {
			return errors.New(helper.ParamErr)
		}
		err = CheckEmailCaptcha(ip, sid, mb.Email, code)
		if err != nil {
			return err
		}
	}

	//pwd := fmt.Sprintf("%d", MurmurHash(password, mb.CreatedAt))
	// 更新会员信息
	record := g.Record{
		"pay_password": password,
	}
	query, _, _ := dialect.Update("tbl_member_base").Set(record).Where(g.Ex{"uid": mb.Uid}).ToSQL()
	_, err = meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

// 更新用户信息
func MemberUpdateEmail(email, ts string, fctx *fasthttp.RequestCtx) error {

	ex := g.Ex{
		"email": email,
	}
	if MemberBindCheck(ex) {
		return errors.New(helper.EmailExist)
	}

	mb, err := MemberCache(fctx, "")
	if err != nil {
		return err
	}
	//会员绑定邮箱后,不允许再修改邮箱
	if mb.EmailVerify != 0 {
		return errors.New(helper.EmailBindAlreadyErr)
	}

	record := g.Record{
		"email":        email,
		"email_verify": 1,
	}
	ex = g.Ex{
		"uid": mb.Uid,
	}
	// 更新会员信息
	query, _, _ := dialect.Update("tbl_member_base").Set(record).Where(ex).ToSQL()
	_, err = meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}
	return nil
}

// 更新用户手机号
func MemberUpdatePhone(ts, phone string, fctx *fasthttp.RequestCtx) error {

	ex := g.Ex{
		"phone": phone,
	}
	if MemberBindCheck(ex) {
		return errors.New(helper.PhoneExist)
	}

	mb, err := MemberCache(fctx, "")
	if err != nil {
		return err
	}

	//会员绑定手机号后，不允许再修改手机号
	if mb.PhoneVerify != 0 {
		return errors.New(helper.PhoneBindAlreadyErr)
	}

	record := g.Record{
		"phone":        phone,
		"phone_verify": 1,
	}
	ex = g.Ex{
		"uid": mb.Uid,
	}
	// 更新会员信息
	query, _, _ := dialect.Update("tbl_member_base").Set(record).Where(ex).ToSQL()
	_, err = meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func MemberUpdateInfo(fctx *fasthttp.RequestCtx, phone, email, telegram string) error {

	mb, err := MemberCache(fctx, "")
	if err != nil {
		return err
	}
	record := g.Record{}
	if phone != "" {
		ex := g.Ex{
			"phone": phone,
		}
		if MemberBindCheck(ex) {
			return errors.New(helper.PhoneExist)
		}
		record["phone"] = phone
	}
	if email != "" {
		ex := g.Ex{
			"email": email,
		}
		if MemberBindCheck(ex) {
			return errors.New(helper.EmailExist)
		}
		record["email"] = email
	}
	if telegram != "" {
		if mb.Telegram != "" {
			return errors.New(helper.RecordExistErr)
		}
		record["telegram"] = telegram
	}

	ex := g.Ex{
		"uid": mb.Uid,
	}
	// 更新会员信息
	query, _, _ := dialect.Update("tbl_member_base").Set(record).Where(ex).ToSQL()
	_, err = meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func ProxyIdCheck(ex g.Ex) (operatorId string, err error) {

	t := dialect.From("tbl_proxy_info")
	query, _, _ := t.Select("operator_id").Where(ex).Limit(1).ToSQL()
	fmt.Println(query)
	err = meta.MerchantDB.Get(&operatorId, query)
	if err != nil {
		return "0", pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return operatorId, nil
}
