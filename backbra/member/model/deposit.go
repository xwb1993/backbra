package model

import (
	"database/sql"
	"errors"
	"fmt"
	"member/contrib/helper"
	ryrpc "member/rpc"
	"strings"
	"time"

	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"

	"github.com/valyala/fasthttp"
)

type tblDeposit struct {
	Id           string  `json:"id" db:"id" cbor:"id"`
	Oid          string  `json:"oid" db:"oid" cbor:"oid"`
	Uid          string  `json:"uid" db:"uid" cbor:"uid"`
	ParentId     string  `json:"parent_id" db:"parent_id" cbor:"parent_id"`
	ParentName   string  `json:"parent_name" db:"parent_name" cbor:"parent_name"`
	Username     string  `json:"username" db:"username" cbor:"username"`
	Fid          string  `json:"fid" db:"fid" cbor:"fid"`
	Fname        string  `json:"fname" db:"fname" cbor:"fname"`
	Amount       string  `json:"amount" db:"amount" cbor:"amount"`
	State        int     `json:"state" db:"state" cbor:"state"`
	CreatedAt    int64   `json:"created_at" db:"created_at" cbor:"created_at"`
	CreatedUid   string  `json:"created_uid" db:"created_uid" cbor:"created_uid"`
	CreatedName  string  `json:"created_name" db:"created_name" cbor:"created_name"`
	ConfirmAt    int64   `json:"confirm_at" db:"confirm_at" cbor:"confirm_at"`
	ConfirmUid   string  `json:"confirm_uid" db:"confirm_uid" cbor:"confirm_uid"`
	ConfirmName  string  `json:"confirm_name" db:"confirm_name" cbor:"confirm_name"`
	ReviewRemark string  `json:"review_remark" db:"review_remark" cbor:"review_remark"`
	TopId        string  `json:"top_id" db:"top_id" cbor:"top_id"`
	TopName      string  `json:"top_name" db:"top_name" cbor:"top_name"`
	Level        int     `json:"level" db:"level" cbor:"level"`
	Discount     float64 `json:"discount" db:"discount" cbor:"discount"`
	Tester       int     `json:"tester" db:"tester" cbor:"tester"`
	SuccessTime  int     `json:"success_time" db:"success_time" cbor:"success_time"`
	UsdtRate     float64 `json:"usdt_rate" db:"usdt_rate" cbor:"usdt_rate"`
	UsdtCount    float64 `json:"usdt_count" db:"usdt_count" cbor:"usdt_count"`
	Prefix       string  `json:"prefix" db:"prefix" cbor:"prefix"`
}

func PayOnline(fctx *fasthttp.RequestCtx, prefix, fid, amount, flag string) (PaymentDepositResp, error) {

	res := PaymentDepositResp{}
	user, err := MemberCache(fctx, "")
	if err != nil {
		return res, err
	}
	if user.State == 4 {
		return res, errors.New(helper.Blocked)
	}

	fmt.Printf("deposit username[%s] args: %s, ts: %s  \n",
		user.Username, fctx.PostArgs().String(), time.Now().Format("2006-01-02 15:04:05"))

	p, err := CachePayFactory(fid)
	if err != nil {
		return res, errors.New(helper.ChannelNotExist)
	}

	res, err = Pay(fctx, prefix, user, p, amount, flag)
	if err != nil {
		return res, err
	}

	return res, nil
}

// Pay 发起支付公共入口
//func Pay(fctx *fasthttp.RequestCtx, prefix string, user ryrpc.TblMemberBase, p tblPayFactory, amount, flag string) (paymentDepositResp, error) {
//
//	data := paymentDepositResp{}
//
//	payment, ok := thirdFuncCb[p.Fid]
//	if !ok {
//		return data, errors.New(helper.NoPayChannel)
//	}
//
//	// 检查存款金额是否符合范围
//	a, ok := validator.CheckFloatScope(amount, p.Fmin, p.Fmax)
//	if !ok {
//		return data, errors.New(helper.AmountOutRange)
//	}
//
//	amount = a.String()
//	// 生成我方存款订单号
//	orderId := helper.GenId()
//	//计算优惠
//	var discount decimal.Decimal
//	if flag == "1" {
//		if user.DepositAmount == 0 {
//			var pdc []PromoDepositConfig
//			pdc, err := PromoDepositConfigList(1)
//			if err != nil {
//				return data, err
//			}
//			discount = decimal.NewFromFloat(pdc[0].Bonus).Div(decimal.NewFromInt(100))
//		} else {
//			var pdc []PromoDepositConfig
//			pdc, err := PromoDepositConfigList(2)
//			if err != nil {
//				return data, err
//			}
//			for _, v := range pdc {
//				if decimal.NewFromFloat(v.MaxAmount).GreaterThanOrEqual(a) && decimal.NewFromFloat(v.MinAmount).LessThanOrEqual(a) {
//					discount = decimal.NewFromFloat(v.Bonus).Div(decimal.NewFromInt(100))
//				}
//			}
//		}
//	}
//	//计算多少赠送礼金
//	bonusAmount := a.Mul(discount)
//	fmt.Println("Pay orderId = ", orderId)
//	// 向渠道方发送存款订单请求
//	data, err := payment.Deposit(fctx, time.Now(), user.Uid, amount, orderId, p)
//	fmt.Println("Pay  payment.Pay err = ", err)
//	if err != nil {
//		return data, err
//	}
//
//	// 生成我方存款订单号
//	d := tblDeposit{
//		Id:           data.OrderID,
//		Oid:          data.Oid,
//		Uid:          user.Uid,
//		TopId:        user.TopID,
//		TopName:      user.TopName,
//		ParentName:   user.ParentName,
//		ParentId:     user.ParentID,
//		Username:     user.Username,
//		Fid:          p.Fid,
//		Fname:        p.Name,
//		Amount:       amount,
//		State:        DepositConfirming,
//		ConfirmAt:    0,
//		CreatedAt:    fctx.Time().Unix(),
//		CreatedUid:   "0",
//		CreatedName:  "",
//		ConfirmUid:   "0",
//		ConfirmName:  "",
//		ReviewRemark: "",
//		Tester:       user.Tester,
//		Discount:     bonusAmount.InexactFloat64(),
//		UsdtRate:     0,
//		UsdtCount:    0,
//		Prefix:       prefix,
//	}
//
//	if p.Ty == 2 {
//		d.UsdtRate = p.PayRate
//		d.UsdtCount = data.UsdtCount
//	}
//
//	fmt.Println("deposit d:", d)
//	// 请求成功插入订单
//	err = insertDeposit(d)
//	if err != nil {
//		fmt.Println("insert into table error: = ", err)
//		return data, errors.New(helper.DBErr)
//	}
//
//	return data, nil
//}

// 存入数据库
func insertDeposit(record tblDeposit) error {

	query, _, _ := dialect.Insert("tbl_deposit").Rows(record).ToSQL()
	_, err := meta.MerchantDB.Exec(query)
	fmt.Println("deposit insert:", query)
	if err != nil {
		return err

	}

	return nil
}

func DepositCallBack(fctx *fasthttp.RequestCtx) {

	//fmt.Println("fid:", fid)
	//p, err := CachePayFactory(fid)
	//if err != nil {
	//	return
	//}
	//payment, ok := thirdFuncCb[p.Fid]
	//if !ok {
	//	return
	//}
	data := EpayCallBack{}
	//var data map[string]string
	// 获取并校验回调参数
	payment := &thirdEpay{}
	data, err := payment.DepositCallBack(fctx)
	fmt.Println("DepositCallBack:", data)
	if err != nil {
		err = fmt.Errorf("DepositCallBack error: [%v]", err)
		fmt.Println(err)
		fctx.SetBody([]byte(`failed`))
		return
	}

	//对结果sign验证
	var content string
	content = fmt.Sprintf(`amount=%s&appId=%s&custId=%s&merchantOrderId=%s&order=%s&orderStatus=%s&orderTime=%s&key=%s`, data.Amount, data.AppId, data.CustId, data.MerchantOrderId, data.Order, data.OrderStatus, data.OrderTime, meta.TgPay.AppKey)
	//content = fmt.Sprintf(`amount=%s&datetime=%s&memberid=%s&orderid=%s&returncode=%s&transaction_id=%s&key=%s`, data.Amount, data.Datetime, data.MemberId, data.OrderId, data.ReturnCode, data.TransactionId, meta.TgPay.AppKey)
	fmt.Println(content)
	sign := strings.ToUpper(helper.MD5Hash(content))
	fmt.Println("depcb_sign==", sign)
	if data.Sign != sign {
		fmt.Printf("sign error: [%v], old sign=%s, new state=%s", err, data.Sign, sign)
		fmt.Println(err)
		fctx.SetBody([]byte("failed"))
		return
	}

	// 查询订单
	order, err := depositFind(data.MerchantOrderId)
	if err != nil {
		err = fmt.Errorf("query order error: [%v]", err)
		fmt.Println(err)
		fctx.SetBody([]byte(`failed`))
		return
	}

	if order.State == DepositSuccess || order.State == DepositCancelled || order.State == DepositRepairSuccess {
		err = fmt.Errorf("duplicated deposite notify: [%d]", order.State)
		fmt.Println(err)
		fctx.SetBody([]byte(`failed`))
		return
	}

	err = compareAmount(data.Amount, order.Amount, 100)
	if err != nil {
		fmt.Printf("compare amount error: [err: %v, req: %s, origin: %s]", err, data.Amount, order.Amount)
		fmt.Println(err)
		fctx.SetBody([]byte(`failed`))
		return
	}
	// 修改订单状态
	var state string
	if data.OrderStatus != "01" {
		state = fmt.Sprintf(`%d`, DepositReviewing)
	} else {
		state = fmt.Sprintf(`%d`, DepositSuccess)
	}
	err = depositUpdate(state, order, data.OrderTime)
	if err != nil {
		fmt.Printf("set order state error: [%v], old state=%d, new state=%s", err, order.State, state)
		fctx.SetBody([]byte("failed"))
		return
	}

	fctx.SetBody([]byte("success"))
}

// 获取订单信息
func depositFind(id string) (tblDeposit, error) {

	d := tblDeposit{}

	ex := g.Ex{"id": id}
	query, _, _ := dialect.From("tbl_deposit").Select(colsDeposit...).Where(ex).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&d, query)
	if err == sql.ErrNoRows {
		return d, errors.New(helper.OrderNotExist)
	}

	if err != nil {
		return d, pushLog(err, helper.DBErr)
	}

	return d, nil
}

// 金额对比
func compareAmount(compare, compared string, cent int64) error {

	ca, err := decimal.NewFromString(compare)
	if err != nil {
		return errors.New("parse amount error")
	}

	ra, err := decimal.NewFromString(compared)
	if err != nil {
		return errors.New("parse amount error")
	}

	if ca.Cmp(ra.Mul(decimal.NewFromInt(cent))) != 0 {
		return errors.New("invalid amount")
	}

	return nil
}

func depositUpdate(state string, order tblDeposit, payAt string) error {

	// 加锁
	err := depositLock(order.Id)
	if err != nil {
		return err
	}
	defer depositUnLock(order.Id)

	// 充值成功处理订单状态
	if state == fmt.Sprintf(`%d`, DepositSuccess) {
		_ = cacheDepositProcessingRem(order.Uid)
		err = depositUpPointSuccess(order.Id, order.Uid, "", "", payAt, state)
		if err != nil {
			fmt.Println("err:", err)
			return err
		}
	} else {
		err = depositUpPointCancel(order.Id, order.Uid, "", "", payAt, state)
		if err != nil {
			fmt.Println("err:", err)

			return err
		}
	}

	return nil
}

// depositLock 锁定充值订单 防止并发多充钱
func depositLock(id string) error {

	key := fmt.Sprintf(depositOrderLockKey, id)
	return Lock(key)
}

// depositUnLock 解锁充值订单
func depositUnLock(id string) {
	key := fmt.Sprintf(depositOrderLockKey, id)
	Unlock(key)
}

// cacheDepositProcessingRem 清除未未成功的订单计数
func cacheDepositProcessingRem(uid string) error {

	automatic_lock_key := fmt.Sprintf("finance:alock:%s", uid)
	return meta.MerchantRedis.Unlink(ctx, automatic_lock_key).Err()
}

// depositOrderFindOne 查询存款订单
func depositOrderFindOne(ex g.Ex) (tblDeposit, error) {

	order := tblDeposit{}
	query, _, _ := dialect.From("tbl_deposit").Select(colsDeposit...).Where(ex).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&order, query)
	if err == sql.ErrNoRows {
		return order, errors.New(helper.OrderNotExist)
	}

	if err != nil {
		return order, pushLog(err, helper.DBErr)
	}

	return order, nil
}

// 存款上分
func depositUpPointSuccess(did, uid, name, remark, payAt, state string) error {

	// 判断状态是否合法
	allow := map[string]bool{
		fmt.Sprintf(`%d`, DepositSuccess): true,
	}
	if _, ok := allow[state]; !ok {
		return errors.New(helper.OrderStateErr)
	}

	// 判断订单是否存在
	ex := g.Ex{"id": did, "state": DepositConfirming}
	order, err := depositOrderFindOne(ex)
	if err != nil {
		return err
	}

	// 如果已经有一笔订单补单成功,则其他订单不允许补单成功
	if fmt.Sprintf(`%d`, DepositSuccess) == state {
		// 这里的ex不能覆盖上面的ex
		//_, err = depositOrderFindOne(g.Ex{"oid": order.Oid, "state": DepositSuccess})
		_, err = depositOrderFindOne(g.Ex{"id": order.Id, "state": DepositSuccess})
		if err != nil && err.Error() != helper.OrderNotExist {
			return err
		}

		if err == nil {
			return errors.New(helper.OrderExist)
		}
	}

	now := time.Now()
	//if payAt != "" {
	//	confirmAt, err := strconv.ParseInt(payAt, 10, 64)
	//	if err == nil {
	//		if len(payAt) == 13 {
	//			confirmAt = confirmAt / 1000
	//		}
	//		now = time.Unix(confirmAt, 0)
	//	}
	//}
	record := g.Record{
		"state":         state,
		"confirm_at":    now.Unix(),
		"confirm_uid":   uid,
		"confirm_name":  name,
		"review_remark": remark,
	}
	//查询用户是否首存
	user, err := MemberFindOneByUid(uid)
	if user.DepositAmount == 0 {
		record["success_time"] = 1
	}
	query, _, _ := dialect.Update("tbl_deposit").Set(record).Where(ex).ToSQL()
	fmt.Println(query)
	money, _ := decimal.NewFromString(order.Amount)
	amount := money.String()
	cashType := helper.TransactionDeposit
	if money.Cmp(decimal.Zero) == -1 {
		cashType = helper.TransactionFinanceDownPoint
		amount = money.Abs().String()
	}

	// 后面都是存款成功的处理
	// 1、查询用户额度
	balance, err := MemberBalanceFindOne(order.Uid)
	if err != nil {
		return err
	}
	brl := decimal.NewFromFloat(balance.Brl)
	balanceAfter := brl.Add(money)
	//dsc := depositSuccessCount(order.Uid)

	var (
		parent ryrpc.TblMemberBase
	)
	if user.ParentID != "0" {
		parent, _ = MemberFindOneByUid(user.ParentID)
	}
	//if user.GrandID != "0" {
	//	grand, _ = MemberFindOneByUid(user.GrandID)
	//}
	//if user.GreatGrandID != "0" {
	//	grandGrand, _ = MemberFindOneByUid(user.GreatGrandID)
	//}

	if err != nil {
		return err
	}
	var discount, bonusAmount decimal.Decimal
	if order.Discount > 0 && user.DepositAmount == 0 {
		var pdc []PromoDepositConfig
		pdc, err = PromoDepositConfigList(1)
		if err != nil {
			return err
		}
		for _, v := range pdc {
			if money.GreaterThanOrEqual(decimal.NewFromFloat(v.MinAmount)) {
				discount = decimal.NewFromFloat(v.Bonus).Div(decimal.NewFromInt(100))
			}
			//计算多少赠送礼金
			bonusAmount = money.Mul(discount)
			if v.MaxAmount > 0 {
				if bonusAmount.GreaterThanOrEqual(decimal.NewFromFloat(v.MaxAmount)) {
					bonusAmount = decimal.NewFromFloat(v.MaxAmount)
				}
			}
		}
	} else if order.Discount > 0 {
		var pdc []PromoDepositConfig
		pdc, err = PromoDepositConfigList(2)
		if err != nil {
			return err
		}
		for _, v := range pdc {
			if money.GreaterThanOrEqual(decimal.NewFromFloat(v.MinAmount)) {
				discount = decimal.NewFromFloat(v.Bonus).Div(decimal.NewFromInt(100))
			}
			//计算多少赠送礼金
			bonusAmount = money.Mul(discount)
			if v.MaxAmount > 0 {
				if bonusAmount.GreaterThanOrEqual(decimal.NewFromFloat(v.MaxAmount)) {
					bonusAmount = decimal.NewFromFloat(v.MaxAmount)
				}
			}
		}
	}
	//计算多少赠送礼金
	//bonusAmount := money.Mul(discount)
	//是首存要给上级邀请奖金
	var firstDepositBonus float64
	tbc, err := getBonusConfig("invite")
	if err != nil {
		return pushLog(err, helper.DBErr)
	}
	rechargeAmount := money.InexactFloat64()
	if tbc.RechargeAmountLv1 <= rechargeAmount && rechargeAmount < tbc.RechargeAmountLv2 {
		firstDepositBonus = tbc.RateAmountLv1
	} else if tbc.RechargeAmountLv2 <= rechargeAmount && rechargeAmount < tbc.RechargeAmountLv3 {
		firstDepositBonus = tbc.RateAmountLv2
	} else if rechargeAmount >= tbc.RechargeAmountLv3 {
		firstDepositBonus = tbc.RateAmountLv3
	}
	// 开启事务
	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	// 2、更新订单状态
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(err, helper.DBErr)
	}

	// 3、更新余额
	ex = g.Ex{
		"uid": order.Uid,
	}
	br := g.Record{
		"brl":                 g.L(fmt.Sprintf("brl+%s", money.Add(bonusAmount).String())),
		"deposit_lock_amount": g.L(fmt.Sprintf("deposit_lock_amount+%s", money.Add(bonusAmount).String())),
	}
	query, _, _ = dialect.Update("tbl_member_balance").Set(br).Where(ex).ToSQL()
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(err, helper.DBErr)
	}

	// 4、新增账变记录
	id := helper.GenId()
	mbTrans := MemberTransaction{
		AfterAmount:  balanceAfter.String(),
		Amount:       amount,
		BeforeAmount: fmt.Sprintf(`%f`, balance.Brl),
		BillNo:       order.Id,
		CreatedAt:    time.Now().UnixMilli(),
		ID:           id,
		CashType:     cashType,
		UID:          order.Uid,
		Username:     order.Username,
		Remark:       "deposit",
	}

	query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(mbTrans).ToSQL()
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(err, helper.DBErr)
	}

	//// 5、会员充值金额和积分增加
	//ex = g.Ex{
	//	"uid": order.Uid,
	//}
	//mbs := g.Record{
	//	"deposit_amount": g.L(fmt.Sprintf("deposit_amount+%s", money.String())),
	//}
	//query, _, _ = dialect.Update("tbl_member_base").Set(mbs).Where(ex).ToSQL()
	//_, err = tx.Exec(query)
	//if err != nil {
	//	_ = tx.Rollback()
	//	return pushLog(err, helper.DBErr)
	//}

	if user.CanBonus == 1 {
		// 6、赠送金额及张变
		bonusTrans := MemberTransaction{
			AfterAmount:  balanceAfter.Add(bonusAmount).String(),
			Amount:       bonusAmount.String(),
			BeforeAmount: balanceAfter.String(),
			BillNo:       order.Id,
			CreatedAt:    time.Now().UnixMilli(),
			ID:           helper.GenId(),
			CashType:     helper.TransactionDepositBonus,
			UID:          order.Uid,
			Username:     order.Username,
			Remark:       "deposit bonus",
		}

		query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(bonusTrans).ToSQL()
		_, err = tx.Exec(query)
		if err != nil {
			_ = tx.Rollback()
			return pushLog(err, helper.DBErr)
		}
	}
	mbs := g.Record{
		"deposit_amount": g.L(fmt.Sprintf("deposit_amount+%s", money.String())),
	}
	if user.DepositAmount == 0 {

		mbs["first_deposit_amount"] = amount
		promoRecord := g.Record{
			"bonus_amount": firstDepositBonus,
			"state":        2,
		}
		promoRecord["first_deposit_at"] = now.Unix()
		promoRecord["settled_at"] = now.Unix()
		promoRecord["deposit_amount"] = amount
		query, _, _ = dialect.Update("tbl_promo_invite_record").Set(promoRecord).Where(g.Ex{"child_uid": user.Uid}).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			_ = tx.Rollback()
			return pushLog(err, helper.DBErr)
		}

		// 给上面三级邀请奖金
		if user.ParentID != "0" && parent.CanBonus == 1 {

			exparent := g.Ex{
				"uid": user.ParentID,
			}
			brparent := g.Record{
				"brl":                 g.L(fmt.Sprintf("brl+%f", firstDepositBonus)),
				"deposit_lock_amount": g.L(fmt.Sprintf("deposit_lock_amount+%f", firstDepositBonus)),
			}
			query, _, _ = dialect.Update("tbl_member_balance").Set(brparent).Where(exparent).ToSQL()
			_, err = tx.Exec(query)
			if err != nil {
				_ = tx.Rollback()
				return pushLog(err, helper.DBErr)
			}
			parentBalance, err := MemberBalanceFindOne(user.ParentID)
			if err != nil {
				return err
			}
			parentBrl := decimal.NewFromFloat(parentBalance.Brl)
			if err != nil {
				return err
			}
			balAfter := parentBrl.Add(decimal.NewFromFloat(firstDepositBonus))
			// 上级新增账变记录
			parTrans := MemberTransaction{
				AfterAmount:  balAfter.String(),
				Amount:       fmt.Sprintf("%f", firstDepositBonus),
				BeforeAmount: parentBrl.String(),
				BillNo:       helper.GenId(),
				CreatedAt:    time.Now().UnixMilli(),
				ID:           helper.GenId(),
				CashType:     helper.TransactionInvitationBonus,
				UID:          user.ParentID,
				Username:     user.ParentName,
				Remark:       "invitation bonus",
			}

			query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(parTrans).ToSQL()
			_, err = tx.Exec(query)
			if err != nil {
				_ = tx.Rollback()
				return pushLog(err, helper.DBErr)
			}
		}
	}

	// 5、会员充值金额和积分增加
	ex = g.Ex{
		"uid": order.Uid,
	}

	query, _, _ = dialect.Update("tbl_member_base").Set(mbs).Where(ex).ToSQL()
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(err, helper.DBErr)
	}
	err = tx.Commit()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	if user.Invited != 1 && user.ParentID != "" && user.ParentID != "0" {
		dsc := depositSuccessAmount(order.Uid)
		if dsc >= tbc.RechargeAmountLv1 {
			pt := g.Record{
				"invite_num": g.L("invite_num+1"),
			}
			query, _, _ = dialect.Update("tbl_member_base").Set(pt).Where(g.Ex{"uid": user.ParentID}).ToSQL()
			_, err = meta.MerchantDB.Exec(query)
			if err != nil {
				return pushLog(err, helper.DBErr)
			}
		}
	}
	return nil
}

// 存款上分
func depositUpPointCancel(did, uid, name, remark, payAt, state string) error {

	// 判断状态是否合法
	allow := map[string]bool{
		fmt.Sprintf(`%d`, DepositReviewing): true,
	}
	if _, ok := allow[state]; !ok {
		return errors.New(helper.OrderStateErr)
	}

	// 判断订单是否存在
	ex := g.Ex{"id": did, "state": DepositConfirming}
	order, err := depositOrderFindOne(ex)
	if err != nil {
		return err
	}

	// 如果已经有一笔订单补单成功,则其他订单不允许补单成功
	if fmt.Sprintf(`%d`, DepositSuccess) == state || fmt.Sprintf(`%d`, DepositRepairSuccess) == state {
		// 这里的ex不能覆盖上面的ex
		//_, err = depositOrderFindOne(g.Ex{"oid": order.Oid, "state": DepositSuccess})
		_, err = depositOrderFindOne(g.Ex{"id": order.Id, "state": []int{DepositSuccess, DepositRepairSuccess}})
		if err != nil && err.Error() != helper.OrderNotExist {
			return err
		}

		if err == nil {
			return errors.New(helper.OrderExist)
		}
	}

	now := time.Now()
	//if payAt != "" {
	//	confirmAt, err := strconv.ParseInt(payAt, 10, 64)
	//	if err == nil {
	//		if len(payAt) == 13 {
	//			confirmAt = confirmAt / 1000
	//		}
	//		now = time.Unix(confirmAt, 0)
	//	}
	//}
	record := g.Record{
		"state":         state,
		"confirm_at":    now.Unix(),
		"confirm_uid":   uid,
		"confirm_name":  name,
		"review_remark": remark,
	}
	query, _, _ := dialect.Update("tbl_deposit").Set(record).Where(ex).ToSQL()
	fmt.Println(query)
	// 存款失败 直接修改订单状态
	_, err = meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	return nil
}

func depositSuccessCount(uid string) int64 {

	var data sql.NullInt64
	query, _, _ := dialect.From("tbl_deposit").Select(g.COUNT("id")).Where(g.Ex{"uid": uid, "state": DepositSuccess}).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&data, query)
	if errors.Is(err, sql.ErrNoRows) {
		return 0
	}

	if err != nil {
		return 0
	}

	return data.Int64
}

func depositSuccessAmount(uid string) float64 {

	var data sql.NullFloat64
	query, _, _ := dialect.From("tbl_deposit").Select("amount").Where(g.Ex{"uid": uid, "state": DepositSuccess}).Order(g.C("amount").Desc()).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&data, query)
	if errors.Is(err, sql.ErrNoRows) {
		return 0
	}

	if err != nil {
		return 0
	}

	return data.Float64
}
