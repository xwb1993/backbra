package model

import (
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"member/contrib/helper"
)

func MemberBalanceFindOne(uid string) (TblMemberBalance, error) {

	balance := TblMemberBalance{}
	query, _, _ := dialect.From("tbl_member_balance").Select(colsMemberBalance...).Where(g.Ex{"uid": uid}).Limit(1).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&balance, query)
	if err != nil {
		return balance, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return balance, err
}
