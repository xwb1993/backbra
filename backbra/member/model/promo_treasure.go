package model

import (
	"database/sql"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"
	"member/contrib/helper"
	ryrpc "member/rpc"
	"time"
)

// 签到活动配置
func PromoTreasureConfigList() ([]PromoTreasureConfig, error) {

	var data []PromoTreasureConfig
	query, _, _ := dialect.From("tbl_promo_treasure_config").
		Select(colsPromoTreasureConfig...).Where(g.Ex{}).Order(g.C("invite_num").Asc()).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}

// 签到记录
func PromoTreasureMemberRecord(uid string) ([]PromoTreasureRecord, error) {

	var data []PromoTreasureRecord
	query, _, _ := dialect.From("tbl_promo_treasure_record").
		Select(colsPromoTreasureRecord...).Where(g.Ex{"uid": uid}).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data, query)
	if err != nil {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}

// 签到记录
func PromoTreasureApply(mb ryrpc.TblMemberBase, inviteNum int) error {

	if mb.LastTreasure == inviteNum {
		return errors.New(helper.DuplicateApplyErr)
	}

	if mb.LastTreasure > inviteNum {
		return errors.New(helper.ApplySequenceErr)
	}

	data := PromoTreasureConfig{}
	query, _, _ := dialect.From("tbl_promo_treasure_config").
		Select(colsPromoTreasureConfig...).Where(g.Ex{"invite_num": inviteNum}).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&data, query)
	if err != nil && err != sql.ErrNoRows {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return errors.New(helper.ParamErr)
	}

	b, err := MemberBalanceFindOne(mb.Uid)
	if err != nil {
		return err
	}

	// 执行签到流程
	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	ts := time.Now()
	// 增加会员余额
	amount, _ := decimal.NewFromString(data.Amount)
	treasureRecord := PromoTreasureRecord{
		ID:        helper.GenId(),
		UID:       mb.Uid,
		Username:  mb.Username,
		InviteNum: inviteNum,
		Amount:    amount.String(),
		CreatedAt: ts.Unix(),
	}

	balance := decimal.NewFromFloat(b.Brl)
	balanceAfter := balance.Add(amount)
	id := helper.GenId()
	trans := MemberTransaction{
		AfterAmount:  balanceAfter.String(),
		Amount:       amount.String(),
		BeforeAmount: balance.String(),
		BillNo:       id,
		CreatedAt:    ts.UnixMilli(),
		ID:           id,
		CashType:     helper.TransactionTreasureDividend,
		UID:          mb.Uid,
		Username:     mb.Username,
		Remark:       fmt.Sprintf("invite num [%d], bonus amount [%s]", inviteNum, amount.String()),
	}
	query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	_, err = tx.Exec(query)
	fmt.Println(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	// 中心钱包上下分
	record := g.Record{
		"brl": g.L(fmt.Sprintf("brl+%s", amount.Abs().String())),
	}
	query, _, _ = dialect.Update("tbl_member_balance").Set(record).Where(g.Ex{"uid": mb.Uid}).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	query, _, _ = dialect.Update("tbl_member_base").Set(g.Record{"last_treasure": inviteNum}).Where(g.Ex{"uid": mb.Uid}).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	query, _, _ = dialect.Insert("tbl_promo_treasure_record").Rows(treasureRecord).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	err = tx.Commit()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	return nil
}
