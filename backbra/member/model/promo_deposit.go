package model

import (
	"database/sql"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"member/contrib/helper"
)

func PromoDepositConfigList(ty int) ([]PromoDepositConfig, error) {

	var data []PromoDepositConfig
	query, _, _ := dialect.From("tbl_promo_deposit").
		Select(colsPromoDeposit...).Where(g.Ex{"ty": ty}).Order(g.C("min_amount").Desc()).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}

func getPromoDepositConfig(ty int, amount float64) (PromoDepositConfig, error) {

	var data PromoDepositConfig
	query, _, _ := dialect.From("tbl_promo_deposit").
		Select(colsPromoDeposit...).Where(g.Ex{"ty": ty, "min_amount": g.Op{"lte": amount}, "max_amount": g.Op{"gt": amount}}).Limit(1).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&data, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}
