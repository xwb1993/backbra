package model

import (
	"database/sql"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"member/contrib/helper"
)

func MemberVipList() ([]MemberVip, error) {

	var data []MemberVip
	query, _, _ := dialect.From("tbl_member_vip").
		Select(colsMemberVip...).Where(g.Ex{}).Order(g.C("vip").Asc()).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}

func getVip(vip int) (MemberVip, error) {

	var data MemberVip
	query, _, _ := dialect.From("tbl_member_vip").
		Select(colsMemberVip...).Where(g.Ex{"vip": vip}).Limit(1).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&data, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}
