package model

import (
	ryrpc "member/rpc"
)

// MessageList  站内信列表
func MessageList(ty, page, pageSize, isRead int, username string) (ryrpc.TblMessageData, error) {

	param := ryrpc.TblMessageListParam{
		Name: username,
		Ty:   ty,
		Read: isRead,
		Page: page,
		Size: pageSize,
	}

	return ryrpc.MessageList(param)
}

func MessageNum(username string) (int64, error) {

	return ryrpc.MessageUnreadTotal(username)
}

// MessageRead  站内信已读
func MessageRead(id string) error {

	_, err := ryrpc.MessageUpdate(id)
	return err
}

// 站内信删除已读
func MessageDeleteByFilter(username string, flag int) error {

	_, err := ryrpc.MessageDeleteByFilter(username, flag)

	//fmt.Println("MessageDeleteByIds ok = ", ok)
	return err
}

func MessageDeleteByIds(ids []string) error {

	_, err := ryrpc.MessageDeleteById(ids)

	//fmt.Println("MessageDeleteByIds ok = ", ok)
	return err
}

// 发送站内信
func messageSend(title, content, sendName string, isTop, isVip, ty int, names []string) error {

	record := ryrpc.TblMessageBulkParam{
		Title:    title,
		Content:  content,
		SendName: sendName,
		IsTop:    isTop,
		IsVip:    isVip,
		Ty:       ty,
		Names:    names,
	}

	_, err := ryrpc.MessageBulk(record)
	//fmt.Println("messageSend ok = ", ok)
	return err
}
