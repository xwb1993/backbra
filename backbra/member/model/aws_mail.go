package model

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ses"
)

func awsSendMail(to, code, subject string) error {

	sess, err := session.NewSession(&aws.Config{
		Region:      aws.String(meta.AwsEmail.Region),
		Credentials: credentials.NewStaticCredentials(meta.AwsEmail.AccessKeyID, meta.AwsEmail.SecretAccessKey, ""),
	})
	// 创建会话错误
	if err != nil {
		fmt.Println("Error creating session:", err)
		return err
	}

	htmlBody := fmt.Sprintf("<h1>Your code is : %s</h1>", code)

	// 组装电子邮件。.
	input := &ses.SendEmailInput{
		Destination: &ses.Destination{
			CcAddresses: []*string{},
			ToAddresses: []*string{
				aws.String(to),
			},
		},
		Message: &ses.Message{
			Body: &ses.Body{
				Html: &ses.Content{
					Charset: aws.String("UTF-8"),
					Data:    aws.String(htmlBody),
				},
				Text: &ses.Content{
					Charset: aws.String("UTF-8"),
					Data:    aws.String(""),
				},
			},
			Subject: &ses.Content{
				Charset: aws.String("UTF-8"),
				Data:    aws.String(subject),
			},
		},
		Source: aws.String(meta.AwsEmail.From),
	}

	svc := ses.New(sess)
	// 尝试发送电子邮件。
	result, err := svc.SendEmail(input)
	// 如果出现错误，则显示错误消息。
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case ses.ErrCodeMessageRejected:
				fmt.Println(ses.ErrCodeMessageRejected, aerr.Error())
			case ses.ErrCodeMailFromDomainNotVerifiedException:
				fmt.Println(ses.ErrCodeMailFromDomainNotVerifiedException, aerr.Error())
			case ses.ErrCodeConfigurationSetDoesNotExistException:
				fmt.Println(ses.ErrCodeConfigurationSetDoesNotExistException, aerr.Error())
			default:
				fmt.Println(aerr.Error())
			}
		} else {
			// 打印错误的消息。
			fmt.Println(err.Error())
		}

		return err
	}

	fmt.Println("Email Sent to address: " + to)
	fmt.Println(result)
	return nil
}
