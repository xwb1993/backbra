package model

import (
	"crypto/tls"
	//"github.com/go-gomail/gomail"
	"gopkg.in/gomail.v2"
	"log"
)

// StartTLS Email Example

func SendMail(from, to, smtpHost string, smtpPort int, password, subject, body string) error {

	m := gomail.NewMessage()
	m.SetHeader("From", from)
	m.SetHeader("To", to)
	//m.SetAddressHeader("Cc", "olsen.jan.2012@gmail.com", "Dan")
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", "Your code is "+body)
	/*
		m.Attach("template.html")

		t, _ := template.ParseFiles("template.html")
		m.AddAlternativeWriter("text/html", func(w io.Writer) error {
			return t.Execute(w, struct {
				Name    string
				Message string
			}{
				Name:    "Hasan Yousef",
				Message: "This is a test message in a HTML template",
			})
		})
	*/
	d := gomail.NewDialer(smtpHost, smtpPort, from, password)
	// 启用TLS加密
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	// Send the email to Bob, Cora and Dan.
	if err := d.DialAndSend(m); err != nil {
		return err
	}

	log.Println("Email sent successfully!")
	return nil
}
