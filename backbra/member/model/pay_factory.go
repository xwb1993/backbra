package model

import (
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"
	"github.com/valyala/fasthttp"
	"member/contrib/helper"
	"strings"
)

type WithdrawConfigData struct {
	Config          TblPayFactory       `json:"config"` //提款配置
	MemberBankList  []TblMemberBankcard `json:"member_bank_list"`
	MemberBankT     int                 `json:"member_bank_t"`
	FreeWithdrawNum int                 `json:"free_withdraw_num" db:"free_withdraw_num" cbor:"free_withdraw_num"` //免费提款次数
	WithdrawLimit   int                 `json:"withdraw_limit" db:"withdraw_limit" cbor:"withdraw_limit"`          //提款限额
}

type Bank struct {
	BankCode string
	BankName string
}

// CachePayment 获取支付方式
func CachePayFactory(fid string) (TblPayFactory, error) {

	m := TblPayFactory{}
	query, _, _ := dialect.From("tbl_pay_factory").
		Select(colsPayFactory...).Where(g.Ex{"fid": fid}).ToSQL()
	err := meta.MerchantDB.Get(&m, query)
	if err != nil {
		return m, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}
	return m, nil
}

func ChannelList(fctx *fasthttp.RequestCtx, flags int) (TblPayFactoryData, error) {

	var data TblPayFactoryData
	data.C = `Prezado usuário, quando o valor da primeira recarga for maior que 50 reais, você receberá no máximo 20% de recompensa de recarga, e quando o valor da recarga for maior que 50 reais, você receberá no máximo 10% de recompensa de recarga! 6 vezes ao dia, quanto maior o valor da recarga, maior a proporção de presentes!`
	query, _, _ := dialect.From("tbl_pay_factory").
		Select(colsPayFactory...).Where(g.Ex{"state": 1, "flags": flags}).Order(g.C("fid").Asc()).ToSQL()
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if flags == 1 {
		user, err := MemberCache(fctx, "")
		if err != nil {
			return data, err
		}
		var pdc []PromoDepositConfig
		pdc, err = PromoDepositConfigList(1)
		if err != nil {
			return data, err
		}
		if len(pdc) > 0 {
			data.R, _ = decimal.NewFromFloat(pdc[0].Bonus).Div(decimal.NewFromInt(100)).Float64()
		}
		if user.DepositAmount > 0 {
			pdc, err = PromoDepositConfigList(2)
			if err != nil {
				return data, err
			}
		}
		for k, v := range data.D {
			al := strings.Split(v.AmountList, ",")
			var ll []DepositAmount
			var discount, bonusAmount decimal.Decimal
			for _, v1 := range al {
				amount, _ := decimal.NewFromString(v1)
				for _, v2 := range pdc {
					min := decimal.NewFromFloat(v2.MinAmount)
					max := decimal.NewFromFloat(v2.MaxAmount)
					//if amount.GreaterThanOrEqual(min) && amount.LessThanOrEqual(max) {
					if amount.GreaterThanOrEqual(min) {
						discount = decimal.NewFromFloat(v2.Bonus).Div(decimal.NewFromInt(100))
						//计算多少赠送礼金
						money, _ := decimal.NewFromString(v1)
						bonusAmount = money.Mul(discount)
						if v2.MaxAmount > 0 {
							if bonusAmount.GreaterThanOrEqual(max) {
								bonusAmount = max
							}
						}
						ll = append(ll, DepositAmount{Amount: v1, Discount: bonusAmount.StringFixed(2)})
						break
					}
				}
			}
			data.D[k].AmountArray = ll
		}
	}
	data.T = len(data.D)

	return data, nil
}
