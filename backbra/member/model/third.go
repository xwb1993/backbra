package model

import (
	"time"

	"github.com/valyala/fasthttp"
)

type third interface {
	// 发起三方代收
	Deposit(fctx *fasthttp.RequestCtx, uid, amount, orderNo string, p TblPayFactory) (PaymentDepositResp, error)
	// 三方代收回调
	DepositCallBack(fctx *fasthttp.RequestCtx) (EpayCallBack, error)
	// 三方代付
	Withdraw(fctx *fasthttp.RequestCtx, ts time.Time, uid, amount, orderNo, accountNo, accountName, pixId, flag string, p TblPayFactory) (paymentWithdrawResp, error)
	// 三方代付回调
	WithdrawCallBack(fctx *fasthttp.RequestCtx) (paymentCallbackResp, error)
}

//var (
//	epay = new(thirdEpay)
//)
//
//var thirdFuncCb = map[string]third{
//	"666777888": epay,
//	"666777999": epay,
//	"778889999": epay,
//}
