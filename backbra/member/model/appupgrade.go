package model

import "fmt"

func UpgradeInfo(device string) ([]byte, error) {
	key := fmt.Sprintf("upgrade:%s", device)

	b, err := meta.MerchantRedis.Get(ctx, key).Bytes()
	return b, err
}
