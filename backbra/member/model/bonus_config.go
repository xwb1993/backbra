package model

import (
	"database/sql"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"member/contrib/helper"
)

type TblBonusConfig struct {
	Id                string  `db:"id" cbor:"id" json:"id"`
	Name              string  `db:"name" cbor:"name" json:"name"`
	Code              string  `db:"code" cbor:"code" json:"code"`
	RechargeAmountLv1 float64 `db:"recharge_amount_lv1" cbor:"recharge_amount_lv1" json:"recharge_amount_lv1"`
	RateAmountLv1     float64 `db:"rate_amount_lv1" cbor:"rate_amount_lv1" json:"rate_amount_lv1"`
	RechargeAmountLv2 float64 `db:"recharge_amount_lv2" cbor:"recharge_amount_lv2" json:"recharge_amount_lv2"`
	RateAmountLv2     float64 `db:"rate_amount_lv2" cbor:"rate_amount_lv2" json:"rate_amount_lv2"`
	RechargeAmountLv3 float64 `db:"recharge_amount_lv3" cbor:"recharge_amount_lv3" json:"recharge_amount_lv3"`
	RateAmountLv3     float64 `db:"rate_amount_lv3" cbor:"rate_amount_lv3" json:"rate_amount_lv3"`
	LvlOneRebate      float64 `db:"lvl_one_rebate" cbor:"lvl_one_rebate" json:"lvl_one_rebate"`
	LvlTwoRebate      float64 `db:"lvl_two_rebate" cbor:"lvl_two_rebate" json:"lvl_two_rebate"`
	LvlThreeRebate    float64 `db:"lvl_three_rebate" cbor:"lvl_three_rebate" json:"lvl_three_rebate"`
}

func getBonusConfig(code string) (TblBonusConfig, error) {

	var data TblBonusConfig
	query, _, _ := dialect.From("tbl_bonus_config").
		Select(colsBonusConfig...).Where(g.Ex{"code": code}).Limit(1).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&data, query)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}
