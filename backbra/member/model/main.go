package model

import (
	"context"
	"errors"
	"fmt"
	"member/contrib/conn"
	"member/contrib/helper"
	"member/contrib/session"
	"member/contrib/tracerr"
	ryrpc "member/rpc"
	"runtime"
	"strings"
	"time"

	"github.com/ip2location/ip2location-go/v9"

	"github.com/bytedance/gopkg/util/xxhash3"
	g "github.com/doug-martin/goqu/v9"
	_ "github.com/doug-martin/goqu/v9/dialect/mysql"
	"github.com/go-redis/redis/v8"
	"github.com/jmoiron/sqlx"
	"github.com/meilisearch/meilisearch-go"
	"lukechampine.com/frand"
)

type MetaTable struct {
	MerchantDB    *sqlx.DB
	MerchantBean  *conn.Connection
	MerchantRedis *redis.Client
	IpDB          *ip2location.DB
	Program       string
	SecretKey     string
	Callback      string
	WebUrl        string
	Contate       string
	Email         Email
	AwsEmail      AwsEmail
	Rand          *frand.RNG
	Meili         *meilisearch.Client
	TgPay         TgPay
}

var (
	loc                       *time.Location
	meta                      *MetaTable
	ctx                       = context.Background()
	dialect                   = g.Dialect("mysql")
	colsMember                = helper.EnumFields(ryrpc.TblMemberBase{})
	colsPayFactory            = helper.EnumFields(TblPayFactory{})
	colsMemberVip             = helper.EnumFields(MemberVip{})
	colsPromoSignConfig       = helper.EnumFields(PromoSignConfig{})
	colsPromoSignRecord       = helper.EnumFields(PromoSignRecord{})
	colsPromoSignRewardRecord = helper.EnumFields(PromoSignRewardRecord{})
	colsPromoTreasureConfig   = helper.EnumFields(PromoTreasureConfig{})
	colsPromoTreasureRecord   = helper.EnumFields(PromoTreasureRecord{})
	colsDeposit               = helper.EnumFields(tblDeposit{})
	colsWithdraw              = helper.EnumFields(tblWithdraw{})
	colsMemberBalance         = helper.EnumFields(TblMemberBalance{})
	colsBankcard              = helper.EnumFields(TblMemberBankcard{})
	colsPromoDeposit          = helper.EnumFields(PromoDepositConfig{})
	colsGame                  = helper.EnumFields(Game{})
	colsInviteRecord          = helper.EnumFields(tblPromoInviteRecord{})
	colsReportAgency          = helper.EnumFields(TblReportAgency{})
	colsBankType              = helper.EnumFields(TblBanktype{})
	colsReportUser            = helper.EnumFields(tblReportUser{})
	colsPromoWeeklyConfig     = helper.EnumFields(tblPromoWeekBetConfig{})
	colsBonusConfig           = helper.EnumFields(TblBonusConfig{})
)

func Constructor(mt *MetaTable, uri string) {

	meta = mt
	meta.Rand = frand.New()
	session.New(mt.MerchantRedis, "lh8")
	loc, _ = time.LoadLocation("America/Sao_Paulo")

	ryrpc.Constructor(uri)
}

type zinc_t struct {
	ID       string `json:"id"`
	Content  string `json:"content"`
	Flags    string `json:"flags"`
	Filename string `json:"filename"`
	Index    string `json:"_index"`
}

func pushLog(err error, code string) error {

	//return err
	fmt.Println(err)
	_, file, line, _ := runtime.Caller(1)
	paths := strings.Split(file, "/")
	l := len(paths)
	if l > 2 {
		file = paths[l-2] + "/" + paths[l-1]
	}
	path := fmt.Sprintf("%s:%d", file, line)
	id := helper.GenId()
	ts := time.Now()
	data := zinc_t{
		ID:       id,
		Content:  tracerr.SprintSource(err, 2, 2),
		Flags:    code,
		Filename: path,
		Index:    fmt.Sprintf("%s_%04d%02d", meta.Program, ts.Year(), ts.Month()),
	}

	b, err := helper.JsonMarshal(data)
	if err != nil {
		fmt.Println("pushLog MarshalBinary err =  ", err.Error())
		return err
	}

	_, err = meta.MerchantBean.Put("zinc_fluent_log", b, 0, 0, 0)
	if err != nil {
		fmt.Println("pushLog MerchantBean Put err =  ", err.Error())
		return err
	}

	return fmt.Errorf("系统错误 %s", id)
}

func Close() {
	meta.MerchantBean.Conn.Release()
	meta.MerchantDB.Close()
	meta.MerchantRedis.Close()
}

func MurmurHash(str string, seed uint32) uint64 {

	s := fmt.Sprintf("%s%d", str, seed)

	v := xxhash3.HashString(s)
	return v
}

func Lock(id string) error {

	val := fmt.Sprintf("%s:%s", defaultRedisKeyPrefix, id)
	ok, err := meta.MerchantRedis.SetNX(ctx, val, "1", LockTimeout).Result()
	if err != nil {
		return pushLog(err, helper.RedisErr)
	}
	if !ok {
		return errors.New(helper.RequestBusy)
	}

	return nil
}

func Unlock(id string) {

	val := fmt.Sprintf("%s:%s", defaultRedisKeyPrefix, id)
	res, err := meta.MerchantRedis.Unlink(ctx, val).Result()
	if err != nil || res != 1 {
		fmt.Println("Unlock res = ", res)
		fmt.Println("Unlock err = ", err)
	}
}

func IpLocation(ipStr string) (string, string, string, error) {

	results, err := meta.IpDB.Get_all(ipStr)
	if err != nil {
		return "", "", "", err
	}

	return results.Country_long, results.Region, results.City, nil
}
