package model

import "time"

// 常量定义
const (
	apiTimeOut = time.Second * 8
)

var (
	devices = map[int]uint8{
		24: 1,
		25: 2,
		35: 4,
		36: 3,
	}
	Devices = map[int]bool{
		DeviceTypeWeb:            true,
		DeviceTypeH5:             true,
		DeviceTypeAndroidFlutter: true,
		DeviceTypeIOSFlutter:     true,
	}
	WebDevices = map[int]bool{
		DeviceTypeWeb: true,
		DeviceTypeH5:  true,
	}
)

// 设备端类型
const (
	DeviceTypeWeb            = 24 //web
	DeviceTypeH5             = 25 //h5
	DeviceTypeAndroidFlutter = 35 //android_flutter
	DeviceTypeIOSFlutter     = 36 //ios_flutter
)

// 存款状态
const (
	DepositConfirming    = 361 //确认中
	DepositSuccess       = 362 //存款成功
	DepositCancelled     = 363 //存款已取消
	DepositReviewing     = 364 //存款审核中
	DepositRepairSuccess = 365 //补单成功
)

const (
	RecordTradeDeposit  int = 271 // 存款
	RecordTradeWithdraw int = 272 // 取款
)

var (
	// 通过redis锁定提款订单的key
	depositOrderLockKey = "d:order:%s"
	// 通过redis锁定提款订单的key
	withdrawOrderLockKey = "w:order:%s"
)

const (
	defaultRedisKeyPrefix = "rlock:"
	LockTimeout           = 20 * time.Second
)

// 取款状态
const (
	WithdrawReviewing     = 371 //审核中
	WithdrawReviewReject  = 372 //审核拒绝
	WithdrawDealing       = 373 //出款中
	WithdrawSuccess       = 374 //提款成功
	WithdrawFailed        = 375 //出款失败
	WithdrawAbnormal      = 376 //异常订单
	WithdrawAutoPayFailed = 377 // 代付失败
	WithdrawHangup        = 378 // 挂起
	WithdrawDispatched    = 379 // 已派单
	WithdrawAutoPaying    = 380 // 三方出款中
)
