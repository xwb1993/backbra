package middleware

import (
	"encoding/base64"
	"errors"
	"member/contrib/helper"
	"member/contrib/session"

	"github.com/valyala/fasthttp"
)

var allows = map[string]bool{
	"/member/app/upgrade":   true,
	"/member/cs/list":       true,
	"/member/game/hot/list": true,
	"/member/game/fav/list": true,
	"/member/game/rec/list": true,
	"/member/tag/list":      true,

	"/member/game/search":               true,
	"/member/report/version":            true,
	"/member/report/pprof/":             true,
	"/member/report/pprof/block":        true,
	"/member/report/pprof/allocs":       true,
	"/member/report/pprof/cmdline":      true,
	"/member/report/pprof/goroutine":    true,
	"/member/report/pprof/heap":         true,
	"/member/report/pprof/profile":      true,
	"/member/report/pprof/trace":        true,
	"/member/report/pprof/threadcreate": true,
	"/member/reg":                       true,
	"/member/login":                     true,
	"/sms/send":                         true,
	"/sms/send/mail":                    true,
	"/member/lastwin":                   true,
	"/member/banner":                    true,
	"/member/notice":                    true,
	"/pay/callback":                     true,
	"/pay/callback/epayu":               true,
	"/pay/callback/ew":                  true,
	"/pay/callback/ewu":                 true,
	"/member/sign/config":               true,
	"/member/treasure/config":           true,
	"/member/nav":                       true,
	"/member/game/list":                 true,
	"/member/password/forget":           true,
	"/treasure/config":                  true,
	"/sign/config":                      true,
	"/weekly/config":                    true,
}

func CheckTokenMiddleware(ctx *fasthttp.RequestCtx) (interface{}, error) {

	var special = map[string]bool{
		"/member/game/list":     true,
		"/member/game/hot/list": true,
		"/member/game/rec/list": true,
		"/member/game/fav/list": true,
	}
	path := string(ctx.Path())
	d := string(ctx.Request.Header.Peek("d"))
	//fmt.Println("path: ", path, "d :", d)

	if d == "35" || d == "36" && ctx.IsPost() {

		b := string(ctx.PostBody())
		b = base64.StdEncoding.EncodeToString([]byte(b))
		data, err := base64.StdEncoding.DecodeString(b)
		if err != nil {
			//fmt.Println("POST DecodeString err = ", err.Error())
			resp := helper.Response{
				Status: false,
				Data:   "paramNull",
			}
			return resp, errors.New(helper.ParamNull)
		}

		//fmt.Println("CheckTokenMiddleware = SetBodyRaw")
		ctx.Request.ResetBody()
		ctx.Request.SetBodyRaw(data)
	}

	if _, ok := special[path]; ok {
		data, err := session.Get(ctx)
		if err == nil {
			ctx.SetUserValue("token", data)
		}
	}

	if _, ok := allows[path]; ok {
		return "", nil
	}

	resp := helper.Response{
		Status: false,
		Data:   "token",
	}
	data, err := session.Get(ctx)
	if err != nil {
		return resp, errors.New(helper.AccessTokenExpires)
	}

	ctx.SetUserValue("token", data)

	return "", nil
}
