package middleware

import (
	"encoding/json"
	"fmt"
	"github.com/valyala/fasthttp"
)

type middleware_t func(ctx *fasthttp.RequestCtx) (interface{}, error)

var MiddlewareList = []middleware_t{
	//CorsMiddleware,
	CheckTokenMiddleware,
}

func Use(next fasthttp.RequestHandler) fasthttp.RequestHandler {

	return fasthttp.RequestHandler(func(ctx *fasthttp.RequestCtx) {

		for _, cb := range MiddlewareList {
			if body, err := cb(ctx); err != nil {
				fmt.Fprint(ctx, err)

				bytes, err := json.Marshal(body)
				if err != nil {
					ctx.SetBody([]byte(err.Error()))
					return
				}
				ctx.SetBody(bytes)
				return
			}
		}

		next(ctx)
	})
}
