package controller

import (
	"member/contrib/helper"
	"member/model"

	"github.com/valyala/fasthttp"
)

type FavController struct{}

func (that *FavController) Insert(ctx *fasthttp.RequestCtx) {

	ty := string(ctx.QueryArgs().Peek("ty"))
	gameId := string(ctx.QueryArgs().Peek("id"))
	if !helper.CtypeDigit(gameId) {
		helper.Print(ctx, false, helper.IDErr)
		return
	}

	err := model.FavInsert(ctx, gameId, ty)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

func (that *FavController) List(ctx *fasthttp.RequestCtx) {

	ty := string(ctx.QueryArgs().Peek("ty"))
	hot := ctx.QueryArgs().GetUintOrZero("hot")
	platform_id := string(ctx.QueryArgs().Peek("platform_id"))
	game_type := ctx.QueryArgs().GetUintOrZero("game_type")

	data, err := model.FavList(ctx, game_type, hot, platform_id, ty)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

func (that *FavController) Delete(ctx *fasthttp.RequestCtx) {

	id := string(ctx.QueryArgs().Peek("id"))
	if !helper.CtypeDigit(id) {
		helper.Print(ctx, false, helper.IDErr)
		return
	}

	err := model.FavDelete(ctx, id)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)

}
