package controller

import (
	"fmt"
	"member/contrib/helper"
	"member/model"

	"github.com/valyala/fasthttp"
)

type GameController struct{}

func (that *GameController) Search(ctx *fasthttp.RequestCtx) {

	ty := ctx.QueryArgs().GetUintOrZero("ty")
	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	tagId := string(ctx.QueryArgs().Peek("tag_id"))
	platformId := string(ctx.QueryArgs().Peek("platform_id"))
	w := string(ctx.QueryArgs().Peek("w"))
	if helper.CtypePunct(w) {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	if !helper.CtypeDigit(platformId) {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	if !helper.CtypeDigit(tagId) {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if page < 1 {
		page = 1
	}
	if pageSize > 100 {
		pageSize = 100
	}
	if pageSize < 10 {
		pageSize = 10
	}

	data, err := model.GameFullTextMeili(ty, page, pageSize, platformId, tagId, w)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

func (that *GameController) Rec(ctx *fasthttp.RequestCtx) {

	ty := ctx.QueryArgs().GetUintOrZero("ty")
	page := ctx.QueryArgs().GetUintOrZero("page")
	hot := ctx.QueryArgs().GetUintOrZero("hot")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	platformId := string(ctx.QueryArgs().Peek("platform_id"))

	if !helper.CtypeDigit(platformId) {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if page < 1 {
		page = 1
	}
	if pageSize < 10 {
		pageSize = 10
	}
	if pageSize > 50 {
		pageSize = 50
	}
	data, err := model.GameListMeili(ctx, ty, hot, page, pageSize, platformId, "is_new")
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

func (that *GameController) Hot(ctx *fasthttp.RequestCtx) {

	ty := ctx.QueryArgs().GetUintOrZero("ty")
	hot := ctx.QueryArgs().GetUintOrZero("hot")
	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	platformId := string(ctx.QueryArgs().Peek("platform_id"))

	if !helper.CtypeDigit(platformId) {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if page < 1 {
		page = 1
	}
	if pageSize < 10 {
		pageSize = 10
	}
	if pageSize > 50 {
		pageSize = 50
	}
	data, err := model.GameListMeili(ctx, ty, hot, page, pageSize, platformId, "is_hot")
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

func (that *GameController) Fav(ctx *fasthttp.RequestCtx) {

	ty := ctx.QueryArgs().GetUintOrZero("ty")
	hot := ctx.QueryArgs().GetUintOrZero("hot")
	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")

	platformId := string(ctx.QueryArgs().Peek("platform_id"))

	if !helper.CtypeDigit(platformId) {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if page < 1 {
		page = 1
	}
	if pageSize < 10 {
		pageSize = 10
	}
	if pageSize > 50 {
		pageSize = 50
	}

	data, err := model.GameListMeili(ctx, ty, hot, page, pageSize, platformId, "is_fav")
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)

}

// 游戏列表
func (that *GameController) List(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	gameType := ctx.QueryArgs().GetUintOrZero("game_type")
	isHot := ctx.QueryArgs().GetUintOrZero("is_hot")
	isNew := ctx.QueryArgs().GetUintOrZero("is_new")
	tagId := string(ctx.QueryArgs().Peek("tag_id"))

	platformId := string(ctx.QueryArgs().Peek("platform_id"))

	if !helper.CtypeDigit(platformId) {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if page < 1 {
		page = 1
	}
	if pageSize < 10 {
		pageSize = 10
	}
	if pageSize > 50 {
		pageSize = 50
	}

	filter := "online = 1"

	if isHot > 0 {
		filter = " AND is_hot = 1"
	}
	if isNew > 0 {
		filter += " AND is_new = 1"
	}
	if gameType != 0 {
		filter += fmt.Sprintf(" AND game_type = %d", gameType)
	}
	if platformId != "0" {
		filter += fmt.Sprintf(" AND platform_id = %s", platformId)
	}
	if tagId != "0" {
		filter += fmt.Sprintf(" AND tag_ids = %s", tagId)
	}

	data, err := model.GameList(ctx, page, pageSize, filter)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}
