package controller

import (
	"github.com/valyala/fasthttp"
	"member/contrib/helper"
	"member/model"
)

type MemberVipController struct {
}

func (that MemberVipController) List(ctx *fasthttp.RequestCtx) {

	s, err := model.MemberVipList()
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}
