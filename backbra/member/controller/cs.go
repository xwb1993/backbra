package controller

import (
	"member/contrib/helper"
	"member/model"
	"strings"

	"github.com/valyala/fasthttp"
)

type CsController struct{}

func (that *CsController) List(ctx *fasthttp.RequestCtx) {

	field := string(ctx.QueryArgs().Peek("fields"))

	fields := strings.SplitN(field, ",", 4)
	data, err := model.CsList(fields)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}
