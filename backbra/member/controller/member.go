package controller

import (
	"encoding/json"
	"fmt"
	"github.com/valyala/fasthttp"
	"member/contrib/helper"
	"member/contrib/validator"
	"member/model"
	"strconv"
	"strings"
)

type MemberController struct{}

type memberRegParam struct {
	LinkID     string `json:"link_id"`
	RegUrl     string `json:"reg_url"`
	Email      string `json:"email"`
	DeviceNo   string `json:"device_no"`
	Password   string `json:"password"`
	Phone      string `json:"phone"`
	Sid        string `json:"sid"`
	Ts         string `json:"ts"`
	VerifyCode string `json:"verify_code"`
	ProxyId    string `json:"proxy_id"` //业务员id
}

type loginParam struct {
	DeviceNo string `json:"device_no"`
	Vid      string `json:"vid"`
	Code     string `json:"code"`
	Username string `json:"username"`
	Password string `json:"password"`
}

// 修改用户密码参数
type forgetPassword struct {
	Sid      string `json:"sid"`
	Ts       string `json:"ts"`
	Code     string `json:"code"`
	Phone    string `json:"phone"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (that *MemberController) Info(ctx *fasthttp.RequestCtx) {

	m, err := model.MemberInfo(ctx)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, m)
}

func (that *MemberController) Token(ctx *fasthttp.RequestCtx) {

	m, err := model.MemberCache(ctx, "")
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	m.CreatedAt = 0
	m.Password = ""

	helper.Print(ctx, true, m)
}

func (that *MemberController) Balance(ctx *fasthttp.RequestCtx) {

	a, err := model.MemberAmount(ctx)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, a)
}

func (that *MemberController) LastWinRecord(ctx *fasthttp.RequestCtx) {

	a, err := model.LastWinRecord()
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, a)
}

func (that *MemberController) Login(ctx *fasthttp.RequestCtx) {

	param := loginParam{}
	data := ctx.PostBody()
	err := json.Unmarshal(data, &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if param.Username == "" {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if !validator.CheckUPassword(param.Password, 6, 20) {
		helper.Print(ctx, false, helper.PasswordFMTErr)
		return
	}

	device := string(ctx.Request.Header.Peek("d"))
	prefix := string(ctx.Request.Header.Peek("X-Prefix"))

	//fmt.Println("Login prefix = ", prefix)

	i, err := strconv.Atoi(device)
	if err != nil {
		helper.Print(ctx, false, helper.DeviceTypeErr)
		return
	}

	if _, ok := model.Devices[i]; !ok {
		helper.Print(ctx, false, helper.DeviceTypeErr)
		return
	}

	ip := helper.FromRequest(ctx)
	var (
		phone string
		email string
	)
	if strings.Contains(param.Username, "@") {
		email = param.Username
	} else {
		phone = param.Username
	}

	id, err := model.MemberLogin(ctx, prefix, param.Vid, param.Code, phone, email, param.Password, ip, device, param.DeviceNo)
	if err != nil {
		if err.Error() == helper.Blocked || err.Error() == helper.UserNotExist ||
			err.Error() == helper.DeviceBanErr || strings.Contains(err.Error(), helper.IpBanErr) {
			helper.Print(ctx, false, err.Error())
			return
		}

		helper.Print(ctx, false, err.Error())
		return
	}

	ctx.Response.Header.Set("id", id)
	helper.Print(ctx, true, helper.Success)
}

func (that *MemberController) Reg(ctx *fasthttp.RequestCtx) {

	param := memberRegParam{}
	data := ctx.PostBody()
	err := json.Unmarshal(data, &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if param.LinkID != "" {
		if !validator.CheckStringAlnum(param.LinkID) {
			helper.Print(ctx, false, helper.InviteCodeErr)
			return
		}
	}
	if !validator.CheckUPassword(param.Password, 6, 20) {
		helper.Print(ctx, false, helper.PasswordFMTErr)
		return
	}
	ip := helper.FromRequest(ctx)

	if param.Phone != "" {
		err = model.CheckSmsCaptcha(ip, param.Sid, "55"+param.Phone, param.VerifyCode)
		if err != nil {
			helper.Print(ctx, false, helper.PhoneVerificationErr)
			return
		}
	}
	if param.Email != "" {
		err = model.CheckEmailCaptcha(ip, param.Sid, param.Email, param.VerifyCode)
		if err != nil {
			helper.Print(ctx, false, helper.EmailVerificationErr)
			return
		}
	}

	createdAt := uint32(ctx.Time().Unix())
	device := string(ctx.Request.Header.Peek("d"))
	i, err := strconv.Atoi(device)
	if err != nil {
		helper.Print(ctx, false, helper.DeviceTypeErr)
		return
	}

	if _, ok := model.Devices[i]; !ok {
		helper.Print(ctx, false, helper.DeviceTypeErr)
		return
	}

	prefix := string(ctx.Request.Header.Peek("X-Prefix"))
	// 注册地址 去除域名前缀
	uid, err := model.MemberReg(i, prefix, param.Sid, param.Email, param.Password, ip, param.DeviceNo, param.RegUrl, param.LinkID, param.Phone, param.Ts, param.ProxyId, createdAt)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	ctx.Response.Header.Set("id", uid)
	helper.Print(ctx, true, helper.Success)
}

// 会员账号是否可用
func (that *MemberController) Valid(ctx *fasthttp.RequestCtx) {

	username := strings.ToLower(string(ctx.QueryArgs().Peek("username")))
	//字母数字组合，4-9，前2个字符必须为字母
	if !validator.CheckUName(username, 5, 14) {
		helper.Print(ctx, false, helper.UsernameErr)
		return
	}

	// 检测会员是否已注册
	if model.MemberExist(username) {
		helper.Print(ctx, false, helper.UsernameExist)
		return
	}

	helper.Print(ctx, true, helper.Success)
}

func (that *MemberController) Logout(ctx *fasthttp.RequestCtx) {

	model.MemberLogout(ctx)
	helper.Print(ctx, true, helper.Success)
}

func (that *MemberController) UpdateAvatar(ctx *fasthttp.RequestCtx) {

	avatar := ctx.QueryArgs().GetUintOrZero("avatar")
	if avatar == 0 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	err := model.MemberUpdateAvatar(ctx, avatar)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

func (that *MemberController) Nav(ctx *fasthttp.RequestCtx) {

	res, err := model.MemberNav()
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	ctx.SetBody(res)
}

// 用户忘记密码
func (that *MemberController) ForgetPassword(ctx *fasthttp.RequestCtx) {

	params := forgetPassword{}
	data := ctx.PostBody()
	err := json.Unmarshal(data, &params)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if !validator.CheckUPassword(params.Password, 8, 20) {
		helper.Print(ctx, false, helper.PasswordFMTErr)
		return
	}

	ip := helper.FromRequest(ctx)
	err = model.MemberForgetPwd(params.Password, params.Phone, params.Email, ip, params.Sid, params.Code, params.Ts)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

type changePasswordParam struct {
	OldPassword     string `json:"old_password"`
	Password        string `json:"password"`
	ConfirmPassword string `json:"confirm_password"`
}

// 用户修改密码
func (that *MemberController) UpdatePassword(ctx *fasthttp.RequestCtx) {

	params := changePasswordParam{}
	data := ctx.PostBody()
	err := json.Unmarshal(data, &params)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if !validator.CheckUPassword(params.Password, 6, 20) {
		helper.Print(ctx, false, helper.PasswordFMTErr)
		return
	}

	if params.ConfirmPassword != params.Password {
		helper.Print(ctx, false, helper.PasswordInconsistent)
		return
	}

	err = model.MemberPasswordUpdate(params.OldPassword, params.Password, ctx)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

type updatePasswordParam struct {
	Ty       int    `json:"ty"`
	Sid      string `json:"sid"`
	Ts       string `json:"ts"`
	Code     string `json:"code"`
	Password string `json:"password"`
	Old      string `json:"old"`
}

// 用户修改密码
func (that *MemberController) UpdatePayPassword(ctx *fasthttp.RequestCtx) {

	params := updatePasswordParam{}
	data := ctx.PostBody()
	err := json.Unmarshal(data, &params)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	t := map[int]bool{
		1: true,
		2: true,
	}
	if _, ok := t[params.Ty]; !ok {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if !validator.CheckIntScope(params.Password, 100000, 999999) {
		helper.Print(ctx, false, helper.PasswordFMTErr)
		return
	}

	err = model.MemberPayPasswordUpdate(params.Ty, params.Sid, params.Code, params.Password, params.Ts, params.Old, ctx)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

type bindEmailParam struct {
	Sid   string `json:"sid"`
	Ts    string `json:"ts"`
	Code  string `json:"code"`
	Email string `json:"email"`
}

// 用户绑定邮箱
func (that *MemberController) BindEmail(ctx *fasthttp.RequestCtx) {

	params := bindEmailParam{}
	data := ctx.PostBody()
	err := json.Unmarshal(data, &params)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if params.Email == "" || !strings.Contains(params.Email, "@") {
		helper.Print(ctx, false, helper.EmailFMTErr)
		return
	}

	ip := helper.FromRequest(ctx)
	err = model.CheckEmailCaptcha(ip, params.Sid, params.Email, params.Code)
	if err != nil {
		helper.Print(ctx, false, helper.EmailVerificationErr)
		return
	}

	err = model.MemberUpdateEmail(params.Email, params.Ts, ctx)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

type bindPhoneParam struct {
	Phone string `json:"phone"`
	Ts    string `json:"ts"`
	Sid   string `json:"sid"`
	Code  string `json:"code"`
}

// 用户绑定手机号
func (that *MemberController) BindPhone(ctx *fasthttp.RequestCtx) {

	params := bindPhoneParam{}
	data := ctx.PostBody()
	err := json.Unmarshal(data, &params)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	ip := helper.FromRequest(ctx)
	err = model.CheckSmsCaptcha(ip, params.Sid, "55"+params.Phone, params.Code)
	if err != nil {
		helper.Print(ctx, false, helper.PhoneVerificationErr)
		return
	}

	err = model.MemberUpdatePhone(params.Ts, params.Phone, ctx)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

type updateInfoParam struct {
	Phone    string `json:"phone"`
	Email    string `json:"email"`
	Telegram string `json:"telegram"`
}

func (that *MemberController) UpdateInfo(ctx *fasthttp.RequestCtx) {

	params := updateInfoParam{}
	data := ctx.PostBody()
	err := json.Unmarshal(data, &params)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	err = model.MemberUpdateInfo(ctx, params.Phone, params.Email, params.Telegram)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}
