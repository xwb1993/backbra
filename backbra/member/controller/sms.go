package controller

import (
	"encoding/json"
	"fmt"
	"github.com/valyala/fasthttp"
	"member/contrib/helper"
	"member/model"
	"strings"
)

type SmsController struct{}
type smsSendFormT struct {
	Ty   int    `json:"ty"` //1=注册  2=忘记密码
	Tel  string `json:"tel"`
	Flag string `json:"flag"`
}
type emailSendFormT struct {
	Username string `json:"username"`
	Mail     string `json:"mail"`
	Ty       string `json:"ty"` //1=注册  2=忘记密码
}

func (that SmsController) Send(ctx *fasthttp.RequestCtx) {

	form := smsSendFormT{}
	data := ctx.PostBody()
	err := json.Unmarshal(data, &form)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	tys := map[int]bool{
		1: true,
		2: true,
	}
	if _, ok := tys[form.Ty]; !ok {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	flags := map[string]bool{
		"text":  true,
		"voice": true,
	}
	if _, ok := flags[form.Flag]; !ok {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	day := ctx.Time().Format("0102")
	sip := helper.FromRequest(ctx)
	form.Tel = "55" + form.Tel
	id, ts, err := model.SmsOffline(form.Tel, day, sip, form.Flag, fmt.Sprintf(`%d`, form.Ty))
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, fmt.Sprintf("%s:%d", id, ts))
}

func (that *SmsController) SendMail(ctx *fasthttp.RequestCtx) {

	form := emailSendFormT{}
	data := ctx.PostBody()
	err := json.Unmarshal(data, &form)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	tys := map[string]bool{
		"1": true,
		"2": true,
	}
	if _, ok := tys[form.Ty]; !ok {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if !strings.Contains(form.Mail, "@") {
		helper.Print(ctx, false, helper.EmailFMTErr)
		return
	}

	day := ctx.Time().Format("0102")
	sip := helper.FromRequest(ctx)
	id, ts, err := model.MailOffline(form.Username, form.Mail, day, sip, form.Ty)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, fmt.Sprintf("%s:%d", id, ts))
}

type sendOnlineParam struct {
	Ty  int    `json:"ty"` // 3=修改密码  4=提现 2=修改邮箱 5 绑定手机
	Tel string `json:"tel"`
}

func (that *SmsController) SendOnline(ctx *fasthttp.RequestCtx) {

	form := sendOnlineParam{}
	data := ctx.PostBody()
	err := json.Unmarshal(data, &form)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	tys := map[string]bool{
		"3": true,
		"4": true,
		"2": true,
		"5": true,
	}
	if _, ok := tys[fmt.Sprintf(`%d`, form.Ty)]; !ok {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	mb, err := model.MemberCache(ctx, "")
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	day := ctx.Time().Format("0102")
	sip := helper.FromRequest(ctx)
	form.Tel = "55" + form.Tel

	id, ts, err := model.SmsOnline(mb, day, sip, "text", fmt.Sprintf(`%d`, form.Ty), form.Tel)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, fmt.Sprintf("%s:%d", id, ts))
}

type sendOnlineMailParam struct {
	Ty   int    `json:"ty"` // 1=绑定 3修改密码 4=提现 5=修改手机号
	Mail string `json:"mail"`
}

func (that *SmsController) SendOnlineMail(ctx *fasthttp.RequestCtx) {

	form := sendOnlineMailParam{}
	data := ctx.PostBody()
	err := json.Unmarshal(data, &form)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	tys := map[string]bool{
		"2": true,
		"3": true,
		"4": true,
		"5": true,
	}
	if _, ok := tys[fmt.Sprintf(`%d`, form.Ty)]; !ok {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if form.Mail != "" {
		if !strings.Contains(form.Mail, "@") {
			helper.Print(ctx, false, helper.EmailFMTErr)
			return
		}
	}

	mb, err := model.MemberCache(ctx, "")
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	day := ctx.Time().Format("0102")
	sip := helper.FromRequest(ctx)
	id, ts, err := model.MailOnline(mb, form.Mail, day, sip, fmt.Sprintf(`%d`, form.Ty))
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, fmt.Sprintf("%s:%d", id, ts))
}

func (that SmsController) CallbackKmi(ctx *fasthttp.RequestCtx) {

	//smsId := string(ctx.QueryArgs().Peek("smsId"))           //消息ID
	//to := string(ctx.QueryArgs().Peek("to"))                 //手机号码
	//sendResult := string(ctx.QueryArgs().Peek("sendResult")) //发送结果（SUCCESS、FAILURE）
	//smsCount := ctx.QueryArgs().GetUintOrZero("smsCount")    //短信数量
	//dr := string(ctx.QueryArgs().Peek("dr"))                 //DR结果
	//sendTime := ctx.QueryArgs().GetUintOrZero("sendTime")    //发送时间（系统毫秒级）

	ctx.SetBody([]byte(`SUCCESS`))
}
