package controller

import (
	"member/contrib/helper"
	"member/model"

	"github.com/valyala/fasthttp"
)

type UpgradeController struct{}

var (
	apps = map[string]string{
		"26": "ios",
		"27": "android",
		"35": "android",
		"36": "ios",
	}
)

func (that *UpgradeController) Info(ctx *fasthttp.RequestCtx) {

	d := string(ctx.Request.Header.Peek("d"))
	dv := string(ctx.QueryArgs().Peek("dv"))
	if dv != "" {
		d = dv
	}
	device, ok := apps[d]
	if !ok {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	s, err := model.UpgradeInfo(device)
	if err != nil && s != nil {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	ctx.SetBody(s)
}
