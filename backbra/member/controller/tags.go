package controller

import (
	"member/contrib/helper"
	"member/model"

	"github.com/valyala/fasthttp"
)

type TagController struct{}

func (that *TagController) List(ctx *fasthttp.RequestCtx) {

	gameType := string(ctx.QueryArgs().Peek("game_type"))
	platformId := string(ctx.QueryArgs().Peek("platform_id"))

	if !helper.CtypeDigit(platformId) {
		helper.Print(ctx, false, helper.IDErr)
		return
	}
	if !helper.CtypeDigit(gameType) {
		helper.Print(ctx, false, helper.IDErr)
		return
	}

	data, err := model.TagList(platformId, gameType)
	if err != nil {
		helper.Print(ctx, false, err)
		return
	}
	helper.Print(ctx, true, data)
}
