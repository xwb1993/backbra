package controller

import (
	"github.com/valyala/fasthttp"
	"member/contrib/helper"
	"member/model"
)

type PromoDepositController struct{}

func (that PromoDepositController) Config(ctx *fasthttp.RequestCtx) {

	mb, err := model.MemberCache(ctx, "")
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}
	ty := 2
	if mb.DepositAmount == 0 {
		ty = 1
	}
	s, err := model.PromoDepositConfigList(ty)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}
