package controller

import (
	"encoding/json"
	"fmt"
	"github.com/valyala/fasthttp"
	"member/contrib/helper"
	"member/model"
)

type BankcardController struct{}

type insertBankParam struct {
	PixId       string `json:"pix_id"`
	PayPassword string `json:"pay_password"`
	Realname    string `json:"realname"`
	Flag        string `json:"flag"` //1 cpf 2 phone 3 email
	Account     string `json:"pix_account"`
}

// 绑定银行卡
func (that *BankcardController) Insert(ctx *fasthttp.RequestCtx) {

	param := insertBankParam{}
	data := ctx.PostBody()
	err := json.Unmarshal(data, &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if !helper.CtypeDigit(param.PixId) {
		helper.Print(ctx, false, helper.BankcardIDErr)
		return
	}

	mb, err := model.MemberCache(ctx, "")
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	// 用户绑定银行卡
	err = model.BankcardInsert(mb.Uid, mb.Username, param.Realname, param.Flag, param.Account, param.PixId, "PIX", param.PayPassword)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

func (that *BankcardController) List(ctx *fasthttp.RequestCtx) {

	mb, err := model.MemberCache(ctx, "")
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	// 更新权限信息
	res, err := model.BankcardList(mb.Uid)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, res)
}

func (that *BankcardController) BankType(ctx *fasthttp.RequestCtx) {

	// 更新权限信息
	res, err := model.BanktypeList()
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, res)
}
