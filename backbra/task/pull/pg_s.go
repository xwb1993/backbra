package pull

import (
	"crypto/hmac"
	"crypto/sha256"
	"fmt"
	"github.com/google/uuid"
	"github.com/valyala/fasthttp"
	"strconv"
	"task/common"
	"task/contrib/helper"
	"time"
)

const pgSId = 16595015200306

var (
	pgsp = pgSReq{
		Api:       "",
		ApiKey:    "",
		ApiSecret: "",
		Page:      1,
		PageSize:  500,
	}
	pgsm = map[string]string{
		"60":      "Leprechaun Riches",
		"65":      "Mahjong Ways",
		"74":      "Mahjong Ways 2",
		"87":      "Treasures of Aztec",
		"89":      "Lucky Neko",
		"48":      "Double Fortune",
		"53":      "The Great Icescape",
		"54":      "Captains Bounty",
		"71":      "Caishen Wins",
		"75":      "Ganesha Fortune",
		"79":      "Dreams of Macau",
		"84":      "Queen of Bounty ",
		"98":      "Fortune Ox",
		"104":     "Wild Bandito",
		"106":     "Ways of the Qilin",
		"57":      "Dragon Hatch",
		"73":      "Egypt's Book of Mystery",
		"82":      "Phoenix Rises",
		"83":      "Wild Fireworks",
		"92":      "Thai River Wonders",
		"94":      "Bali Vacation",
		"103":     "Crypto Gold",
		"1":       "Honey Trap of Diao Chan",
		"3":       "Fortune Gods",
		"0":       "Game Lobby",
		"24":      "Win Win Won",
		"6":       "Medusa II",
		"26":      "Tree of Fortune",
		"7":       "Medusa ",
		"25":      "Plushie Frenzy",
		"17":      "Wizdom Wonders",
		"2":       "Gem Saviour",
		"18":      "Hood vs Wolf",
		"28":      "Hotpot",
		"29":      "Dragon Legend",
		"35":      "Mr. Hallow-Win",
		"34":      "Legend of Hou Yi",
		"36":      "Prosperity Lion",
		"33":      "Hip Hop Panda",
		"37":      "Santas Gift Rush",
		"31":      "Baccarat Deluxe",
		"38":      "Gem Saviour Sword",
		"39":      "Piggy Gold",
		"41":      "Symbols of Egypt",
		"44":      "Emperors Favour",
		"42":      "Ganesha Gold",
		"43":      "Three Monkeys ",
		"40":      "Jungle Delight",
		"50":      "Journey to the Wealth",
		"61":      "Flirting Scholar",
		"59":      "Ninja vs Samurai",
		"64":      "Muay Thai Champion",
		"63":      "Dragon Tiger Luck",
		"68":      "Fortune Mouse",
		"20":      "Reel Love",
		"62":      "Gem Saviour Conquest",
		"67":      "Shaolin Soccer",
		"70":      "Candy Burst ",
		"69":      "Bikini Paradise ",
		"85":      "Genie's 3 Wishes",
		"80":      "Circus Delight",
		"90":      "Secrets of Cleopatra",
		"58":      "Vampires Charm",
		"88":      "Jewels of Prosperity",
		"97":      "Jack Frosts Winter",
		"86":      "Galactic Gems",
		"91":      "Guardians of Ice and Fire",
		"93":      "Opera Dynasty",
		"95":      "Majestic Treasures",
		"100":     "Candy Bonanza",
		"105":     "Heist  Stakes",
		"101":     "Rise of Apollo",
		"109":     "Sushi Oishi",
		"110":     "Jurassic Kingdom",
		"102":     "Mermaid Riches",
		"111":     "Groundhog Harvest",
		"113":     "Raider Jane's Crypt of Fortune",
		"115":     "Supermarket Spree",
		"108":     "Buffalo Win",
		"107":     "Legendary Monkey King",
		"119":     "Spirited Wonders",
		"116":     "Farm Invaders",
		"114":     "Emoji Riches",
		"117":     "Cocktail Nights",
		"118":     "Mask Carnival",
		"112":     "Oriental Prosperity",
		"126":     "Fortune Tiger",
		"122":     "Garuda Gems",
		"121":     "Destiny of Sun & Moon",
		"125":     "Butterfly Blossom",
		"123":     "Rooster Rumble",
		"124":     "Battleground Royale",
		"127":     "Speed Winner",
		"128":     "Legend of Perseus",
		"129":     "Win Win Fish Prawn Crab",
		"130":     "Lucky Piggy",
		"132":     "Wild Coaster",
		"135":     "Wild Bounty Showdown",
		"1312883": "Prosperity Fortune Tree",
		"1338274": "Totem Wonders",
		"1340277": "Asgardian Rising",
		"1368367": "Alchemy Gold",
		"1372643": "Diner Delights",
		"1381200": "Hawaiian Tiki",
		"1402846": "Midas Fortune",
		"1418544": "Bakery Bonanza",
		"1420892": "Rave Party Fever",
		"1448762": "Songkran Splash",
		"1543462": "Fortune Rabbit",
	}

	pasKey = fmt.Sprintf("pull:%s", pgSgId)
)

type pgSReq struct {
	Api       string
	ApiKey    string
	ApiSecret string
	FromTime  int64
	ToTime    int64
	Page      int64
	PageSize  int64
}

func PGSReadT(date, flag string) bool {

	pgsp.ToTime = time.Now().UnixMilli()

	// 全量补单
	if flag == "full" {

		if len(date) != 8 {
			return false
		}
		year, _ := strconv.Atoi(date[:4])
		month, _ := strconv.Atoi(date[4:6])
		day, _ := strconv.Atoi(date[6:])
		//fmt.Println(year, month, day)
		t := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local)
		pgsp.FromTime = t.UnixMilli()
		_ = pgSProcessT(pgsp, flag)
		return true
	} else if flag == "min" {

		min, _ := strconv.ParseInt(date, 10, 64)
		pgsp.FromTime = time.Now().UnixMilli() - min*60000
		_ = pgSProcessT(pgsp, "full")
		return true
	} else {

		pgsp.FromTime = time.Now().UnixMilli() - 1800000
		_ = pgSProcessT(pgsp, "full")
		return true
	}
}

func pgSProcessT(req pgSReq, flag string) error {

	req.Api = GameConfig.One.API
	req.ApiKey = GameConfig.One.APIKey
	req.ApiSecret = GameConfig.One.APISecret
	b, ts := pgSPage(req, flag)
	for b && ts > 0 {
		time.Sleep(4 * time.Second)
		req.Page = ts
		b, ts = pgSPage(req, flag)
	}

	return nil
}

func pgSPage(config pgSReq, flag string) (bool, int64) {

	req := map[string]interface{}{
		"traceId":  uuid.New().String(),
		"fromTime": config.FromTime,
		"toTime":   config.ToTime,
		"pageNo":   config.Page,
		"pageSize": config.PageSize,
	}
	requestBody, _ := helper.JsonMarshal(req)
	fmt.Println("json:", string(requestBody))
	header := map[string]string{
		"Content-Type": "application/json",
		"X-API-Key":    config.ApiKey,
		"X-Signature":  HmacSha256(string(requestBody), config.ApiSecret),
	}
	fmt.Println("header:", header)

	reqUri := fmt.Sprintf(`%s/transaction/list`, config.Api)
	fmt.Println("requri=", reqUri)
	body, statusCode, err := common.HttpDoTimeout(requestBody, "POST", reqUri, header, time.Duration(60)*time.Second)
	if err != nil {
		if flag == "full" {
			fmt.Println("err1 = ", err.Error(), err)
			fmt.Println("timeout=", err.Error() == "timeout")
		}
		if err.Error() == "timeout" {
			body, statusCode, err = common.HttpDoTimeout(requestBody, "POST", reqUri, header, time.Duration(60)*time.Second)
		}
		if err != nil {
			if flag == "full" {
				fmt.Println("err10 = ", err.Error(), err)
				fmt.Println("timeout=", err.Error() == "timeout")
			}
			return false, 0
		}
	}
	fmt.Println("err:", string(body))
	if statusCode != fasthttp.StatusOK {
		fmt.Printf("unexpected status code: %d. Expecting %d\r\n", statusCode, fasthttp.StatusOK)
		return false, 0
	}

	var response Response
	err = helper.JsonUnmarshal(body, &response)
	if err != nil {
		fmt.Println("Error decoding JSON:", err)
		return false, 0
	}
	if response.Status != "SC_OK" {
		return false, 0
	}

	m := response.Data.Headers

	for _, v := range response.Data.Transactions {
		record := Transaction{
			BetID:                 v[m["betId"]].(string),
			RoundID:               v[m["roundId"]].(string),
			ExternalTransactionID: v[m["externalTransactionId"]].(string),
			Username:              v[m["username"]].(string),
			CurrencyCode:          v[m["currencyCode"]].(string),
			GameCode:              v[m["gameCode"]].(string),
			VendorCode:            v[m["vendorCode"]].(string),
			GameCategoryCode:      v[m["gameCategoryCode"]].(string),
			BetAmount:             v[m["betAmount"]].(float64),
			WinAmount:             v[m["winAmount"]].(float64),
			WinLoss:               v[m["winLoss"]].(float64),
			EffectiveTurnover:     v[m["effectiveTurnover"]].(float64),
			JackpotAmount:         v[m["jackpotAmount"]].(float64),
			Status:                v[m["status"]].(float64),
			VendorBetTime:         int64(v[m["vendorBetTime"]].(float64)),
			VendorSettleTime:      int64(v[m["vendorSettleTime"]].(float64)),
		}

		err = pgSParse(record, flag)
		if err != nil {
			return false, 0
		}
	}

	if response.Data.TotalPages > response.Data.CurrentPage {
		return true, response.Data.CurrentPage + 1
	}

	return true, 0
}

type Response struct {
	TraceID string `json:"traceId"`
	Status  string `json:"status"`
	Message string `json:"message"`
	Data    Data   `json:"data"`
}

type Data struct {
	Headers      map[string]int  `json:"headers"`
	Transactions [][]interface{} `json:"transactions"`
	CurrentPage  int64           `json:"currentPage"`
	TotalItems   int64           `json:"totalItems"`
	TotalPages   int64           `json:"totalPages"`
}

type Transaction struct {
	BetID                 string  `json:"betId"`
	RoundID               string  `json:"roundId"`
	ExternalTransactionID string  `json:"externalTransactionId"`
	Username              string  `json:"username"`
	CurrencyCode          string  `json:"currencyCode"`
	GameCode              string  `json:"gameCode"`
	VendorCode            string  `json:"vendorCode"`
	GameCategoryCode      string  `json:"gameCategoryCode"`
	BetAmount             float64 `json:"betAmount"`
	WinAmount             float64 `json:"winAmount"`
	WinLoss               float64 `json:"winLoss"`
	EffectiveTurnover     float64 `json:"effectiveTurnover"`
	JackpotAmount         float64 `json:"jackpotAmount"`
	Status                float64 `json:"status"`
	VendorBetTime         int64   `json:"vendorBetTime"`
	VendorSettleTime      int64   `json:"vendorSettleTime"`
}

func pgSParse(v Transaction, flag string) error {

	username := v.Username
	gameCode := v.GameCode
	traceId := v.BetID
	orderNum := v.ExternalTransactionID
	createdAt := v.VendorBetTime
	settleAt := v.VendorSettleTime
	betAmount := v.BetAmount
	winAmount := v.WinLoss
	gameName := v.GameCode
	info, _ := MemberFindOne(username[3:])
	fmt.Println("info:", info)
	if info.Uid == "" {
		info.Uid = "0"
		return nil
	}

	if info.Uid == "" {
		info.Uid = "0"
	}
	if info.ParentID == "" {
		info.ParentID = "0"
	}
	if info.TopID == "" {
		info.TopName = "0"
	}
	if info.ParentName == "" {
		info.ParentName = ""
	}
	if info.TopName == "" {
		info.TopName = ""
	}
	record := GameRecord{
		Uid:            info.Uid,
		Result:         "Game Name:" + gameName + "|Hand number:" + traceId,
		GameType:       "3",
		GameName:       gameName,
		GameCode:       gameCode,
		ApiType:        pgSId,
		ApiName:        "PG",
		Flag:           int(v.Status),
		Handicap:       gameName,
		PlayType:       gameName,
		CreatedAt:      time.Now().UnixMilli(),
		Prefix:         username[:3],
		PlayerName:     info.Username,
		Name:           info.Username,
		BetAmount:      betAmount,
		BetTime:        createdAt,
		ApiBetTime:     createdAt,
		SettleTime:     settleAt,
		ApiSettleTime:  settleAt,
		ValidBetAmount: betAmount,
		ApiBillNo:      orderNum,
		BillNo:         orderNum,
		Resettle:       0,
		Presettle:      0,
		RowId:          "pgdy" + orderNum,
		MainBillNo:     orderNum,
	}
	record.ParentUid = info.ParentID
	record.ParentName = info.ParentName
	record.GrandId = info.GrandID
	record.GrandName = info.GrandName
	record.GrantGrantId = info.GreatGrandID
	record.GrantGrantName = info.GreatGrandName
	record.TopUid = info.TopID
	record.TopName = info.TopName
	record.NetAmount = winAmount

	//入库处理
	if cb, ok := modes[flag]; ok {
		cb("pgdy", record)
	}

	return nil
}

func HmacSha256(source string, key string) string {

	mac := hmac.New(sha256.New, []byte(key))
	mac.Write([]byte(source))
	return fmt.Sprintf("%x", mac.Sum(nil))
}
