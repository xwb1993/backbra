package reg

import (
	"context"
	"fmt"
	"task/common"
	"task/contrib/conn"
	"task/contrib/helper"
	ryrpc "task/rpc"
	"time"

	"github.com/meilisearch/meilisearch-go"
	"github.com/valyala/fasthttp"
)

var (
	meili *meilisearch.Client
	ctx   = context.Background()
)

func handler(str string) {

	var doc ryrpc.TblSmsLog

	args := fasthttp.AcquireArgs()
	defer fasthttp.ReleaseArgs(args)

	args.Reset()
	args.Parse(str)

	fmt.Println("reg handler str = ", str)

	if !args.Has("uid") {
		fmt.Println("reg handler args.Has(uid) = ", str)
		return
	}
	if !args.Has("id") {
		fmt.Println("reg  handler args.Has(captchaId) = ", str)
		return
	}
	if !args.Has("t") {
		fmt.Println("reg  handler args.Has(captchaId) = ", str)
		return
	}

	//uid := string(args.Peek("uid"))
	captchaId := string(args.Peek("id"))
	ty := string(args.Peek("t"))

	index := meili.Index("smslog")
	err := index.GetDocument(captchaId, nil, &doc)
	if err != nil {
		fmt.Println("reg meili GetDocument = ", err.Error())
		return
	}
	ts := time.Now()
	doc.UpdatedAt = ts.Unix()
	if ty == "1" {
		doc.State = "3"
	} else if ty == "2" {
		doc.State = "2"
	}

	b, err := helper.JsonMarshal(doc)
	if err != nil {
		fmt.Println("reg meili JsonMarshal err = ", err.Error())
		return
	}

	_, err = index.AddDocuments(b, "id")

	if err != nil {
		fmt.Println("reg meili AddDocuments err = ", err.Error())
		return
	}
}

func Parse(cfg common.Conf, args []string) {

	merchantBean := conn.BeanNew(cfg.Beanstalkd)
	meili = meilisearch.NewClient(meilisearch.ClientConfig{
		Host:   cfg.Meilisearch.Host,
		APIKey: cfg.Meilisearch.Key,
	})

	defer func() {
		merchantBean.Conn.Release()
	}()

	topic := []string{"smslog"}

	for {

		id, res, err := merchantBean.Reserve(topic, time.Duration(30)*time.Second)
		if err != nil {
			//fmt.Println("merchantBean.Reserve err = ", err.Error())
			continue
		}

		//fmt.Println("收到消息： = ", string(res))
		handler(string(res))
		merchantBean.Delete(id)

	}
}
