package common

import (
	g "github.com/doug-martin/goqu/v9"
	"github.com/go-redis/redis/v8"
	"github.com/jmoiron/sqlx"
	"task/contrib/conn"
	"task/contrib/helper"
	ryrpc "task/rpc"
)

var (
	dialect        = g.Dialect("mysql")
	logRedis       *redis.Client
	taskBean       *conn.Connection
	taskDb         *sqlx.DB
	colsMemberVip  = helper.EnumFields(MemberVip{})
	colsMemberBase = helper.EnumFields(ryrpc.TblMemberBase{})
)

func InitBean(bean *conn.Connection) {
	taskBean = bean
}

func InitRedis(cli *redis.Client) {
	logRedis = cli
}

func InitDb(db *sqlx.DB) {
	taskDb = db
}
