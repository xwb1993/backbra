package main

import (
	"game/contrib/helper"
	"github.com/nickalie/go-webpbin"
	"os"
	"path/filepath"
	"strings"
	"testing"
)

func TestGen(t *testing.T) {
	t.Log(helper.GenId())

	//t.Log(len(strings.Split("", ":")))
}

func TestPNGToWebp(t *testing.T) {

	//path := "/Users/brandon/Documents/BR/EVO"
	//path := "/Users/brandon/Documents/BR/JDB"
	//path := "/Users/brandon/Documents/BR/JL"
	//path := "/Users/brandon/Documents/BR/LOGO"
	//path := "/Users/brandon/Documents/BR/PG"
	//path := "/Users/brandon/Documents/BR/VIVO"
	path := "/Users/brandon/Documents/BR/PP"
	files, err := os.ReadDir(path)
	if err != nil {
		return
	}

	for _, fi := range files {

		if fi.IsDir() {
			// 目录则直接跳过
			continue
		} else {
			fName := fi.Name()
			if strings.HasSuffix(strings.ToLower(fName), "png") {
				err := webpbin.NewCWebP().
					Quality(80).
					InputFile(filepath.Join(path, fName)).
					OutputFile(filepath.Join(path, strings.Replace(fName, "png", "png.webp", 1))).
					Run()
				if err != nil {
					t.Log(err.Error())
				}
			}
		}
	}

}
