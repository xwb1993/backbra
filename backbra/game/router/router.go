package router

import (
	"fmt"
	"game/controller"
	"runtime/debug"
	"time"

	"github.com/fasthttp/router"
	"github.com/valyala/fasthttp"
)

var (
	apiTimeoutMsg = `{"status": "false","data":"服务器响应超时，请稍后重试"}`
	ApiTimeout    = time.Second * 15
	buildInfo     BuildInfo
)

type BuildInfo struct {
	GitReversion   string
	BuildTime      string
	BuildGoVersion string
}

var (
	gameCtl = new(controller.WalletController)
	//单一钱包 PG电子
	pgCtl = new(controller.PGController)
	//单一钱包 EVO
	evoCtl = new(controller.EVOController)
	//单一钱包 BTI
	btiCtl = new(controller.BTIController)
	//单一钱包 JILI
	jiliCtl = new(controller.JILIController)
	//单一钱包 PP电子
	ppCtl = new(controller.PPController)
	//单一钱包 JDB
	jdbCtl = new(controller.JDBController)
	//单一钱包 VIVO
	vivoCtl = new(controller.VIVOController)
	//单一钱包 ONE
	oneCtl = new(controller.OneController)
)

func apiServerPanic(ctx *fasthttp.RequestCtx, rcv interface{}) {

	err := rcv.(error)
	fmt.Println(err)
	debug.PrintStack()

	if r := recover(); r != nil {
		fmt.Println("recovered failed", r)
	}

	ctx.SetStatusCode(500)
	return
}

func Version(ctx *fasthttp.RequestCtx) {

	ctx.SetContentType("text/html; charset=utf-8")
	fmt.Fprintf(ctx, "game<br />Git reversion = %s<br />Build Time = %s<br />Go version = %s<br />System Time = %s<br />",
		buildInfo.GitReversion, buildInfo.BuildTime, buildInfo.BuildGoVersion, ctx.Time())

	//ctx.Request.Header.VisitAll(func (key, value []byte) {
	//	fmt.Fprintf(ctx, "%s: %s<br/>", string(key), string(value))
	//})
}

// SetupRouter 设置路由列表
func DefaultRouter(b BuildInfo) *router.Router {
	def := router.New()
	def.PanicHandler = apiServerPanic
	buildInfo = b
	g := def.Group("/game")
	g.GET("/launch", fasthttp.TimeoutHandler(gameCtl.Launch, ApiTimeout, apiTimeoutMsg))
	return def
}

func PPRouter() *router.Router {
	pp := router.New()
	pp.PanicHandler = apiServerPanic
	// 身份校验
	pp.POST("/auth", fasthttp.TimeoutHandler(ppCtl.Authenticate, ApiTimeout, apiTimeoutMsg))
	// 获取余额
	pp.POST("/balance", fasthttp.TimeoutHandler(ppCtl.Balance, ApiTimeout, apiTimeoutMsg))
	// 投注
	pp.POST("/bet", fasthttp.TimeoutHandler(ppCtl.Bet, ApiTimeout, apiTimeoutMsg))
	// 派彩
	pp.POST("/result", fasthttp.TimeoutHandler(ppCtl.Result, ApiTimeout, apiTimeoutMsg))
	// 免费旋转奖励派彩
	pp.POST("/bonus/win", fasthttp.TimeoutHandler(ppCtl.BonusWin, ApiTimeout, apiTimeoutMsg))
	// 累积奖金赢奖派彩
	pp.POST("/jackpot/win", fasthttp.TimeoutHandler(ppCtl.JackpotWin, ApiTimeout, apiTimeoutMsg))
	// 游戏回合结束
	pp.POST("/end/round", fasthttp.TimeoutHandler(ppCtl.EndRound, ApiTimeout, apiTimeoutMsg))
	// 退款请求
	pp.POST("/refund", fasthttp.TimeoutHandler(ppCtl.Refund, ApiTimeout, apiTimeoutMsg))
	// 活动彩金派发
	pp.POST("/prom/win", fasthttp.TimeoutHandler(ppCtl.PromoWin, ApiTimeout, apiTimeoutMsg))
	// 活动彩金派发
	pp.POST("/promo/win", fasthttp.TimeoutHandler(ppCtl.PromoWin, ApiTimeout, apiTimeoutMsg))
	// 场馆调整（只在真人游戏使用）
	pp.POST("/adjustment", fasthttp.TimeoutHandler(ppCtl.Adjustment, ApiTimeout, apiTimeoutMsg))
	return pp
}

func PGRouter() *router.Router {
	pg := router.New()
	pg.PanicHandler = apiServerPanic
	//pg电游回调接口
	//pg电游-session校验
	pg.POST("/VerifySession", fasthttp.TimeoutHandler(pgCtl.VerifySession, ApiTimeout, apiTimeoutMsg))
	//pg电游-余额获取
	pg.POST("/Cash/Get", fasthttp.TimeoutHandler(pgCtl.CashGet, ApiTimeout, apiTimeoutMsg))
	//pg电游-投注/结算
	pg.POST("/Cash/TransferInOut", fasthttp.TimeoutHandler(pgCtl.CashTransferInOut, ApiTimeout, apiTimeoutMsg))
	//pg电游-余额调整
	pg.POST("/Cash/Adjustment", fasthttp.TimeoutHandler(pgCtl.CashAdjustment, ApiTimeout, apiTimeoutMsg))
	return pg
}

func EvoRouter() *router.Router {
	evo := router.New()
	evo.PanicHandler = apiServerPanic
	//EVO
	//EVO-check
	evo.POST("/check", fasthttp.TimeoutHandler(evoCtl.Check, ApiTimeout, apiTimeoutMsg))
	//EVO-余额
	evo.POST("/balance", fasthttp.TimeoutHandler(evoCtl.Balance, ApiTimeout, apiTimeoutMsg))
	//EVO-投注扣款
	evo.POST("/debit", fasthttp.TimeoutHandler(evoCtl.Debit, ApiTimeout, apiTimeoutMsg))
	//EVO-派彩结算
	evo.POST("/credit", fasthttp.TimeoutHandler(evoCtl.Credit, ApiTimeout, apiTimeoutMsg))
	//EVO-取消
	evo.POST("/cancel", fasthttp.TimeoutHandler(evoCtl.Cancel, ApiTimeout, apiTimeoutMsg))
	//EVO-红利
	evo.POST("/promo_payout", fasthttp.TimeoutHandler(evoCtl.Promo, ApiTimeout, apiTimeoutMsg))
	//sid
	evo.POST("/sid", fasthttp.TimeoutHandler(evoCtl.SID, ApiTimeout, apiTimeoutMsg))
	return evo
}

func BTIBalanceRouter() *router.Router {
	bti := router.New()
	bti.PanicHandler = apiServerPanic
	bti.GET("/balance", fasthttp.TimeoutHandler(btiCtl.Balance, ApiTimeout, apiTimeoutMsg))
	return bti
}

func BTIValidateTokenRouter() *router.Router {
	bti := router.New()
	bti.PanicHandler = apiServerPanic
	bti.GET("/", fasthttp.TimeoutHandler(btiCtl.ValidateToken, ApiTimeout, apiTimeoutMsg))
	return bti
}

func BTIReserveRouter() *router.Router {
	bti := router.New()
	bti.PanicHandler = apiServerPanic
	bti.POST("/", fasthttp.TimeoutHandler(btiCtl.Reserve, ApiTimeout, apiTimeoutMsg))
	return bti
}

func BTIDebitReserveRouter() *router.Router {
	bti := router.New()
	bti.PanicHandler = apiServerPanic
	bti.POST("/", fasthttp.TimeoutHandler(btiCtl.DebitReserve, ApiTimeout, apiTimeoutMsg))
	return bti
}

func BTICommitRouter() *router.Router {
	bti := router.New()
	bti.PanicHandler = apiServerPanic
	bti.GET("/", fasthttp.TimeoutHandler(btiCtl.Commit, ApiTimeout, apiTimeoutMsg))
	return bti
}

func BTICancelRouter() *router.Router {
	bti := router.New()
	bti.PanicHandler = apiServerPanic
	bti.GET("/", fasthttp.TimeoutHandler(btiCtl.Cancel, ApiTimeout, apiTimeoutMsg))
	return bti
}

func BTICreditRouter() *router.Router {
	bti := router.New()
	bti.PanicHandler = apiServerPanic
	bti.POST("/", fasthttp.TimeoutHandler(btiCtl.Credit, ApiTimeout, apiTimeoutMsg))
	return bti
}

func BTIDebitCustomerRouter() *router.Router {
	bti := router.New()
	bti.PanicHandler = apiServerPanic
	bti.POST("/", fasthttp.TimeoutHandler(btiCtl.DebitCustomer, ApiTimeout, apiTimeoutMsg))
	return bti
}

func JILIRouter() *router.Router {

	jili := router.New()
	jili.PanicHandler = apiServerPanic
	// 验证账号
	jili.POST("/auth", fasthttp.TimeoutHandler(jiliCtl.Auth, ApiTimeout, apiTimeoutMsg))
	// 玩家注单
	jili.POST("/bet", fasthttp.TimeoutHandler(jiliCtl.Bet, ApiTimeout, apiTimeoutMsg))
	// 取消注单
	jili.POST("/cancelBet", fasthttp.TimeoutHandler(jiliCtl.CancelBet, ApiTimeout, apiTimeoutMsg))
	// 牌局型注单
	jili.POST("/sessionBet", fasthttp.TimeoutHandler(jiliCtl.SessionBet, ApiTimeout, apiTimeoutMsg))
	// 取消牌局型注单
	jili.POST("/cancelSessionBet", fasthttp.TimeoutHandler(jiliCtl.CancelSessionBet, ApiTimeout, apiTimeoutMsg))

	return jili
}

func IBCRouter() *router.Router {

	ibc := router.New()
	ibc.PanicHandler = apiServerPanic

	//单一钱包 沙巴
	ibcCtl := new(controller.IBCController)

	//IBC沙巴体育/电竞的回调
	//IBC沙巴体育/电竞-会员余额
	ibc.POST("/getbalance", fasthttp.TimeoutHandler(ibcCtl.Balance, ApiTimeout, apiTimeoutMsg))
	//IBC沙巴体育/电竞-下注
	ibc.POST("/placebet", fasthttp.TimeoutHandler(ibcCtl.Bet, ApiTimeout, apiTimeoutMsg))
	//IBC沙巴体育/电竞-下注明细
	ibc.POST("/placebetparlay", fasthttp.TimeoutHandler(ibcCtl.BetParlay, ApiTimeout, apiTimeoutMsg))
	//IBC沙巴体育/电竞-确认下注
	ibc.POST("/confirmbet", fasthttp.TimeoutHandler(ibcCtl.Confirm, ApiTimeout, apiTimeoutMsg))
	//IBC沙巴体育/电竞-确认下注明细
	ibc.POST("/confirmbetparlay", fasthttp.TimeoutHandler(ibcCtl.ConfirmParlay, ApiTimeout, apiTimeoutMsg))
	//IBC沙巴体育/电竞-取消下注
	ibc.POST("/cancelbet", fasthttp.TimeoutHandler(ibcCtl.CancelBet, ApiTimeout, apiTimeoutMsg))
	//IBC沙巴体育/电竞-结算
	ibc.POST("/settle", fasthttp.TimeoutHandler(ibcCtl.Settle, ApiTimeout, apiTimeoutMsg))
	//IBC沙巴体育/电竞-二次结算
	ibc.POST("/resettle", fasthttp.TimeoutHandler(ibcCtl.Resettle, ApiTimeout, apiTimeoutMsg))
	//IBC沙巴体育/电竞-取消结算
	ibc.POST("/unsettle", fasthttp.TimeoutHandler(ibcCtl.UnSettle, ApiTimeout, apiTimeoutMsg))
	//IBC沙巴体育/电竞-交易被接受后，赛事供货商将会通过此方法传输交易
	ibc.POST("/cashout", fasthttp.TimeoutHandler(ibcCtl.CashOut, ApiTimeout, apiTimeoutMsg))
	//IBC沙巴体育/电竞-调整 此方法支持推广活动或任何会影响玩家钱包余额。
	ibc.POST("/adjustbalance", fasthttp.TimeoutHandler(ibcCtl.AdjustBalance, ApiTimeout, apiTimeoutMsg))

	return ibc
}

func PBRouter() *router.Router {

	pb := router.New()
	pb.PanicHandler = apiServerPanic

	return pb
}

func VivoRouter() *router.Router {

	vivo := router.New()
	vivo.PanicHandler = apiServerPanic
	//Authenticate
	vivo.GET("/Authenticate", fasthttp.TimeoutHandler(vivoCtl.Authenticate, ApiTimeout, apiTimeoutMsg))
	//ChangeBalance
	vivo.GET("/ChangeBalance", fasthttp.TimeoutHandler(vivoCtl.ChangeBalance, ApiTimeout, apiTimeoutMsg))
	//Status
	vivo.GET("/Status", fasthttp.TimeoutHandler(vivoCtl.Status, ApiTimeout, apiTimeoutMsg))
	//GetBalance
	vivo.GET("/GetBalance", fasthttp.TimeoutHandler(vivoCtl.GetBalance, ApiTimeout, apiTimeoutMsg))
	return vivo
}

func OneRouter() *router.Router {

	one := router.New()
	one.PanicHandler = apiServerPanic
	//余额
	one.POST("/wallet/balance", fasthttp.TimeoutHandler(oneCtl.Balance, ApiTimeout, apiTimeoutMsg))
	//投注
	one.POST("/wallet/bet", fasthttp.TimeoutHandler(oneCtl.Bet, ApiTimeout, apiTimeoutMsg))
	//结算
	one.POST("/wallet/bet_result", fasthttp.TimeoutHandler(oneCtl.BetResult, ApiTimeout, apiTimeoutMsg))
	//回滚
	one.POST("/wallet/rollback", fasthttp.TimeoutHandler(oneCtl.Rollback, ApiTimeout, apiTimeoutMsg))
	return one
}

func JDBRouter() *router.Router {
	jdb := router.New()
	jdb.PanicHandler = apiServerPanic
	//JDB的回调
	jdb.POST("/", fasthttp.TimeoutHandler(jdbCtl.Spribe, ApiTimeout, apiTimeoutMsg))
	return jdb
}
