package controller

import (
	"fmt"
	"game/model"
	"github.com/valyala/fasthttp"
)

type EVOController struct{}

func (that *EVOController) SID(ctx *fasthttp.RequestCtx) {

	body := ctx.PostBody()
	fmt.Println("EVO Sid body = ", string(body))

	res := model.EVOSid(body)
	ctx.SetContentType("application/json")
	if res.Status == "OK" {
		model.JsonResponse("EVO", ctx, res)
	} else {
		errRes := model.EvoErrResponse{
			Status: res.Status,
			Uuid:   res.Uuid,
		}
		model.JsonResponse("EVO", ctx, errRes)
	}
}

func (that *EVOController) Check(ctx *fasthttp.RequestCtx) {

	body := ctx.PostBody()
	fmt.Println("EVO Check body = ", string(body))

	token := string(ctx.QueryArgs().Peek("authToken"))

	fmt.Println("token:" + token)
	res := model.EVOCheck(token)
	if res.Status == "OK" {
		resp := model.EVOCheckUser(body)
		ctx.SetContentType("application/json")
		model.JsonResponse("EVO", ctx, resp)
	} else {

		ctx.SetContentType("application/json")
		model.JsonResponse("EVO", ctx, res)
	}
}

func (that *EVOController) Balance(ctx *fasthttp.RequestCtx) {

	body := ctx.PostBody()
	fmt.Println("EVO Balance body = ", string(body))

	token := string(ctx.QueryArgs().Peek("authToken"))
	res := model.EVOCheck(token)
	if res.Status == "OK" {
		res = model.EVOBalance(body)
	}
	ctx.SetContentType("application/json")
	if res.Status == "OK" {
		model.JsonResponse("EVO", ctx, res)
	} else {
		errRes := model.EvoErrResponse{
			Status: res.Status,
			Uuid:   res.Uuid,
		}
		model.JsonResponse("EVO", ctx, errRes)
	}

}

func (that *EVOController) Debit(ctx *fasthttp.RequestCtx) {

	body := ctx.PostBody()
	fmt.Println("EVO Debit body = ", string(body))
	token := string(ctx.QueryArgs().Peek("authToken"))
	res := model.EVOCheck(token)
	if res.Status == "OK" {
		res = model.EVODebit(body, ctx.Time())
	}
	ctx.SetContentType("application/json")
	if res.Status == "OK" {
		model.JsonResponse("EVO", ctx, res)
	} else {
		errRes := model.EvoErrResponse{
			Status: res.Status,
			Uuid:   res.Uuid,
		}
		model.JsonResponse("EVO", ctx, errRes)
	}
}

func (that *EVOController) Credit(ctx *fasthttp.RequestCtx) {

	body := ctx.PostBody()
	fmt.Println("EVO Credit body = ", string(body))

	token := string(ctx.QueryArgs().Peek("authToken"))
	res := model.EVOCheck(token)
	if res.Status == "OK" {
		res = model.EVOCredit(body, ctx.Time())
	}
	ctx.SetContentType("application/json")
	if res.Status == "OK" {
		model.JsonResponse("EVO", ctx, res)
	} else {
		errRes := model.EvoErrResponse{
			Status: res.Status,
			Uuid:   res.Uuid,
		}
		model.JsonResponse("EVO", ctx, errRes)
	}
}

func (that *EVOController) Cancel(ctx *fasthttp.RequestCtx) {

	body := ctx.PostBody()
	fmt.Println("EVO Bonus body = ", string(body))

	token := string(ctx.QueryArgs().Peek("authToken"))
	res := model.EVOCheck(token)
	if res.Status == "OK" {
		res = model.EVOCancel(body, ctx.Time())
	}

	ctx.SetContentType("application/json")
	if res.Status == "OK" {
		model.JsonResponse("EVO", ctx, res)
	} else {
		errRes := model.EvoErrResponse{
			Status: res.Status,
			Uuid:   res.Uuid,
		}
		model.JsonResponse("EVO", ctx, errRes)
	}
}

func (that *EVOController) Promo(ctx *fasthttp.RequestCtx) {

	body := ctx.PostBody()
	fmt.Println("EVO Bonus body = ", string(body))

	token := string(ctx.QueryArgs().Peek("authToken"))
	res := model.EVOCheck(token)
	if res.Status == "OK" {
		res = model.EVOBonus(body, ctx.Time())
	}

	ctx.SetContentType("application/json")
	if res.Status == "OK" {
		model.JsonResponse("EVO", ctx, res)
	} else {
		errRes := model.EvoErrResponse{
			Status: res.Status,
			Uuid:   res.Uuid,
		}
		model.JsonResponse("EVO", ctx, errRes)
	}
}
