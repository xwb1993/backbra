package controller

import (
	"fmt"
	"game/model"
	"github.com/valyala/fasthttp"
)

type VIVOController struct{}

func (that *VIVOController) Authenticate(ctx *fasthttp.RequestCtx) {
	fmt.Println("Authenticate", ctx.QueryArgs().String())
	model.XMLResponse("VIVO", ctx, model.VIVOAuthenticate(ctx))
}

func (that *VIVOController) ChangeBalance(ctx *fasthttp.RequestCtx) {
	fmt.Println("ChangeBalance", ctx.QueryArgs().String())
	model.XMLResponse("VIVO", ctx, model.VIVOChangeBalance(ctx))
}

func (that *VIVOController) Status(ctx *fasthttp.RequestCtx) {
	fmt.Println("Status", ctx.QueryArgs().String())
	model.XMLResponse("VIVO", ctx, model.VIVOStatus(ctx))
}

func (that *VIVOController) GetBalance(ctx *fasthttp.RequestCtx) {
	fmt.Println("GetBalance", ctx.QueryArgs().String())
	model.XMLResponse("VIVO", ctx, model.VIVOGetBalance(ctx))
}
