package controller

import (
	"fmt"
	"game/contrib/helper"
	"game/model"
	"strconv"

	"github.com/valyala/fasthttp"
)

type WalletController struct{}

func (that *WalletController) Launch(ctx *fasthttp.RequestCtx) {

	param := map[string]string{}
	pid := string(ctx.QueryArgs().Peek("pid"))
	code := string(ctx.QueryArgs().Peek("code"))
	d := string(ctx.Request.Header.Peek("d"))
	i, err := strconv.Atoi(d)
	if err != nil {
		helper.Print(ctx, false, helper.DeviceTypeErr)
		return
	}

	if _, ok := model.Devices[i]; !ok {
		helper.Print(ctx, false, helper.DeviceTypeErr)
		return
	}

	mb, err := model.MemberCache(ctx, "")
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	if mb.Tester == 0 {
		helper.Print(ctx, false, helper.UserNotExist)
		return
	}

	if mb.State == 3 {
		helper.Print(ctx, false, helper.Blocked)
		return
	}

	platF, err := model.PlatformRedis(pid)
	if err != nil {
		helper.Print(ctx, false, helper.PlatIDErr)
		return
	}
	if platF.Maintained == 2 {
		helper.Print(ctx, false, helper.PlatformMaintain)
		return
	}
	//场馆原始id
	param["pid"] = pid //三方场馆的ID
	param["uid"] = mb.Uid
	param["puid"] = mb.Pid
	param["deviceType"] = model.Devices[i]
	param["tester"] = fmt.Sprintf("%d", mb.Tester) // 0 正式用户  1 测试用户
	param["gamecode"] = code
	param["backurl"] = ""
	param["username"] = mb.Username
	param["id"] = helper.GenId()
	param["ip"] = helper.FromRequest(ctx)
	ms := ctx.Time().UnixMilli()
	param["ms"] = fmt.Sprintf("%d", ms)
	param["s"] = fmt.Sprintf("%d", ctx.Time().Unix())
	param["token"] = string(ctx.Request.Header.Peek("t"))

	game, ok := model.Plats[pid]
	if !ok {
		helper.Print(ctx, false, helper.PlatIDErr)
		return
	}

	//读取etcd的配置信息
	plat, err := model.PlatIsExists(mb.Username, pid)
	if err != nil {
		fmt.Println("Launch v2", err.Error())
		helper.Print(ctx, false, err.Error())
		return
	}

	if plat.Username == "" {
		param["password"] = model.RandStr(6)

		err := game.Reg(param)
		if err != nil {
			//注册失败
			helper.Print(ctx, false, err.Error())
			return
		}

		//平台注册成功后，需要新增到数据
		_, err = model.MemberPlatformInsert(param)
		if err != nil {
			//注册失败
			helper.Print(ctx, false, err.Error())
			return
		}
	} else {
		param["password"] = plat.Password
	}

	content, err := game.Launch(param)
	if err != nil {
		//登录失败
		helper.Print(ctx, false, content)
		return
	}

	helper.Print(ctx, true, content)
}
