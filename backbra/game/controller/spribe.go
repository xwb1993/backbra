package controller

import (
	"game/model"
	"github.com/valyala/fasthttp"
)

type JDBController struct{}

func (that *JDBController) Spribe(ctx *fasthttp.RequestCtx) {
	rsp, body := model.Jdb(ctx)
	model.JsonJDBResponse("JDB", ctx, rsp, body)
}
