package main

import (
	"fmt"
	"game/contrib/apollo"
	"game/contrib/conn"
	"game/contrib/helper"
	"game/contrib/session"
	"game/model"
	"game/pkg"
	"game/router"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/BurntSushi/toml"
	"github.com/valyala/fasthttp"
	_ "go.uber.org/automaxprocs"
)

type HostSwitch map[string]fasthttp.RequestHandler

// CheckHost Implement a CheckHost method on our new type
func (hs HostSwitch) CheckHost(ctx *fasthttp.RequestCtx) {

	var (
		path = string(ctx.Path())
		host = string(ctx.Host())
	)
	fmt.Println("host: ", host)
	fmt.Println("path: ", path)
	if path == "/game/launch" {
		data, err := session.Get(ctx)
		if err != nil {
			helper.Print(ctx, false, helper.AccessTokenExpires)
			return
		}

		ctx.SetUserValue("token", data)
	}

	if handler := hs[host]; handler != nil {
		handler(ctx)
	} else {
		handler = hs["default"]
		handler(ctx)
	}
}

func main() {

	var (
		err error
	)
	argc := len(os.Args)
	if argc != 4 {
		fmt.Printf("%s <etcds> <cfgPath> <proxy> \r\n", os.Args[0])
		return
	}

	fmt.Printf("proxy : %s\n", os.Args[3])

	cfg := pkg.Conf{}
	endpoints := strings.Split(os.Args[1], ",")

	mt := new(model.MetaTable)
	apollo.New(endpoints, pkg.ETCDName, pkg.ETCDPass)
	defer apollo.Close()
	//err = apollo.ParseTomlStruct(os.Args[2], &cfg)
	_, err = toml.DecodeFile(os.Args[2], &cfg)
	if err != nil {
		fmt.Printf("ParseTomlStruct error: %s", err.Error())
		return
	}

	platPath := "./platform.toml"
	if strings.HasPrefix(os.Args[2], fmt.Sprintf("/%s/", cfg.Prefix)) {
		platPath = fmt.Sprintf("/%s%s", cfg.Prefix, platPath)
	}
	//err = apollo.ParseTomlStruct(platPath, &mt.PlatCfg)
	_, err = toml.DecodeFile(platPath, &mt.PlatCfg)
	if err != nil {
		log.Fatalf("Error in apollo.ParseText /platform.toml: %s", err)
	}

	model.New(os.Args[3])

	mt.IsDev = cfg.Dev
	mt.Prefix = cfg.Prefix
	mt.WebURL = cfg.WebURL
	//br1.toml文件里没发现 H5URL 这个参数
	//mt.H5URL = cfg.H5URL
	mt.MerchantBean = conn.BeanNew(cfg.Beanstalkd)
	mt.MerchantDB = conn.InitDB(cfg.Db.Game, cfg.Db.MaxIdleConn, cfg.Db.MaxOpenConn)
	//mt.MerchantRedis = conn.InitRedisSentinel(cfg.Redis.Addr, cfg.Redis.Password, cfg.Redis.Sentinel, cfg.Redis.Db)
	mt.MerchantRedis = conn.InitRedis(cfg.Redis.Addr[0], cfg.Redis.Password, 0)
	mt.Program = filepath.Base(os.Args[0])
	model.Constructor(mt)

	defer func() {
		model.Close()
		mt = nil
	}()

	b := router.BuildInfo{
		GitReversion:   pkg.GitReversion,
		BuildTime:      pkg.BuildTime,
		BuildGoVersion: pkg.BuildGoVersion,
	}

	hs := make(HostSwitch)
	def := router.DefaultRouter(b)
	pp := router.PPRouter()
	ppHost := "pp." + cfg.CallbackDomain
	hs[ppHost] = pp.Handler

	pg := router.PGRouter()
	pgHost := "pg." + cfg.CallbackDomain
	hs[pgHost] = pg.Handler

	one := router.OneRouter()
	oneHost := "one." + cfg.CallbackDomain
	hs[oneHost] = one.Handler

	evo := router.EvoRouter()
	evoHost := "evo." + cfg.CallbackDomain
	hs[evoHost] = evo.Handler

	jili := router.JILIRouter()
	jiliHost := "jili." + cfg.CallbackDomain
	hs[jiliHost] = jili.Handler

	ibc := router.IBCRouter()
	ibcHost := "saba." + cfg.CallbackDomain
	hs[ibcHost] = ibc.Handler

	pb := router.PBRouter()
	pbHost := "pb." + cfg.CallbackDomain
	hs[pbHost] = pb.Handler

	spribe := router.JDBRouter()
	spribeHost := "spribe." + cfg.CallbackDomain
	hs[spribeHost] = spribe.Handler

	vivo := router.VivoRouter()
	vivoHost := "vivo." + cfg.CallbackDomain
	hs[vivoHost] = vivo.Handler

	viver := router.OneRouter()
	viverHost := "viver." + cfg.CallbackDomain
	hs[viverHost] = viver.Handler

	// bti router
	btiBalance := router.BTIBalanceRouter()
	btiValidateToken := router.BTIValidateTokenRouter()
	btiReserve := router.BTIReserveRouter()
	btiDebitReserve := router.BTIDebitReserveRouter()
	btiCommit := router.BTICommitRouter()
	btiCancel := router.BTICancelRouter()
	btiCredit := router.BTICreditRouter()
	btiDebitCustomer := router.BTIDebitCustomerRouter()
	// bti hosts
	var (
		btiBalanceHost        string
		btiValidateHost       string
		btiReserveHost        string
		btiDebitReserveHost   string
		btiCommitReserveHost  string
		btiCancelReserveHost  string
		btiCreditCustomerHost string
		btiDebitCustomerHost  string
	)

	btiBalanceHost = "bti." + cfg.CallbackDomain
	btiValidateHost = "bti-validate." + cfg.CallbackDomain
	btiReserveHost = "bti-reserve." + cfg.CallbackDomain
	btiDebitReserveHost = "bti-debitreserve." + cfg.CallbackDomain
	btiCommitReserveHost = "bti-commitreserve." + cfg.CallbackDomain
	btiCancelReserveHost = "bti-cancelreserve." + cfg.CallbackDomain
	btiCreditCustomerHost = "bti-creditcustomer." + cfg.CallbackDomain
	btiDebitCustomerHost = "bti-debitcustomer." + cfg.CallbackDomain

	// bti handler
	hs[btiBalanceHost] = btiBalance.Handler
	hs[btiValidateHost] = btiValidateToken.Handler
	hs[btiReserveHost] = btiReserve.Handler
	hs[btiDebitReserveHost] = btiDebitReserve.Handler
	hs[btiCommitReserveHost] = btiCommit.Handler
	hs[btiCancelReserveHost] = btiCancel.Handler
	hs[btiCreditCustomerHost] = btiCredit.Handler
	hs[btiDebitCustomerHost] = btiDebitCustomer.Handler

	hs["default"] = def.Handler
	fmt.Printf("gitReversion = %s\r\nbuildGoVersion = %s\r\nbuildTime = %s\r\n", pkg.GitReversion, pkg.BuildGoVersion, pkg.BuildTime)
	fmt.Println("game running ", cfg.Port.Game)

	// 启动小飞机推送版本信息
	//if !cfg.Dev {
	//	model.TelegramBotNotice(mt.Program, gitReversion, buildTime, buildGoVersion, "api", cfg.Prefix)
	//}

	log.Fatal(fasthttp.ListenAndServe(cfg.Port.Game, hs.CheckHost))
	/*
		if err := srv.ListenAndServe(cfg.Port.Game); err != nil {
			log.Fatalf("Error in ListenAndServe: %s", err)
		}
	*/
}
