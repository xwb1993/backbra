package model

import (
	"fmt"
	"game/contrib/conn"
	"game/contrib/helper"
	g "github.com/doug-martin/goqu/v9"
	"github.com/valyala/fasthttp"
	"testing"
)

/*
{
    "traceId":"673aa8d9-8da5-4fa3-b88d-132c8b223ca6",
    "status":"SC_OK",
    "message":"Successful response.",
    "data":[
        {
            "name":"Pragmatic Play",
            "code":"PP",
            "categoryCode":"SLOTS"
        },
        {
            "name":"PG Soft",
            "code":"PGS",
            "categoryCode":"SLOTS"
        },
        {
            "name":"Jili",
            "code":"JL",
            "categoryCode":"SLOTS"
        }
    ]
}
*/

func TestOneGameVendors(t *testing.T) {

	apiKey := "eac54269185f6b26d987ea4c278ba3bd65fc1973ac502eef82eb6c05c17982bc"
	apiSecret := "e8cde5085cf40b86a3939702efd50c1970c863c397d46b22cfdb6e4b4b592612"
	New("0.0.0.0")
	req := map[string]string{
		"traceId":         UUID(),
		"displayLanguage": "pt",
	}
	requestBody, _ := helper.JsonMarshal(req)
	headers := map[string]string{
		"Content-Type": "application/json",
		"X-API-Key":    apiKey,
		"X-Signature":  HmacSha256(string(requestBody), apiSecret),
	}
	requestURI := "https://stg.gasea168.com/game/vendors"
	statusCode, body, err := httpPost(requestBody, requestURI, headers)
	fmt.Println(requestURI)
	fmt.Println(string(requestBody))
	fmt.Println(headers)
	if err != nil {
		t.Error(err)
	}

	if statusCode != fasthttp.StatusOK {
		t.Error(statusCode)
	}

	t.Log(string(body))
}

func TestOneGameLists(t *testing.T) {

	apiKey := "eac54269185f6b26d987ea4c278ba3bd65fc1973ac502eef82eb6c05c17982bc"
	apiSecret := "e8cde5085cf40b86a3939702efd50c1970c863c397d46b22cfdb6e4b4b592612"
	New("0.0.0.0")
	req := map[string]string{
		"traceId":         UUID(),
		"vendorCode":      "PGS",
		"pageNo":          "1",
		"pageSize":        "500",
		"displayLanguage": "pt",
	}
	requestBody, _ := helper.JsonMarshal(req)
	headers := map[string]string{
		"Content-Type": "application/json",
		"X-API-Key":    apiKey,
		"X-Signature":  HmacSha256(string(requestBody), apiSecret),
	}
	requestURI := "https://stg.gasea168.com/game/list"
	statusCode, body, err := httpPost(requestBody, requestURI, headers)
	fmt.Println(requestURI)
	fmt.Println(string(requestBody))
	fmt.Println(headers)
	if err != nil {
		t.Error(err)
	}

	if statusCode != fasthttp.StatusOK {
		t.Error(statusCode)
	}

	t.Log(string(body))
}

func TestOneGenGameLists(t *testing.T) {

	db := conn.InitDB("root:SVAdwsbB@@agw2WG@tcp(23.234.28.245:13309)/lb8888?charset=utf8&parseTime=True&loc=Local", 10, 10)
	var data []Game
	ex := g.Ex{
		"platform_id": helper.PG,
	}
	query, _, _ := dialect.From("tbl_game_lists").Select(colGame...).Where(ex).ToSQL()
	fmt.Println(query)
	err := db.Select(&data, query)
	if err != nil {
		t.Error(err)
	}

	for k := range data {
		data[k].ID = helper.GenId()
		data[k].PlatformID = helper.OnePG
		data[k].GameID = "PGS_" + data[k].GameID
		query, _, _ = dialect.Insert("tbl_game_lists").Rows(&data[k]).ToSQL()
		fmt.Println(query)
		_, err = db.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("%s,[%s]", err.Error(), query))
		}
	}
}
