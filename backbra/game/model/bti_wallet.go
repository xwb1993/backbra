package model

import (
	"database/sql"
	"fmt"
	"game/contrib/helper"
	"game/contrib/validator"
	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"
	"github.com/valyala/fasthttp"
	"strings"
	"time"
)

type BTIBalanceRsp struct {
	Status  string `json:"status"`
	Token   string `json:"token"`
	Message string `json:"message"`
	Balance string `json:"balance"`
}

type BTIValidateTokenRsp struct {
	ErrorCode    int
	ErrorMessage string
	CustId       string
	CustLogin    string
	Balance      string
	City         string
	Country      string
	CurrencyCode string
}

type BTIRsp struct {
	ErrorCode    int
	ErrorMessage string
	Balance      string
	TrxID        string
}

type plat_bti_t struct{}

func (plat_bti_t) Reg(args map[string]string) error {
	return nil
}

func (plat_bti_t) Launch(args map[string]string) (string, error) {
	return fmt.Sprintf("%s?operatorToken=%s", meta.PlatCfg.BTI.API, args["puid"]), nil
}

func BTIBalance(ctx *fasthttp.RequestCtx) BTIBalanceRsp {

	fmt.Println(ctx.QueryArgs().String())
	token := string(ctx.QueryArgs().Peek("token"))
	rsp := BTIBalanceRsp{
		Status:  "failure",
		Balance: "0",
		Message: "",
		Token:   token,
	}
	if token == "" {
		rsp.Message = "fail"
		return rsp
	}
	mb, err := GetMemberByPUID(token)
	if err != nil {
		rsp.Message = "fail"
		return rsp
	}

	balance, _ := getBalance(mb, 2)
	rsp.Status = "success"
	rsp.Balance = balance.String()
	return rsp
}

// token s9Y9Y7Adfx6ePLqdnhx+Ow==
func BTIValidateToken(ctx *fasthttp.RequestCtx) BTIValidateTokenRsp {

	fmt.Println(ctx.QueryArgs().String())
	authToken := string(ctx.QueryArgs().Peek("auth_token"))
	rsp := BTIValidateTokenRsp{
		Balance:      "0",
		ErrorCode:    0,
		ErrorMessage: "No error",
		CurrencyCode: "BRL",
	}
	if !validator.CtypeDigit(authToken) {
		rsp.ErrorCode = -1
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	mb, err := GetMemberByPUID(authToken)
	if err != nil {
		rsp.ErrorCode = -2
		rsp.ErrorMessage = "Member Not exist"
		return rsp
	}

	balance, _ := getBalance(mb, 2)
	rsp.CustId = meta.Prefix + mb.Pid
	rsp.CustLogin = meta.Prefix + mb.Pid
	rsp.Balance = balance.String()
	rsp.City = "Brasília"
	rsp.Country = "BR" //ISO 3166-1
	return rsp
}

func BTIReserve(ctx *fasthttp.RequestCtx) BTIRsp {

	fmt.Println(ctx.QueryArgs().String())
	body := ctx.PostBody()
	//agentID := string(ctx.QueryArgs().Peek("agent_id"))
	//customerID := string(ctx.QueryArgs().Peek("customer_id"))
	custID := string(ctx.QueryArgs().Peek("cust_id"))
	reserveID := string(ctx.QueryArgs().Peek("reserve_id"))
	sAmount := string(ctx.QueryArgs().Peek("amount"))

	rsp := BTIRsp{
		TrxID:        helper.GenId(),
		Balance:      "0",
		ErrorCode:    0,
		ErrorMessage: "No error",
	}
	//if agentID != meta.PlatCfg.BTI.AgentID || customerID != meta.PlatCfg.BTI.CustomerID {
	//	rsp.ErrorCode = -1
	//	rsp.ErrorMessage = "Generic Error"
	//	return rsp
	//}

	mb, err := GetMemberByPUID(custID)
	if err != nil {
		rsp.ErrorCode = -2
		rsp.ErrorMessage = "Member Not exist"
		return rsp
	}

	err = BetLock(mb.Username)
	if err != nil {
		rsp.ErrorCode = -1
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	defer BetUnlock(mb.Username)

	balance, _ := getBalance(mb, 2)
	rsp.Balance = balance.String()
	req := BTIReserveReq{}
	err = helper.XMLUnmarshal(body, &req)
	if err != nil {
		_ = pushError(err, helper.FormatErr)
		rsp.ErrorCode = -1
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	ex := g.Ex{
		"bill_no":   reserveID,
		"cash_type": helper.TransactionReserve,
	}
	trans, err := walletTransactionFindOne(ex)
	if err != nil && err != sql.ErrNoRows {
		rsp.ErrorCode = -1
		rsp.ErrorMessage = "Generic Error"
		return rsp
	} else if err == nil { //重复请求
		rsp.TrxID = trans.ID
		rsp.Balance = balance.String()
		return rsp
	}

	amount, _ := decimal.NewFromString(sAmount)
	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		rsp.ErrorCode = -1
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	trxID, err := walletReserveTransaction(tx, mb, reserveID, "", helper.BTI, balance, amount, ctx.Time().UnixMilli())
	if err != nil {
		if strings.HasPrefix(err.Error(), "Error 1062") {
			rsp.TrxID = trans.ID
			rsp.Balance = balance.String()
			return rsp
		}

		rsp.ErrorCode = -1
		rsp.ErrorMessage = "Generic Error"
		if err.Error() == helper.BalanceErr {
			rsp.ErrorCode = -4
			rsp.ErrorMessage = "Insufficient Amount"
		}

		return rsp
	}

	rsp.TrxID = trxID
	err = tx.Commit()
	if err != nil {
		rsp.ErrorCode = -1
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	balance, _ = getBalance(mb, 2)
	rsp.Balance = balance.String()
	return rsp
}

func BTIDebitReserve(ctx *fasthttp.RequestCtx) BTIRsp {

	fmt.Println(ctx.QueryArgs().String())
	body := ctx.PostBody()
	fmt.Println(string(body))
	//agentID := string(ctx.QueryArgs().Peek("agent_id"))
	//customerID := string(ctx.QueryArgs().Peek("customer_id"))
	custID := string(ctx.QueryArgs().Peek("cust_id"))
	reserveID := string(ctx.QueryArgs().Peek("reserve_id"))
	reqID := string(ctx.QueryArgs().Peek("req_id"))
	purchaseID := string(ctx.QueryArgs().Peek("purchase_id"))
	sAmount := string(ctx.QueryArgs().Peek("amount"))

	rsp := BTIRsp{
		TrxID:        helper.GenId(),
		Balance:      "0",
		ErrorCode:    0,
		ErrorMessage: "No error",
	}

	//if agentID != meta.PlatCfg.BTI.AgentID || customerID != meta.PlatCfg.BTI.CustomerID {
	//	rsp.ErrorCode = -1
	//	rsp.ErrorMessage = "Generic Error"
	//	return rsp
	//}

	mb, err := GetMemberByPUID(custID)
	if err != nil {
		rsp.ErrorMessage = "Member Not exist"
		return rsp
	}

	ex := g.Ex{
		"bill_no":   reserveID,
		"cash_type": helper.TransactionReserve,
	}
	reserve, err := walletTransactionFindOne(ex)
	if err != nil && err != sql.ErrNoRows {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	if err == sql.ErrNoRows {
		rsp.ErrorMessage = "Reserve ID Error"
		return rsp
	}

	balance, _ := decimal.NewFromString(reserve.AfterAmount)
	rsp.Balance = balance.String()
	req := BTIDebitReserveReq{}
	err = helper.XMLUnmarshal(body, &req)
	if err != nil {
		_ = pushError(err, helper.FormatErr)
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	ex = g.Ex{
		"bill_no":      reserveID,
		"operation_no": purchaseID,
		"cash_type":    helper.TransactionDebitReserve,
	}
	trans, err := walletTransactionFindOne(ex)
	if err != nil && err != sql.ErrNoRows {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	} else if err == nil { //重复请求
		rsp.TrxID = trans.ID
		return rsp
	}

	amount, _ := decimal.NewFromString(sAmount)
	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	trxID, err := walletDebitReserveTransaction(tx, mb, purchaseID, reqID, helper.BTI, reserveID, balance, amount, ctx.Time().UnixMilli())
	if err != nil {
		if strings.HasPrefix(err.Error(), "Error 1062") {
			return rsp
		}

		rsp.ErrorMessage = "Generic Error"
		if err.Error() == helper.BalanceErr {
			rsp.ErrorMessage = "Insufficient Amount"
		}

		return rsp
	}

	rowID := "bti" + purchaseID
	ts := time.Now()
	result := fmt.Sprintf("BetTypeName : %s,BranchName : %s,LeagueName : %s,HomeTeam : %s,AwayTeam : %s,YourBet : %s,OddsDec : %s",
		req.Bet.BetTypeName, req.Bet.BranchName, req.Bet.LeagueName, req.Bet.HomeTeam, req.Bet.AwayTeam, req.Bet.YourBet, req.Bet.OddsDec)
	odds, _ := decimal.NewFromString(req.Bet.OddsDec)
	gm := GameRecord{
		BillNo:         purchaseID,
		Result:         result,
		GameName:       req.Bet.BranchName,
		GameCode:       req.Bet.LeagueID,
		PlayType:       req.Bet.LeagueID,
		RowId:          rowID,
		ApiBillNo:      purchaseID,
		MainBillNo:     purchaseID,
		HandicapType:   req.Bet.BetTypeName,
		Handicap:       req.Bet.BranchName,
		StartTime:      0,
		Resettle:       0,
		Presettle:      0,
		ParentName:     mb.ParentName,
		ParentUid:      mb.ParentID,
		TopName:        mb.TopName,
		TopUid:         mb.TopID,
		Uid:            mb.Uid,
		GrandName:      mb.GrandName,
		GrandID:        mb.GrandID,
		GreatGrandName: mb.GreatGrandName,
		GreatGrandID:   mb.GreatGrandID,
		Name:           mb.Username,
		Prefix:         meta.Prefix,
		CreatedAt:      ts.UnixMilli(),
		UpdatedAt:      ts.UnixMilli(),
		PlayerName:     mb.Username,
		GameType:       gameTypeSport,
		ApiType:        helper.BTI,
		ApiName:        "Esporte BTI",
	}
	gm.Odds, _ = odds.Float64()
	gm.BetAmount, _ = amount.Float64()
	gm.Flag = 3
	gm.BetTime = ts.UnixMilli()
	query, _, _ := dialect.Insert("tbl_game_record").Rows(&gm).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	rsp.TrxID = trxID
	err = tx.Commit()
	if err != nil {
		rsp.ErrorMessage = "Generic Error"
	}

	return rsp
}

func BTICancelReserve(ctx *fasthttp.RequestCtx) BTIRsp {

	fmt.Println(ctx.QueryArgs().String())
	body := ctx.PostBody()
	fmt.Println(string(body))
	//agentID := string(ctx.QueryArgs().Peek("agent_id"))
	//customerID := string(ctx.QueryArgs().Peek("customer_id"))
	custID := string(ctx.QueryArgs().Peek("cust_id"))
	reserveID := string(ctx.QueryArgs().Peek("reserve_id"))

	rsp := BTIRsp{
		TrxID:        helper.GenId(),
		Balance:      "0",
		ErrorCode:    0,
		ErrorMessage: "No error",
	}

	//if agentID != meta.PlatCfg.BTI.AgentID || customerID != meta.PlatCfg.BTI.CustomerID {
	//	rsp.ErrorCode = -1
	//	rsp.ErrorMessage = "Generic Error"
	//	return rsp
	//}

	mb, err := GetMemberByPUID(custID)
	if err != nil {
		rsp.ErrorMessage = "Member Not exist"
		return rsp
	}

	err = BetLock(mb.Username)
	if err != nil {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	defer BetUnlock(mb.Username)

	balance, _ := getBalance(mb, 2)
	rsp.Balance = balance.String()
	ex := g.Ex{
		"bill_no":   reserveID,
		"cash_type": helper.TransactionReserve,
	}
	trans, err := walletTransactionFindOne(ex)
	if err != nil && err != sql.ErrNoRows {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	if err == sql.ErrNoRows {
		rsp.ErrorMessage = "Reserve ID Error"
		return rsp
	}

	ex = g.Ex{
		"bill_no":   reserveID,
		"cash_type": helper.TransactionCancelReserve,
	}
	_, err = walletTransactionFindOne(ex)
	if err != nil && err != sql.ErrNoRows {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	} else if err == nil { //重复请求
		return rsp
	}

	amount, _ := decimal.NewFromString(trans.Amount)
	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	_, err = walletCancelReserveTransaction(tx, mb, reserveID, "", helper.BTI, balance, amount, ctx.Time().UnixMilli())
	if err != nil {
		if strings.HasPrefix(err.Error(), "Error 1062") {
			return rsp
		}

		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	err = tx.Commit()
	if err != nil {
		rsp.ErrorMessage = "Generic Error"
	}

	balance, _ = getBalance(mb, 2)
	rsp.Balance = balance.String()
	return rsp
}

func BTICommitReserve(ctx *fasthttp.RequestCtx) BTIRsp {

	fmt.Println(ctx.QueryArgs().String())
	body := ctx.PostBody()
	fmt.Println(string(body))
	//agentID := string(ctx.QueryArgs().Peek("agent_id"))
	//customerID := string(ctx.QueryArgs().Peek("customer_id"))
	custID := string(ctx.QueryArgs().Peek("cust_id"))
	reserveID := string(ctx.QueryArgs().Peek("reserve_id"))
	purchaseID := string(ctx.QueryArgs().Peek("purchase_id"))

	rsp := BTIRsp{
		TrxID:        helper.GenId(),
		Balance:      "0",
		ErrorCode:    0,
		ErrorMessage: "No error",
	}

	//if agentID != meta.PlatCfg.BTI.AgentID || customerID != meta.PlatCfg.BTI.CustomerID {
	//	rsp.ErrorCode = -1
	//	rsp.ErrorMessage = "Generic Error"
	//	return rsp
	//}

	mb, err := GetMemberByPUID(custID)
	if err != nil {
		rsp.ErrorMessage = "Member Not exist"
		return rsp
	}

	ex := g.Ex{
		"bill_no":   reserveID,
		"cash_type": helper.TransactionReserve,
	}
	trx, err := walletTransactionFindOne(ex)
	if err != nil {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	var amount sql.NullFloat64
	ex = g.Ex{
		"remark":    reserveID,
		"cash_type": helper.TransactionDebitReserve,
	}
	query, _, _ := dialect.From("tbl_balance_transaction").Select(g.SUM("amount")).Where(ex).Limit(1).ToSQL()
	fmt.Println(query)
	err = meta.MerchantDB.Get(&amount, query)
	if err != nil {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	// 预扣除金额
	reserveAmount, _ := decimal.NewFromString(trx.Amount)
	// 实际投注金额
	betAmount := decimal.NewFromFloat(amount.Float64)
	err = BetLock(mb.Username)
	if err != nil {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	defer BetUnlock(mb.Username)

	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	ts := ctx.Time()
	balance, _ := getBalance(mb, 2)
	rsp.Balance = balance.String()
	rsp.TrxID, err = walletCommitReserveTransaction(tx, mb, balance, reserveAmount, betAmount, helper.BTI, reserveID, ts.UnixMilli())
	if err != nil {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	rowID := "bti" + purchaseID
	gm := g.Record{
		"flag":       "0",
		"updated_at": ts.UnixMilli(),
	}
	query, _, _ = dialect.Update("tbl_game_record").Set(gm).Where(g.Ex{"row_id": rowID}).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	err = tx.Commit()
	if err != nil {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	balance, _ = getBalance(mb, 2)
	rsp.Balance = balance.String()
	return rsp
}

func BTICreditCustomer(ctx *fasthttp.RequestCtx) BTIRsp {

	fmt.Println(ctx.QueryArgs().String())
	body := ctx.PostBody()
	fmt.Println(string(body))
	//agentID := string(ctx.QueryArgs().Peek("agent_id"))
	//customerID := string(ctx.QueryArgs().Peek("customer_id"))
	custID := string(ctx.QueryArgs().Peek("cust_id"))
	reqID := string(ctx.QueryArgs().Peek("req_id"))
	purchaseID := string(ctx.QueryArgs().Peek("purchase_id"))
	sAmount := string(ctx.QueryArgs().Peek("amount"))

	rsp := BTIRsp{
		TrxID:        helper.GenId(),
		Balance:      "0",
		ErrorCode:    0,
		ErrorMessage: "No error",
	}

	//if agentID != meta.PlatCfg.BTI.AgentID || customerID != meta.PlatCfg.BTI.CustomerID {
	//	rsp.ErrorCode = -1
	//	rsp.ErrorMessage = "Generic Error"
	//	return rsp
	//}

	mb, err := GetMemberByPUID(custID)
	if err != nil {
		rsp.ErrorMessage = "Member Not exist"
		return rsp
	}

	err = BetLock(mb.Username)
	if err != nil {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	defer BetUnlock(mb.Username)

	balance, _ := getBalance(mb, 2)
	rsp.Balance = balance.String()
	//req := BTICustomerReq{}
	//err = helper.XMLUnmarshal(body, &req)
	//if err != nil {
	//	_ = pushError(err, helper.FormatErr)
	//	rsp.ErrorMessage = "Generic Error"
	//	return rsp
	//}

	ex := g.Ex{
		"bill_no":   purchaseID,
		"cash_type": helper.TransactionDebitReserve,
	}
	trx, err := walletTransactionFindOne(ex)
	if err != nil && err != sql.ErrNoRows {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	} else if err == sql.ErrNoRows { //订单不存在
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	betAmount, _ := decimal.NewFromString(trx.Amount)
	amount, _ := decimal.NewFromString(sAmount)
	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	ts := ctx.Time()
	rsp.TrxID, err = walletCreditCustomerTransaction(tx, mb, purchaseID, reqID, helper.BTI, trx.Remark, balance, amount.Abs(), ts.UnixMilli())
	if err != nil {
		if strings.HasPrefix(err.Error(), "Error 1062") {
			return rsp
		}

		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	rowID := "bti" + purchaseID
	netAmount := amount.Sub(betAmount)
	validBetAmount := betAmount
	if betAmount.GreaterThan(netAmount.Abs()) {
		validBetAmount = netAmount.Abs()
	}
	gm := g.Record{
		"flag":             "1",
		"valid_bet_amount": validBetAmount.String(),
		"settle_amount":    amount.String(),
		"net_amount":       netAmount.String(),
		"settle_time":      ts.UnixMilli(),
		"updated_at":       ts.UnixMilli(),
	}
	query, _, _ := dialect.Update("tbl_game_record").Set(gm).Where(g.Ex{"row_id": rowID}).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	err = tx.Commit()
	if err != nil {
		rsp.ErrorMessage = "Generic Error"
	}

	balance, _ = getBalance(mb, 2)
	rsp.Balance = balance.String()
	return rsp
}

func BTIDebitCustomer(ctx *fasthttp.RequestCtx) BTIRsp {

	fmt.Println(ctx.QueryArgs().String())
	body := ctx.PostBody()
	fmt.Println(string(body))
	//agentID := string(ctx.QueryArgs().Peek("agent_id"))
	//customerID := string(ctx.QueryArgs().Peek("customer_id"))
	custID := string(ctx.QueryArgs().Peek("cust_id"))
	reqID := string(ctx.QueryArgs().Peek("req_id"))
	purchaseID := string(ctx.QueryArgs().Peek("purchase_id"))
	sAmount := string(ctx.QueryArgs().Peek("amount"))

	rsp := BTIRsp{
		TrxID:        helper.GenId(),
		Balance:      "0",
		ErrorCode:    0,
		ErrorMessage: "No error",
	}

	//if agentID != meta.PlatCfg.BTI.AgentID || customerID != meta.PlatCfg.BTI.CustomerID {
	//	rsp.ErrorCode = -1
	//	rsp.ErrorMessage = "Generic Error"
	//	return rsp
	//}

	mb, err := GetMemberByPUID(custID)
	if err != nil {
		rsp.ErrorMessage = "Member Not exist"
		return rsp
	}

	err = BetLock(mb.Username)
	if err != nil {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	defer BetUnlock(mb.Username)

	balance, _ := getBalance(mb, 2)
	rsp.Balance = balance.String()
	//req := BTICustomerReq{}
	//err = helper.XMLUnmarshal(body, &req)
	//if err != nil {
	//	_ = pushError(err, helper.FormatErr)
	//	rsp.ErrorMessage = "Generic Error"
	//	return rsp
	//}

	ex := g.Ex{
		"bill_no":      purchaseID,
		"operation_no": reqID,
		"cash_type":    helper.TransactionDebitCustomer,
	}
	trx, err := walletTransactionFindOne(ex)
	if err != nil && err != sql.ErrNoRows {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	} else if err == nil { //重复请求
		rsp.TrxID = trx.ID
		rsp.Balance = trx.AfterAmount
		return rsp
	}

	amount, _ := decimal.NewFromString(sAmount)
	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	ts := ctx.Time()
	rsp.TrxID, err = walletDebitCustomerTransaction(tx, mb, purchaseID, reqID, helper.BTI, trx.Remark, balance, amount.Abs(), ts.UnixMilli())
	if err != nil {
		if strings.HasPrefix(err.Error(), "Error 1062") {
			return rsp
		}

		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	rowID := "bti" + purchaseID
	gm := g.Record{
		"net_amount":  g.L(fmt.Sprintf("net_amount-%s", amount.Abs().String())),
		"settle_time": ts.UnixMilli(),
		"updated_at":  ts.UnixMilli(),
	}
	query, _, _ := dialect.Update("tbl_game_record").Set(gm).Where(g.Ex{"row_id": rowID}).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	err = tx.Commit()
	if err != nil {
		rsp.ErrorMessage = "Generic Error"
	}

	balance, _ = getBalance(mb, 2)
	rsp.Balance = balance.String()
	return rsp
}
