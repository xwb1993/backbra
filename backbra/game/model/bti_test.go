package model

import (
	"game/contrib/helper"
	"testing"
)

func TestBTIValidateToken(t *testing.T) {

	key := helper.RandomKey(16)
	t.Log(key)
	authToken, err := DesECBEncrypt([]byte("999998378"), []byte(key))
	if err != nil {
		t.Error(err)
	}

	t.Log(authToken)
}
