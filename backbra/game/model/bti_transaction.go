package model

import (
	"database/sql"
	"errors"
	"fmt"
	"game/contrib/helper"
	ryrpc "game/rpc"
	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"
	"strings"
)

// 中心钱包帐变(预投注) BTI使用
func walletReserveTransaction(tx *sql.Tx, mb ryrpc.TblMemberBase, billNo, operationNo, platformID string, balance, amount decimal.Decimal, createAt int64) (string, error) {

	id := helper.GenId()
	if amount.Cmp(balance) > 0 {
		_ = tx.Rollback()
		return "0", errors.New(helper.BalanceErr)
	}

	afterAmount := balance.Sub(amount)
	// 预投注，投注金额可以为0
	trans := memberTransaction{
		ID:           id,
		UID:          mb.Uid,
		PlatformID:   platformID,
		Username:     mb.Username,
		BillNo:       billNo,
		CashType:     helper.TransactionReserve,
		Amount:       amount.String(),
		BeforeAmount: balance.String(),
		AfterAmount:  afterAmount.String(),
		OperationNo:  operationNo,
		CreatedAt:    createAt,
		Remark:       "reserve",
		Tester:       mb.Tester,
	}
	query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	fmt.Println(query)
	_, err := tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		//记录日志
		if strings.HasPrefix(err.Error(), "Error 1062") {
			PushLog(nil, "", "duplicate", "", 200, nil, err)
		}

		return id, err
	}

	if amount.Cmp(decimal.Zero) > 0 {

		ex := g.Ex{
			"uid": mb.Uid,
		}
		record := g.Record{
			"brl": g.L(fmt.Sprintf("brl-%s", amount.String())),
		}
		query, _, _ := dialect.Update("tbl_member_balance").Set(record).Where(ex).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			return id, err
		}
	}

	return id, nil
}

// 中心钱包帐变(注单资讯指令) BTI使用
func walletDebitReserveTransaction(tx *sql.Tx, mb ryrpc.TblMemberBase, billNo, operationNo, platformID, remark string, balance, amount decimal.Decimal, createAt int64) (string, error) {

	afterAmount := balance
	id := helper.GenId()
	// 预投注，投注金额可以为0
	trans := memberTransaction{
		ID:           id,
		UID:          mb.Uid,
		PlatformID:   platformID,
		Username:     mb.Username,
		BillNo:       billNo,
		CashType:     helper.TransactionDebitReserve,
		Amount:       amount.String(),
		BeforeAmount: balance.String(),
		AfterAmount:  afterAmount.String(),
		OperationNo:  operationNo,
		CreatedAt:    createAt,
		Remark:       remark,
		Tester:       mb.Tester,
	}
	query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	fmt.Println(query)
	_, err := tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		//记录日志
		if strings.HasPrefix(err.Error(), "Error 1062") {
			PushLog(nil, "", "duplicate", "", 200, nil, err)
		}

		return id, err
	}

	return id, nil
}

// 中心钱包帐变(取消投注) BTI使用
func walletCancelReserveTransaction(tx *sql.Tx, mb ryrpc.TblMemberBase, billNo, operationNo, platformID string, balance, amount decimal.Decimal, createAt int64) (string, error) {

	id := helper.GenId()
	afterAmount := balance.Add(amount)
	// 预投注，投注金额可以为0
	trans := memberTransaction{
		ID:           id,
		UID:          mb.Uid,
		PlatformID:   platformID,
		Username:     mb.Username,
		BillNo:       billNo,
		CashType:     helper.TransactionCancelReserve,
		Amount:       amount.String(),
		BeforeAmount: balance.String(),
		AfterAmount:  afterAmount.String(),
		OperationNo:  operationNo,
		CreatedAt:    createAt,
		Tester:       mb.Tester,
	}
	query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	fmt.Println(query)
	_, err := tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		//记录日志
		if strings.HasPrefix(err.Error(), "Error 1062") {
			PushLog(nil, "", "duplicate", "", 200, nil, err)
		}

		return id, err
	}

	// 取消投注，把预投注订单改为取消状态
	ex := g.Ex{
		"bill_no":   billNo,
		"cash_type": []int{helper.TransactionReserve, helper.TransactionDebitReserve},
	}
	record := g.Record{
		"remark": "cancelReserve",
	}
	query, _, _ = dialect.Update("tbl_balance_transaction").Set(record).Where(ex).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		return id, err
	}

	if amount.Cmp(decimal.Zero) > 0 {

		ex := g.Ex{
			"uid": mb.Uid,
		}
		record := g.Record{
			"brl": g.L(fmt.Sprintf("brl+%s", amount.String())),
		}
		query, _, _ := dialect.Update("tbl_member_balance").Set(record).Where(ex).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			return id, err
		}
	}

	return id, nil
}

// 中心钱包帐变(确认投注) BTI使用
func walletCommitReserveTransaction(tx *sql.Tx, mb ryrpc.TblMemberBase, balance, reserveAmount, betAmount decimal.Decimal, platformID, reserveID string, createAt int64) (string, error) {

	id := helper.GenId()
	// 预投注金额未完全使用
	if reserveAmount.GreaterThan(betAmount) {
		// 剩余未使用金额
		leftAmount := reserveAmount.Sub(betAmount)
		afterAmount := balance.Add(leftAmount)
		// 预投注，投注金额可以为0
		trans := memberTransaction{
			ID:           id,
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       reserveID,
			CashType:     helper.TransactionCommitReserve,
			Amount:       leftAmount.String(),
			BeforeAmount: balance.String(),
			AfterAmount:  afterAmount.String(),
			OperationNo:  "",
			CreatedAt:    createAt,
			Tester:       mb.Tester,
		}
		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		fmt.Println(query)
		_, err := tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			//记录日志
			if strings.HasPrefix(err.Error(), "Error 1062") {
				PushLog(nil, "", "duplicate", "", 200, nil, err)
			}

			return id, err
		}

		record := g.Record{
			"brl": g.L(fmt.Sprintf("brl+%s", leftAmount.String())),
		}
		query, _, _ = dialect.Update("tbl_member_balance").Set(record).Where(g.Ex{"uid": mb.Uid}).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			return id, err
		}
	}

	// 确认投注，把预投注订单改为投注成功状态
	ex := g.Ex{
		"bill_no":   reserveID,
		"cash_type": helper.TransactionReserve,
	}
	record := g.Record{
		"remark": "running",
	}
	query, _, _ := dialect.Update("tbl_balance_transaction").Set(record).Where(ex).ToSQL()
	fmt.Println(query)
	_, err := tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		return id, err
	}

	return id, nil
}

// 中心钱包帐变(结算) BTI使用
func walletCreditCustomerTransaction(tx *sql.Tx, mb ryrpc.TblMemberBase, billNo, operationNo, platformID, remark string, balance, amount decimal.Decimal, createAt int64) (string, error) {

	id := helper.GenId()
	afterAmount := balance.Add(amount)
	trans := memberTransaction{
		ID:           id,
		UID:          mb.Uid,
		PlatformID:   platformID,
		Username:     mb.Username,
		BillNo:       billNo,
		CashType:     helper.TransactionCreditCustomer,
		Amount:       amount.String(),
		BeforeAmount: balance.String(),
		AfterAmount:  afterAmount.String(),
		OperationNo:  operationNo,
		CreatedAt:    createAt,
		Remark:       remark,
		Tester:       mb.Tester,
	}
	query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	fmt.Println(query)
	_, err := tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		//记录日志
		if strings.HasPrefix(err.Error(), "Error 1062") {
			PushLog(nil, "", "duplicate", "", 200, nil, err)
		}

		return id, err
	}

	if amount.Cmp(decimal.Zero) > 0 {

		ex := g.Ex{
			"uid": mb.Uid,
		}
		record := g.Record{
			"brl": g.L(fmt.Sprintf("brl+%s", amount.String())),
		}
		query, _, _ := dialect.Update("tbl_member_balance").Set(record).Where(ex).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			return id, err
		}
	}

	return id, nil
}

// 中心钱包帐变(重新结算) BTI使用
func walletDebitCustomerTransaction(tx *sql.Tx, mb ryrpc.TblMemberBase, billNo, operationNo, platformID, remark string, balance, amount decimal.Decimal, createAt int64) (string, error) {

	afterAmount := balance.Sub(amount)
	id := helper.GenId()
	// 预投注，投注金额可以为0
	trans := memberTransaction{
		ID:           id,
		UID:          mb.Uid,
		PlatformID:   platformID,
		Username:     mb.Username,
		BillNo:       billNo,
		CashType:     helper.TransactionDebitCustomer,
		Amount:       amount.String(),
		BeforeAmount: balance.String(),
		AfterAmount:  afterAmount.String(),
		OperationNo:  operationNo,
		CreatedAt:    createAt,
		Remark:       remark,
		Tester:       mb.Tester,
	}
	query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	fmt.Println(query)
	_, err := tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		//记录日志
		if strings.HasPrefix(err.Error(), "Error 1062") {
			PushLog(nil, "", "duplicate", "", 200, nil, err)
		}

		return id, err
	}

	record := g.Record{
		"brl": g.L(fmt.Sprintf("brl-%s", amount.String())),
	}
	query, _, _ = dialect.Update("tbl_member_balance").Set(record).Where(g.Ex{"uid": mb.Uid}).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		return id, err
	}

	return id, nil
}
