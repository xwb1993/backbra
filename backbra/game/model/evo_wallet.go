package model

import (
	"database/sql"
	"errors"
	"fmt"
	"game/contrib/helper"
	"github.com/google/uuid"
	"github.com/shopspring/decimal"
	"github.com/valyala/fasthttp"
	"strings"
	"time"
)

const EVO string = "EVO"

type plat_evo_t struct{}

func (plat_evo_t) Reg(args map[string]string) error {
	return nil
}

func (plat_evo_t) Launch(args map[string]string) (string, error) {

	var (
		isMobile bool
		isApp    bool
	)
	d, ok := args["deviceType"]
	if ok && d == "2" {
		isMobile = true
	}
	if ok && (d == "3" || d == "4") {
		isApp = true
	}
	uid, _ := uuid.NewUUID()
	request := &evoLoginRequest{
		Uuid: uid.String(),
		Player: evoPlayer{
			Id:       meta.Prefix + args["puid"],
			Update:   false,
			Country:  meta.PlatCfg.EVO.Country,
			Language: meta.PlatCfg.EVO.Language,
			Currency: meta.PlatCfg.EVO.Currency,
			Session: evoSession{
				Id: args["puid"],
				Ip: args["ip"],
			},
			FirstName: meta.Prefix,
			LastName:  args["username"],
			Nickname:  args["username"],
		},
		Config: evoLoginConfig{
			Channel: evoChannel{
				Mobile:  isMobile,
				Wrapped: isApp,
			},
		},
	}

	// 进入指定的桌子
	gameCode, ok := args["gamecode"]
	if ok && gameCode != "" {
		request.Config.Game = evoGame{
			Table: evoGameTable{
				ID: gameCode,
			},
		}
	}

	headers := map[string]string{
		"Content-Type": "application/json",
	}
	requestBody, _ := helper.JsonMarshal(request)
	requestURI := fmt.Sprintf("%s/ua/v1/%s/%s", meta.PlatCfg.EVO.API, meta.PlatCfg.EVO.CasinoKey, meta.PlatCfg.EVO.APIToken)
	statusCode, body, err := HttpPostWithPushLog(requestBody, args["username"], EVO, requestURI, headers)
	if err != nil {
		return "", errors.New(helper.PlatformLoginErr)
	}

	if statusCode != fasthttp.StatusOK {
		return "", errors.New(helper.PlatformLoginErr)
	}

	rsp := evoLoginRsp{}
	err = helper.JsonUnmarshal(body, &rsp)
	if err != nil {
		fmt.Println("evo:Launch:format", string(body), err)
		return "", errors.New(helper.FormatErr)
	}

	return rsp.Entry, nil
}

func EVOCheck(token string) EvoResponse {

	uid, _ := uuid.NewUUID()
	res := EvoResponse{
		Status:  "OK",
		Balance: 0,
		Uuid:    uid.String(),
	}

	if token != meta.PlatCfg.EVO.APIToken {
		res.Status = "INVALID_TOKEN_ID"
	}

	return res
}

// EVOSid 描述：用于 REST API 测试自动化的 sid 生成（不依赖于测试 UI 可用性）。返回的“sid”将用于“check”中的身份验证。
func EVOSid(body []byte) EVOSidResponse {

	ud, _ := uuid.NewUUID()
	res := EVOSidResponse{
		Status: "OK",
		Sid:    "",
		Uuid:   ud.String(),
	}

	req := EvoCheckUserReq{}
	err := helper.JsonUnmarshal(body, &req)
	if err != nil {
		return res
	}

	mb, err := GetMemberByPUID(req.UserId)
	if err != nil {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	res.Status = "OK"
	res.Sid = mb.Pid
	return res
}

// EVOCheckUser 检查玩家
func EVOCheckUser(body []byte) EvoCheckResponse {

	res := EvoCheckResponse{
		Status:  "OK",
		Balance: 0,
	}

	req := EvoCheckUserReq{}
	err := helper.JsonUnmarshal(body, &req)
	if err != nil {
		res.Status = "ACCOUNT_LOCKED"
		return res
	}

	mb, err := GetMemberByPUID(req.UserId)
	if err != nil {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	if mb.Pid != req.Sid {
		res.Status = "INVALID_SID"
		return res
	}

	res.Status = "OK"
	res.Sid = mb.Pid
	return res
}

// EVOBalance 余额
func EVOBalance(body []byte) EvoResponse {

	res := EvoResponse{
		Status:  "OK",
		Balance: 0,
	}

	req := EvoBalanceReq{}
	err := helper.JsonUnmarshal(body, &req)
	if err != nil {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	mb, err := GetMemberByPUID(req.UserId)
	if err != nil {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	if mb.Pid != req.Sid {
		res.Status = "INVALID_SID"
		return res
	}

	balance, err := getBalance(mb, 2)
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	res.Status = "OK"
	res.Balance, _ = balance.Truncate(3).Float64()
	res.Bonus = 0
	return res
}

// EVODebit 扣除投注金额
func EVODebit(body []byte, t time.Time) EvoResponse {

	res := EvoResponse{
		Status:  "OK",
		Balance: 0,
	}

	req := EvoDebitReq{}
	err := helper.JsonUnmarshal(body, &req)
	if err != nil {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	transactionId := req.Transaction.Id
	transactionRefId := req.Transaction.RefId
	mb, err := GetMemberByPUID(req.UserId)
	if err != nil {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	if mb.Pid != req.Sid {
		res.Status = "INVALID_SID"
		return res
	}

	err = BetLock(mb.Username)
	if err != nil {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	defer BetUnlock(mb.Username)

	amount := decimal.NewFromFloat(req.Transaction.Amount)
	status, _, betAmount, err := walletBetStatus(transactionRefId, transactionId, helper.EVO, "deduct")
	if err != nil && err != sql.ErrNoRows {
		res.Status = "BET_ALREADY_EXIST"
		return res
	} else if err == nil { //此处说明已经有该注单下注帐变了
		if status == "void" {
			res.Status = "FINAL_ERROR_ACTION_FAILED"
			return res
		}
		if status == "settled" || status == "running" {
			res.Status = "BET_ALREADY_EXIST"
			return res
		}

		amount = amount.Sub(betAmount)
		// 第二次扣款金额小于等于上一次扣款
		if amount.Cmp(decimal.Zero) <= 0 {
			res.Status = "INSUFFICIENT_FUNDS"
			return res
		}
	}

	// 不存在投注单，判断是否有取消注单请求帐变
	err = walletCancelStatus(transactionRefId, transactionId)
	if err != nil && err != sql.ErrNoRows {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	} else if err == nil {
		//先取消再投注
		err = walletLateBetTransaction(mb, transactionRefId, transactionId, helper.EVO, amount.Abs(), t.UnixMilli())
		if err != nil {
			fmt.Println(err)
			if strings.HasPrefix(err.Error(), "Error 1062") {
				res.Status = "FINAL_ERROR_ACTION_FAILED"
				return res
			}

			res.Status = "FINAL_ERROR_ACTION_FAILED"
			return res
		}
	}

	balance, err := getBalance(mb, 2)
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	_, err = walletBetTransaction(tx, mb, transactionRefId, transactionId, helper.EVO, balance, amount, t)
	if err != nil {
		if strings.HasPrefix(err.Error(), "Error 1062") {
			res.Status = "FINAL_ERROR_ACTION_FAILED"
			return res
		}

		res.Status = "FINAL_ERROR_ACTION_FAILED"
		if err.Error() == helper.BalanceErr {
			res.Status = "INSUFFICIENT_FUNDS"
		}

		return res
	}

	err = tx.Commit()
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	balance, err = getBalance(mb, 2)
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	res.Status = "OK"
	res.Balance, _ = balance.Truncate(3).Float64()
	res.Bonus = 0
	return res
}

// EVOCredit 增加
func EVOCredit(body []byte, t time.Time) EvoResponse {

	res := EvoResponse{
		Status:  "OK",
		Balance: 0,
	}

	req := EvoCreditReq{}
	err := helper.JsonUnmarshal(body, &req)
	if err != nil {
		res.Status = "UNKNOWN_ERROR"
		return res
	}

	transactionId := req.Transaction.Id
	transactionRefId := req.Transaction.RefId
	mb, err := GetMemberByPUID(req.UserId)
	if err != nil {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	if mb.Pid != req.Sid {
		res.Status = "INVALID_SID"
		return res
	}

	balance, err := getBalance(mb, 2)
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	winLoss := decimal.NewFromFloat(req.Transaction.Amount)
	status, _, _, err := walletBetStatus(transactionRefId, "", helper.EVO, "settle")
	if err != nil && err != sql.ErrNoRows {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	if err == sql.ErrNoRows {
		res.Status = "BET_DOES_NOT_EXIST"
		return res
	}

	if status == "settled" {
		res.Balance, _ = balance.Float64()
		res.Status = "BET_ALREADY_SETTLED"
		return res
	}

	if status == "cancel" {
		res.Balance, _ = balance.Float64()
		res.Status = "BET_ALREADY_SETTLED"
		return res
	}

	// evo可能同时出现同会员多笔派彩
	evoSettleLockKey := fmt.Sprintf("evo:settle:%s", mb.Username)
	err = Lock(evoSettleLockKey)
	if err != nil {
		time.Sleep(1 * time.Second)
	}

	defer Unlock(evoSettleLockKey)

	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	_, err = walletSettleTransaction(tx, mb, transactionRefId, transactionId, helper.EVO, balance, winLoss, t)
	if err != nil {
		fmt.Println(err.Error())
		if strings.HasPrefix(err.Error(), "Error 1062") {
			res.Status = "FINAL_ERROR_ACTION_FAILED"
			return res
		}

		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	err = tx.Commit()
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	// 参加了活动
	balance, err = getBalance(mb, 2)
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	res.Status = "OK"
	res.Balance, _ = balance.Truncate(3).Float64()
	res.Bonus = 0

	return res
}

// EVOBonus 红利
func EVOBonus(body []byte, t time.Time) EvoResponse {

	res := EvoResponse{
		Status:  "OK",
		Balance: 0,
	}

	req := EvoPromoReq{}
	err := helper.JsonUnmarshal(body, &req)
	if err != nil {
		res.Status = "UNKNOWN_ERROR"
		return res
	}

	transactionId := req.PromoTransaction.Id
	transactionRefId := req.PromoTransaction.VoucherId
	username := req.UserId
	if username == "" {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	mb, err := GetMemberByPUID(req.UserId)
	if err != nil {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	balance, err := getBalance(mb, 2)
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	amount := decimal.NewFromFloat(req.PromoTransaction.Amount)
	if amount.Cmp(decimal.Zero) < 1 {
		res.Status = "OK"
		res.Balance, _ = balance.Truncate(3).Float64()

		return res
	}

	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	err = walletEVOPrizeTransaction(tx, mb, transactionRefId, transactionId, helper.EVO, balance, amount.Abs(), t)
	if err != nil {
		if strings.HasPrefix(err.Error(), "Error 1062") {
			res.Status = "FINAL_ERROR_ACTION_FAILED"
			return res
		}

		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	err = tx.Commit()
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	balance, err = getBalance(mb, 2)
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	res.Status = "OK"
	res.Balance, _ = balance.Truncate(3).Float64()

	return res
}

func EVOCancel(body []byte, t time.Time) EvoResponse {

	res := EvoResponse{
		Status:  "OK",
		Balance: 0,
	}
	req := EvoCancelReq{}
	err := helper.JsonUnmarshal(body, &req)
	if err != nil {
		res.Status = "body unmarshal error"
		return res
	}

	transactionId := req.Transaction.Id
	transactionRefId := req.Transaction.RefId
	username := req.UserId
	mb, err := GetMemberByPUID(username)
	if err != nil {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	if mb.Pid != req.Sid {
		res.Status = "INVALID_SID"
		return res
	}

	status, _, amount, err := walletBetStatus(transactionRefId, transactionId, helper.EVO, "cancel")
	if err != nil && err != sql.ErrNoRows {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	if err == sql.ErrNoRows {
		//先取消再投注
		err = walletAdvanceCancelTransaction(mb, transactionRefId, transactionId, helper.EVO, decimal.Zero, t.UnixMilli())
		if err != nil {
			fmt.Println(err)
			if strings.HasPrefix(err.Error(), "Error 1062") {
				res.Status = "FINAL_ERROR_ACTION_FAILED"
				return res
			}

			res.Status = "FINAL_ERROR_ACTION_FAILED"
			return res
		}
	}

	if status == "void" {
		res.Status = "BET_ALREADY_SETTLED"
		return res
	}

	if status == "settled" {
		payoutAmount, err := walletSettledBetStatus(transactionRefId, helper.EVO, "cancel")
		if err != nil && err != sql.ErrNoRows {
			res.Status = "FINAL_ERROR_ACTION_FAILED"
			return res
		}

		// 结算金额减去投注金额，为需要扣除的金额
		amount = amount.Sub(payoutAmount)
	}

	balance, err := getBalance(mb, 2)
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	_, err = walletCancelTransaction(tx, mb, transactionRefId, transactionId, helper.EVO, balance, amount.Abs(), t)
	if err != nil {
		if strings.HasPrefix(err.Error(), "Error 1062") {
			res.Status = "FINAL_ERROR_ACTION_FAILED"
			return res
		}

		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	err = tx.Commit()
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	balance, err = getBalance(mb, 2)
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	res.Balance, _ = balance.Truncate(3).Float64()
	return res
}
