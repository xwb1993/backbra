package model

import (
	"database/sql"
	"errors"
	"fmt"
	"game/contrib/helper"
	ryrpc "game/rpc"
	g "github.com/doug-martin/goqu/v9"
	"github.com/valyala/fasthttp"
)

// 通过用户名获取用户在redis中的数据
func MemberCache(fCtx *fasthttp.RequestCtx, name string) (ryrpc.TblMemberBase, error) {

	m := ryrpc.TblMemberBase{}
	if name == "" {
		value := fCtx.UserValue("token")
		if value == nil {
			return m, errors.New(helper.UsernameErr)
		}

		name = string(value.([]byte))
		if name == "" {
			return m, errors.New(helper.UsernameErr)
		}
	}

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMember...).Where(g.Ex{"username": name}).Limit(1).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&m, query)
	if err != nil && err != sql.ErrNoRows {
		return m, pushError(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return m, errors.New(helper.UsernameErr)
	}

	return m, nil
}

func MemberBalanceFindOne(uid string) (MemberBalance, error) {

	balance := MemberBalance{}
	query, _, _ := dialect.From("tbl_member_balance").Select(colsMemberBalance...).Where(g.Ex{"uid": uid}).Limit(1).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&balance, query)
	if err != nil {
		return balance, pushError(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return balance, err
}

func getBonusConfig(code string) (ryrpc.TblBonusConfig, error) {

	var data ryrpc.TblBonusConfig
	query, _, _ := dialect.From("tbl_bonus_config").Select(colsBonusConfig...).Where(g.Ex{"code": code}).Limit(1).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&data, query)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return data, pushError(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}

func MemberFindByUid(uid string) (ryrpc.TblMemberBase, error) {

	m := ryrpc.TblMemberBase{}

	ex := g.Ex{}
	if uid != "" {
		ex["uid"] = uid
	}

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMember...).Where(ex).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&m, query)
	if err != nil && err != sql.ErrNoRows {
		return m, pushError(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return m, errors.New(helper.UIDErr)
	}

	return m, nil
}
