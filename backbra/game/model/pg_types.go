package model

type PGVerifyData struct {
	PlayerName string `json:"player_name"`
	Nickname   string `json:"nickname"`
	Currency   string `json:"currency"`
}

type PGBalanceData struct {
	UpdatedTime   int64   `json:"updated_time"`
	BalanceAmount float64 `json:"balance_amount"`
	CurrencyCode  string  `json:"currency_code"`
}

type PGAdjustData struct {
	UpdatedTime   int64   `json:"updated_time"`
	AdjustAmount  float64 `json:"adjust_amount"`
	BalanceBefore float64 `json:"balance_before"`
	BalanceAfter  float64 `json:"balance_after"`
}

type PGResponse struct {
	Data  interface{} `json:"data"`
	Error interface{} `json:"error"`
}

type PGError struct {
	Code    interface{} `json:"code"`
	Message interface{} `json:"message"`
}

type pgRegRsp struct {
	Data struct {
		ActionResult int `json:"action_result"`
	} `json:"data"`
	Error struct {
		Code    string `json:"code"`
		Message string `json:"message"`
		TraceId string `json:"traceId"`
	} `json:"error"`
}
