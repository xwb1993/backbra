package model

import (
	"database/sql"
	"errors"
	"fmt"
	"game/contrib/helper"
	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"
	"github.com/valyala/bytebufferpool"
	"github.com/valyala/fasthttp"
	"strings"
	"time"
)

var (
	IBCPlatformPID = map[string]string{
		"IBCSB":  helper.IBCSB,
		"IBCESB": helper.IBCSB,
	}
	IBCAPIName = map[string]string{
		"IBCSB":  "SABA_Sports",
		"IBCESB": "SABA_Esports",
	}
	IBCGameType = map[string]string{
		"IBCSB":  gameTypeSport,
		"IBCESB": gameTypeESport,
	}
	IbcHandicapType = map[string]string{
		"1": "Malay Odds",
		"2": "China Odds",
		"3": "Decimal Odds",
		"4": "Indo Odds",
		"5": "American Odds",
	}
	usEasternLoc, _ = time.LoadLocation("US/Eastern")
)

const IBC = "IBC"

type plat_ibc_t struct{}

func gunzip(p []byte) ([]byte, error) {

	var bb bytebufferpool.ByteBuffer
	_, err := fasthttp.WriteGunzip(&bb, p)
	if err != nil {
		return nil, err
	}
	return bb.B, nil
}

func unpack(ctx *fasthttp.RequestCtx) ([]byte, error) {

	var (
		err error
	)
	body := ctx.PostBody()
	fmt.Println(string(body))
	ce := string(ctx.Request.Header.Peek("Content-Encoding"))
	if ce == "gzip" {
		body, err = gunzip(body)
		if err != nil {
			return body, err
		}
	}

	return body, nil
}

type IBCReg struct {
	ErrorCode int    `json:"error_code"`
	Message   string `json:"message"`
}

// 沙巴体育注册
func (plat_ibc_t) Reg(args map[string]string) error {

	req := map[string]string{
		"vendor_id":        meta.PlatCfg.IBC.VendorID,
		"Vendor_Member_ID": meta.Prefix + args["puid"],
		"OperatorId":       meta.PlatCfg.IBC.OperatorId,
		"UserName":         meta.Prefix + args["puid"],
		"OddsType":         meta.PlatCfg.IBC.OddsType,
		"Currency":         meta.PlatCfg.IBC.Currency,
		"MaxTransfer":      meta.PlatCfg.IBC.MaxTransfer,
		"MinTransfer":      meta.PlatCfg.IBC.MinTransfer,
	}
	requestURI := fmt.Sprintf("%s/api/CreateMember/", meta.PlatCfg.IBC.API)
	requestBody := ParamEncode(req)
	header := map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
	}
	statusCode, body, err := HttpPostWithPushLog([]byte(requestBody), args["username"], IBC, requestURI, header)
	if err != nil {
		return errors.New(helper.PlatformRegErr)
	}

	if statusCode != fasthttp.StatusOK {
		return errors.New(helper.PlatformRegErr)
	}

	rsp := IBCReg{}
	err = helper.JsonUnmarshal(body, &rsp)
	if err != nil {
		return errors.New(helper.FormatErr)
	}

	if rsp.ErrorCode == 0 || rsp.ErrorCode == 2 {
		return nil
	}

	return errors.New(helper.PlatformRegErr)
}

type IBCLaunch struct {
	ErrorCode int    `json:"error_code"`
	Message   string `json:"message"`
	Data      string `json:"Data"`
}

// 沙巴体育登录
func (plat_ibc_t) Launch(args map[string]string) (string, error) {

	plat := "2"
	if args["deviceType"] == "1" {
		plat = "1"
	}

	req := map[string]string{
		"vendor_id":        meta.PlatCfg.IBC.VendorID,
		"Vendor_Member_ID": meta.Prefix + args["puid"],
		"platform":         plat,
	}
	requestURI := fmt.Sprintf("%s/api/GetSabaUrl/", meta.PlatCfg.IBC.API)
	requestBody := ParamEncode(req)
	header := map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
	}
	statusCode, body, err := HttpPostWithPushLog([]byte(requestBody), args["username"], IBC, requestURI, header)
	if err != nil {
		return "", errors.New(helper.PlatformLoginErr)
	}

	if statusCode != fasthttp.StatusOK {
		return "", errors.New(helper.PlatformLoginErr)
	}

	rsp := IBCLaunch{}
	err = helper.JsonUnmarshal(body, &rsp)
	if err != nil {
		return "", errors.New(helper.FormatErr)
	}

	if rsp.ErrorCode == 0 {
		loginURL := fmt.Sprintf("%s&lang=%s", rsp.Data, meta.PlatCfg.IBC.Lang)
		pid := args["pid"]
		if pid == helper.IBCES && args["deviceType"] == "1" {
			loginURL += "&game=esports"
			return loginURL, nil
		}

		if pid == helper.IBCES {
			loginURL += "&types=esports"
			fmt.Println(loginURL)
			return loginURL, nil
		}

		return loginURL, nil
	}

	return "", errors.New(helper.PlatformLoginErr)
}

func IBCCheckTicketStatus(refId string) error {

	args := map[string]string{
		"vendor_id": meta.PlatCfg.IBC.VendorID,
		"refId":     refId,
	}
	requestBody := ParamEncode(args)
	requestURI := meta.PlatCfg.IBC.API + "/api/checkticketstatus"
	header := map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
	}
	statusCode, body, err := HttpPostWithPushLog([]byte(requestBody), "", IBC, requestURI, header)
	if err != nil {
		fmt.Println("http post ", err)
		return pushError(err, helper.ServerErr)
	}

	fmt.Println("IBCCheckTicketStatus 1", requestURI, requestBody, string(body))
	if statusCode != fasthttp.StatusOK {
		return errors.New(helper.ServerErr)
	}

	req := IBCCheckTicketResult{}
	err = helper.JsonUnmarshal(body, &req)
	if err != nil {
		return pushError(err, helper.FormatErr)
	}

	for _, v := range req.Data.TransHistory {
		if v.Action == "ConfirmBet" || v.Action == "CancelBet" || v.Action == "Settle" /*|| v.Action == "ReSettle" || v.Action == "UnSettle" */ {
			err = ibcRetryOperation(v.OperationId)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func ibcRetryOperation(operationId string) error {

	args := map[string]string{
		"vendor_id":   meta.PlatCfg.IBC.VendorID,
		"operationId": operationId,
	}
	requestBody := ParamEncode(args)
	requestURI := meta.PlatCfg.IBC.API + "/api/retryoperation"
	header := map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
	}
	statusCode, body, err := HttpPostWithPushLog([]byte(requestBody), "", IBC, requestURI, header)
	if err != nil {
		return pushError(err, helper.ServerErr)
	}

	fmt.Println("IBCCheckTicketStatus 2", requestURI, requestBody, string(body))
	if statusCode != fasthttp.StatusOK {
		return errors.New(helper.ServerErr)
	}

	return nil
}

// ibcCheck 验签
func ibcCheck(key string) IBCResult {

	res := IBCResult{
		Status: "0",
		Msg:    "success",
	}

	if key != meta.PlatCfg.IBC.VendorID {
		res.Status = "311"
		res.Msg = "Invalid key"
		return res
	}

	return res
}

// IBCBalance 沙巴体育查询余额
func IBCBalance(ctx *fasthttp.RequestCtx) IBCResult {

	res := IBCResult{
		Status: "0",
		Msg:    "success",
	}
	body, err := unpack(ctx)
	if err != nil {
		res.Status = "506"
		res.Msg = "Parsing error"
		return res
	}

	fmt.Println(string(body))
	req := IBCBalanceReq{}
	err = helper.JsonUnmarshal(body, &req)
	if err != nil {
		res.Status = "506"
		res.Msg = "Parsing error"
	}

	res = ibcCheck(req.Key)
	if res.Status != "0" {
		return res
	}

	//platParam := map[string]interface{}{}
	//platParam = PlatformConf(IBCID, platParam)
	userId := req.Message.UserId
	mb, err := GetMemberByPUID(userId)
	if err != nil {
		res.Status = "203"
		res.Msg = "User not exist"
		return res
	}

	balance, err := getBalance(mb, 2)
	if err != nil {
		res.Status = "307"
		res.Msg = "Invalid Account"
		return res
	}

	res.Balance, _ = balance.Truncate(3).Float64()
	ms := time.Now().UTC().In(usEasternLoc).Format("2006-01-02T15:04:05.999")
	res.BalanceTs = fmt.Sprintf("%s-04:00", ms)

	return res
}

// IBCBet 沙巴体育投注
// 1, 203, 302, 303, 306, 307, 308, 309, 504, 505
func IBCBet(ctx *fasthttp.RequestCtx) IBCResult {

	res := IBCResult{
		Status: "0",
		Msg:    "success",
	}
	body, err := unpack(ctx)
	if err != nil {
		res.Status = "506"
		res.Msg = "Parsing error"
		return res
	}

	fmt.Println(string(body))
	req := IBCBetReq{}
	err = helper.JsonUnmarshal(body, &req)
	if err != nil {
		res.Status = "506"
		res.Msg = "Parsing error"
	}

	res = ibcCheck(req.Key)
	if res.Status != "0" {
		return res
	}

	userId := req.Message.UserId
	platform := "IBCSB"
	sportType := req.Message.SportType
	if sportType == 43 {
		platform = "IBCESB"
	}
	pid := IBCPlatformPID[platform]
	gameType, _ := IBCGameType[platform]
	apiName, _ := IBCAPIName[platform]

	mb, err := GetMemberByPUID(userId)
	if err != nil {
		res.Status = "203"
		res.Msg = "User not exist"
		return res
	}

	betAmount := decimal.NewFromFloat(req.Message.ActualAmount)
	//判断余额
	balance, err := getBalance(mb, 2)
	if err != nil {
		res.Msg = "balance error"
		res.Status = "505"
		return res
	}

	refId := req.Message.RefId
	if refId == "" {
		res.Msg = "refId error"
		res.Status = "101"
		return res
	}

	res.RefId = refId
	res.LicenseeTxId = refId
	if betAmount.Cmp(balance) > 0 {
		res.Msg = "Insufficient quota"
		res.Status = "502"
		return res
	}

	status, _, _, err := walletBetStatus(refId, "", pid, "deduct")
	if err != nil && err != sql.ErrNoRows {
		res.Status = "999"
		res.Msg = "System error"
		return res
	} else if err == nil {
		if status == "running" {
			res.Status = "1"
			res.Msg = "Repeated trade"
			return res
		}
	}

	if betAmount.Cmp(decimal.Zero) == 1 {

		tx, err := meta.MerchantDB.Begin()
		if err != nil {
			res.Status = "999"
			res.Msg = "System error"
			return res
		}

		var transIDs []string
		err = walletIBCTransaction(tx, mb, transIDs,
			refId, "", pid, "running", betAmount, helper.TransactionBet, ctx.Time().UnixMilli())
		if err != nil {
			if strings.HasPrefix(err.Error(), "Error 1062") {
				res.Status = "1"
				res.Msg = "Repeated trade"
				return res
			}

			res.Status = "999"
			res.Msg = "System error"
			return res
		}

		err = tx.Commit()
		if err != nil {
			res.Status = "999"
			res.Msg = "System error"
			return res
		}
	}

	//处理投注记录
	handicap := fmt.Sprintf("%s,%s", req.Message.BetTypeName, req.Message.BetTeam)
	result := fmt.Sprintf(`match id:%d|sport type name:%s|bet type name:%s|handicap:%s|league name:%s|home name:%s|away name:%s`,
		req.Message.MatchId, req.Message.SportTypeName, req.Message.BetTypeName, handicap, req.Message.LeagueName, req.Message.HomeName, req.Message.AwayName)
	oddsType := fmt.Sprintf("%d", req.Message.OddsType)

	betTimeStr := req.Message.BetTime
	betTimeStr = strings.Replace(betTimeStr, "-04:00", "", 1)
	betTime, err := time.ParseInLocation("2006-01-02T15:04:05", betTimeStr, usEasternLoc)
	if err != nil {
		res.Msg = "betTime error"
		res.Status = "306"
		return res
	}

	kickOffTime := req.Message.KickOffTime
	kickOffTime = strings.Replace(kickOffTime, "-04:00", "", 1)
	//平台的投注时间
	startTime, err := time.ParseInLocation("2006-01-02T15:04:05", kickOffTime, usEasternLoc)
	if err != nil {
		res.Msg = "kickOffTime error"
		res.Status = "306"
		return res
	}

	betTime = betTime.In(loc)
	startTime = startTime.In(loc)

	//盘口类型
	handicapType := "Special Odds"
	val, ok := IbcHandicapType[oddsType]
	if ok {
		handicapType = val
	}

	gm := GameRecord{
		BillNo:         refId,
		Result:         result,
		GameName:       req.Message.SportTypeName,
		GameCode:       fmt.Sprintf("%d", req.Message.SportType),
		PlayType:       req.Message.BetTypeName,
		RowId:          platform + refId,
		ApiBillNo:      refId,
		MainBillNo:     "",
		HandicapType:   handicapType,
		Handicap:       handicap,
		Odds:           req.Message.Odds,
		StartTime:      startTime.UnixMilli(),
		Resettle:       0,
		Presettle:      0,
		TopName:        mb.TopName,
		TopUid:         mb.TopID,
		ParentName:     mb.ParentName,
		ParentUid:      mb.ParentID,
		GrandName:      mb.GrandName,
		GrandID:        mb.GrandID,
		GreatGrandName: mb.GreatGrandName,
		GreatGrandID:   mb.GreatGrandID,
		Uid:            mb.Uid,
		Name:           mb.Username,
		Prefix:         meta.Prefix,
		CreatedAt:      time.Now().UnixMilli(),
		UpdatedAt:      time.Now().UnixMilli(),
		BetTime:        time.Now().UnixMilli(),
		SettleTime:     0,
		ApiBetTime:     betTime.UnixMilli(),
		ApiSettleTime:  0,
		PlayerName:     meta.Prefix + mb.Username,
		GameType:       gameType,
		ApiType:        pid,
		ApiName:        apiName,
		ValidBetAmount: 0,
		NetAmount:      0,
	}
	gm.BetAmount, _ = betAmount.Abs().Float64()
	gm.Flag = 0
	query, _, _ := dialect.Insert("tbl_game_record").Rows(&gm).ToSQL()
	fmt.Println(query)
	_, err = meta.MerchantDB.Exec(query)
	if err != nil {
		res.Status = "999"
		res.Msg = "System error"
		return res
	}

	//balance, err = GetMemberBalance(userId, 3)
	//if err != nil {
	//	res.Msg = "balance error"
	//	res.Status = "505"
	//	return res
	//}

	res.Balance, _ = balance.Truncate(3).Float64()
	return res
}

// IBCConfirm 1, 203, 302, 303, 306, 307, 308, 309, 505
// 确认投注
func IBCConfirm(ctx *fasthttp.RequestCtx) IBCResult {

	res := IBCResult{
		Status: "0",
		Msg:    "success",
	}
	body, err := unpack(ctx)
	if err != nil {
		res.Status = "506"
		res.Msg = "Parsing error"
		return res
	}

	fmt.Println(string(body))
	req := IBCConfirmReqData{}
	err = helper.JsonUnmarshal(body, &req)
	if err != nil {
		res.Status = "506"
		res.Msg = "Parsing error"
	}

	res = ibcCheck(req.Key)
	if res.Status != "0" {
		return res
	}

	userId := req.Message.UserId
	mb, err := GetMemberByPUID(userId)
	if err != nil {
		res.Status = "203"
		res.Msg = "User not exist"
		return res
	}

	for _, v := range req.Message.Txns {

		ex := g.Ex{
			"bill_no":   v.RefId,
			"username":  mb.Username,
			"cash_type": helper.TransactionBet,
		}
		//判断是否有订单
		ok, err := walletTransactionExist(ex)
		if err != nil {
			res.Status = "999"
			res.Msg = "System error"
			return res
		}

		if !ok {
			res.Status = "504"
			res.Msg = "Order not exist"
			return res
		}

		var rowID string
		query, _, _ := dialect.From("tbl_game_record").
			Select("row_id").Where(g.Ex{"api_bill_no": v.RefId, "flag": 0}).ToSQL()
		fmt.Println(query)
		err = meta.MerchantDB.Get(&rowID, query)
		if err != nil {
			res.Status = "999"
			res.Msg = "System error"
			return res
		}

		record := g.Record{
			"bet_amount": decimal.NewFromFloat(v.ActualAmount).String(),
			"odds":       fmt.Sprintf("%.3f", v.Odds),
			"bill_no":    fmt.Sprintf("%d", v.TxId),
		}
		query, _, _ = dialect.Update("tbl_game_record").Where(g.Ex{"row_id": rowID}).Set(record).ToSQL()
		fmt.Println(query)
		_, err = meta.MerchantDB.Exec(query)
		if err != nil {
			res.Status = "999"
			res.Msg = "System error"
			return res
		}
	}

	balance, err := getBalance(mb, 2)
	if err != nil {
		res.Msg = "balance error"
		res.Status = "505"
		return res
	}

	res.Balance, _ = balance.Truncate(3).Float64()
	return res
}

// IBCCancelBet 1, 203, 302, 303, 306, 307, 308, 309, 505
// 派彩
func IBCCancelBet(ctx *fasthttp.RequestCtx) IBCResult {

	res := IBCResult{
		Status: "0",
		Msg:    "success",
	}
	body, err := unpack(ctx)
	if err != nil {
		res.Status = "506"
		res.Msg = "Parsing error"
		return res
	}

	fmt.Println(string(body))
	req := IBCCancelBetReqData{}
	err = helper.JsonUnmarshal(body, &req)
	if err != nil {
		res.Status = "506"
		res.Msg = "Parsing error"
	}

	res = ibcCheck(req.Key)
	if res.Status != "0" {
		return res
	}

	userId := req.Message.UserId
	prefix := userId[:3]
	mb, err := GetMemberByPUID(userId)
	if err != nil {
		res.Status = "203"
		res.Msg = "User not exist"
		return res
	}

	balance := decimal.NewFromFloat(0)
	for _, v := range req.Message.Txns {

		refId := v.RefId
		if refId == "" {
			res.Msg = "refId error"
			res.Status = "101"
			return res
		}

		status, transIDs, amount, err := walletBetStatus(v.RefId, "", helper.IBCSB, "cancel")
		if err != nil && err != sql.ErrNoRows {
			res.Status = "999"
			res.Msg = "System error"
			return res
		}

		if err == sql.ErrNoRows || status == "void" {
			balance, err = getBalance(mb, 2)
			if err != nil {
				res.Msg = "balance error"
				res.Status = "505"
				return res
			}

			res.Balance, _ = balance.Float64()
			return res
		}

		tx, err := meta.MerchantDB.Begin()
		if err != nil {
			res.Status = "999"
			res.Msg = "System error"
			return res
		}

		err = walletIBCTransaction(tx, mb, transIDs,
			v.RefId, "", helper.IBCSB, "void", amount.Abs(), helper.TransactionBetCancel, ctx.Time().UnixMilli())
		if err != nil {
			if strings.HasPrefix(err.Error(), "Error 1062") {
				res.Status = "1"
				res.Msg = "Repeated trade"
				return res
			}

			res.Status = "999"
			res.Msg = "System error"
			return res
		}

		err = tx.Commit()
		if err != nil {
			res.Status = "999"
			res.Msg = "System error"
			return res
		}

		//这个放到 消息队列 去处理
		record := g.Record{
			"prefix":           prefix,
			"flag":             "3",
			"valid_bet_amount": "0",
			"net_amount":       "0",
			"settle_time":      "0",
			"api_settle_time":  "0",
		}
		query, _, _ := dialect.Update("tbl_game_record").Set(record).Where(g.Ex{"bill_no": refId}).ToSQL()
		fmt.Println(query)
		_, err = meta.MerchantDB.Exec(query)
		if err != nil {
			res.Status = "999"
			res.Msg = "System error"
			return res
		}
	}

	//balance, err := getBalance(mb, 2)
	//if err != nil {
	//	res.Msg = "balance error"
	//	res.Status = "505"
	//	return res
	//}

	res.Balance, _ = balance.Truncate(3).Float64()
	return res
}

// IBCSettle 1, 203, 302, 303, 306, 307, 308, 309, 505
// 派彩
func IBCSettle(ctx *fasthttp.RequestCtx) IBCResult {

	res := IBCResult{
		Status: "0",
		Msg:    "success",
	}
	body, err := unpack(ctx)
	if err != nil {
		res.Status = "506"
		res.Msg = "Parsing error"
		return res
	}

	fmt.Println(string(body))
	req := IBCSettleReqData{}
	err = helper.JsonUnmarshal(body, &req)
	if err != nil {
		res.Status = "506"
		res.Msg = "Parsing error"
	}

	res = ibcCheck(req.Key)
	if res.Status != "0" {
		return res
	}

	for _, v := range req.Message.Txns {

		userId := v.UserId
		mb, err := GetMemberByPUID(userId)
		if err != nil {
			res.Status = "203"
			res.Msg = "User not exist"
			return res
		}

		settleTime, err := time.ParseInLocation("2006-01-02T15:04:05.999-04:00", v.WinlostDate, loc)
		if err != nil {
			res.Msg = "winlostDate error"
			res.Status = "306"
			return res
		}

		var (
			flag     = "1"
			cashType = helper.TransactionPayout
			flags    = "settle"
			remark   = "settled"
		)
		if v.Status == "void" || v.Status == "refund" || v.Status == "reject" {
			flag = "3"
			flags = "cancel"
			cashType = helper.TransactionBetCancel
		}
		status, transIDs, betAmount, err := walletBetStatus(v.RefId, "", helper.IBCSB, flags)
		if err != nil && err != sql.ErrNoRows {
			res.Status = "999"
			res.Msg = "System error"
			return res
		}

		if err == sql.ErrNoRows {
			res.Status = "504"
			res.Msg = "Order not exist"
			return res
		}

		ts := time.Now()
		creditAmount := decimal.NewFromFloat(v.CreditAmount)
		debitAmount := decimal.NewFromFloat(v.DebitAmount)
		netAmount := creditAmount.Sub(betAmount)
		amount := creditAmount.Sub(debitAmount)
		validBetAmount := betAmount
		if betAmount.GreaterThan(netAmount.Abs()) {
			validBetAmount = netAmount.Abs()
		}
		// 重复发送，直接返回
		if status == "settled" && flags == "settle" ||
			status == "void" && flags == "cancel" {
			record := g.Record{
				"flag":             flag,
				"valid_bet_amount": validBetAmount.String(),
				"net_amount":       netAmount.String(),
				"settle_time":      fmt.Sprintf("%d", ts.UnixMilli()),
				"api_settle_time":  fmt.Sprintf("%d", settleTime.UnixMilli()),
			}
			// 提前结算
			if v.ExtraStatus == "cashout" {
				record["presettle"] = 1
			}
			query, _, _ := dialect.Update("tbl_game_record").
				Set(record).Where(g.Ex{"bill_no": fmt.Sprintf("%d", v.TxId), "flag": 0}).ToSQL()
			fmt.Println(query)
			_, err = meta.MerchantDB.Exec(query)
			if err != nil {
				res.Status = "999"
				res.Msg = "System error"
				return res
			}
		}

		tx, err := meta.MerchantDB.Begin()
		if err != nil {
			res.Status = "999"
			res.Msg = "System error"
			return res
		}

		err = walletIBCTransaction(tx, mb, transIDs, v.RefId,
			"", helper.IBCSB, remark, amount.Abs(), cashType, ts.UnixMilli())
		if err != nil {
			if strings.HasPrefix(err.Error(), "Error 1062") {
				continue
			}

			res.Status = "999"
			res.Msg = "System error"
			return res
		}

		err = tx.Commit()
		if err != nil {
			res.Status = "999"
			res.Msg = "System error"
			return res
		}

		record := g.Record{
			"flag":             flag,
			"valid_bet_amount": validBetAmount.String(),
			"net_amount":       netAmount.String(),
			"settle_time":      fmt.Sprintf("%d", ts.UnixMilli()),
			"api_settle_time":  fmt.Sprintf("%d", settleTime.UnixMilli()),
		}
		// 提前结算
		if v.ExtraStatus == "cashout" {
			record["presettle"] = 1
		}
		query, _, _ := dialect.Update("tbl_game_record").Set(record).Where(g.Ex{"bill_no": fmt.Sprintf("%d", v.TxId)}).ToSQL()
		fmt.Println(query)
		_, err = meta.MerchantDB.Exec(query)
		if err != nil {
			res.Status = "999"
			res.Msg = "System error"
			return res
		}
	}

	return res
}

// IBCResettle 二次结算
func IBCResettle(ctx *fasthttp.RequestCtx) IBCResult {

	res := IBCResult{
		Status: "0",
		Msg:    "success",
	}
	body, err := unpack(ctx)
	if err != nil {
		res.Status = "506"
		res.Msg = "Parsing error"
		return res
	}

	fmt.Println(string(body))
	req := IBCSettleReqData{}
	err = helper.JsonUnmarshal(body, &req)
	if err != nil {
		res.Status = "506"
		res.Msg = "Parsing error"
	}

	res = ibcCheck(req.Key)
	if res.Status != "0" {
		return res
	}

	for _, v := range req.Message.Txns {

		userId := v.UserId
		mb, err := GetMemberByPUID(userId)
		if err != nil {
			res.Status = "203"
			res.Msg = "User not exist"
			return res
		}

		settleTime, err := time.ParseInLocation("2006-01-02T15:04:05.999-04:00", v.WinlostDate, loc)
		if err != nil {
			res.Msg = "winlostDate error"
			res.Status = "306"
			return res
		}

		_, _, betAmount, err := walletBetStatus(v.RefId, "", helper.IBCSB, "resettle")
		if err != nil && err != sql.ErrNoRows {
			res.Status = "999"
			res.Msg = "System error"
			return res
		}

		if err == sql.ErrNoRows {
			res.Status = "504"
			res.Msg = "Order not exist"
			return res
		}

		//_, err = walletSettledBetStatus(prefix, v.RefId)
		//if err != nil && err != sql.ErrNoRows {
		//	res.Status = "999"
		//	res.Msg = "System error"
		//	return res
		//}
		//
		//if err == sql.ErrNoRows {
		//	res.Status = "504"
		//	res.Msg = "Order not exist"
		//	return res
		//}

		payout := decimal.NewFromFloat(v.Payout)
		netAmount := payout.Sub(betAmount)
		creditAmount := decimal.NewFromFloat(v.CreditAmount)
		debitAmount := decimal.NewFromFloat(v.DebitAmount)
		amount := creditAmount.Sub(debitAmount)

		cashType := helper.TransactionResettlePlus
		if amount.Cmp(decimal.Zero) < 0 {
			cashType = helper.TransactionResettleDeduction
		}

		tx, err := meta.MerchantDB.Begin()
		if err != nil {
			res.Status = "999"
			res.Msg = "System error"
			return res
		}

		var transIDs []string
		ts := time.Now()
		err = walletIBCTransaction(tx, mb, transIDs, v.RefId,
			"", helper.IBCSB, "settled", amount.Abs(), cashType, ts.UnixMilli())
		if err != nil {
			if strings.HasPrefix(err.Error(), "Error 1062") {
				res.Status = "1"
				res.Msg = "Repeated trade"
				return res
			}

			res.Status = "999"
			res.Msg = "System error"
			return res
		}

		err = tx.Commit()
		if err != nil {
			res.Status = "999"
			res.Msg = "System error"
			return res
		}

		//这个放到 消息队列 去处理
		record := g.Record{
			"flag":             "1",
			"valid_bet_amount": netAmount.Abs().String(),
			"net_amount":       netAmount.String(),
			"settle_time":      fmt.Sprintf("%d", ts.UnixMilli()),
			"api_settle_time":  fmt.Sprintf("%d", settleTime.UnixMilli()),
			"resettle":         "1",
		}
		query, _, _ := dialect.Update("tbl_game_record").Set(record).Where(g.Ex{"bill_no": fmt.Sprintf("%d", v.TxId)}).ToSQL()
		fmt.Println(query)
		_, err = meta.MerchantDB.Exec(query)
		if err != nil {
			res.Status = "999"
			res.Msg = "System error"
			return res
		}
	}

	return res
}

// IBCUnsettle 取消结算
func IBCUnsettle(ctx *fasthttp.RequestCtx) IBCResult {

	res := IBCResult{
		Status: "0",
		Msg:    "success",
	}
	body, err := unpack(ctx)
	if err != nil {
		res.Status = "506"
		res.Msg = "Parsing error"
		return res
	}

	fmt.Println(string(body))
	req := IBCUnsettleReqData{}
	err = helper.JsonUnmarshal(body, &req)
	if err != nil {
		res.Status = "506"
		res.Msg = "Parsing error"
	}

	res = ibcCheck(req.Key)
	if res.Status != "0" {
		return res
	}

	for _, v := range req.Message.Txns {

		userId := v.UserId
		mb, err := GetMemberByPUID(userId)
		if err != nil {
			res.Status = "203"
			res.Msg = "User not exist"
			return res
		}

		updateTime, err := time.ParseInLocation("2006-01-02T15:04:05.999-04:00", v.UpdateTime, loc)
		if err != nil {
			res.Msg = "winlostDate error"
			res.Status = "306"
			return res
		}

		status, transIDs, _, err := walletBetStatus(v.RefId, "", helper.IBCSB, "rollback")
		if err != nil && err != sql.ErrNoRows {
			res.Status = "999"
			res.Msg = "System error"
			return res
		}

		if err == sql.ErrNoRows {
			res.Status = "504"
			res.Msg = "Order not exist"
			return res
		}

		if status == "running" {
			return res
		}

		if status != "settled" {
			res.Status = "999"
			res.Msg = "System error"
			return res
		}

		creditAmount := decimal.NewFromFloat(v.CreditAmount)
		debitAmount := decimal.NewFromFloat(v.DebitAmount)
		amount := debitAmount.Sub(creditAmount)

		tx, err := meta.MerchantDB.Begin()
		if err != nil {
			res.Status = "999"
			res.Msg = "System error"
			return res
		}

		ts := time.Now()
		err = walletIBCTransaction(tx, mb, transIDs,
			v.RefId, "", helper.IBCSB, "running", amount.Abs(), helper.TransactionCancelPayout, ts.UnixMilli())
		if err != nil {
			if strings.HasPrefix(err.Error(), "Error 1062") {
				res.Status = "1"
				res.Msg = "Repeated trade"
				return res
			}

			res.Status = "999"
			res.Msg = "System error"
			return res
		}

		err = tx.Commit()
		if err != nil {
			res.Status = "999"
			res.Msg = "System error"
			return res
		}

		//这个放到 消息队列 去处理
		record := g.Record{
			"flag":             "0",
			"valid_bet_amount": "0.00",
			"net_amount":       "0.00",
			"settle_time":      "0",
			"api_settle_time":  fmt.Sprintf("%d", updateTime.UnixMilli()),
		}
		query, _, _ := dialect.Update("tbl_game_record").Set(record).Where(g.Ex{"bill_no": fmt.Sprintf("%d", v.TxId)}).ToSQL()
		fmt.Println(query)
		_, err = meta.MerchantDB.Exec(query)
		if err != nil {
			res.Status = "999"
			res.Msg = "System error"
			return res
		}
	}

	return res
}

// IBCBetParlay 过关投注
func IBCBetParlay(ctx *fasthttp.RequestCtx) IBCResult {

	res := IBCResult{
		Status: "0",
		Msg:    "success",
	}
	body, err := unpack(ctx)
	if err != nil {
		res.Status = "506"
		res.Msg = "Parsing error"
		return res
	}

	fmt.Println(string(body))
	req := IBCPlaceBetParlayReqData{}
	err = helper.JsonUnmarshal(body, &req)
	if err != nil {
		res.Status = "506"
		res.Msg = "Parsing error"
	}

	res = ibcCheck(req.Key)
	if res.Status != "0" {
		return res
	}

	userId := req.Message.UserId
	mb, err := GetMemberByPUID(userId)
	if err != nil {
		res.Status = "203"
		res.Msg = "User not exist"
		return res
	}

	totalBetAmount := decimal.NewFromFloat(req.Message.TotalBetAmount)
	//判断余额
	balance, err := getBalance(mb, 2)
	if err != nil {
		res.Msg = "balance error"
		res.Status = "505"
		return res
	}

	//余额不足
	if balance.Cmp(totalBetAmount) < 0 {
		res.Msg = "Insufficient quota"
		res.Status = "502"
		return res
	}

	if len(req.Message.Txns) < 1 {
		res.Status = "506"
		res.Msg = "txns null"
		return res
	}

	handicapType := "HongKong Odds"
	betTime, err := time.ParseInLocation("2006-01-02T15:04:05.999-04:00", req.Message.UpdateTime, loc)
	if err != nil {
		res.Msg = "winlostDate error"
		res.Status = "306"
		return res
	}

	//err = BetLock(mem.Username)
	//if err != nil {
	//	res.Status = "905"
	//	res.Msg = "System busy"
	//	return res
	//}
	//defer BetUnlock(mem.Username)

	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		res.Status = "999"
		res.Msg = "System error"
		return res
	}

	if len(req.Message.Txns) < 1 {
		res.Status = "506"
		res.Msg = "txns null"
		return res
	}

	if len(req.Message.TicketDetail) < 1 {
		res.Status = "506"
		res.Msg = "ticketDetail null"
		return res
	}

	var (
		gms  []GameRecord
		txns []IBCResultTxn
	)
	sportType := req.Message.TicketDetail[0].SportType
	for _, v := range req.Message.Txns {

		platform := "IBCSB"
		if sportType == 43 {
			platform = "IBCESB"
		}
		pid := IBCPlatformPID[platform]
		gameType, _ := IBCGameType[platform]
		apiName, _ := IBCAPIName[platform]

		betAmount := decimal.NewFromFloat(v.BetAmount)
		refId := v.RefId
		if refId == "" {
			res.Msg = "refId error"
			res.Status = "101"
			return res
		}

		status, _, _, err := walletBetStatus(refId, "", helper.IBCSB, "deduct")
		if err != nil && err != sql.ErrNoRows {
			res.Status = "999"
			res.Msg = "System error"
			return res
		} else if err == nil {
			if status == "running" {
				res.Status = "1"
				res.Msg = "Repeated trade"
				return res
			}
		}

		if betAmount.Cmp(decimal.Zero) == 1 {

			var transIDs []string
			err = walletIBCTransaction(tx, mb, transIDs,
				refId, "", helper.IBCSB, "running", betAmount, helper.TransactionBet, ctx.Time().UnixMilli())
			if err != nil {
				if strings.HasPrefix(err.Error(), "Error 1062") {
					res.Status = "1"
					res.Msg = "Repeated trade"
					return res
				}

				res.Status = "999"
				res.Msg = "System error"
				return res
			}
		}

		var result string
		if len(req.Message.TicketDetail) > 0 {
			handicap := fmt.Sprintf("%s,%s", req.Message.TicketDetail[0].BetTypeName, req.Message.TicketDetail[0].BetTeam)
			result = fmt.Sprintf(`match id:%d|sport type name:%s|bet type name:%s|handicap:%s|league name:%s|home name:%s|away name:%s`,
				req.Message.TicketDetail[0].MatchId, req.Message.TicketDetail[0].SportTypeName, req.Message.TicketDetail[0].BetTypeName, handicap,
				req.Message.TicketDetail[0].LeagueName, req.Message.TicketDetail[0].HomeName, req.Message.TicketDetail[0].AwayName)
		}
		var odds float64
		if len(v.Detail) > 0 {
			odds, _ = v.Detail[0]["odds"].(float64)
		}

		gm := GameRecord{
			BillNo:         refId,
			Result:         result,
			GameName:       "Parlay",
			GameCode:       "",
			PlayType:       v.ParlayType,
			RowId:          platform + refId,
			ApiBillNo:      refId,
			MainBillNo:     "",
			HandicapType:   handicapType,
			Handicap:       "0",
			Odds:           odds,
			StartTime:      ctx.Time().UnixMilli(),
			Resettle:       0,
			Presettle:      0,
			TopName:        mb.TopName,
			TopUid:         mb.TopID,
			ParentName:     mb.ParentName,
			ParentUid:      mb.ParentID,
			GrandName:      mb.GrandName,
			GrandID:        mb.GrandID,
			GreatGrandName: mb.GreatGrandName,
			GreatGrandID:   mb.GreatGrandID,
			Uid:            mb.Uid,
			Name:           mb.Username,
			Prefix:         meta.Prefix,
			CreatedAt:      ctx.Time().UnixMilli(),
			UpdatedAt:      ctx.Time().UnixMilli(),
			BetTime:        ctx.Time().UnixMilli(),
			SettleTime:     0,
			ApiBetTime:     betTime.UnixMilli(),
			ApiSettleTime:  0,
			PlayerName:     meta.Prefix + mb.Username,
			GameType:       gameType,
			ApiType:        pid,
			ApiName:        apiName,
			ValidBetAmount: 0,
			NetAmount:      0,
		}
		gm.BetAmount, _ = betAmount.Abs().Float64()
		gms = append(gms, gm)
		txns = append(txns, IBCResultTxn{
			RefId:        refId,
			LicenseeTxId: refId,
		})
	}

	err = tx.Commit()
	if err != nil {
		res.Status = "999"
		res.Msg = "System error"
		return res
	}

	query, _, _ := dialect.Insert("tbl_game_record").Rows(&gms).ToSQL()
	fmt.Println(query)
	_, err = meta.MerchantDB.Exec(query)
	if err != nil {
		res.Status = "999"
		res.Msg = "System error"
		return res
	}

	res.Txns = txns

	return res
}

// IBCConfirmParlay 过关投注确认
func IBCConfirmParlay(ctx *fasthttp.RequestCtx) IBCConfirmBetParlayResult {

	res := IBCConfirmBetParlayResult{
		Status: "0",
		Msg:    "success",
	}
	body, err := unpack(ctx)
	if err != nil {
		res.Status = "506"
		res.Msg = "Parsing error"
		return res
	}

	fmt.Println(string(body))
	req := IBCConfirmBetParlayReqData{}
	err = helper.JsonUnmarshal(body, &req)
	if err != nil {
		res.Status = "506"
		res.Msg = "Parsing error"
	}

	//res = ibcCheck(req.Key)
	//if res.Status != "0" {
	//	return res
	//}

	userId := req.Message.UserId
	mb, err := GetMemberByPUID(userId)
	if err != nil {
		res.Status = "203"
		res.Msg = "User not exist"
		return res
	}

	if len(req.Message.Txns) < 1 {
		res.Status = "506"
		res.Msg = "txns null"
		return res
	}

	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		res.Status = "999"
		res.Msg = "System error"
		return res
	}

	for _, v := range req.Message.Txns {

		ex := g.Ex{
			"bill_no":   v.RefId,
			"username":  mb.Username,
			"cash_type": helper.TransactionBet,
		}
		//判断是否有订单
		ok, err := walletTransactionExist(ex)
		if err != nil {
			res.Status = "999"
			res.Msg = "System error"
			return res
		}

		if !ok {
			res.Status = "504"
			res.Msg = "Order not exist"
			return res
		}

		fmt.Println(v)
		odds := decimal.NewFromFloat(v.Odds)
		if odds.Cmp(decimal.Zero) > 0 {
			var rowID string
			query, _, _ := dialect.From("tbl_game_record").
				Select("row_id").Where(g.Ex{"api_bill_no": v.RefId, "flag": 0}).ToSQL()
			fmt.Println(query)
			err = meta.MerchantDB.Get(&rowID, query)
			if err != nil {
				res.Status = "999"
				res.Msg = "System error"
				return res
			}

			record := g.Record{
				"odds":    odds,
				"bill_no": fmt.Sprintf("%d", v.TxId),
			}
			t := dialect.Update("tbl_game_record")
			query, _, _ = t.Where(g.Ex{"row_id": rowID}).Set(record).ToSQL()
			fmt.Println(query)
			_, err = meta.MerchantDB.Exec(query)
			if err != nil {
				res.Status = "999"
				res.Msg = "System error"
				return res
			}
		}
	}

	err = tx.Commit()
	if err != nil {
		res.Status = "999"
		res.Msg = "System error"
		return res
	}

	return res
}

// IBCAdjustBalance 调整
func IBCAdjustBalance(ctx *fasthttp.RequestCtx) IBCResult {

	res := IBCResult{
		Status: "0",
		Msg:    "success",
	}
	body, err := unpack(ctx)
	if err != nil {
		res.Status = "506"
		res.Msg = "Parsing error"
		return res
	}

	fmt.Println(string(body))
	req := IBCAdjustBalanceReq{}
	err = helper.JsonUnmarshal(body, &req)
	if err != nil {
		res.Status = "506"
		res.Msg = "Parsing error"
	}

	userId := req.Message.UserId
	mb, err := GetMemberByPUID(userId)
	if err != nil {
		res.Status = "203"
		res.Msg = "User not exist"
		return res
	}

	refId := req.Message.RefId
	if refId == "" {
		res.Msg = "refId error"
		res.Status = "101"
		return res
	}

	balance, _ := getBalance(mb, 2)
	ex := g.Ex{
		"bill_no":   refId,
		"cash_type": []int{helper.TransactionAdjustPlus, helper.TransactionAdjustDiv},
	}
	_, err = walletTransactionFindOne(ex)
	if err != nil && err != sql.ErrNoRows {
		res.Status = "999"
		res.Msg = "System error"
		return res
	}

	creditAmount := decimal.NewFromFloat(req.Message.BalanceInfo.CreditAmount)
	debitAmount := decimal.NewFromFloat(req.Message.BalanceInfo.DebitAmount)
	amount := creditAmount
	cashType := helper.TransactionAdjustPlus
	if debitAmount.GreaterThan(creditAmount) {
		cashType = helper.TransactionAdjustDiv
		amount = debitAmount
		if debitAmount.GreaterThan(balance) {
			res.Msg = "Insufficient quota"
			res.Status = "502"
			return res
		}
	}
	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		res.Status = "999"
		res.Msg = "System error"
		return res
	}

	_, err = walletAdjustTransaction(tx, mb, refId, "", helper.IBCSB, balance, amount.Abs(), cashType, ctx.Time())
	if err != nil {
		if strings.HasPrefix(err.Error(), "Error 1062") {
			res.Status = "1"
			res.Msg = "Repeated trade"
			return res
		}

		res.Status = "999"
		res.Msg = "System error"
		return res
	}

	err = tx.Commit()
	if err != nil {
		res.Status = "999"
		res.Msg = "System error"
		return res
	}

	return res
}
