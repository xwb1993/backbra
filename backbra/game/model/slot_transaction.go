package model

import (
	"database/sql"
	"fmt"
	"game/contrib/helper"
	ryrpc "game/rpc"
	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"
	"strconv"
	"strings"
	"time"
)

// 中心钱包帐变
func walletBetNSettleTransaction(tx *sql.Tx, mb ryrpc.TblMemberBase, billNo, operationNo, platformID string, balance, betAmount, amount decimal.Decimal, ts time.Time) (winMoney, validAmount float64, err error) {
	betAfterAmount := balance.Sub(betAmount)
	//if betAmount.Cmp(decimal.Zero) > 0 {
	trans := memberTransaction{
		ID:           helper.GenId(),
		UID:          mb.Uid,
		PlatformID:   platformID,
		Username:     mb.Username,
		BillNo:       billNo,
		CashType:     helper.TransactionBet,
		Amount:       betAmount.String(),
		BeforeAmount: balance.String(),
		AfterAmount:  betAfterAmount.String(),
		OperationNo:  operationNo,
		CreatedAt:    ts.UnixMilli(),
		Remark:       "settled",
		Tester:       mb.Tester,
	}
	query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		//记录日志
		if strings.HasPrefix(err.Error(), "Error 1062") {
			PushLog(nil, "", "duplicate", "", 200, nil, err)
		}

		return winMoney, validAmount, err
	}
	//}
	var value float64
	var discount, settleAmount, validBetAmount decimal.Decimal
	config := ryrpc.TblGameConfig{}
	ex := g.Ex{}
	if platformID == helper.PG {
		ex["cfg_type"] = helper.PGFEE
		query, _, _ = dialect.From("tbl_game_config").Select(colsGameConfig...).Where(ex).ToSQL()
		fmt.Println(query)
		err = meta.MerchantDB.Get(&config, query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			return winMoney, validAmount, err
		}
		if amount.Sub(betAmount).Cmp(decimal.Zero) > 0 {
			value, _ = strconv.ParseFloat(config.CfgValue, 64)
			discount = decimal.NewFromFloat(value).Div(decimal.NewFromInt(100))
			fmt.Println("discount===", discount)
			//扣税金额
			settleAmount = amount.Sub(betAmount).Mul(discount)
		}
	}

	//扣完税后赢的钱
	profitAmount := amount.Sub(settleAmount)
	//扣完税和本金后赢的钱
	winAmount := profitAmount.Sub(betAmount)
	fmt.Println("settleAmount===", settleAmount)
	fmt.Println("profitAmount===", profitAmount)
	fmt.Println("winAmount===", winAmount)

	cmp := winAmount.Cmp(decimal.Zero)
	if cmp != 0 {
		operate := "-"
		if cmp == 1 {
			operate = "+"
		}
		record := g.Record{
			"brl": g.L(fmt.Sprintf("brl%s%s", operate, winAmount.Abs().String())),
		}
		query, _, _ = dialect.Update("tbl_member_balance").Set(record).Where(g.Ex{"uid": mb.Uid}).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			return winMoney, validAmount, err
		}
	}
	if cmp == 0 {
		validAmount = 0
	}

	if profitAmount.Cmp(decimal.Zero) > 0 {
		settleAfterAmount := betAfterAmount.Add(profitAmount)
		trans := memberTransaction{
			ID:           helper.GenId(),
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       billNo,
			CashType:     helper.TransactionPayout,
			Amount:       profitAmount.String(),
			BeforeAmount: betAfterAmount.String(),
			AfterAmount:  settleAfterAmount.String(),
			OperationNo:  operationNo,
			CreatedAt:    ts.Add(1 * time.Millisecond).UnixMilli(),
			Remark:       "settled",
			Tester:       mb.Tester,
		}
		query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			//记录日志
			if strings.HasPrefix(err.Error(), "Error 1062") {
				PushLog(nil, "", "duplicate", "", 200, nil, err)
			}

			return winMoney, validAmount, err
		}
	}
	if betAmount.Cmp(decimal.Zero) > 0 {
		if winAmount.Cmp(decimal.Zero) > 0 {
			if mb.WinFlowMultiple != "0" {
				value, _ = strconv.ParseFloat(mb.WinFlowMultiple, 64)
				discount = decimal.NewFromFloat(value).Div(decimal.NewFromInt(100))
				validBetAmount = winAmount.Mul(discount)
			} else {
				ex["cfg_type"] = helper.TotalWinFlowMultiple
				query, _, _ = dialect.From("tbl_game_config").Select(colsGameConfig...).Where(ex).ToSQL()
				fmt.Println(query)
				err = meta.MerchantDB.Get(&config, query)
				if err != nil {
					fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
					return winMoney, validAmount, err
				}
				value, _ = strconv.ParseFloat(config.CfgValue, 64)
				discount = decimal.NewFromFloat(value).Div(decimal.NewFromInt(100))
				validBetAmount = winAmount.Mul(discount)
			}
		} else {
			if mb.WinFlowMultiple != "0" {
				value, _ = strconv.ParseFloat(mb.LoseFlowMultiple, 64)
				discount = decimal.NewFromFloat(value).Div(decimal.NewFromInt(100))
				validBetAmount = winAmount.Mul(discount).Abs()
			} else {
				ex["cfg_type"] = helper.TotalLoseFlowMultiple
				query, _, _ = dialect.From("tbl_game_config").Select(colsGameConfig...).Where(ex).ToSQL()
				fmt.Println(query)
				err = meta.MerchantDB.Get(&config, query)
				if err != nil {
					fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
					return winMoney, validAmount, err
				}
				value, _ = strconv.ParseFloat(config.CfgValue, 64)
				discount = decimal.NewFromFloat(value).Div(decimal.NewFromInt(100))
				validBetAmount = winAmount.Mul(discount).Abs()
			}
		}
	}

	winMoney, _ = profitAmount.Float64()
	validAmount, _ = validBetAmount.Float64()
	if validAmount > 0 {
		err = FlowBonusTransaction(tx, mb, validAmount)
		if err != nil {
			return winMoney, validAmount, err
		}
	}

	//// 3、更新余额
	//br := g.Record{
	//	"deposit_lock_amount": g.L(fmt.Sprintf("deposit_lock_amount-%f", validAmount)),
	//}
	//query, _, _ = dialect.Update("tbl_member_balance").Set(br).Where(g.Ex{"uid": mb.Uid}).ToSQL()
	//fmt.Println(query)
	//_, err = tx.Exec(query)
	//if err != nil {
	//	fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
	//	_ = tx.Rollback()
	//	//记录日志
	//	if strings.HasPrefix(err.Error(), "Error 1062") {
	//		PushLog(nil, "", "duplicate", "", 200, nil, err)
	//	}
	//}

	return winMoney, validAmount, nil
}

// TransactionAdjustPlus
// TransactionAdjustDiv
// 中心钱包帐变
func walletBetNSettleAdjustTransaction(tx *sql.Tx, mb ryrpc.TblMemberBase, billNo, operationNo, platformID, remark string, amount decimal.Decimal, cashType int, createAt int64) error {

	balance, err := getBalance(mb, 2)
	if err != nil {
		_ = tx.Rollback()
		return err
	}

	// 结算为输的注单，只修改状态，不产生帐变，场馆业务代码自行控制
	afterAmount := balance.Add(amount)
	if cashType == helper.TransactionAdjustDiv {
		afterAmount = balance.Sub(amount)
	}

	id := helper.GenId()
	if amount.Cmp(decimal.Zero) > 0 {
		trans := memberTransaction{
			ID:           id,
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       billNo,
			CashType:     cashType,
			Amount:       amount.String(),
			BeforeAmount: balance.String(),
			AfterAmount:  afterAmount.String(),
			OperationNo:  operationNo,
			CreatedAt:    createAt,
			Remark:       remark,
			Tester:       mb.Tester,
		}
		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			//记录日志
			if strings.HasPrefix(err.Error(), "Error 1062") {
				PushLog(nil, "", "duplicate", "", 200, nil, err)
			}

			return err
		}

		ex := g.Ex{
			"uid": mb.Uid,
		}
		operate := "+"
		if cashType == helper.TransactionAdjustDiv {
			operate = "-"
		}
		record := g.Record{
			"brl": g.L(fmt.Sprintf("brl%s%s", operate, amount.String())),
		}
		query, _, _ = dialect.Update("tbl_member_balance").Set(record).Where(ex).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			return err
		}
	}

	return nil
}

// TransactionBetCancel
// TransactionSettledBetCancel
// 中心钱包帐变
func walletBetNSettleCancelTransaction(tx *sql.Tx, mb ryrpc.TblMemberBase, transIDs []string, billNo, operationNo, platformID, remark string, amount decimal.Decimal, cashType int, createAt int64) error {

	balance, err := getBalance(mb, 2)
	if err != nil {
		_ = tx.Rollback()
		return err
	}

	// 结算为输的注单，只修改状态，不产生帐变，场馆业务代码自行控制
	afterAmount := balance.Add(amount)
	if cashType == helper.TransactionSettledBetCancel {
		afterAmount = balance.Sub(amount)
	}

	id := helper.GenId()
	if len(transIDs) > 0 {
		ex := g.Ex{
			"id": transIDs,
		}
		operationNo += ":"
		operationNo += id
		record := g.Record{
			"remark": "void",
		}
		query, _, _ := dialect.Update("tbl_balance_transaction").Set(record).Where(ex).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			return err
		}
	}

	// cq9结算为0的帐变也要存储
	if amount.Cmp(decimal.Zero) > 0 {
		trans := memberTransaction{
			ID:           id,
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       billNo,
			CashType:     cashType,
			Amount:       amount.String(),
			BeforeAmount: balance.String(),
			AfterAmount:  afterAmount.String(),
			OperationNo:  operationNo,
			CreatedAt:    createAt,
			Remark:       remark,
			Tester:       mb.Tester,
		}
		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			//记录日志
			if strings.HasPrefix(err.Error(), "Error 1062") {
				PushLog(nil, "", "duplicate", "", 200, nil, err)
			}

			return err
		}
	}

	if amount.Cmp(decimal.Zero) > 0 {

		ex := g.Ex{
			"uid": mb.Uid,
		}
		operate := "+"
		if cashType == helper.TransactionSettledBetCancel {
			operate = "-"
		}
		record := g.Record{
			"brl": g.L(fmt.Sprintf("brl%s%s", operate, amount.String())),
		}
		query, _, _ := dialect.Update("tbl_member_balance").Set(record).Where(ex).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			return err
		}
	}

	return nil
}

// FlowBonusTransaction 给上面三级流水奖金
func FlowBonusTransaction(tx *sql.Tx, mb ryrpc.TblMemberBase, validAmount float64) error {

	tbc, err := getBonusConfig("invite")
	if err != nil {
		return err
	}
	var parent, grand, greatGrand ryrpc.TblMemberBase
	if mb.ParentID != "0" {
		parent, err = MemberFindByUid(mb.ParentID)
		if err != nil {
			return err
		}
	}
	if mb.GrandID != "0" {
		grand, err = MemberFindByUid(mb.GrandID)
		if err != nil {
			return err
		}
	}
	if mb.GreatGrandID != "0" {
		greatGrand, err = MemberFindByUid(mb.GreatGrandID)
		if err != nil {
			return err
		}
	}
	//上级流水奖金
	if mb.ParentID != "0" && parent.CanBonus == 1 {
		flowBonus := tbc.LvlOneRebate * validAmount
		exParent := g.Ex{
			"uid": mb.ParentID,
		}
		brParent := g.Record{
			"brl":                 g.L(fmt.Sprintf("brl+%f", flowBonus)),
			"deposit_lock_amount": g.L(fmt.Sprintf("deposit_lock_amount+%f", flowBonus)),
		}
		query, _, _ := dialect.Update("tbl_member_balance").Set(brParent).Where(exParent).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			return err
		}
		parentBalance, err := MemberBalanceFindOne(mb.ParentID)
		if err != nil {
			return err
		}
		parentBrl, err := decimal.NewFromString(parentBalance.Brl)
		if err != nil {
			return err
		}
		balAfter := parentBrl.Add(decimal.NewFromFloat(flowBonus))
		// 上级新增账变记录
		parentTrans := memberTransaction{
			AfterAmount:  balAfter.String(),
			Amount:       fmt.Sprintf("%f", flowBonus),
			BeforeAmount: parentBalance.Brl,
			BillNo:       helper.GenId(),
			CreatedAt:    time.Now().UnixMilli(),
			ID:           helper.GenId(),
			CashType:     helper.TransactionFlowBonus,
			UID:          mb.ParentID,
			Username:     mb.ParentName,
			Remark:       "flow bonus",
		}

		query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(parentTrans).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			return err
		}
	}

	//上上级流水奖金
	if mb.GrandID != "0" && grand.CanBonus == 1 {
		flowBonus := tbc.LvlTwoRebate * validAmount
		exGrand := g.Ex{
			"uid": mb.GrandID,
		}
		brGrand := g.Record{
			"brl":                 g.L(fmt.Sprintf("brl+%f", flowBonus)),
			"deposit_lock_amount": g.L(fmt.Sprintf("deposit_lock_amount+%f", flowBonus)),
		}
		query, _, _ := dialect.Update("tbl_member_balance").Set(brGrand).Where(exGrand).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			return err
		}
		grandBalance, err := MemberBalanceFindOne(mb.GrandID)
		if err != nil {
			return err
		}
		grandBrl, err := decimal.NewFromString(grandBalance.Brl)
		if err != nil {
			return err
		}
		balAfter := grandBrl.Add(decimal.NewFromFloat(flowBonus))
		// 上级新增账变记录
		grandTrans := memberTransaction{
			AfterAmount:  balAfter.String(),
			Amount:       fmt.Sprintf("%f", flowBonus),
			BeforeAmount: grandBalance.Brl,
			BillNo:       helper.GenId(),
			CreatedAt:    time.Now().UnixMilli(),
			ID:           helper.GenId(),
			CashType:     helper.TransactionFlowBonus,
			UID:          mb.GrandID,
			Username:     mb.GrandName,
			Remark:       "flow bonus",
		}

		query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(grandTrans).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			return err
		}
	}

	//上上上级流水奖金
	if mb.GreatGrandID != "0" && greatGrand.CanBonus == 1 {
		flowBonus := tbc.LvlThreeRebate * validAmount
		exGreatGrand := g.Ex{
			"uid": mb.GreatGrandID,
		}
		brGreatGrand := g.Record{
			"brl":                 g.L(fmt.Sprintf("brl+%f", flowBonus)),
			"deposit_lock_amount": g.L(fmt.Sprintf("deposit_lock_amount+%f", flowBonus)),
		}
		query, _, _ := dialect.Update("tbl_member_balance").Set(brGreatGrand).Where(exGreatGrand).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			return err
		}
		greatGrandBalance, err := MemberBalanceFindOne(mb.GreatGrandID)
		if err != nil {
			return err
		}
		greatGrandBrl, err := decimal.NewFromString(greatGrandBalance.Brl)
		if err != nil {
			return err
		}
		balAfter := greatGrandBrl.Add(decimal.NewFromFloat(flowBonus))
		// 上级新增账变记录
		greatGrandTrans := memberTransaction{
			AfterAmount:  balAfter.String(),
			Amount:       fmt.Sprintf("%f", flowBonus),
			BeforeAmount: greatGrandBalance.Brl,
			BillNo:       helper.GenId(),
			CreatedAt:    time.Now().UnixMilli(),
			ID:           helper.GenId(),
			CashType:     helper.TransactionFlowBonus,
			UID:          mb.GreatGrandID,
			Username:     mb.GreatGrandName,
			Remark:       "flow bonus",
		}

		query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(greatGrandTrans).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			return err
		}
	}

	return nil
}
