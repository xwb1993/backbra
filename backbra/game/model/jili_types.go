package model

import (
	"game/contrib/helper"
)

var (
	jlPlatform = map[string]string{
		helper.JILI:      "JILI",
		helper.JILIPoker: "JILIPoker",
		helper.JILIFISH:  "JILIFISH",
	}
	jlGameName = map[string]string{
		"1":   "Royal Fishing",
		"119": "All-star Fishing",
		"20":  "Bombing Fishing",
		"212": "Dinosaur Tycoon II",
		"32":  "Jackpot Fishing",
		"42":  "Dinosaur Tycoon",
		"60":  "Dragon Fortune",
		"71":  "Boom Legend",
		"74":  "Mega Fishing",
		"82":  "Happy Fishing",
		"100": "Super Rich",
		"101": "Medusa",
		"102": "RomaX",
		"103": "Golden Empire",
		"106": "TWIN WINS",
		"108": "Magic Lamp",
		"109": "Fortune Gems",
		"110": "Ali Baba",
		"115": "Agent Ace",
		"116": "Happy Taxi",
		"126": "Bone Fortune",
		"130": "Thor X",
		"134": "Mega Ace",
		"135": "Mayan Empire",
		"136": "Samba",
		"137": "Gold Rush",
		"142": "Bonus Hunter",
		"144": "JILI CAISHEN",
		"145": "Neko Fortune",
		"146": "World Cup",
		"153": "Crazy Pusher",
		"16":  "Jungle King",
		"164": "Pirate Queen",
		"166": "Wild Racer",
		"17":  "Shanghai Beauty",
		"176": "Master Tiger",
		"181": "Wild Ace",
		"183": "Golden Joker",
		"2":   "Chin Shi Huang",
		"21":  "Fa Fa Fa",
		"223": "Fortune Gems 2",
		"23":  "Candy Baby",
		"27":  "SevenSevenSeven",
		"30":  "Bubble Beauty",
		"33":  "FortunePig",
		"35":  "Crazy777",
		"36":  "Bao boon chin",
		"38":  "Fengshen",
		"4":   "God Of Martial",
		"40":  "Crazy FaFaFa",
		"43":  "XiYangYang",
		"45":  "Golden Bank",
		"46":  "Dragon Treasure",
		"47":  "Charge Buffalo",
		"48":  "Lucky Goldbricks",
		"49":  "Super Ace",
		"5":   "Hot Chilli",
		"51":  "Money Coming",
		"58":  "Golden Queen",
		"6":   "Fortune Tree",
		"76":  "Party Night",
		"77":  "Boxing King",
		"78":  "Secret Treasure",
		"85":  "Pharaoh Treasure",
		"87":  "Book of Gold",
		"9":   "War Of Dragons",
		"91":  "Lucky Coming",
		"92":  "Crazy Hunter",
		"111": "Number King",
		"112": "Journey West M",
		"113": "Poker King",
		"118": "Big Small",
		"122": "iRich Bingo",
		"123": "Dragon & Tiger",
		"124": "7up7down",
		"125": "Sic Bo",
		"139": "Fortune Bingo",
		"148": "Bingo Carnaval",
		"149": "Calaca Bingo",
		"150": "Lucky Bingo",
		"151": "Super Bingo",
		"152": "Baccarat",
		"173": "West Hunter Bingo",
		"174": "Jackpot Bingo",
		"177": "Bingo Adventure",
		"178": "Go Goal BIngo",
		"179": "Win Drop",
		"182": "Golden Land",
		"197": "Color Game",
		"200": "PAPPU",
		"61":  "Dragon & Tiger",
		"62":  "Dice",
		"63":  "7 UP-DOWN",
		"66":  "Lucky Number",
	}
)

type JLRegResult struct {
	ErrorCode int    `json:"ErrorCode"`
	Message   string `json:"Message"`
	Data      string `json:"Data"`
}

type JLRsp struct {
	ErrorCode int     `json:"errorCode"`
	Message   string  `json:"message"`
	Username  string  `json:"username"`
	Currency  string  `json:"currency"`
	Balance   float64 `json:"balance"`
}

type JLAuthReq struct {
	ReqId string `json:"reqId"`
	Token string `json:"token"`
}

type JLBetReq struct {
	ReqId         string  `json:"reqId"`
	Token         string  `json:"token"`
	Currency      string  `json:"currency"`
	Game          int     `json:"game"`
	Round         int64   `json:"round"`
	WagersTime    int64   `json:"wagersTime"`
	BetAmount     float64 `json:"betAmount"`
	WinloseAmount float64 `json:"winloseAmount"`
	IsFreeRound   bool    `json:"isFreeRound"`
	UserId        string  `json:"userId"`
	TransactionId int64   `json:"transactionId"`
	Platform      string  `json:"platform"`
	StatementType int     `json:"statementType"`
	GameCategory  int     `json:"gameCategory"` // 1电子 2棋牌 5捕鱼
}

type JLCancelBetReq struct {
	ReqId         string  `json:"reqId"`
	Token         string  `json:"token"`
	Currency      string  `json:"currency"`
	Game          int     `json:"game"`
	Round         int64   `json:"round"`
	UserId        string  `json:"userId"`
	BetAmount     float64 `json:"betAmount"`
	WinloseAmount float64 `json:"winloseAmount"`
}

type JLSessionBetReq struct {
	ReqId           string  `json:"reqId"`
	Token           string  `json:"token"`
	Currency        string  `json:"currency"`
	Game            int     `json:"game"`
	Round           int64   `json:"round"`
	Offline         bool    `json:"offline"`
	WagersTime      int64   `json:"wagersTime"`
	BetAmount       float64 `json:"betAmount"`
	WinloseAmount   float64 `json:"winloseAmount"`
	SessionId       int64   `json:"sessionId"`
	Type            int     `json:"type"`
	UserId          string  `json:"userId"`
	Turnover        float64 `json:"turnover"`
	Preserve        float64 `json:"preserve"`
	Platform        string  `json:"platform"`
	SessionTotalBet float64 `json:"sessionTotalBet"`
	StatementType   int     `json:"statementType"`
}

type JLCancelSessionBetReq struct {
	ReqId         string  `json:"reqId"`
	Token         string  `json:"token"`
	Currency      string  `json:"currency"`
	Game          int     `json:"game"`
	Round         int64   `json:"round"`
	Offline       bool    `json:"offline"`
	BetAmount     float64 `json:"betAmount"`
	WinloseAmount float64 `json:"winloseAmount"`
	SessionId     int64   `json:"sessionId"`
	Type          int     `json:"type"`
	UserId        string  `json:"userId"`
	Preserve      float64 `json:"preserve"`
}
