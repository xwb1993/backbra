package model

import (
	"database/sql"
	"fmt"
	"game/contrib/helper"
	ryrpc "game/rpc"
	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"
	"strings"
	"time"
)

// evo游戏奖金
func walletEVOPrizeTransaction(tx *sql.Tx, mb ryrpc.TblMemberBase, billNo, operationNo, platformID string, balance, amount decimal.Decimal, ts time.Time) error {

	if amount.GreaterThan(decimal.Zero) {
		betAfterAmount := balance.Add(amount)
		trans := memberTransaction{
			ID:           helper.GenId(),
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       billNo,
			CashType:     helper.TransactionEVOPrize,
			Amount:       amount.String(),
			BeforeAmount: balance.String(),
			AfterAmount:  betAfterAmount.String(),
			OperationNo:  operationNo,
			CreatedAt:    ts.UnixMilli(),
			Remark:       "prize",
			Tester:       mb.Tester,
		}
		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		fmt.Println(query)
		_, err := tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			//记录日志
			if strings.HasPrefix(err.Error(), "Error 1062") {
				PushLog(nil, "", "duplicate", "", 200, nil, err)
			}

			return err
		}

		record := g.Record{
			"brl": g.L(fmt.Sprintf("brl+%s", amount.String())),
		}
		query, _, _ = dialect.Update("tbl_member_balance").Set(record).Where(g.Ex{"uid": mb.Uid}).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			return err
		}
	}

	return nil
}

// 提前收到的取消投注请求（在投注请求之前）
func walletAdvanceCancelTransaction(mb ryrpc.TblMemberBase, billNo, operationNo, platformID string, amount decimal.Decimal, createdAt int64) error {

	ex := g.Ex{
		"bill_no":      billNo,
		"operation_no": operationNo,
		"cash_type":    helper.TransactionBetCancel,
	}
	var id string
	query, _, _ := dialect.From("tbl_balance_transaction").Select("id").Where(ex).ToSQL()
	err := meta.MerchantDB.Get(&id, query)
	if err == nil {
		return nil
	}

	if err != nil && err != sql.ErrNoRows {
		return err
	}

	balance, _ := getBalance(mb, 2)
	trans := memberTransaction{
		ID:           helper.GenId(),
		UID:          mb.Uid,
		PlatformID:   platformID,
		Username:     mb.Username,
		BillNo:       billNo,
		CashType:     helper.TransactionBetCancel,
		Amount:       amount.String(),
		BeforeAmount: balance.String(),
		AfterAmount:  balance.String(),
		OperationNo:  operationNo,
		CreatedAt:    createdAt,
		Remark:       "void",
		Tester:       mb.Tester,
	}
	query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	fmt.Println(query)
	_, err = meta.MerchantDB.Exec(query)
	if err != nil {
		//记录日志
		if strings.HasPrefix(err.Error(), "Error 1062") {
			PushLog(nil, "", "duplicate", "", 200, nil, err)
		}

		return err
	}

	return nil
}
