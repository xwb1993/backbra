package model

import (
	"fmt"
	"game/contrib/conn"
	"game/contrib/helper"
	g "github.com/doug-martin/goqu/v9"
	"github.com/valyala/fasthttp"
	"io"
	"net/http"
	"os"
	"testing"
	"time"
)

type PPGameList struct {
	Error       string `json:"error"`
	Description string `json:"description"`
	GameList    []struct {
		GameID               string   `json:"gameID"`
		GameName             string   `json:"gameName"`
		GameTypeID           string   `json:"gameTypeID"`
		TypeDescription      string   `json:"typeDescription"`
		Technology           string   `json:"technology"`
		Platform             string   `json:"platform"`
		DemoGameAvailable    bool     `json:"demoGameAvailable"`
		AspectRatio          string   `json:"aspectRatio"`
		TechnologyID         string   `json:"technologyID"`
		GameIdNumeric        int      `json:"gameIdNumeric"`
		Jurisdictions        []string `json:"jurisdictions"`
		FrbAvailable         bool     `json:"frbAvailable"`
		VariableFrbAvailable bool     `json:"variableFrbAvailable"`
		Lines                int      `json:"lines"`
		DataType             string   `json:"dataType"`
		Features             []string `json:"features"`
	} `json:"gameList"`
}

func TestPPGameList(t *testing.T) {

	db := conn.InitDB("root:SVAdwsbB@@agw2WG@tcp(23.234.28.245:13309)/lb8888?charset=utf8&parseTime=True&loc=Local", 10, 10)
	New("0.0.0.0")
	req := map[string]string{
		"secureLogin": "zf559_lb888",
		"options":     "GetDataTypes",
		"hash":        MD5Hash("options=GetDataTypes&secureLogin=zf559_lb888testKey"),
	}
	requestBody := ParamEncode(req)
	headers := map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
	}
	requestURI := "https://api.prerelease-env.biz/IntegrationService/v3/http/CasinoGameAPI/getCasinoGames/"
	statusCode, body, err := httpPost([]byte(requestBody), requestURI, headers)
	if err != nil {
		t.Error(err)
	}

	if statusCode != fasthttp.StatusOK {
		t.Error(statusCode)
	}

	rsp := PPGameList{}
	err = helper.JsonUnmarshal(body, &rsp)
	if err != nil {
		t.Error(err)
	}

	for _, v := range rsp.GameList {
		data := Game{
			ID:         helper.GenId(),
			Name:       v.GameName,                                                                            //游戏名称
			EnName:     v.GameName,                                                                            //英文名称
			BrAlias:    v.GameName,                                                                            //巴西别名
			ClientType: "",                                                                                    //0:all 1:web 2:h5 4:app 此处值为支持端的数值之和
			GameID:     v.GameID,                                                                              //游戏ID
			Img:        fmt.Sprintf("https://common-static.ppgames.net/game_pic/square/200/%s.png", v.GameID), //手机图片
			Online:     1,                                                                                     //0 下线 1上线
			IsHot:      1,                                                                                     //0 正常 1热门
			IsNew:      1,                                                                                     //是否最新:0=否,1=是
			Sorting:    1,                                                                                     //排序
			TagID:      "[]",
			CreatedAt:  time.Now().Unix(), //添加时间
		}
		switch v.DataType {
		case "RNG":
			data.PlatformID = helper.PP
			data.GameType = 3 //游戏类型:1=真人,2=捕鱼,3=电子,4=体育
		case "LC":
			data.PlatformID = helper.PPCasino
			data.GameType = 1 //游戏类型:1=真人,2=捕鱼,3=电子,4=体育
		case "VSB":
			data.PlatformID = helper.PPSB
			data.GameType = 4 //游戏类型:1=真人,2=捕鱼,3=电子,4=体育
		default:
			continue
		}
		query, _, _ := dialect.Insert("tbl_game_lists").Rows(&data).ToSQL()
		fmt.Println(query)
		_, err = db.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("%s,[%s]", err.Error(), query))
		}
	}
}

func TestPPSaveImg(t *testing.T) {

	db := conn.InitDB("root:SVAdwsbB@@agw2WG@tcp(23.234.28.245:13309)/lb8888?charset=utf8&parseTime=True&loc=Local", 10, 10)
	var gameIds []string
	ex := g.Ex{
		"platform_id": []string{helper.PP, helper.PPCasino, helper.PPSB},
	}
	query, _, _ := dialect.From("tbl_game_lists").Select("game_id").Where(ex).ToSQL()
	fmt.Println(query)
	err := db.Select(&gameIds, query)
	if err != nil {
		t.Error(err)
	}

	for _, v := range gameIds {
		if v == "" {
			continue
		}

		path := fmt.Sprintf("https://common-static.ppgames.net/game_pic/square/200/%s.png", v)
		savePath := fmt.Sprintf("/Users/brandon/Documents/BR/PP/%s.png", v)

		saveWebFile(path, savePath)
	}
}

func saveWebFile(path, savePath string) {

	resp, err := http.Get(path)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer resp.Body.Close()

	// 创建一个文件用于保存
	out, err := os.Create(savePath)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer out.Close()

	// 然后将响应流和文件流对接起来
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
}
