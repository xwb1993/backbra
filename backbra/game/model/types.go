package model

type PlatJson struct {
	ID         string `db:"id" json:"id" cbor:"id"`
	Name       string `db:"name" json:"name" cbor:"name"`
	GameType   int    `db:"game_type" json:"game_type" cbor:"game_type"`
	State      int    `db:"state" json:"state" cbor:"state"`
	Maintained int    `db:"maintained" json:"maintained" cbor:"maintained"`
	Seq        int    `db:"seq" json:"seq" cbor:"seq"`
}

// 中心钱包账变表
type memberTransaction struct {
	AfterAmount  string `db:"after_amount" json:"after_amount" cbor:"after_amount"`    //账变后的金额
	Amount       string `db:"amount" json:"amount" cbor:"amount"`                      //用户填写的转换金额
	BeforeAmount string `db:"before_amount" json:"before_amount" cbor:"before_amount"` //账变前的金额
	BillNo       string `db:"bill_no" json:"bill_no" cbor:"bill_no"`                   //转账|充值|提现ID
	CashType     int    `db:"cash_type" json:"cash_type" cbor:"cash_type"`             //0:转入1:转出2:转入失败补回3:转出失败扣除4:存款5:提现
	CreatedAt    int64  `db:"created_at" json:"created_at" cbor:"created_at"`          //
	ID           string `db:"id" json:"id" cbor:"id"`                                  //
	UID          string `db:"uid" json:"uid" cbor:"uid"`                               //用户ID
	Username     string `db:"username" json:"username" cbor:"username"`                //用户名
	Remark       string `db:"remark" json:"remark" cbor:"remark"`                      //备注
	OperationNo  string `db:"operation_no" json:"operation_no" cbor:"operation_no"`    //操作码
	PlatformID   string `db:"platform_id" json:"platform_id" cbor:"platform_id"`       //场馆id
	Tester       int    `db:"tester" json:"tester" cbor:"tester"`                      //1正式 2测试 3代理
}

// MemberPlatform 会员场馆表
type MemberPlatform struct {
	ID        string `db:"id" json:"id" redis:"id" cbor:"id"`                                 //
	Username  string `db:"username" json:"username" redis:"username" cbor:"username"`         //用户名
	Pid       string `db:"pid" json:"pid" redis:"pid" cbor:"pid"`                             //场馆ID
	Password  string `db:"password" json:"password" redis:"password" cbor:"password"`         //平台密码
	Balance   string `db:"balance" json:"balance" redis:"balance" cbor:"balance"`             //平台余额
	State     int    `db:"state" json:"state" redis:"state" cbor:"state"`                     //状态:1=正常,2=锁定
	CreatedAt uint32 `db:"created_at" json:"created_at" redis:"created_at" cbor:"created_at"` //站点前缀
}

type Game struct {
	ID         string `json:"id" db:"id" cbor:"id"`
	PlatformID string `json:"platform_id" db:"platform_id" cbor:"platform_id"` //场馆ID
	Name       string `json:"name" db:"name" cbor:"name"`                      //游戏名称
	EnName     string `json:"en_name" db:"en_name" cbor:"en_name"`             //英文名称
	BrAlias    string `json:"br_alias" db:"br_alias" cbor:"br_alias"`          //巴西别名
	ClientType string `json:"client_type" db:"client_type" cbor:"client_type"` //0:all 1:web 2:h5 4:app 此处值为支持端的数值之和
	GameType   int    `json:"game_type" db:"game_type" cbor:"game_type"`       //游戏类型:1=真人,2=捕鱼,3=电子,4=体育
	GameID     string `json:"game_id" db:"game_id" cbor:"game_id"`             //游戏ID
	Img        string `json:"img" db:"img" cbor:"img"`                         //手机图片
	Online     int    `json:"online" db:"online" cbor:"online"`                //0 下线 1上线
	IsHot      int    `json:"is_hot" db:"is_hot" cbor:"is_hot"`                //0 正常 1热门
	IsNew      int    `json:"is_new" db:"is_new" cbor:"is_new"`                //是否最新:0=否,1=是
	Sorting    int    `json:"sorting" db:"sorting" cbor:"sorting"`             //排序
	TagID      string `json:"tag_id" db:"tag_id" cbor:"tag_id"`                //标签
	CreatedAt  int64  `json:"created_at" db:"created_at" cbor:"created_at"`    //添加时间
}

type Tag_t struct {
	Tid       string `db:"tid" cbor:"tid"`
	GameType  int    `db:"game_type" cbor:"game_type"`
	Name      string `db:"name" cbor:"name"`
	State     string `db:"state" cbor:"state"` //0:关闭1:开启
	CreatedAt int64  `db:"created_at" cbor:"created_at"`
}

// 游戏投注记录结构
type GameRecord struct {
	RowId          string  `db:"row_id" json:"row_id" cbor:"row_id"`
	BillNo         string  `db:"bill_no" json:"bill_no" cbor:"bill_no"`
	ApiType        string  `db:"api_type" json:"api_type" cbor:"api_type"`
	PlayerName     string  `db:"player_name" json:"player_name" cbor:"player_name"`
	Name           string  `db:"name" json:"name" cbor:"name"`
	Uid            string  `db:"uid" json:"uid" cbor:"uid"`
	TopUid         string  `db:"top_uid" json:"top_uid"`                                           //总代uid
	TopName        string  `db:"top_name" json:"top_name"`                                         //总代代理
	ParentUid      string  `db:"parent_uid" json:"parent_uid" cbor:"parent_uid"`                   //上级uid
	ParentName     string  `db:"parent_name" json:"parent_name" cbor:"parent_name"`                //上级代理
	GrandID        string  `db:"grand_id" json:"grand_id"  cbor:"grand_id"`                        //祖父级代理id
	GrandName      string  `db:"grand_name" json:"grand_name" cbor:"grand_name"`                   //祖父级代理
	GreatGrandID   string  `db:"great_grand_id" json:"great_grand_id"  cbor:"great_grand_id"`      //曾祖父级代理id
	GreatGrandName string  `db:"great_grand_name" json:"great_grand_name" cbor:"great_grand_name"` //曾祖父级代理
	NetAmount      float64 `db:"net_amount" json:"net_amount" cbor:"net_amount"`
	BetTime        int64   `db:"bet_time" json:"bet_time" cbor:"bet_time"`
	SettleTime     int64   `db:"settle_time" json:"settle_time" cbor:"settle_time"`
	ApiBetTime     int64   `db:"api_bet_time" json:"api_bet_time" cbor:"api_bet_time"`
	ApiSettleTime  int64   `db:"api_settle_time" json:"api_settle_time" cbor:"api_settle_time"`
	StartTime      int64   `db:"start_time" json:"start_time" cbor:"start_time"`
	Resettle       uint8   `db:"resettle" json:"resettle" cbor:"resettle"`
	Presettle      uint8   `db:"presettle" json:"presettle" cbor:"presettle"`
	GameType       string  `db:"game_type" json:"game_type" cbor:"game_type"`
	GameCode       string  `db:"game_code" json:"game_code" cbor:"game_code"`
	BetAmount      float64 `db:"bet_amount" json:"bet_amount" cbor:"bet_amount"`
	ValidBetAmount float64 `db:"valid_bet_amount" json:"valid_bet_amount" cbor:"valid_bet_amount"`
	RebateAmount   float64 `db:"rebate_amount" json:"rebate_amount" cbor:"rebate_amount"`
	Flag           int     `db:"flag" json:"flag" cbor:"flag"`
	PlayType       string  `db:"play_type" json:"play_type" cbor:"play_type"`
	Prefix         string  `db:"prefix" json:"prefix" cbor:"prefix"`
	Result         string  `db:"result" json:"result" cbor:"result"`
	CreatedAt      int64   `db:"created_at" json:"created_at" cbor:"created_at"`
	UpdatedAt      int64   `db:"updated_at" json:"updated_at" cbor:"updated_at"`
	ApiName        string  `db:"api_name" json:"api_name" cbor:"api_name"`
	ApiBillNo      string  `db:"api_bill_no" json:"api_bill_no" cbor:"api_bill_no"`
	MainBillNo     string  `db:"main_bill_no" json:"main_bill_no" cbor:"main_bill_no"`
	GameName       string  `db:"game_name" json:"game_name" cbor:"game_name"`
	HandicapType   string  `db:"handicap_type" json:"handicap_type" cbor:"handicap_type"`
	Handicap       string  `db:"handicap" json:"handicap" cbor:"handicap"`
	Odds           float64 `db:"odds" json:"odds" cbor:"odds"`
	SettleAmount   float64 `db:"settle_amount" json:"settle_amount" cbor:"settle_amount"`
	Tester         string  `db:"tester" json:"tester" cbor:"tester"`
}

type MemberBalance struct {
	UID        string `json:"-" cbor:"-" db:"uid"`
	Brl        string `json:"brl" cbor:"brl" db:"brl"`                         //巴西余额
	LockAmount string `json:"lock_amount" cbor:"lock_amount" db:"lock_amount"` //锁定余额
}
