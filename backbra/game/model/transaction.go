package model

import (
	"database/sql"
	"fmt"
	"game/contrib/helper"
	ryrpc "game/rpc"
	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"
	"strings"
	"time"
)

// 投注
func walletBetTransaction(tx *sql.Tx, mb ryrpc.TblMemberBase, billNo, operationNo, platformID string, balance, betAmount decimal.Decimal, ts time.Time) (string, error) {

	id := helper.GenId()
	betAfterAmount := balance.Sub(betAmount)
	trans := memberTransaction{
		ID:           id,
		UID:          mb.Uid,
		PlatformID:   platformID,
		Username:     mb.Username,
		BillNo:       billNo,
		CashType:     helper.TransactionBet,
		Amount:       betAmount.String(),
		BeforeAmount: balance.String(),
		AfterAmount:  betAfterAmount.String(),
		OperationNo:  operationNo,
		CreatedAt:    ts.UnixMilli(),
		Remark:       "running",
		Tester:       mb.Tester,
	}
	query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	fmt.Println(query)
	_, err := tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		//记录日志
		if strings.HasPrefix(err.Error(), "Error 1062") {
			PushLog(nil, "", "duplicate", "", 200, nil, err)
		}

		return id, err
	}

	if betAmount.GreaterThan(decimal.Zero) {
		record := g.Record{
			"brl": g.L(fmt.Sprintf("brl-%s", betAmount.String())),
		}
		query, _, _ = dialect.Update("tbl_member_balance").Set(record).Where(g.Ex{"uid": mb.Uid}).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			return id, err
		}
	}

	return id, nil
}

/*
billNo 主单号
operationNo 子单号（一个）
*/
// 结算，一次下注，一次结算
func walletSettleTransaction(tx *sql.Tx, mb ryrpc.TblMemberBase, billNo, operationNo, platformID string, balance, settleAmount decimal.Decimal, ts time.Time) (string, error) {

	id := helper.GenId()
	record := g.Record{
		"remark": "settled",
	}
	ex := g.Ex{
		"bill_no": billNo,
	}
	if operationNo != "" {
		ex["operation_no"] = operationNo
	}
	query, _, _ := dialect.Update("tbl_balance_transaction").Set(record).Where(ex).ToSQL()
	fmt.Println(query)
	_, err := tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		return id, err
	}

	if settleAmount.GreaterThan(decimal.Zero) {
		betAfterAmount := balance.Add(settleAmount)
		trans := memberTransaction{
			ID:           id,
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       billNo,
			CashType:     helper.TransactionPayout,
			Amount:       settleAmount.String(),
			BeforeAmount: balance.String(),
			AfterAmount:  betAfterAmount.String(),
			OperationNo:  operationNo,
			CreatedAt:    ts.UnixMilli(),
			Remark:       "settled",
			Tester:       mb.Tester,
		}
		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		fmt.Println(query)
		_, err := tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			//记录日志
			if strings.HasPrefix(err.Error(), "Error 1062") {
				PushLog(nil, "", "duplicate", "", 200, nil, err)
			}

			return id, err
		}

		record := g.Record{
			"brl": g.L(fmt.Sprintf("brl+%s", settleAmount.String())),
		}
		query, _, _ = dialect.Update("tbl_member_balance").Set(record).Where(g.Ex{"uid": mb.Uid}).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			return id, err
		}
	}

	return id, nil
}

/*
billNo 主单号
operationNos 子单号（多个）
*/
// 结算，多次下注，一次结算
func walletSettleTransactions(tx *sql.Tx, mb ryrpc.TblMemberBase, billNo, platformID string, operationNos []string, balance, settleAmount decimal.Decimal, ts time.Time) (string, error) {

	id := helper.GenId()
	record := g.Record{
		"remark": "settled",
	}
	ex := g.Ex{
		"bill_no": billNo,
	}
	if len(operationNos) > 0 {
		ex["operation_no"] = operationNos
	}
	query, _, _ := dialect.Update("tbl_balance_transaction").Set(record).Where(ex).ToSQL()
	fmt.Println(query)
	_, err := tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		return id, err
	}

	if settleAmount.GreaterThan(decimal.Zero) {
		betAfterAmount := balance.Add(settleAmount)
		trans := memberTransaction{
			ID:           id,
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       billNo,
			CashType:     helper.TransactionPayout,
			Amount:       settleAmount.String(),
			BeforeAmount: balance.String(),
			AfterAmount:  betAfterAmount.String(),
			CreatedAt:    ts.UnixMilli(),
			Remark:       "settled",
			Tester:       mb.Tester,
		}
		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		fmt.Println(query)
		_, err := tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			//记录日志
			if strings.HasPrefix(err.Error(), "Error 1062") {
				PushLog(nil, "", "duplicate", "", 200, nil, err)
			}

			return id, err
		}

		record := g.Record{
			"brl": g.L(fmt.Sprintf("brl+%s", settleAmount.String())),
		}
		query, _, _ = dialect.Update("tbl_member_balance").Set(record).Where(g.Ex{"uid": mb.Uid}).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			return id, err
		}
	}

	return id, nil
}

// 取消投注 投注一笔，取消一笔
func walletCancelTransaction(tx *sql.Tx, mb ryrpc.TblMemberBase, billNo, operationNo, platformID string, balance, betAmount decimal.Decimal, ts time.Time) (string, error) {

	id := helper.GenId()
	record := g.Record{
		"remark": "void",
	}
	ex := g.Ex{
		"bill_no": billNo,
	}
	if operationNo != "" {
		ex["operation_no"] = operationNo
	}
	query, _, _ := dialect.Update("tbl_balance_transaction").Set(record).Where(ex).ToSQL()
	fmt.Println(query)
	_, err := tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		return id, err
	}

	if betAmount.GreaterThan(decimal.Zero) {
		betAfterAmount := balance.Add(betAmount)
		trans := memberTransaction{
			ID:           id,
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       billNo,
			CashType:     helper.TransactionBetCancel,
			Amount:       betAmount.String(),
			BeforeAmount: balance.String(),
			AfterAmount:  betAfterAmount.String(),
			OperationNo:  operationNo,
			CreatedAt:    ts.UnixMilli(),
			Remark:       "void",
			Tester:       mb.Tester,
		}
		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		fmt.Println(query)
		_, err := tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			//记录日志
			if strings.HasPrefix(err.Error(), "Error 1062") {
				PushLog(nil, "", "duplicate", "", 200, nil, err)
			}

			return id, err
		}

		record := g.Record{
			"brl": g.L(fmt.Sprintf("brl+%s", betAmount.String())),
		}
		query, _, _ = dialect.Update("tbl_member_balance").Set(record).Where(g.Ex{"uid": mb.Uid}).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			return id, err
		}
	}

	return id, nil
}

// 取消投注（已结算为赢的注单）
func walletCancelSettledWinTransaction(tx *sql.Tx, mb ryrpc.TblMemberBase, billNo, operationNo, platformID string, balance, winAmount decimal.Decimal, ts time.Time) (string, error) {

	id := helper.GenId()
	record := g.Record{
		"remark": "void",
	}
	ex := g.Ex{
		"bill_no": billNo,
	}
	if operationNo != "" {
		ex["operation_no"] = operationNo
	}
	query, _, _ := dialect.Update("tbl_balance_transaction").Set(record).Where(ex).ToSQL()
	fmt.Println(query)
	_, err := tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		return id, err
	}

	if winAmount.GreaterThan(decimal.Zero) {
		betAfterAmount := balance.Sub(winAmount)
		trans := memberTransaction{
			ID:           id,
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       billNo,
			CashType:     helper.TransactionSettledBetCancel,
			Amount:       winAmount.String(),
			BeforeAmount: balance.String(),
			AfterAmount:  betAfterAmount.String(),
			OperationNo:  operationNo,
			CreatedAt:    ts.UnixMilli(),
			Remark:       "void",
			Tester:       mb.Tester,
		}
		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		fmt.Println(query)
		_, err := tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			//记录日志
			if strings.HasPrefix(err.Error(), "Error 1062") {
				PushLog(nil, "", "duplicate", "", 200, nil, err)
			}

			return id, err
		}

		record := g.Record{
			"brl": g.L(fmt.Sprintf("brl-%s", winAmount.String())),
		}
		query, _, _ = dialect.Update("tbl_member_balance").Set(record).Where(g.Ex{"uid": mb.Uid}).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			return id, err
		}
	}

	return id, nil
}

// 取消投注 投注多笔，取消多笔
func walletCancelTransactions(tx *sql.Tx, mb ryrpc.TblMemberBase, billNo, platformID string, operationNos []string, balance, betAmount decimal.Decimal, ts time.Time) (string, error) {

	id := helper.GenId()
	record := g.Record{
		"remark": "void",
	}
	ex := g.Ex{
		"bill_no": billNo,
	}
	if len(operationNos) > 0 {
		ex["operation_no"] = operationNos
	}
	query, _, _ := dialect.Update("tbl_balance_transaction").Set(record).Where(ex).ToSQL()
	fmt.Println(query)
	_, err := tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		return id, err
	}

	if betAmount.GreaterThan(decimal.Zero) {
		betAfterAmount := balance.Add(betAmount)
		trans := memberTransaction{
			ID:           id,
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       billNo,
			CashType:     helper.TransactionBetCancel,
			Amount:       betAmount.String(),
			BeforeAmount: balance.String(),
			AfterAmount:  betAfterAmount.String(),
			CreatedAt:    ts.UnixMilli(),
			Remark:       "void",
			Tester:       mb.Tester,
		}
		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		fmt.Println(query)
		_, err := tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			//记录日志
			if strings.HasPrefix(err.Error(), "Error 1062") {
				PushLog(nil, "", "duplicate", "", 200, nil, err)
			}

			return id, err
		}

		record := g.Record{
			"brl": g.L(fmt.Sprintf("brl+%s", betAmount.String())),
		}
		query, _, _ = dialect.Update("tbl_member_balance").Set(record).Where(g.Ex{"uid": mb.Uid}).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			return id, err
		}
	}

	return id, nil
}

// 免费旋转奖金
func walletBonusWinTransaction(tx *sql.Tx, mb ryrpc.TblMemberBase, billNo, operationNo, platformID string, balance, bonus decimal.Decimal, ts time.Time) (string, error) {

	id := helper.GenId()
	betAfterAmount := balance.Add(bonus)
	trans := memberTransaction{
		ID:           id,
		UID:          mb.Uid,
		PlatformID:   platformID,
		Username:     mb.Username,
		BillNo:       billNo,
		CashType:     helper.TransactionBonusWin,
		Amount:       bonus.String(),
		BeforeAmount: balance.String(),
		AfterAmount:  betAfterAmount.String(),
		OperationNo:  operationNo,
		CreatedAt:    ts.UnixMilli(),
		Remark:       "bonus",
		Tester:       mb.Tester,
	}
	query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	fmt.Println(query)
	_, err := tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		//记录日志
		if strings.HasPrefix(err.Error(), "Error 1062") {
			PushLog(nil, "", "duplicate", "", 200, nil, err)
		}

		return id, err
	}

	record := g.Record{
		"brl": g.L(fmt.Sprintf("brl+%s", bonus.String())),
	}
	query, _, _ = dialect.Update("tbl_member_balance").Set(record).Where(g.Ex{"uid": mb.Uid}).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		return id, err
	}

	return id, nil
}

// 累计奖池赢奖
func walletJackpotWinTransaction(tx *sql.Tx, mb ryrpc.TblMemberBase, billNo, operationNo, platformID string, balance, bonus decimal.Decimal, ts time.Time) (string, error) {

	id := helper.GenId()
	betAfterAmount := balance.Add(bonus)
	trans := memberTransaction{
		ID:           id,
		UID:          mb.Uid,
		PlatformID:   platformID,
		Username:     mb.Username,
		BillNo:       billNo,
		CashType:     helper.TransactionJackPotWin,
		Amount:       bonus.String(),
		BeforeAmount: balance.String(),
		AfterAmount:  betAfterAmount.String(),
		OperationNo:  operationNo,
		CreatedAt:    ts.UnixMilli(),
		Remark:       "jackpot",
		Tester:       mb.Tester,
	}
	query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	fmt.Println(query)
	_, err := tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		//记录日志
		if strings.HasPrefix(err.Error(), "Error 1062") {
			PushLog(nil, "", "duplicate", "", 200, nil, err)
		}

		return id, err
	}

	record := g.Record{
		"brl": g.L(fmt.Sprintf("brl+%s", bonus.String())),
	}
	query, _, _ = dialect.Update("tbl_member_balance").Set(record).Where(g.Ex{"uid": mb.Uid}).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		return id, err
	}

	return id, nil
}

// 促销赢奖
func walletPromoWinTransaction(tx *sql.Tx, mb ryrpc.TblMemberBase, billNo, operationNo, platformID string, balance, bonus decimal.Decimal, ts time.Time) (string, error) {

	id := helper.GenId()
	betAfterAmount := balance.Add(bonus)
	trans := memberTransaction{
		ID:           id,
		UID:          mb.Uid,
		PlatformID:   platformID,
		Username:     mb.Username,
		BillNo:       billNo,
		CashType:     helper.TransactionPromoWin,
		Amount:       bonus.String(),
		BeforeAmount: balance.String(),
		AfterAmount:  betAfterAmount.String(),
		OperationNo:  operationNo,
		CreatedAt:    ts.UnixMilli(),
		Remark:       "jackpot",
		Tester:       mb.Tester,
	}
	query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	fmt.Println(query)
	_, err := tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		//记录日志
		if strings.HasPrefix(err.Error(), "Error 1062") {
			PushLog(nil, "", "duplicate", "", 200, nil, err)
		}

		return id, err
	}

	record := g.Record{
		"brl": g.L(fmt.Sprintf("brl+%s", bonus.String())),
	}
	query, _, _ = dialect.Update("tbl_member_balance").Set(record).Where(g.Ex{"uid": mb.Uid}).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		return id, err
	}

	return id, nil
}

func walletAdjustTransaction(tx *sql.Tx, mb ryrpc.TblMemberBase, billNo, operationNo, platformID string, balance, amount decimal.Decimal, cashType int, ts time.Time) (string, error) {

	// 结算为输的注单，只修改状态，不产生帐变，场馆业务代码自行控制
	afterAmount := balance.Add(amount)
	if cashType == helper.TransactionAdjustDiv {
		afterAmount = balance.Sub(amount)
	}

	id := helper.GenId()
	if amount.Cmp(decimal.Zero) > 0 {
		trans := memberTransaction{
			ID:           id,
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       billNo,
			CashType:     cashType,
			Amount:       amount.String(),
			BeforeAmount: balance.String(),
			AfterAmount:  afterAmount.String(),
			OperationNo:  operationNo,
			CreatedAt:    ts.UnixMilli(),
			Tester:       mb.Tester,
		}
		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		fmt.Println(query)
		_, err := tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			//记录日志
			if strings.HasPrefix(err.Error(), "Error 1062") {
				PushLog(nil, "", "duplicate", "", 200, nil, err)
			}

			return id, err
		}

		ex := g.Ex{
			"uid": mb.Uid,
		}
		operate := "+"
		if cashType == helper.TransactionAdjustDiv {
			operate = "-"
		}
		record := g.Record{
			"brl": g.L(fmt.Sprintf("brl%s%s", operate, amount.String())),
		}
		query, _, _ = dialect.Update("tbl_member_balance").Set(record).Where(ex).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			return id, err
		}
	}

	return id, nil
}
