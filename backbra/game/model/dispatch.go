package model

import (
	"crypto/tls"
	"fmt"
	"game/contrib/helper"
	"github.com/valyala/fasthttp/fasthttpproxy"
	"net/url"
	"time"

	"github.com/valyala/fasthttp"
)

type CallFunc func(map[string]interface{}) (int, string)

var (
	fc *fasthttp.Client
)

const (
	apiTimeOut = time.Second * 12
)

func ParamEncode(args map[string]string) string {
	if len(args) < 1 {
		return ""
	}

	data := url.Values{}
	for k, v := range args {
		data.Set(k, v)
	}
	return data.Encode()
}

func PushLog(requestBody []byte, username, name, requestURI string, code int, body []byte, err error) {

	ts := time.Now()
	l := map[string]interface{}{
		"requestURL":  requestURI,
		"requestBody": string(requestBody),
		"statusCode":  fmt.Sprintf("%d", code),
		"body":        string(body),
		"level":       "info",
		"err":         "",
		"name":        name,
		"username":    username,
		"ts":          time.Now().In(loc).UnixMicro(),
		"_index":      fmt.Sprintf("platform_%04d%02d", ts.Year(), ts.Month()),
	}

	if err != nil {
		l["err"] = err.Error()
		l["level"] = "warm"
	}
	if code != fasthttp.StatusCreated && code != fasthttp.StatusOK {
		l["level"] = "warm"
	}

	if l["level"] == "warm" {
		//debug.PrintStack()
		//fmt.Println("PushLog Requesturl = ", requestURI)
	}

	b, _ := helper.JsonMarshal(&l)
	//fmt.Println("PushLog", string(b))
	_ = BeanPut("zinc_fluent_log", b)
}

func HttpPostWithPushLog(requestBody []byte, username, name, requestURI string, headers map[string]string) (int, []byte, error) {

	//fmt.Println(requestURI)
	statusCode, body, err := httpPost(requestBody, requestURI, headers)
	//fmt.Println(statusCode)
	//fmt.Println(body)
	PushLog(requestBody, username, name, requestURI, statusCode, body, err)

	return statusCode, body, err
}

func HttpGetWithPushLog(username, name, requestURI string) (int, []byte, error) {

	statusCode, body, err := httpGet(requestURI)
	PushLog(nil, username, name, requestURI, statusCode, body, err)

	return statusCode, body, err
}

func HttpGetHeaderWithLog(username, name, requestURI string, headers map[string]string) (int, []byte, error) {

	statusCode, body, err := httpGetHeader(requestURI, headers)
	PushLog(nil, username, name, requestURI, statusCode, body, err)

	return statusCode, body, err
}

func httpPost(requestBody []byte, requestURI string, headers map[string]string) (int, []byte, error) {

	req := fasthttp.AcquireRequest()
	resp := fasthttp.AcquireResponse()

	defer func() {
		fasthttp.ReleaseResponse(resp)
		fasthttp.ReleaseRequest(req)
	}()

	req.SetRequestURI(requestURI)
	req.Header.SetMethod("POST")
	req.SetBody(requestBody)

	//req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	if headers != nil {
		for k, v := range headers {
			req.Header.Set(k, v)
		}
	}

	err := fc.DoTimeout(req, resp, apiTimeOut)
	return resp.StatusCode(), resp.Body(), err
}

func httpGet(requestURI string) (int, []byte, error) {

	return fc.GetTimeout(nil, requestURI, apiTimeOut)
}

func httpGetHeader(requestURI string, headers map[string]string) (int, []byte, error) {

	req := fasthttp.AcquireRequest()
	resp := fasthttp.AcquireResponse()

	defer func() {
		fasthttp.ReleaseResponse(resp)
		fasthttp.ReleaseRequest(req)
	}()

	req.SetRequestURI(requestURI)
	req.Header.SetMethod("GET")
	//req.SetBody(requestBody)
	//req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	if headers != nil {
		for k, v := range headers {
			req.Header.Set(k, v)
		}
	}

	err := fc.DoTimeout(req, resp, apiTimeOut)

	return resp.StatusCode(), resp.Body(), err
}

func New(socks5 string) {

	fc = &fasthttp.Client{
		MaxConnsPerHost: 60000,
		TLSConfig:       &tls.Config{InsecureSkipVerify: true},
		ReadTimeout:     apiTimeOut,
		WriteTimeout:    apiTimeOut,
	}

	if socks5 != "0.0.0.0" {
		fc.Dial = fasthttpproxy.FasthttpHTTPDialer(socks5)

	}
}
