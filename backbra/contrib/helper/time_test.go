package helper

import (
	"fmt"
	"testing"
	"time"
)

func TestTime(t *testing.T) {

	loc, _ := time.LoadLocation("Asia/Bangkok")
	santiagoLoc, _ := time.LoadLocation("America/Halifax")
	tm, _ := LocFormatToTime("2022-11-12 10:04:05", loc)
	ts := tm.In(santiagoLoc)
	date := fmt.Sprintf("%02d%02d%d", ts.Year()%100, ts.Month(), ts.Day())
	t.Log("jl:", ts.Format("2006-01-02 15:04:05"), date)
}
