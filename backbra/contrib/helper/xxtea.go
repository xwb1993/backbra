package helper

import (
	"github.com/xxtea/xxtea-go/xxtea"
	//"log"
)

func XxteaDecryptString(src, key string) string {

	dst := xxtea.Decrypt([]byte(src), []byte(key))
	
	return string(dst)
}
