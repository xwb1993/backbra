package ryrpc

import "fmt"

func MessageFlushAll() error {
	var data string

	err := client.Call("/message/init", "1", &data)
	if err != nil {
		return err
	}
	return nil
}

func MessageList(param TblMessageListParam) (TblMessageData, error) {

	var data TblMessageData

	err := client.Call("/message/list", param, &data)
	if err != nil {
		return data, err
	}
	return data, nil
}

func MessageUpdate(id string) (bool, error) {

	var data bool

	err := client.Call("/message/update", id, &data)
	if err != nil {
		return data, err
	}
	return data, nil
}

func MessageBulk(param TblMessageBulkParam) (bool, error) {

	var data bool

	err := client.Call("/message/bulk", param, &data)
	if err != nil {
		return data, err
	}
	return data, nil
}

func MessageDeleteById(param []string) (bool, error) {

	var data bool

	err := client.Call("/message/delete/id", param, &data)
	if err != nil {
		return data, err
	}
	return data, nil
}

func MessageDeleteByFilter(username string, flag int) (bool, error) {

	var data bool

	filter := fmt.Sprintf("username = %s", username)
	if flag == 2 {
		filter += " AND is_read = 2"
	}
	err := client.Call("/message/delete/filter", filter, &data)
	if err != nil {
		return data, err
	}
	return data, nil
}

func MessageUnreadTotal(username string) (int64, error) {

	var data int64

	err := client.Call("/message/total/unread", username, &data)
	if err != nil {
		return data, err
	}
	return data, nil
}
