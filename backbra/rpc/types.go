package ryrpc

type TblSmsLog struct {
	ID        string `json:"id"`
	Ty        string `json:"ty"`
	State     string `json:"state"`
	Source    string `json:"source"`
	IP        string `json:"ip"`
	Phone     string `json:"phone"`
	Flags     string `json:"flags"`
	Code      string `json:"code"`
	Ts        int64  `json:"ts"`
	CreateAt  int64  `json:"create_at"`
	UpdatedAt int64  `json:"updated_at"`
}

type TblMessageData struct {
	T int64        `json:"t" cbor:"t"`
	S int          `json:"s" cbor:"s"`
	D []TblMessage `json:"d" cbor:"d"`
}

type TblMessageListParam struct {
	Name string `json:"name"`
	Ty   int    `json:"ty"`
	Read int    `json:"read"`
	Page int    `json:"page"`
	Size int    `json:"size"`
}

type TblMessageBulkParam struct {
	Title    string   `json:"title"`
	Content  string   `json:"content"`
	SendName string   `json:"sendName"`
	IsTop    int      `json:"isTop"`
	IsVip    int      `json:"isVip"`
	Ty       int      `json:"ty"`
	Names    []string `json:"names"`
}

type TblMessage struct {
	Id       string `json:"id" cbor:"id"`               // id
	Username string `json:"username" cbor:"username"`   //会员名
	Title    string `json:"title" cbor:"title"`         //标题
	Content  string `json:"content" cbor:"content"`     //内容
	IsTop    int    `json:"is_top" cbor:"is_top"`       //0不置顶 1置顶
	IsVip    int    `json:"is_vip" cbor:"is_vip"`       //0非vip站内信 1vip站内信
	Ty       int    `json:"ty" cbor:"ty"`               //1站内消息 2活动消息
	IsRead   int    `json:"is_read" cbor:"is_read"`     //是否已读 0未读 1已读
	SendName string `json:"send_name" cbor:"send_name"` //发送人名
	SendAt   int64  `json:"send_at" cbor:"send_at"`     //发送时间
}

type TblMemberBase struct {
	Uid               string  `json:"uid" db:"uid" cbor:"uid"`
	Pid               string  `json:"pid" db:"pid" cbor:"pid"`
	Cid               string  `json:"cid" db:"cid" cbor:"cid"`
	Username          string  `json:"username" db:"username" cbor:"username"`
	TopID             string  `json:"top_id" db:"top_id" cbor:"top_id"`                                        //总代id
	TopName           string  `json:"top_name" db:"top_name" cbor:"top_name"`                                  //总代名
	GreatGrandID      string  `json:"great_grand_id" db:"great_grand_id" cbor:"great_grand_id"`                //曾祖父级代理id
	GreatGrandName    string  `json:"great_grand_name" db:"great_grand_name" cbor:"great_grand_name"`          //曾祖父级代理名
	GrandID           string  `json:"grand_id" db:"grand_id" cbor:"grand_id"`                                  //祖父级代理id
	GrandName         string  `json:"grand_name" db:"grand_name" cbor:"grand_name"`                            //祖父级代理名
	ParentID          string  `json:"parent_id" db:"parent_id" cbor:"parent_id"`                               //父级代理id
	ParentName        string  `json:"parent_name" db:"parent_name" cbor:"parent_name"`                         //父级代理名
	Password          string  `json:"-" db:"password" cbor:"-"`                                                //登录密码
	CreatedAt         uint32  `json:"created_at" db:"created_at" cbor:"created_at"`                            //注册时间
	CreatedIp         string  `json:"created_ip" db:"created_ip" cbor:"created_ip"`                            //注册ip
	DepositAmount     float64 `json:"deposit_amount" db:"deposit_amount" cbor:"deposit_amount"`                //累计存款
	Score             float64 `json:"score" db:"score" cbor:"score"`                                           //有效流水
	KeepScore         float64 `json:"keep_score" db:"keep_score" cbor:"keep_score"`                            //保级流水
	Vip               int     `json:"vip" db:"vip" cbor:"vip"`                                                 //vip
	Phone             string  `json:"phone" db:"phone" cbor:"phone"`                                           //手机
	Email             string  `json:"email" db:"email" cbor:"email"`                                           //邮箱
	InviteRegisterNum int     `json:"invite_register_num" db:"invite_register_num" cbor:"invite_register_num"` //邀请注册人数
	InviteNum         int     `json:"invite_num" db:"invite_num" cbor:"invite_num"`                            //邀请人数（首充才算）
	LastTreasure      int     `json:"last_treasure" db:"last_treasure" cbor:"last_treasure"`                   //箱子
	Tester            int     `json:"tester" db:"tester" cbor:"tester"`                                        //1正式2模拟
	Avatar            uint8   `json:"avatar" db:"avatar" cbor:"avatar"`                                        //头像id
	InviteCode        string  `json:"invite_code" db:"invite_code" cbor:"invite_code"`                         //邀请码
	PayPassword       string  `json:"pay_password" db:"pay_password" cbor:"pay_password"`                      //支付密码
	Telegram          string  `json:"telegram" db:"telegram" cbor:"telegram"`                                  //小飞机
	PhoneVerify       int     `json:"phone_verify" db:"phone_verify" cbor:"phone_verify"`                      //手机是否已验证 1是 0否
	EmailVerify       int     `json:"email_verify" db:"email_verify" cbor:"email_verify"`                      //邮箱是否已验证 1是 0否
	BankcardTotal     int     `json:"bankcard_total" db:"bankcard_total" cbor:"bankcard_total"`                //用户绑定银行卡的数量
	CanBonus          int     `json:"can_bonus" db:"can_bonus" cbor:"can_bonus"`                               //1可以派送奖金2不可以
	CanRebate         int     `json:"can_rebate" db:"can_rebate" cbor:"can_rebate"`                            //1可以返现2不可以
	State             int     `json:"state" db:"state" cbor:"state"`                                           //1正常 2登陆锁定 3游戏锁定 4充提锁定
	Remake            string  `json:"remake" db:"remake" cbor:"remake"`                                        //备注
	Prefix            string  `json:"prefix" db:"prefix" cbor:"prefix"`                                        //1可以返现2不可以
	Invited           int     `json:"invited" db:"invited" cbor:"invited"`                                     //0未邀请 1已邀请
	OperatorId        string  `json:"operator_id" db:"operator_id" cbor:"operator_id"`                         //渠道id
	ProxyId           string  `json:"proxy_id" db:"proxy_id" cbor:"proxy_id"`                                  //业务员id
	WinFlowMultiple   string  `json:"win_flow_multiple" db:"win_flow_multiple" cbor:"win_flow_multiple"`       //赢打码系数
	LoseFlowMultiple  string  `json:"lose_flow_multiple" db:"lose_flow_multiple" cbor:"lose_flow_multiple"`    //输打码系数
}

type ip_t struct {
	Country string `json:"country" cbor:"country"`
	Region  string `json:"region" cbor:"region"`
	City    string `json:"city" cbor:"city"`
}
