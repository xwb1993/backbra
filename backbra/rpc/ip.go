package ryrpc


func Ip2Location(ip string) (string, string, string, error) {
	var data ip_t
	err := client.Call("/redis/ip/one", ip, &data)
	return data.Country, data.Region, data.City, err
}
