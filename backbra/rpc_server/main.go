package main

import (
	"fmt"
	"github.com/BurntSushi/toml"
	"github.com/meilisearch/meilisearch-go"
	rysrv "github.com/ryrpc/server"
	"github.com/valyala/fasthttp"
	"log"
	"os"
	"rcp/contrib/apollo"
	"rcp/model"
	"rcp/pkg"
	"strings"

	_ "go.uber.org/automaxprocs"
)

func main() {

	argc := len(os.Args)
	if argc != 3 {
		fmt.Printf("%s <etcds> <cfgPath>\r\n", os.Args[0])
		return
	}

	cfg := pkg.Conf{}
	endpoints := strings.Split(os.Args[1], ",")
	apollo.New(endpoints, pkg.ETCDName, pkg.ETCDPass)
	//err := apollo.ParseTomlStruct(os.Args[2], &cfg)
	if _, err := toml.DecodeFile(os.Args[2], &cfg); err != nil {
		fmt.Printf("ParseTomlStruct error: %s", err.Error())
		return
	}
	//apollo.New(endpoints, ETCDName, ETCDPass)
	//apollo.ParseTomlStruct(os.Args[2], &cfg)
	apollo.Close()

	//mt.Program = filepath.Base(os.Args[0])
	meili := meilisearch.NewClient(meilisearch.ClientConfig{
		Host:   cfg.Meilisearch.Host,
		APIKey: cfg.Meilisearch.Key,
	})

	/*
		defer func() {
			model.Close()
			mt = nil
		}()
	*/

	repo := rysrv.NewRepository()

	repo.Use("meili", meili)
	repo.Register("/message/init", model.MessageInit)
	repo.Register("/message/list", model.MessageList)
	repo.Register("/message/bulk", model.MessageBulk)
	repo.Register("/message/delete/id", model.MessageDeleteById)
	repo.Register("/message/delete/filter", model.MessageDeleteByFilter)
	repo.Register("/message/update", model.MessageUpdate)
	repo.Register("/message/total/unread", model.MessageUnreadTotal)

	fmt.Println("admin running", cfg.Port.RPC)
	err := fasthttp.ListenAndServe(cfg.Port.RPC, repo.RequestHandler())
	if err != nil {
		log.Fatalf("Error in ListenAndServe: %s", err.Error())
	}
}
