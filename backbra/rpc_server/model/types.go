package model

type TblMessage struct {
	Id       string `json:"id" cbor:"id"`               // id
	Username string `json:"username" cbor:"username"`   //会员名
	Title    string `json:"title" cbor:"title"`         //标题
	Content  string `json:"content" cbor:"content"`     //内容
	IsTop    int    `json:"is_top" cbor:"is_top"`       //0不置顶 1置顶
	IsVip    int    `json:"is_vip" cbor:"is_vip"`       //0非vip站内信 1vip站内信
	Ty       int    `json:"ty" cbor:"ty"`               //1站内消息 2活动消息
	IsRead   int    `json:"is_read" cbor:"is_read"`     //是否已读 0未读 1已读
	SendName string `json:"send_name" cbor:"send_name"` //发送人名
	SendAt   int64  `json:"send_at" cbor:"send_at"`     //发送时间
}

type TblMemberBase struct {
	Uid            string `json:"uid" db:"uid" cbor:"uid"`
	Username       string `json:"username" db:"username" cbor:"username"`
	TopID          string `json:"top_id" db:"top_id" cbor:"top_id"`                               //总代id
	TopName        string `json:"top_name" db:"top_name" cbor:"top_name"`                         //总代名
	GreatGrandID   string `json:"great_grand_id" db:"great_grand_id" cbor:"great_grand_id"`       //曾祖父级代理id
	GreatGrandName string `json:"great_grand_name" db:"great_grand_name" cbor:"great_grand_name"` //曾祖父级代理名
	GrandID        string `json:"grand_id" db:"grand_id" cbor:"grand_id"`                         //祖父级代理id
	GrandName      string `json:"grand_name" db:"grand_name" cbor:"grand_name"`                   //祖父级代理名
	ParentID       string `json:"parent_id" db:"parent_id" cbor:"parent_id"`                      //父级代理id
	ParentName     string `json:"parent_name" db:"parent_name" cbor:"parent_name"`                //父级代理名
	Password       string `json:"-" db:"password" cbor:"-"`
	CreatedAt      uint32 `json:"created_at" db:"created_at" cbor:"created_at"`
	CreatedIp      string `json:"created_ip" db:"created_ip" cbor:"created_ip"`
	DepositAmount  string `json:"deposit_amount" db:"deposit_amount" cbor:"deposit_amount"`
	Score          string `json:"score" db:"score" cbor:"score"`
	Vip            int    `json:"vip" db:"vip" cbor:"vip"`
	Phone          string `json:"phone" db:"phone" cbor:"phone"`
	Email          string `json:"email" db:"email" cbor:"email"`
	Avatar         uint8  `db:"avatar" json:"avatar"` //头像id
}
