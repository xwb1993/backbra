package model

import (
	"context"

	"github.com/go-redis/redis/v8"
	"github.com/jmoiron/sqlx"
	"github.com/meilisearch/meilisearch-go"
	"github.com/valyala/fasthttp"
)

var (
	ctx = context.Background()
)

func redisx(fctx *fasthttp.RequestCtx) *redis.Client {

	temp := fctx.UserValue("redis")

	return temp.(*redis.Client)
}

func db(fctx *fasthttp.RequestCtx) *sqlx.DB {

	temp := fctx.UserValue("db")

	return temp.(*sqlx.DB)
}

func meili(fctx *fasthttp.RequestCtx) *meilisearch.Client {

	temp := fctx.UserValue("meili")

	return temp.(*meilisearch.Client)
}
