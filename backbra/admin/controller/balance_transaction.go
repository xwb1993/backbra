package controller

import (
	"admin/contrib/helper"
	"admin/contrib/validator"
	"admin/model"
	g "github.com/doug-martin/goqu/v9"
	"github.com/valyala/fasthttp"
	"net/url"
	"strconv"
	"strings"
)

type BalanceTransferController struct{}

// List 账变记录列表
func (that *BalanceTransferController) List(ctx *fasthttp.RequestCtx) {

	username := string(ctx.QueryArgs().Peek("username"))        // 用户名
	platformID := string(ctx.QueryArgs().Peek("platform_id"))   // 场馆id 0中心钱包
	billNo := string(ctx.QueryArgs().Peek("bill_no"))           // 订单号
	operationNo := string(ctx.QueryArgs().Peek("operation_no")) // 操作号
	uid := string(ctx.QueryArgs().Peek("uid"))                  //
	types := string(ctx.QueryArgs().Peek("types"))              // 账变类型
	startTime := string(ctx.QueryArgs().Peek("start_time"))     // 查询开始时间
	endTime := string(ctx.QueryArgs().Peek("end_time"))         // 查询结束时间
	page := ctx.QueryArgs().GetUintOrZero("page")               // 页码
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")      // 页大小

	if page == 0 {
		page = 1
	}

	if pageSize < 10 || pageSize > 200 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	ex := g.Ex{}

	if platformID != "" {
		ex["platform_id"] = platformID
	}

	if operationNo != "" {
		ex["operation_no"] = operationNo
	}

	if types != "" {
		cashTypes := strings.Split(types, ",")
		for _, v := range cashTypes {
			ct, err := strconv.Atoi(v)
			if _, ok := helper.CashTypes[ct]; !ok || err != nil {
				helper.Print(ctx, false, helper.CashTypeErr)
				return
			}
		}

		if len(cashTypes) > 0 {
			ex["cash_type"] = cashTypes
		}
	}

	if billNo != "" {
		ex = g.Ex{
			"bill_no": billNo,
		}
	} else if uid != "" {
		if !validator.CheckStringDigit(uid) {
			helper.Print(ctx, false, helper.UsernameErr)
			return
		}
		ex["uid"] = uid
	} else if username != "" { // 用户名校验

		ex["username"], _ = url.PathUnescape(username)
	}

	data, err := model.RecordTransaction(page, pageSize, startTime, endTime, ex)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

// MerchantList 渠道账变记录列表
func (that *BalanceTransferController) MerchantList(ctx *fasthttp.RequestCtx) {

	username := string(ctx.QueryArgs().Peek("username"))        // 用户名
	platformID := string(ctx.QueryArgs().Peek("platform_id"))   // 场馆id 0中心钱包
	billNo := string(ctx.QueryArgs().Peek("bill_no"))           // 订单号
	operationNo := string(ctx.QueryArgs().Peek("operation_no")) // 操作号
	operatorId := string(ctx.QueryArgs().Peek("operator_id"))   // 渠道id
	types := string(ctx.QueryArgs().Peek("types"))              // 账变类型
	startTime := string(ctx.QueryArgs().Peek("start_time"))     // 查询开始时间
	endTime := string(ctx.QueryArgs().Peek("end_time"))         // 查询结束时间
	page := ctx.QueryArgs().GetUintOrZero("page")               // 页码
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")      // 页大小

	if page == 0 {
		page = 1
	}

	if pageSize < 10 || pageSize > 200 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	if operatorId == "" {
		helper.Print(ctx, false, helper.OperatorIdErr)
		return
	}
	ex := g.Ex{}

	if platformID != "" {
		ex["platform_id"] = platformID
	}

	if operationNo != "" {
		ex["operation_no"] = operationNo
	}

	if types != "" {
		cashTypes := strings.Split(types, ",")
		for _, v := range cashTypes {
			ct, err := strconv.Atoi(v)
			if _, ok := helper.CashTypes[ct]; !ok || err != nil {
				helper.Print(ctx, false, helper.CashTypeErr)
				return
			}
		}

		if len(cashTypes) > 0 {
			ex["cash_type"] = cashTypes
		}
	}

	if billNo != "" {
		ex = g.Ex{
			"bill_no": billNo,
		}
	}

	data, err := model.MerchantRecordTransaction(page, pageSize, startTime, endTime, operatorId, username, ex)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

// BusinessList 渠道账变记录列表
func (that *BalanceTransferController) BusinessList(ctx *fasthttp.RequestCtx) {

	username := string(ctx.QueryArgs().Peek("username"))        // 用户名
	platformID := string(ctx.QueryArgs().Peek("platform_id"))   // 场馆id 0中心钱包
	billNo := string(ctx.QueryArgs().Peek("bill_no"))           // 订单号
	operationNo := string(ctx.QueryArgs().Peek("operation_no")) // 操作号
	operatorId := string(ctx.QueryArgs().Peek("operator_id"))   // 渠道id
	proxyId := string(ctx.QueryArgs().Peek("proxy_id"))         // 业务员id
	types := string(ctx.QueryArgs().Peek("types"))              // 账变类型
	startTime := string(ctx.QueryArgs().Peek("start_time"))     // 查询开始时间
	endTime := string(ctx.QueryArgs().Peek("end_time"))         // 查询结束时间
	page := ctx.QueryArgs().GetUintOrZero("page")               // 页码
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")      // 页大小

	if page == 0 {
		page = 1
	}

	if pageSize < 10 || pageSize > 200 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	if operatorId == "" {
		helper.Print(ctx, false, helper.OperatorIdErr)
		return
	}
	ex := g.Ex{}

	if platformID != "" {
		ex["platform_id"] = platformID
	}

	if operationNo != "" {
		ex["operation_no"] = operationNo
	}

	if types != "" {
		cashTypes := strings.Split(types, ",")
		for _, v := range cashTypes {
			ct, err := strconv.Atoi(v)
			if _, ok := helper.CashTypes[ct]; !ok || err != nil {
				helper.Print(ctx, false, helper.CashTypeErr)
				return
			}
		}

		if len(cashTypes) > 0 {
			ex["cash_type"] = cashTypes
		}
	}

	if billNo != "" {
		ex = g.Ex{
			"bill_no": billNo,
		}
	}

	data, err := model.BusinessRecordTransaction(page, pageSize, startTime, endTime, proxyId, username, ex)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}
