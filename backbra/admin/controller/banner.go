package controller

import (
	"admin/contrib/helper"
	"admin/model"
	"encoding/json"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/valyala/fasthttp"
	"net/url"
)

type BannerController struct{}

func (that *BannerController) Upload(ctx *fasthttp.RequestCtx) {

	filename, err := model.BannerUpload(ctx)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	helper.Print(ctx, true, filename)
}

func (that *BannerController) Insert(ctx *fasthttp.RequestCtx) {

	data := model.TblBanner{}
	body := ctx.PostBody()

	err := json.Unmarshal(body, &data)
	if err != nil {
		fmt.Println("BannerController Insert json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	args := fasthttp.Args{}
	args.Parse(data.Images)

	//fmt.Println("data.Images = ", data.Images)
	//fmt.Println("args.Len() = ", args.Len())

	if !args.Has("h5") || !args.Has("web") {
		fmt.Println("args.Has(h5) = ", args.Has("h5"))
		fmt.Println("args.Has(web) = ", args.Has("web"))
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	data.Id = helper.GenId()
	data.State = "1"
	data.UpdatedUid = admin["id"]
	data.UpdatedName = admin["name"]
	data.UpdatedAt = uint32(ctx.Time().Unix())
	data.CreatedAt = data.UpdatedAt

	if data.Title != "" {
		decodedValue, err := url.QueryUnescape(data.Title)
		if err != nil {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}
		data.Title = decodedValue
	}

	err = model.BannerInsert(data)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

func (that *BannerController) List(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	state := ctx.QueryArgs().GetUintOrZero("state")
	startTime := string(ctx.QueryArgs().Peek("start_time"))
	endTime := string(ctx.QueryArgs().Peek("end_time"))

	if page < 1 {
		page = 1
	}
	if pageSize < 10 {
		pageSize = 10
	}
	if pageSize > 200 {
		pageSize = 200
	}
	ex := g.Ex{}

	if state != 0 {
		ex["state"] = state
	}

	data, err := model.BannerList(uint(page), uint(pageSize), startTime, endTime, ex)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

// 系统公告编辑
func (that *BannerController) Update(ctx *fasthttp.RequestCtx) {

	data := model.TblBanner{}
	body := ctx.PostBody()

	err := json.Unmarshal(body, &data)
	if err != nil {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	record := g.Record{}
	if data.Title != "" {
		decodedValue, err := url.QueryUnescape(data.Title)
		if err != nil {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}
		record["title"] = decodedValue
	}

	if data.State != "" {
		record["state"] = data.State
	}
	if data.Seq != 0 {
		record["seq"] = data.Seq
	}

	if data.Images != "" {
		args := fasthttp.Args{}
		args.Parse(data.Images)

		if !args.Has("h5") || !args.Has("web") {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}
		record["images"] = data.Images
	}

	if data.RedirectUrl != "" {
		decodedValue, err := url.QueryUnescape(data.RedirectUrl)
		if err != nil {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}
		record["redirect_url"] = decodedValue
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	record["url_type"] = data.UrlType
	record["updated_name"] = admin["name"]
	record["updated_uid"] = admin["id"]
	record["updated_at"] = uint32(ctx.Time().Unix())

	if len(record) == 0 {
		helper.Print(ctx, false, helper.NoDataUpdate)
		return
	}

	err = model.BannerUpdate(data.Id, record)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

// 系统停用 启用 系统审核
func (that *BannerController) UpdateState(ctx *fasthttp.RequestCtx) {

	id := string(ctx.QueryArgs().Peek("id"))
	if !helper.CtypeDigit(id) {
		helper.Print(ctx, false, helper.IDErr)
		return
	}

	state := ctx.QueryArgs().GetUintOrZero("state")
	s := map[int]bool{
		1: true, //停用
		2: true, //启用
		3: true, //启用
	}
	if _, ok := s[state]; !ok {
		helper.Print(ctx, false, helper.StateParamErr)
		return
	}

	err := model.BannerUpdateState(id, state)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

func (that *BannerController) Delete(ctx *fasthttp.RequestCtx) {

	id := string(ctx.QueryArgs().Peek("id"))
	if !helper.CtypeDigit(id) {
		helper.Print(ctx, false, helper.IDErr)
		return
	}

	err := model.BannerDelete(id)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}
