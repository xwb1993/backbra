package controller

import (
	"admin/contrib/helper"
	"admin/model"
	"encoding/json"
	"fmt"
	"github.com/valyala/fasthttp"
)

type CsController struct{}

func (that *CsController) List(ctx *fasthttp.RequestCtx) {

	data, err := model.CsList()
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

func (that *CsController) Update(ctx *fasthttp.RequestCtx) {

	var param model.CSForm

	b := ctx.PostBody()

	err := json.Unmarshal(b, &param)
	if err != nil {
		fmt.Println("CsController Update err ", err.Error())
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	err = model.CsUpdate(param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}
