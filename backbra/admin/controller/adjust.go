package controller

import (
	"admin/contrib/helper"
	"admin/contrib/validator"
	"admin/model"
	"encoding/json"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/valyala/fasthttp"
	"net/url"
	"strconv"
)

type AdjustController struct{}

type insertMemberAdjustParam struct {
	Uid         string `json:"uid" cbor:"uid" db:"uid"`
	Amount      string `json:"amount" cbor:"amount" db:"amount"`
	AdjustMode  int    `json:"adjust_mode" cbor:"adjust_mode" db:"adjust_mode"`
	ApplyRemark string `json:"apply_remark" cbor:"apply_remark" db:"apply_remark"`
}

// Insert 会员列表-账户调整
func (that *AdjustController) Insert(ctx *fasthttp.RequestCtx) {

	param := insertMemberAdjustParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	// amount范围在1-2E越南盾(1k-200000k)
	_, ok := validator.CheckFloatScope(param.Amount, "1", "1000000")
	if !ok {
		helper.Print(ctx, false, helper.AmountErr)
		return
	}

	// get member info
	mb, err := model.MemberFindByUid(param.Uid)
	if err != nil {
		helper.Print(ctx, false, helper.UIDErr)
		return
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	if param.ApplyRemark != "" {
		param.ApplyRemark, _ = url.PathUnescape(param.ApplyRemark)
	}

	record := model.TblMemberAdjust{
		Id:          helper.GenId(),
		Uid:         mb.Uid,
		Username:    mb.Username,
		AdjustMode:  param.AdjustMode,
		ApplyRemark: param.ApplyRemark,
		Amount:      param.Amount,
		State:       1, // 状态:1=审核中,2=同意, 3=拒绝
		ApplyAt:     ctx.Time().Unix(),
		ApplyUid:    admin["id"],   // 申请人
		ApplyName:   admin["name"], // 申请人
		ReviewUid:   "0",
		Tester:      mb.Tester,
	}
	err = model.AdjustInsert(record)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

// List 会员管理-账户调整审核列表
func (that *AdjustController) List(ctx *fasthttp.RequestCtx) {

	id := string(ctx.QueryArgs().Peek("id"))
	uid := string(ctx.QueryArgs().Peek("uid"))
	username := string(ctx.QueryArgs().Peek("username"))       // 会员账号
	applyName := string(ctx.QueryArgs().Peek("apply_name"))    // 申请人
	reviewName := string(ctx.QueryArgs().Peek("review_name"))  // 审核人
	adjustMode := ctx.QueryArgs().GetUintOrZero("adjust_mode") // 调整方式:1=上方,2=下分
	state := ctx.QueryArgs().GetUintOrZero("state")            // 审核状态
	startTime := string(ctx.QueryArgs().Peek("start_time"))    //开始时间
	endTime := string(ctx.QueryArgs().Peek("end_time"))        //结束时间
	sPage := string(ctx.QueryArgs().Peek("page"))              //当前页
	sPageSize := string(ctx.QueryArgs().Peek("page_size"))     //一页多少条
	tester := string(ctx.QueryArgs().Peek("tester"))           // 1正式账号2试玩账号3代理

	if !validator.CheckStringDigit(sPage) || !validator.CheckIntScope(sPageSize, 10, 200) {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	page, _ := strconv.Atoi(sPage)
	pageSize, _ := strconv.Atoi(sPageSize)

	ex := g.Ex{}
	if id != "" {
		if !validator.CheckStringDigit(id) {
			helper.Print(ctx, false, helper.IDErr)
			return
		}

		ex["id"] = id
	}

	if tester == "1" {
		ex["tester"] = 1
	} else if tester == "2" {
		ex["tester"] = 2
	} else if tester == "3" {
		ex["tester"] = 3
	}

	if uid != "" {
		ex["uid"] = uid
	}
	if username != "" {
		ex["username"], _ = url.PathUnescape(username)
	}

	if applyName != "" {
		if !validator.CheckAName(applyName, 5, 20) {
			helper.Print(ctx, false, helper.ApplyNameErr)
			return
		}

		ex["apply_name"], _ = url.PathUnescape(applyName)
	}

	if reviewName != "" {

		if !validator.CheckAName(reviewName, 5, 20) {
			helper.Print(ctx, false, helper.ReviewNameErr)
			return
		}

		ex["review_name"], _ = url.PathUnescape(reviewName)
	}

	if state > 0 {
		s := map[int]bool{
			1: true, //AdjustReviewing
			2: true, //AdjustReviewPass
			3: true, //AdjustReviewReject
		}
		if _, ok := s[state]; !ok {
			helper.Print(ctx, false, helper.ReviewStateErr)
			return
		}

		ex["state"] = state
	} else {
		ex["state"] = []int{2, 3}
	}

	if adjustMode > 0 {
		s := map[int]bool{
			1: true, //AdjustUpMode
			2: true, //AdjustDownMode
		}
		if _, ok := s[adjustMode]; !ok {
			helper.Print(ctx, false, helper.HandOutStateErr)
			return
		}

		ex["adjust_mode"] = adjustMode
	}

	data, err := model.AdjustList(startTime, endTime, ex, page, pageSize)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

type reviewMemberAdjustParam struct {
	Id           string `json:"id"`
	State        string `json:"state"`
	ReviewRemark string `json:"review_remark"`
}

// Review 会员管理-账户调整审核
func (that *AdjustController) Review(ctx *fasthttp.RequestCtx) {

	param := reviewMemberAdjustParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	if param.State != "2" && param.State != "3" {
		helper.Print(ctx, false, helper.StateParamErr)
		return
	}

	if param.ReviewRemark != "" {
		param.ReviewRemark, _ = url.PathUnescape(param.ReviewRemark)
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	record := g.Record{
		"id":            param.Id,
		"review_remark": param.ReviewRemark,
		"state":         param.State,
		"review_at":     ctx.Time().Unix(),
		"review_uid":    admin["id"],
		"review_name":   admin["name"],
	}
	err = model.AdjustReview(param.State, record)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}
