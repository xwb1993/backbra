package controller

import (
	"admin/contrib/helper"
	"admin/model"
	"encoding/json"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/valyala/fasthttp"
)

type ChannelConfigController struct{}

// ConfigList 代理渠道配置列表
func (that ChannelConfigController) ConfigList(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	id := string(ctx.QueryArgs().Peek("id"))
	operatorName := string(ctx.QueryArgs().Peek("operator_name"))
	state := string(ctx.QueryArgs().Peek("state"))

	ex := g.Ex{}
	if id != "" {
		ex["id"] = id
	}
	if operatorName != "" {
		ex["operator_name"] = operatorName
	}
	if state != "" {
		ex["state"] = state
	}
	s, err := model.ChannelConfigList(uint(page), uint(pageSize), ex)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

// ConfigInsert 代理渠道配置新增
func (that *ChannelConfigController) ConfigInsert(ctx *fasthttp.RequestCtx) {

	param := model.TblAgentChannelConfig{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	err = model.ChannelConfigInsert(admin["name"], admin["id"], param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, "succeed")
}

// ConfigUpdate 代理渠道配置更新
func (that ChannelConfigController) ConfigUpdate(ctx *fasthttp.RequestCtx) {

	param := model.TblAgentChannelConfig{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	err = model.ChannelConfigUpdate(admin["name"], admin["id"], param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

type merchantLogin struct {
	OperatorName string `json:"operator_name" cbor:"operator_name"`
	Password     string `json:"password" cbor:"password"`
}

func (that *ChannelConfigController) Login(ctx *fasthttp.RequestCtx) {
	deviceNo := string(ctx.Request.Header.Peek("no"))
	param := merchantLogin{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	resp, err := model.MerchantLogin(deviceNo, param.OperatorName, param.Password)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, resp)
}
