package controller

import (
	"admin/contrib/helper"
	"admin/model"
	"encoding/json"
	"fmt"
	"github.com/valyala/fasthttp"
	"net/url"
)

type GroupController struct{}

func (that *GroupController) Update(ctx *fasthttp.RequestCtx) {

	param := model.GroupUpdateParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	param.Gname, _ = url.QueryUnescape(param.Gname)
	param.Noted, _ = url.QueryUnescape(param.Noted)
	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	err = model.GroupUpdate(param.ID, admin["group_id"], admin["name"], param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, "succeed")
}

func (that *GroupController) Insert(ctx *fasthttp.RequestCtx) {

	param := model.GroupInsertParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	param.Gname, _ = url.QueryUnescape(param.Gname)
	param.Noted, _ = url.QueryUnescape(param.Noted)
	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	// 新增权限信息
	err = model.GroupInsert(admin["group_id"], admin["name"], param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, "succeed")
}

/**
* @Description: 用户组列表获取
* @Author: carl
 */
func (that *GroupController) List(ctx *fasthttp.RequestCtx) {

	gid := string(ctx.QueryArgs().Peek("gid"))
	if gid != "" {
		if !helper.CtypeDigit(gid) {
			helper.Print(ctx, false, helper.GroupIDErr)
			return
		}
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	// 获取权限列表
	data, err := model.GroupList(gid, admin["group_id"])
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	ctx.SetBody([]byte(data))
}
