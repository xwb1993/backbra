package controller

import (
	"admin/contrib/helper"
	"admin/contrib/validator"
	"admin/model"
	"encoding/json"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/valyala/fasthttp"
	"net/url"
	"strconv"
	"strings"
)

type WithdrawController struct{}

// 风控审核 拒绝
type withdrawRiskRejectParam struct {
	ID             string `json:"id"`
	ReviewRemark   string `json:"review_remark"`
	WithdrawRemark string `json:"withdraw_remark"`
}

// 订单挂起
type withdrawHangUp struct {
	ID           string `json:"id"`
	HangUpRemark string `json:"hang_up_remark"`
}

// 提现拒绝
type withdrawReviewReject struct {
	ID             string `json:"id"`
	Remark         string `json:"remark"`
	WithdrawRemark string `json:"withdraw_remark"`
}

// 提款审核
type withdrawReviewParam struct {
	ID  string `json:"id"`
	Pid string `json:"pid"`
}

type riskWaitConfirmParam struct {
	Id        string `json:"id"`         //订单号
	Uid       string `json:"uid"`        //订单号
	RealName  string `json:"real_name"`  //真实姓名
	StartTime string `json:"start_time"` //开始时间
	EndTime   string `json:"end_time"`   //结束时间
	Username  string `json:"username"`   //用户账号
	MaxAmount string `json:"max_amount"` //最大金额
	MinAmount string `json:"min_amount"` //最小金额
	Vips      string `json:"vips"`       //vip等级
	Page      uint   `json:"page"`
	PageSize  uint   `json:"page_size"`
}

// RiskWaitConfirmList 风控待领取列表
func (that *WithdrawController) RiskWaitConfirmList(ctx *fasthttp.RequestCtx) {

	param := riskWaitConfirmParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	if param.StartTime == "" || param.EndTime == "" {
		helper.Print(ctx, false, helper.DateTimeErr)
		return
	}

	ex := g.Ex{}
	if param.RealName != "" {
		ex["real_name"] = param.RealName
	}

	if param.Uid != "" {
		ex["uid"] = param.Uid
	}

	if param.Username != "" {

		mb, err := model.MemberFindByUsername(param.Username)
		if err != nil {
			helper.Print(ctx, false, helper.UserNotExist)
			return
		}

		ex["uid"] = mb.Uid
	}

	if param.MinAmount != "" && param.MaxAmount != "" {

		if !validator.CheckStringDigit(param.MinAmount) || !validator.CheckStringDigit(param.MaxAmount) {
			helper.Print(ctx, false, helper.AmountErr)
			return
		}

		minAmountInt, _ := strconv.Atoi(param.MinAmount)
		maxAmountInt, _ := strconv.Atoi(param.MaxAmount)
		if minAmountInt > maxAmountInt {
			helper.Print(ctx, false, helper.AmountErr)
			return
		}

		ex["amount"] = g.Op{"between": exp.NewRangeVal(minAmountInt, maxAmountInt)}
	}

	if param.Vips != "" {
		vipSlice := strings.Split(param.Vips, ",")
		for _, v := range vipSlice {
			if !validator.CheckStringDigit(v) || !validator.CheckIntScope(v, 1, 11) {
				helper.Print(ctx, false, helper.MemberLevelErr)
				return
			}
		}
		ex["level"] = vipSlice
	}

	if param.Id != "" {
		if !validator.CheckStringDigit(param.Id) {
			helper.Print(ctx, false, helper.IDErr)
			return
		}
		ex = g.Ex{"id": param.Id}
	}
	// 待派单
	ex["state"] = model.WithdrawReviewing

	data, err := model.WithdrawList(ex, 1, param.StartTime, param.EndTime, param.Page, param.PageSize)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

type riskReviewListParam struct {
	Id          string `json:"id"`
	Uid         string `json:"uid"`
	RealName    string `json:"real_name"`
	ConfirmName string `json:"confirm_name"`
	Page        uint   `json:"page"`
	PageSize    uint   `json:"page_size"`
}

// RiskReviewList 风控待审核列表
func (that *WithdrawController) RiskReviewList(ctx *fasthttp.RequestCtx) {

	param := riskReviewListParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	ex := g.Ex{}
	if param.RealName != "" {
		if len([]rune(param.Id)) > 30 {
			helper.Print(ctx, false, helper.RealNameFMTErr)
			return
		}

		ex["real_name"] = param.RealName
	}

	if param.ConfirmName != "" {
		if !validator.CheckStringAlnum(param.ConfirmName) {
			helper.Print(ctx, false, helper.AdminNameErr)
			return
		}
		ex["confirm_name"] = param.ConfirmName
	}

	if param.Id != "" {
		if !validator.CheckStringDigit(param.Id) {
			helper.Print(ctx, false, helper.IDErr)
			return
		}
		ex = g.Ex{"id": param.Id}
	}

	if param.Uid != "" {
		if !validator.CheckStringDigit(param.Uid) {
			helper.Print(ctx, false, helper.IDErr)
			return
		}
		ex = g.Ex{"uid": param.Uid}
	}

	// 已派单
	ex["state"] = model.WithdrawDispatched
	data, err := model.WithdrawList(ex, 3, "", "", param.Page, param.PageSize)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

type riskReviewParam struct {
	Id string `json:"id"`
}

// RiskReview 风控审核 通过
func (that *WithdrawController) RiskReview(ctx *fasthttp.RequestCtx) {

	param := riskReviewParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	if !validator.CheckStringDigit(param.Id) {
		helper.Print(ctx, false, helper.IDErr)
		return
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	// 加锁
	err = model.WithdrawLock(param.Id)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	defer model.WithdrawUnLock(param.Id)

	withdraw, err := model.WithdrawFind(param.Id)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	if withdraw.State != model.WithdrawDispatched {
		helper.Print(ctx, false, helper.OrderStateErr)
		return
	}

	record := g.Record{
		"state":        model.WithdrawDealing,
		"confirm_at":   ctx.Time().Unix(),
		"confirm_uid":  admin["id"],
		"confirm_name": admin["name"],
	}

	err = model.WithdrawUpdateInfo(param.Id, record)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

// RiskReviewReject 风控审核 拒绝
func (that *WithdrawController) RiskReviewReject(ctx *fasthttp.RequestCtx) {

	param := withdrawRiskRejectParam{}
	err := validator.Bind(ctx, &param)
	if err != nil {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	// 加锁
	err = model.WithdrawLock(param.ID)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	defer model.WithdrawUnLock(param.ID)

	withdraw, err := model.WithdrawFind(param.ID)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	if withdraw.State != model.WithdrawDispatched {
		helper.Print(ctx, false, helper.OrderStateErr)
		return
	}

	record := g.Record{
		"state":           model.WithdrawReviewReject,
		"review_remark":   param.ReviewRemark,
		"withdraw_remark": param.WithdrawRemark,
		"confirm_at":      ctx.Time().Unix(),
		"confirm_uid":     admin["id"],
		"confirm_name":    admin["name"],
	}
	err = model.WithdrawRiskReview(param.ID, model.WithdrawReviewReject, record, withdraw)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

// HangUp 订单挂起
func (that *WithdrawController) HangUp(ctx *fasthttp.RequestCtx) {

	param := withdrawHangUp{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	// 加锁
	err = model.WithdrawLock(param.ID)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	defer model.WithdrawUnLock(param.ID)

	withdraw, err := model.WithdrawFind(param.ID)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	if withdraw.State != model.WithdrawDispatched {
		helper.Print(ctx, false, helper.OrderStateErr)
		return
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}
	//
	//if withdraw.ConfirmUID != admin["id"] {
	//	helper.Print(ctx, false, helper.MethodNoPermission)
	//	return
	//}

	record := g.Record{
		"hang_up_uid":    admin["id"],
		"hang_up_remark": param.HangUpRemark,
		"hang_up_name":   admin["name"],
		"state":          model.WithdrawHangup,
		"hang_up_at":     ctx.Time().Unix(),
		"receive_at":     "0",
	}

	err = model.WithdrawUpdateInfo(param.ID, record)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

type hangUpListParam struct {
	Id           string `json:"id"`
	Uid          string `json:"uid"`
	RealName     string `json:"real_name"`
	StartTime    string `json:"start_time"`
	EndTime      string `json:"end_time"`
	MaxAmount    string `json:"max_amount"`
	MinAmount    string `json:"min_amount"`
	Username     string `json:"username"`
	HangUpName   string `json:"hang_up_name"`
	Vips         string `json:"vips"`
	Page         uint   `json:"page"`
	PageSize     uint   `json:"page_size"`
	HangUpRemark string `json:"hang_up_remark"` //挂起原因

}

// HangUpList 风控审核挂起列表
func (that *WithdrawController) HangUpList(ctx *fasthttp.RequestCtx) {

	param := hangUpListParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	if param.StartTime == "" || param.EndTime == "" {
		helper.Print(ctx, false, helper.DateTimeErr)
		return
	}

	ex := g.Ex{}
	if param.RealName != "" {
		ex["real_name"] = param.RealName
	}
	if param.Uid != "" {
		ex["uid"] = param.Uid
	}

	if param.MinAmount != "" && param.MaxAmount != "" {
		if !validator.CheckStringDigit(param.MinAmount) || !validator.CheckStringDigit(param.MaxAmount) {
			helper.Print(ctx, false, helper.AmountErr)
			return
		}

		minAmountInt, _ := strconv.Atoi(param.MinAmount)
		maxAmountInt, _ := strconv.Atoi(param.MaxAmount)
		if minAmountInt > maxAmountInt {
			helper.Print(ctx, false, helper.AmountErr)
			return
		}

		ex["amount"] = g.Op{"between": exp.NewRangeVal(minAmountInt, maxAmountInt)}
	}

	if param.Username != "" {
		if !validator.CheckUName(param.Username, 5, 14) {
			helper.Print(ctx, false, helper.UsernameErr)
			return
		}

		mb, err := model.MemberFindByUsername(param.Username)
		if err != nil {
			helper.Print(ctx, false, helper.UserNotExist)
			return
		}

		ex["uid"] = mb.Uid
	}

	if param.HangUpName != "" {
		if !validator.CheckAName(param.HangUpName, 5, 20) {
			helper.Print(ctx, false, helper.AdminNameErr)
			return
		}

		ex["hang_up_name"] = param.HangUpName
	}

	if param.Vips != "" {
		vipSlice := strings.Split(param.Vips, ",")
		for _, v := range vipSlice {
			if !validator.CheckStringDigit(v) || !validator.CheckIntScope(v, 1, 11) {
				helper.Print(ctx, false, helper.MemberLevelErr)
				return
			}
		}
		ex["level"] = vipSlice
	}

	if param.Id != "" {
		if !validator.CheckStringDigit(param.Id) {
			helper.Print(ctx, false, helper.IDErr)
			return
		}

		ex = g.Ex{"id": param.Id}
	}

	if param.HangUpRemark != "" {
		ex["hang_up_remark"], _ = url.PathUnescape(param.HangUpRemark)
	}
	// 挂起
	ex["state"] = model.WithdrawHangup
	data, err := model.WithdrawList(ex, 1, param.StartTime, param.EndTime, param.Page, param.PageSize)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

type confirmNameUpdateParam struct {
	Id          string `json:"id"`
	ConfirmUid  string `json:"confirm_uid"`
	ConfirmName string `json:"confirm_name"`
}

// ConfirmNameUpdate 修改领取人
func (that *WithdrawController) ConfirmNameUpdate(ctx *fasthttp.RequestCtx) {

	param := confirmNameUpdateParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	if !validator.CheckStringDigit(param.Id) {
		helper.Print(ctx, false, helper.IDErr)
		return
	}

	// 加锁
	err = model.WithdrawLock(param.Id)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	defer model.WithdrawUnLock(param.Id)

	withdraw, err := model.WithdrawFind(param.Id)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	if withdraw.State != model.WithdrawDispatched {
		helper.Print(ctx, false, helper.OrderStateErr)
		return
	}

	newConfirmUID := string(ctx.FormValue("confirm_uid"))
	newConfirmName := string(ctx.FormValue("confirm_name"))
	if newConfirmName == "" || newConfirmUID == "" {
		helper.Print(ctx, false, helper.AdminNameErr)
		return
	}

	record := g.Record{
		"confirm_uid":  newConfirmUID,
		"confirm_name": newConfirmName,
		"receive_at":   ctx.Time().Unix(),
	}
	err = model.WithdrawUpdateInfo(param.Id, record)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

type receiveParam struct {
	Id string `json:"id"`
}

// Receive 领取
func (that *WithdrawController) Receive(ctx *fasthttp.RequestCtx) {

	param := receiveParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	id := param.Id
	if !validator.CheckStringDigit(id) {
		helper.Print(ctx, false, helper.IDErr)
		return
	}

	// 加锁
	err = model.WithdrawLock(id)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	defer model.WithdrawUnLock(id)

	withdraw, err := model.WithdrawFind(id)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	if withdraw.State != model.WithdrawHangup && withdraw.State != model.WithdrawReviewing {
		helper.Print(ctx, false, helper.OrderTakenAlready)
		return
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	record := g.Record{
		"confirm_uid":  admin["id"],
		"confirm_name": admin["name"],
		"state":        model.WithdrawDispatched,
		"receive_at":   ctx.Time().Unix(),
	}
	err = model.WithdrawUpdateInfo(id, record)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	helper.Print(ctx, true, helper.Success)
}

type riskHistoryParam struct {
	Id          string `json:"id"`
	Uid         string `json:"uid"`
	RealName    string `json:"real_name"`
	StartTime   string `json:"start_time"`
	EndTime     string `json:"end_time"`
	MaxAmount   string `json:"max_amount"`
	MinAmount   string `json:"min_amount"`
	Username    string `json:"username"`
	ConfirmName string `json:"confirm_name"`
	Vips        string `json:"vips"`
	State       string `json:"state"`
	Ty          string `json:"ty"`
	SortField   string `json:"sort_field"`
	IsAsc       string `json:"is_asc"`
	Page        uint   `json:"page"`
	PageSize    uint   `json:"page_size"`
	OperatorId  string `json:"operator_id"`
	ProxyId     string `json:"proxy_id"`
}

// RiskHistory 历史记录列表
func (that *WithdrawController) RiskHistory(ctx *fasthttp.RequestCtx) {

	param := riskHistoryParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	if param.StartTime == "" || param.EndTime == "" {
		helper.Print(ctx, false, helper.DateTimeErr)
		return
	}

	if param.SortField != "" {
		sortFields := map[string]string{
			"amount": "amount",
		}

		if _, ok := sortFields[param.SortField]; !ok {
			helper.Print(ctx, false, helper.ParamErr)
			return
		} else {
			param.SortField = sortFields[param.SortField]
		}

		if !validator.CheckIntScope(param.IsAsc, 0, 1) {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}
	}

	ex := g.Ex{}
	if param.RealName != "" {

		ex["real_name"] = param.RealName
	}

	if param.MinAmount != "" && param.MaxAmount != "" {
		if !validator.CheckStringDigit(param.MinAmount) || !validator.CheckStringDigit(param.MaxAmount) {
			helper.Print(ctx, false, helper.AmountErr)
			return
		}

		minAmountInt, _ := strconv.Atoi(param.MinAmount)
		maxAmountInt, _ := strconv.Atoi(param.MaxAmount)
		if minAmountInt > maxAmountInt {
			helper.Print(ctx, false, helper.AmountErr)
			return
		}

		ex["amount"] = g.Op{"between": exp.NewRangeVal(minAmountInt, maxAmountInt)}
	}

	if param.Username != "" {
		if !validator.CheckUName(param.Username, 5, 14) {
			helper.Print(ctx, false, helper.UsernameErr)
			return
		}
		mb, err := model.MemberFindByUsername(param.Username)
		if err != nil {
			helper.Print(ctx, false, helper.UserNotExist)
			return
		}
		ex["uid"] = mb.Uid
	}

	if param.ConfirmName != "" {
		if !validator.CheckStringAlnum(param.ConfirmName) {
			helper.Print(ctx, false, helper.AdminNameErr)
			return
		}

		ex["confirm_name"] = param.ConfirmName
	}

	if param.Vips != "" {
		vipSlice := strings.Split(param.Vips, ",")
		for _, v := range vipSlice {
			if !validator.CheckStringDigit(v) {
				helper.Print(ctx, false, helper.MemberLevelErr)
				return
			}
		}

		var vipInterface []interface{}
		for _, v := range vipSlice {
			vipInterface = append(vipInterface, v)
		}
		ex["level"] = vipInterface
	}

	baseState := []interface{}{
		model.WithdrawReviewReject,
		model.WithdrawDealing,
		model.WithdrawSuccess,
		model.WithdrawFailed,
		model.WithdrawAbnormal,
		model.WithdrawAutoPayFailed,
	}
	ex["state"] = baseState

	if param.State != "" {
		stateInt, err := strconv.Atoi(param.State)
		if err != nil {
			helper.Print(ctx, false, helper.StateParamErr)
			return
		}

		if stateInt < model.WithdrawReviewReject && stateInt > model.WithdrawAutoPayFailed {
			helper.Print(ctx, false, helper.StateParamErr)
			return
		}

		ex["state"] = param.State
	}

	if param.Id != "" {
		if !validator.CheckStringDigit(param.Id) {
			helper.Print(ctx, false, helper.IDErr)
			return
		}

		ex = map[string]interface{}{
			"id":    param.Id,
			"state": baseState,
		}
	}

	if param.Uid != "" {
		if !validator.CheckStringDigit(param.Uid) {
			helper.Print(ctx, false, helper.IDErr)
			return
		}

		ex["uid"] = param.Uid
	}

	data, err := model.WithdrawHistoryList(ex, param.Ty, param.StartTime, param.EndTime, param.IsAsc, param.SortField, param.Page, param.PageSize)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

// FinanceReviewList 提款管理-财务审核
func (that *WithdrawController) FinanceReviewList(ctx *fasthttp.RequestCtx) {

	param := riskHistoryParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	if param.StartTime == "" || param.EndTime == "" {
		helper.Print(ctx, false, helper.DateTimeErr)
		return
	}

	ex := g.Ex{}
	if param.MinAmount != "" && param.MaxAmount != "" {
		if !validator.CheckStringDigit(param.MinAmount) || !validator.CheckStringDigit(param.MaxAmount) {
			helper.Print(ctx, false, helper.AmountErr)
			return
		}

		minAmountInt, _ := strconv.Atoi(param.MinAmount)
		maxAmountInt, _ := strconv.Atoi(param.MaxAmount)
		if minAmountInt > maxAmountInt {
			helper.Print(ctx, false, helper.AmountErr)
			return
		}

		ex["amount"] = g.Op{"between": exp.NewRangeVal(minAmountInt, maxAmountInt)}
	}

	if param.Username != "" {
		if !validator.CheckUName(param.Username, 5, 14) {
			helper.Print(ctx, false, helper.UsernameErr)
			return
		}

		mb, err := model.MemberFindByUsername(param.Username)
		if err != nil {
			helper.Print(ctx, false, helper.UserNotExist)
			return
		}

		ex["uid"] = mb.Uid
	}
	if param.Uid != "" {
		ex["uid"] = param.Uid
	}

	if param.ConfirmName != "" {
		if !validator.CheckAName(param.ConfirmName, 5, 20) {
			helper.Print(ctx, false, helper.AdminNameErr)
			return
		}

		ex["confirm_name"] = param.ConfirmName
	}

	if param.Id != "" {
		if !validator.CheckStringDigit(param.Id) {
			helper.Print(ctx, false, helper.IDErr)
			return
		}

		ex = map[string]interface{}{
			"id": param.Id,
		}
	}

	ex["state"] = []int{
		model.WithdrawDealing,
		model.WithdrawAutoPayFailed,
	}

	data, err := model.WithdrawList(ex, 1, param.StartTime, param.EndTime, param.Page, param.PageSize)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

// HistoryList 后台提款列表
func (that *WithdrawController) HistoryList(ctx *fasthttp.RequestCtx) {

	param := riskHistoryParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	if param.StartTime == "" || param.EndTime == "" {
		helper.Print(ctx, false, helper.DateTimeErr)
		return
	}

	ex := g.Ex{}
	if param.MinAmount != "" && param.MaxAmount != "" {
		if !validator.CheckStringDigit(param.MinAmount) || !validator.CheckStringDigit(param.MaxAmount) {
			helper.Print(ctx, false, helper.AmountErr)
			return
		}

		minAmountInt, _ := strconv.Atoi(param.MinAmount)
		maxAmountInt, _ := strconv.Atoi(param.MaxAmount)
		if minAmountInt > maxAmountInt {
			helper.Print(ctx, false, helper.AmountErr)
			return
		}

		ex["amount"] = g.Op{"between": exp.NewRangeVal(minAmountInt, maxAmountInt)}
	}

	if param.Username != "" {
		if !validator.CheckUName(param.Username, 5, 14) {
			helper.Print(ctx, false, helper.UsernameErr)
			return
		}
		mb, err := model.MemberFindByUsername(param.Username)
		if err != nil {
			helper.Print(ctx, false, helper.UserNotExist)
			return
		}
		ex["uid"] = mb.Uid
	}

	baseState := []interface{}{
		model.WithdrawSuccess,
		model.WithdrawFailed,
		model.WithdrawAbnormal,
		model.WithdrawReviewReject,
		model.WithdrawDispatched,
		model.WithdrawAutoPaying,
	}
	ex["state"] = baseState

	if param.State != "" {
		stateInt, err := strconv.Atoi(param.State)
		if err != nil {
			helper.Print(ctx, false, helper.StateParamErr)
			return
		}

		if stateInt < model.WithdrawSuccess && stateInt > model.WithdrawAbnormal {
			helper.Print(ctx, false, helper.StateParamErr)
			return
		}

		ex["state"] = param.State
	}

	if param.Id != "" {
		if !validator.CheckStringDigit(param.Id) {
			helper.Print(ctx, false, helper.IDErr)
			return
		}

		ex = map[string]interface{}{
			"id":    param.Id,
			"state": baseState,
		}
	}

	if param.Uid != "" {
		if !validator.CheckStringDigit(param.Uid) {
			helper.Print(ctx, false, helper.IDErr)
			return
		}

		ex["uid"] = param.Uid
	}

	if param.SortField != "" {
		sortFields := map[string]string{
			"amount": "amount",
		}

		if _, ok := sortFields[param.SortField]; !ok {
			helper.Print(ctx, false, helper.ParamErr)
			return
		} else {
			param.SortField = sortFields[param.SortField]
		}

		if !validator.CheckIntScope(param.IsAsc, 0, 1) {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}
	}

	data, err := model.WithdrawHistoryList(ex, param.Ty, param.StartTime, param.EndTime, param.IsAsc, param.SortField, param.Page, param.PageSize)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

// MerchantHistoryList 渠道后台提款列表
func (that *WithdrawController) MerchantHistoryList(ctx *fasthttp.RequestCtx) {

	param := riskHistoryParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	if param.StartTime == "" || param.EndTime == "" {
		helper.Print(ctx, false, helper.DateTimeErr)
		return
	}

	if param.OperatorId == "" {
		helper.Print(ctx, false, helper.OperatorIdErr)
		return
	}

	ex := g.Ex{}
	if param.MinAmount != "" && param.MaxAmount != "" {
		if !validator.CheckStringDigit(param.MinAmount) || !validator.CheckStringDigit(param.MaxAmount) {
			helper.Print(ctx, false, helper.AmountErr)
			return
		}

		minAmountInt, _ := strconv.Atoi(param.MinAmount)
		maxAmountInt, _ := strconv.Atoi(param.MaxAmount)
		if minAmountInt > maxAmountInt {
			helper.Print(ctx, false, helper.AmountErr)
			return
		}

		ex["amount"] = g.Op{"between": exp.NewRangeVal(minAmountInt, maxAmountInt)}
	}

	uids := make([]string, 0)
	if param.Username != "" {
		uName, _ := url.PathUnescape(param.Username)
		if !validator.CheckUName(uName, 5, 50) {
			helper.Print(ctx, false, helper.UsernameErr)
			return
		}
		mb, err := model.MemberFindByUnameAndOpeId(uName, param.OperatorId)
		if err != nil {
			helper.Print(ctx, false, helper.UserNotExist)
			return
		}
		ex["username"] = uName
		ex["uid"] = mb.Uid
	} else {
		members, err := model.MemberFindByOperatorId(param.OperatorId)
		if err != nil {
			helper.Print(ctx, false, helper.OperatorIdErr)
			return
		}
		if len(members) > 0 {
			for _, val := range members {
				uids = append(uids, val.Uid)
			}
			ex["uid"] = uids
		}
	}

	baseState := []interface{}{
		model.WithdrawSuccess,
		model.WithdrawFailed,
		model.WithdrawAbnormal,
		model.WithdrawReviewReject,
		model.WithdrawDispatched,
		model.WithdrawAutoPaying,
	}
	ex["state"] = baseState

	if param.State != "" {
		stateInt, err := strconv.Atoi(param.State)
		if err != nil {
			helper.Print(ctx, false, helper.StateParamErr)
			return
		}

		if stateInt < model.WithdrawSuccess && stateInt > model.WithdrawAbnormal {
			helper.Print(ctx, false, helper.StateParamErr)
			return
		}

		ex["state"] = param.State
	}

	if param.Id != "" {
		if !validator.CheckStringDigit(param.Id) {
			helper.Print(ctx, false, helper.IDErr)
			return
		}

		ex = map[string]interface{}{
			"id":    param.Id,
			"state": baseState,
		}
	}

	if param.SortField != "" {
		sortFields := map[string]string{
			"amount": "amount",
		}

		if _, ok := sortFields[param.SortField]; !ok {
			helper.Print(ctx, false, helper.ParamErr)
			return
		} else {
			param.SortField = sortFields[param.SortField]
		}

		if !validator.CheckIntScope(param.IsAsc, 0, 1) {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}
	}

	data, err := model.WithdrawHistoryList(ex, param.Ty, param.StartTime, param.EndTime, param.IsAsc, param.SortField, param.Page, param.PageSize)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

// BusinessHistoryList 业务员后台提款列表
func (that *WithdrawController) BusinessHistoryList(ctx *fasthttp.RequestCtx) {

	param := riskHistoryParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	if param.StartTime == "" || param.EndTime == "" {
		helper.Print(ctx, false, helper.DateTimeErr)
		return
	}

	if param.ProxyId == "" {
		helper.Print(ctx, false, helper.ProxyIdErr)
		return
	}

	ex := g.Ex{}
	if param.MinAmount != "" && param.MaxAmount != "" {
		if !validator.CheckStringDigit(param.MinAmount) || !validator.CheckStringDigit(param.MaxAmount) {
			helper.Print(ctx, false, helper.AmountErr)
			return
		}

		minAmountInt, _ := strconv.Atoi(param.MinAmount)
		maxAmountInt, _ := strconv.Atoi(param.MaxAmount)
		if minAmountInt > maxAmountInt {
			helper.Print(ctx, false, helper.AmountErr)
			return
		}

		ex["amount"] = g.Op{"between": exp.NewRangeVal(minAmountInt, maxAmountInt)}
	}

	uids := make([]string, 0)
	if param.Username != "" {
		uName, _ := url.PathUnescape(param.Username)
		if !validator.CheckUName(uName, 5, 50) {
			helper.Print(ctx, false, helper.UsernameErr)
			return
		}
		mb, err := model.MemberFindByUnameAndProId(uName, param.ProxyId)
		if err != nil {
			helper.Print(ctx, false, helper.UserNotExist)
			return
		}
		ex["username"] = uName
		ex["uid"] = mb.Uid
	} else {
		members, err := model.MemberFindByProxyId(param.ProxyId)
		if err != nil {
			helper.Print(ctx, false, helper.ProxyIdErr)
			return
		}
		if len(members) > 0 {
			for _, val := range members {
				uids = append(uids, val.Uid)
			}
			ex["uid"] = uids
		}
	}

	baseState := []interface{}{
		model.WithdrawSuccess,
		model.WithdrawFailed,
		model.WithdrawAbnormal,
		model.WithdrawReviewReject,
		model.WithdrawDispatched,
		model.WithdrawAutoPaying,
	}
	ex["state"] = baseState

	if param.State != "" {
		stateInt, err := strconv.Atoi(param.State)
		if err != nil {
			helper.Print(ctx, false, helper.StateParamErr)
			return
		}

		if stateInt < model.WithdrawSuccess && stateInt > model.WithdrawAbnormal {
			helper.Print(ctx, false, helper.StateParamErr)
			return
		}

		ex["state"] = param.State
	}

	if param.Id != "" {
		if !validator.CheckStringDigit(param.Id) {
			helper.Print(ctx, false, helper.IDErr)
			return
		}

		ex = map[string]interface{}{
			"id":    param.Id,
			"state": baseState,
		}
	}

	if param.SortField != "" {
		sortFields := map[string]string{
			"amount": "amount",
		}

		if _, ok := sortFields[param.SortField]; !ok {
			helper.Print(ctx, false, helper.ParamErr)
			return
		} else {
			param.SortField = sortFields[param.SortField]
		}

		if !validator.CheckIntScope(param.IsAsc, 0, 1) {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}
	}

	data, err := model.WithdrawHistoryList(ex, param.Ty, param.StartTime, param.EndTime, param.IsAsc, param.SortField, param.Page, param.PageSize)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

// ReviewReject 后台审核拒绝
func (that *WithdrawController) ReviewReject(ctx *fasthttp.RequestCtx) {

	param := withdrawReviewReject{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	// 加锁
	err = model.WithdrawLock(param.ID)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	defer model.WithdrawUnLock(param.ID)

	withdraw, err := model.WithdrawFind(param.ID)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	if withdraw.State != model.WithdrawDealing {
		helper.Print(ctx, false, helper.OrderStateErr)
		return
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	/*
		logMsg := fmt.Sprintf("拒绝【订单号:%s；会员账号:%s；订单金额:%.4f；申请时间:%s；完成时间:%s】",
			withdraw.ID, withdraw.Username, withdraw.Amount, model.TimeFormat(withdraw.CreatedAt), model.TimeFormat(ctx.Time().Unix()))
		defer model.SystemLogWrite(logMsg, ctx)
	*/

	record := g.Record{
		"review_remark":   param.Remark,
		"withdraw_remark": param.WithdrawRemark,
		"withdraw_at":     ctx.Time().Unix(),
		"withdraw_uid":    admin["id"],
		"withdraw_name":   admin["name"],
		"state":           model.WithdrawFailed,
	}

	err = model.WithdrawReject(param.ID, record)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

// Review 审核
func (that *WithdrawController) Review(ctx *fasthttp.RequestCtx) {

	param := withdrawReviewParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	// 加锁
	err = model.WithdrawLock(param.ID)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	defer model.WithdrawUnLock(param.ID)

	withdraw, err := model.WithdrawFind(param.ID)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	// 自动出款, 状态必须是自动出款失败的才可以手动代付或出款
	if withdraw.State != model.WithdrawDealing {
		helper.Print(ctx, false, helper.OrderStateErr)
		return
	}

	param.Pid = "778889999"
	err = model.WithdrawHandToAuto(ctx, withdraw.UID, withdraw.Username, withdraw.ID, param.Pid, withdraw.Amount, withdraw, ctx.Time())
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)

}

type automaticFailedParam struct {
	Id string `json:"id"`
}

// AutomaticFailed 代付失败
func (that *WithdrawController) AutomaticFailed(ctx *fasthttp.RequestCtx) {

	param := automaticFailedParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	if !validator.CheckStringDigit(param.Id) {
		helper.Print(ctx, false, helper.IDErr)
		return
	}

	// 加锁
	err = model.WithdrawLock(param.Id)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	defer model.WithdrawUnLock(param.Id)

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	withdraw, err := model.WithdrawFind(param.Id)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	err = model.WithdrawAutoPaySetFailed(withdraw.ID, ctx.Time().Unix(), admin["id"], admin["name"])
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}
