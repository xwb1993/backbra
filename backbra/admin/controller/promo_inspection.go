package controller

import (
	"admin/contrib/helper"
	"admin/model"
	"github.com/valyala/fasthttp"
)

type PromoInspectionController struct{}

// 充值配置列表
func (that PromoInspectionController) List(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	uid := string(ctx.QueryArgs().Peek("uid"))

	s, err := model.PromoInspectionList(uint(page), uint(pageSize), uid)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

// 渠道打码记录列表
func (that PromoInspectionController) MerchantList(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	uid := string(ctx.QueryArgs().Peek("uid"))
	operatorId := string(ctx.QueryArgs().Peek("operator_id"))

	if operatorId == "" {
		helper.Print(ctx, false, helper.OperatorIdErr)
		return
	}
	s, err := model.MerchantPromoInspectionList(uint(page), uint(pageSize), uid, operatorId)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

// 业务员打码记录列表
func (that PromoInspectionController) BusinessList(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	uid := string(ctx.QueryArgs().Peek("uid"))
	proxyId := string(ctx.QueryArgs().Peek("proxy_id"))

	if proxyId == "" {
		helper.Print(ctx, false, helper.ProxyIdErr)
		return
	}
	s, err := model.BusinessPromoInspectionList(uint(page), uint(pageSize), uid, proxyId)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}
