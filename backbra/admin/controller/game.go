package controller

import (
	"admin/contrib/helper"
	"admin/model"
	"encoding/json"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/valyala/fasthttp"
	"net/url"
	"strings"
)

type GameController struct{}

// 游戏列表
func (that *GameController) Search(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	word := string(ctx.QueryArgs().Peek("word"))
	platformId := ctx.QueryArgs().GetUintOrZero("platform_id")
	gameType := ctx.QueryArgs().GetUintOrZero("game_type")
	online := ctx.QueryArgs().GetUintOrZero("online")

	if page < 1 {
		page = 1
	}
	if pageSize < 10 || pageSize > 100 {
		pageSize = 10
	}

	filter := ""
	if platformId > 0 {
		filter = fmt.Sprintf("platform_id = %d", platformId)
	}
	if gameType > 0 {
		if len(filter) == 0 {
			filter = fmt.Sprintf("game_type = %d", gameType)
		} else {
			filter += fmt.Sprintf(" AND game_type = %d", gameType)
		}
	}
	if online > 0 {
		if len(filter) == 0 {
			filter = fmt.Sprintf("online = %d", online)
		} else {
			filter += fmt.Sprintf(" AND online = %d", online)
		}
	}

	if word != "" {
		decodedValue, err := url.QueryUnescape(word)
		if err != nil {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}
		word = decodedValue
	}
	data, err := model.GameFullTextMeili(page, pageSize, filter, word)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	helper.Print(ctx, true, data)

}

func (that *GameController) UpdateBatchHot(ctx *fasthttp.RequestCtx) {

	val := ctx.QueryArgs().GetUintOrZero("val")
	id := string(ctx.QueryArgs().Peek("id"))
	if !helper.CtypeCommaDigit(id) {
		helper.Print(ctx, false, helper.IDErr)
		return
	}

	ids := strings.SplitN(id, ",", 100)
	err := model.GameBatchUpdate("is_hot", val, ids)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

func (that *GameController) UpdateBatchRec(ctx *fasthttp.RequestCtx) {

	val := ctx.QueryArgs().GetUintOrZero("val")
	id := string(ctx.QueryArgs().Peek("id"))
	if !helper.CtypeCommaDigit(id) {
		helper.Print(ctx, false, helper.IDErr)
		return
	}

	ids := strings.SplitN(id, ",", 100)
	err := model.GameBatchUpdate("is_new", val, ids)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

func (that *GameController) UpdateBatchFav(ctx *fasthttp.RequestCtx) {

	val := ctx.QueryArgs().GetUintOrZero("val")
	id := string(ctx.QueryArgs().Peek("id"))
	if !helper.CtypeCommaDigit(id) {
		helper.Print(ctx, false, helper.IDErr)
		return
	}

	ids := strings.SplitN(id, ",", 100)
	err := model.GameBatchUpdate("is_fav", val, ids)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

// 维护游戏的上线下线
func (that *GameController) UpdateState(ctx *fasthttp.RequestCtx) {

	id := string(ctx.QueryArgs().Peek("id"))
	if !helper.CtypeDigit(id) {
		helper.Print(ctx, false, helper.IDErr)
		return
	}

	online := ctx.QueryArgs().GetUintOrZero("online")

	/*
		// 上线的热门游戏不能超过8个
		if online == 1 {
			total, err := model.CountHotGame(id)
			if err != nil {
				helper.Print(ctx, false, err.Error())
				return
			}

			if total >= 8 {
				helper.Print(ctx, false, helper.HotGameNumLimit)
				return
			}
		}
	*/
	err := model.GameListUpdateState(id, online)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

// 游戏编辑
func (that *GameController) Update(ctx *fasthttp.RequestCtx) {

	param := model.Game_t{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	if param.Img == "" || !strings.Contains(param.Img, ".") {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	record := g.Record{
		"online":      param.Online,
		"is_new":      param.IsNew,
		"is_hot":      param.IsHot,
		"is_fav":      param.IsFav,
		"en_name":     param.EnName,
		"name":        param.Name,
		"img":         param.Img,
		"br_alias":    param.BrAlias,
		"client_type": param.ClientType,
		"sorting":     param.Sorting,
	}

	if param.EnName != "" {
		decodedValue, err := url.QueryUnescape(param.EnName)
		if err != nil {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}
		record["en_name"] = decodedValue
	}
	if param.BrAlias != "" {
		decodedValue, err := url.QueryUnescape(param.BrAlias)
		if err != nil {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}
		record["br_alias"] = decodedValue
	}
	if param.Name != "" {
		decodedValue, err := url.QueryUnescape(param.Name)
		if err != nil {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}
		record["name"] = decodedValue
	}

	err = model.GameListUpdate(param.ID, record)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

func (that *GameController) Upload(ctx *fasthttp.RequestCtx) {

	filename, err := model.GameImgUpload(ctx)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	helper.Print(ctx, true, filename)
}

// 游戏列表
func (that *GameController) List(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	name := string(ctx.QueryArgs().Peek("name"))
	enName := string(ctx.QueryArgs().Peek("en_name"))
	platformId := string(ctx.QueryArgs().Peek("platform_id"))
	gameType := ctx.QueryArgs().GetUintOrZero("game_type")
	//clientType := ctx.QueryArgs().GetUintOrZero("client_type")
	online := string(ctx.QueryArgs().Peek("online"))
	isHot := ctx.QueryArgs().GetUintOrZero("is_hot")
	isNew := ctx.QueryArgs().GetUintOrZero("is_new")

	ex := g.Ex{}
	if name != "" {
		decodedValue, err := url.QueryUnescape(name)
		if err != nil {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}
		ex["name"] = decodedValue
	}
	if isHot > 0 {
		ex["is_hot"] = isHot
	}
	if isNew > 0 {
		ex["is_new"] = isNew
	}

	if online != "" {
		ex["online"] = online
	}

	if enName != "" {
		decodedValue, err := url.QueryUnescape(enName)
		if err != nil {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}
		ex["en_name"] = decodedValue
	}

	if platformId != "" {
		ex["platform_id"] = platformId
	}

	if gameType != 0 {
		ex["game_type"] = gameType
	}

	//if clientType != 0 {
	//	clientTypes := map[int][]int{
	//		1: {0, 1, 3, 5},
	//		2: {0, 2, 3, 6},
	//		4: {0, 4, 6, 5},
	//	}
	//	if _, ok := clientTypes[clientType]; !ok {
	//		helper.Print(ctx, false, helper.DeviceErr)
	//		return
	//	}
	//
	//	ex["client_type"] = clientTypes[clientType]
	//}

	if page < 1 {
		page = 1
	}
	if pageSize < 1 {
		pageSize = 15
	}
	data, err := model.GameList(ex, uint(page), uint(pageSize))
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}
