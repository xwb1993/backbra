package controller

import (
	"admin/contrib/helper"
	"admin/model"
	"encoding/json"
	"fmt"
	"github.com/valyala/fasthttp"
)

type PayFactoryController struct{}

type payFactoryParam struct {
	ID         string `json:"id"`
	Fmax       string `json:"fmax"`
	Fmin       string `json:"fmin"`
	AmountList string `json:"amount_list"`
	ShowName   string `json:"show_name"`
	Key        string `db:"key" json:"key"`
	AppKey     string `db:"app_key" json:"app_key"` //密钥
	State      string `json:"state"`
	PayRate    string `json:"pay_rate"`
	Automatic  string `json:"automatic"`
}

// List 通道列表
func (that *PayFactoryController) List(ctx *fasthttp.RequestCtx) {

	data, err := model.PayFactoryList()
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

func (that *PayFactoryController) Update(ctx *fasthttp.RequestCtx) {

	param := model.TblPayFactory{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	//fmt.Println("payFactoryParam:", param)
	err = model.UpdatePayFactory(param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}
