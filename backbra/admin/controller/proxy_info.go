package controller

import (
	"admin/contrib/helper"
	"admin/model"
	"encoding/json"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/valyala/fasthttp"
)

type ProxyInfoController struct{}

// ConfigList 代理渠道配置列表
func (that ProxyInfoController) ConfigList(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	operatorId := string(ctx.QueryArgs().Peek("operator_id"))
	id := string(ctx.QueryArgs().Peek("id"))
	proxyType := string(ctx.QueryArgs().Peek("type"))
	parentId := string(ctx.QueryArgs().Peek("parent_id"))

	ex := g.Ex{}
	if operatorId != "" {
		ex["operator_id"] = operatorId
	}
	if id != "" {
		ex["id"] = id
	}
	if proxyType != "" {
		ex["type"] = proxyType
	}
	if parentId != "" {
		ex["parent_id"] = parentId
	}

	s, err := model.ProxyInfoList(uint(page), uint(pageSize), ex)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

// ConfigInsert 业务员新增
func (that *ProxyInfoController) ConfigInsert(ctx *fasthttp.RequestCtx) {

	param := model.TblProxyInfo{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	err = model.ProxyInfoInsert(param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, "succeed")
}

// ConfigUpdate 业务员更新
func (that ProxyInfoController) ConfigUpdate(ctx *fasthttp.RequestCtx) {

	param := model.TblProxyInfo{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	err = model.ProxyInfoUpdate(param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

type businessLogin struct {
	LoginAccount string `json:"login_account" cbor:"login_account"`
	Password     string `json:"password" cbor:"password"`
}

func (that *ProxyInfoController) Login(ctx *fasthttp.RequestCtx) {
	deviceNo := string(ctx.Request.Header.Peek("no"))
	param := businessLogin{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	resp, err := model.BusinessLogin(deviceNo, param.LoginAccount, param.Password)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, resp)
}
