package middleware

import (
	"admin/contrib/session"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/valyala/fasthttp"
)

var allows = map[string]bool{
	"/tenant/admin/login": true,
	"/merchant/login":     true,
	"/business/login":     true,
}

type validTokenRes struct {
	Status bool   `json:"status"`
	Data   string `json:"data"`
}

func CheckTokenMiddleware(ctx *fasthttp.RequestCtx) error {

	path := string(ctx.Path())
	fmt.Println(path)
	//return nil
	if _, ok := allows[path]; ok {
		return nil
	}

	data, err := session.Get(ctx)
	if err != nil {
		res, _ := json.Marshal(validTokenRes{
			Status: false,
			Data:   "token",
		})
		return errors.New(string(res))
	}

	ctx.SetUserValue("token", data)

	return nil
}
