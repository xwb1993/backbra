package model

import (
	"admin/contrib/helper"
	"database/sql"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
)

type TblPromoInspection struct {
	Id               string  `json:"id" db:"id" `
	Uid              string  `json:"uid" db:"uid"`
	Username         string  `json:"username" db:"username"`
	ParentId         string  `json:"parent_id" db:"parent_id"`
	ParentName       string  `json:"parent_name" db:"parent_name"`
	Level            int     `json:"level" db:"level"`
	Ty               int     `json:"ty" db:"ty"`
	State            int     `json:"state" db:"state"`
	CapitalAmount    float64 `json:"capital_amount" db:"capital_amount"`
	FlowMultiple     float64 `json:"flow_multiple" db:"flow_multiple"`
	FlowAmount       float64 `json:"flow_amount" db:"flow_amount"`
	FinishedAmount   float64 `json:"finished_amount" db:"finished_amount"`
	UnfinishedAmount float64 `json:"unfinished_amount" db:"unfinished_amount"`
	CreatedAt        int64   `json:"created_at" db:"created_at"`
	StartAt          int64   `json:"start_at" db:"start_at"`
	FinishAt         int64   `json:"finish_at" db:"finish_at"`
	BillNo           string  `json:"bill_no" db:"bill_no"`
	Remark           string  `json:"remark" db:"remark"`
}

type promoInspectionData struct {
	T int                  `json:"t"`
	D []TblPromoInspection `json:"d"`
	S uint                 `json:"s"`
}

func PromoInspectionList(page, pageSize uint, uid string) (promoInspectionData, error) {

	data := promoInspectionData{}
	ex := g.Ex{}
	if uid != "" {
		ex["uid"] = uid
	}
	t := dialect.From("tbl_promo_inspection")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT(1)).Where(ex).ToSQL()
		fmt.Println(query)
		err := meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := t.Select(colsPromoInspection...).Where(ex).Offset(offset).Limit(pageSize).Order(g.C("created_at").Desc(), g.C("ty").Asc()).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	data.S = pageSize
	return data, nil
}

func MerchantPromoInspectionList(page, pageSize uint, uid, operatorId string) (promoInspectionData, error) {

	data := promoInspectionData{}
	ex := g.Ex{}
	uids := make([]string, 0)
	if uid != "" {
		mb, err := MemberFindByUidAndOpeId(uid, operatorId)
		if err != nil {
			return data, errors.New(helper.OperatorIdErr)
		}
		ex["uid"] = mb.Uid
	} else {
		members, err := MemberFindByOperatorId(operatorId)
		if err != nil {
			return data, errors.New(helper.OperatorIdErr)
		}
		if len(members) > 0 {
			for _, val := range members {
				uids = append(uids, val.Uid)
			}
			ex["uid"] = uids
		}
	}

	t := dialect.From("tbl_promo_inspection")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT(1)).Where(ex).ToSQL()
		fmt.Println(query)
		err := meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := t.Select(colsPromoInspection...).Where(ex).Offset(offset).Limit(pageSize).Order(g.C("created_at").Desc(), g.C("ty").Asc()).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	data.S = pageSize
	return data, nil
}

func BusinessPromoInspectionList(page, pageSize uint, uid, proxyId string) (promoInspectionData, error) {

	data := promoInspectionData{}
	ex := g.Ex{}
	uids := make([]string, 0)
	if uid != "" {
		mb, err := MemberFindByUidAndProId(uid, proxyId)
		if err != nil {
			return data, errors.New(helper.ProxyIdErr)
		}
		ex["uid"] = mb.Uid
	} else {
		members, err := MemberFindByProxyId(proxyId)
		if err != nil {
			return data, errors.New(helper.ProxyIdErr)
		}
		if len(members) > 0 {
			for _, val := range members {
				uids = append(uids, val.Uid)
			}
			ex["uid"] = uids
		}
	}

	t := dialect.From("tbl_promo_inspection")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT(1)).Where(ex).ToSQL()
		fmt.Println(query)
		err := meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := t.Select(colsPromoInspection...).Where(ex).Offset(offset).Limit(pageSize).Order(g.C("created_at").Desc(), g.C("ty").Asc()).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	data.S = pageSize
	return data, nil
}
