package model

import (
	"admin/contrib/helper"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"time"
)

// 系统公告
type TblNotices struct {
	Id          string `db:"id" json:"id"`
	Title       string `db:"title" json:"title"`               //标题
	Content     string `db:"content" json:"content"`           //内容
	State       string `db:"state" json:"state"`               //0停用 1启用
	CreatedAt   uint32 `db:"created_at" json:"created_at"`     //创建时间
	CreatedUid  string `db:"created_uid" json:"created_uid"`   //创建人uid
	CreatedName string `db:"created_name" json:"created_name"` //创建人名
}

type NoticeData struct {
	D []TblNotices `json:"d"`
	T int64        `json:"t"`
	S uint         `json:"s"`
}

type noticeRawData struct {
	Status bool         `json:"status"`
	D      []TblNotices `json:"data"`
}

func noticeFlushCache() error {

	var record []TblNotices
	ex := g.Ex{
		"state": "1",
	}
	query, _, _ := dialect.From("tbl_notices").Select(colsNotice...).Where(ex).Order(g.C("created_at").Desc()).ToSQL()
	//fmt.Println(query)
	err := meta.MerchantDB.Select(&record, query)
	if err != nil {
		if err != sql.ErrNoRows {
			err = pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}
		return err
	}

	//fmt.Println("record = ", record)
	if len(record) == 0 {

		meta.MerchantRedis.Unlink(ctx, "notices").Err()
		return nil
	}

	for i, _ := range record {
		record[i].CreatedUid = ""
		record[i].CreatedName = ""
	}
	recs := noticeRawData{
		Status: true,
		D:      record,
	}

	b, err := json.Marshal(recs)
	if err != nil {
		fmt.Println("noticeFlushCache json.Marshal err = ", err.Error())
		return err
	}

	pipe := meta.MerchantRedis.Pipeline()
	defer pipe.Close()

	pipe.Del(ctx, "notices")
	pipe.Set(ctx, "notices", b, 0)
	pipe.Persist(ctx, "notices")
	pipe.Exec(ctx)

	return nil
}

// 公告添加
func NoticeInsert(data TblNotices) error {

	query, _, _ := dialect.Insert("tbl_notices").Rows(&data).ToSQL()
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}
	noticeFlushCache()
	return nil
}

// 公告列表
func NoticeList(page, pageSize uint, startTime, endTime string, ex g.Ex) (NoticeData, error) {

	data := NoticeData{}

	if startTime != "" && endTime != "" {
		st, err := time.ParseInLocation("2006-01-02 15:04:05", startTime, time.Local)
		if err != nil {
			return data, errors.New(helper.TimeTypeErr)
		}

		et, err := time.ParseInLocation("2006-01-02 15:04:05", endTime, time.Local)
		if err != nil {
			return data, errors.New(helper.TimeTypeErr)
		}

		ex["created_at"] = g.Op{"between": exp.NewRangeVal(st.Unix(), et.Unix())}
	}

	t := dialect.From("tbl_notices")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT(1)).Where(ex).ToSQL()
		err := meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	data.S = pageSize
	offset := (page - 1) * pageSize
	query, _, _ := t.Select(colsNotice...).Where(ex).Order(g.C("created_at").Desc()).Offset(offset).Limit(pageSize).ToSQL()
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}

// 公告 启用 停用
func NoticeUpdateState(id string, state int) error {

	data := TblNotices{}
	ex := g.Ex{
		"id": id,
	}
	query, _, _ := dialect.From("tbl_notices").Select(colsNotice...).Where(ex).ToSQL()
	err := meta.MerchantDB.Get(&data, query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	record := g.Record{
		"state": state,
	}
	query, _, _ = dialect.Update("tbl_notices").Set(record).Where(ex).ToSQL()
	_, err = meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	noticeFlushCache()

	return nil
}

// 公告删除
func NoticeDelete(id string) error {

	ex := g.Ex{
		"id": id,
	}
	query, _, _ := dialect.Delete("tbl_notices").Where(ex).ToSQL()
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}
	noticeFlushCache()
	return nil
}

// 公告更新
func NoticeUpdate(id string, record g.Record) error {

	ex := g.Ex{
		"id": id,
	}

	query, _, _ := dialect.Update("tbl_notices").Set(record).Where(ex).ToSQL()
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	noticeFlushCache()

	return nil
}
