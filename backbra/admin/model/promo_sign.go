package model

import (
	"admin/contrib/helper"
	"admin/contrib/validator"
	"database/sql"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"net/url"
)

func PromoSignConfigInsert(data PromoSignConfig) error {

	query, _, _ := dialect.Insert("tbl_promo_sign_config").Rows(&data).ToSQL()
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func PromoSignConfigUpdate(vip int, data PromoSignConfig) error {

	query, _, _ := dialect.Update("tbl_promo_sign_config").Set(&data).Where(g.Ex{"vip": vip}).ToSQL()
	fmt.Println(query)
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func PromoSignConfigList(page, pageSize uint, ex g.Ex) (PromoSignConfigData, error) {

	data := PromoSignConfigData{}
	t := dialect.From("tbl_promo_sign_config")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT(1)).Where(ex).ToSQL()
		fmt.Println(query)
		err := meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := t.Select(colsPromoSignConfig...).Where(ex).Offset(offset).Limit(pageSize).Order(g.C("vip").Asc()).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	data.S = pageSize
	return data, nil
}

func PromoSignRecordList(page, pageSize uint, ex g.Ex) (PromoSignRecordData, error) {

	data := PromoSignRecordData{}
	t := dialect.From("tbl_promo_sign_record")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT(1)).Where(ex).ToSQL()
		fmt.Println(query)
		err := meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := t.Select(colsPromoSignRecord...).Where(ex).Offset(offset).Limit(pageSize).Order(g.C("vip").Asc()).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	data.S = pageSize
	return data, nil
}

func PromoSignRewardRecordList(page, pageSize uint, ex g.Ex) (PromoSignRewardRecordData, error) {

	data := PromoSignRewardRecordData{}
	t := dialect.From("tbl_promo_sign_reward_record")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT(1)).Where(ex).ToSQL()
		fmt.Println(query)
		err := meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := t.Select(colsPromoSignRewardRecord...).Where(ex).Offset(offset).Limit(pageSize).Order(g.C("vip").Asc()).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	data.S = pageSize
	return data, nil
}

func MerchantPromoSignRecordList(page, pageSize uint, username, operatorId string) (PromoSignRecordData, error) {

	data := PromoSignRecordData{}

	ex := g.Ex{}
	uids := make([]string, 0)
	if username != "" { // 用户名校验
		uName, _ := url.PathUnescape(username)
		if !validator.CheckUName(uName, 5, 50) {
			return data, errors.New(helper.UsernameErr)
		}
		mb, err := MemberFindByUnameAndOpeId(uName, operatorId)
		if err != nil {
			return data, errors.New(helper.UsernameErr)
		}
		ex["username"] = uName
		ex["uid"] = mb.Uid
	} else {
		members, err := MemberFindByOperatorId(operatorId)
		if err != nil {
			return data, errors.New(helper.OperatorIdErr)
		}
		if len(members) > 0 {
			for _, val := range members {
				uids = append(uids, val.Uid)
			}
			ex["uid"] = uids
		}
	}
	t := dialect.From("tbl_promo_sign_record")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT(1)).Where(ex).ToSQL()
		fmt.Println(query)
		err := meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := t.Select(colsPromoSignRecord...).Where(ex).Offset(offset).Limit(pageSize).Order(g.C("vip").Asc()).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	data.S = pageSize
	return data, nil
}

func BusinessPromoSignRecordList(page, pageSize uint, username, proxyId string) (PromoSignRecordData, error) {

	data := PromoSignRecordData{}

	ex := g.Ex{}
	uids := make([]string, 0)
	if username != "" { // 用户名校验
		uName, _ := url.PathUnescape(username)
		if !validator.CheckUName(uName, 5, 50) {
			return data, errors.New(helper.UsernameErr)
		}
		mb, err := MemberFindByUnameAndProId(uName, proxyId)
		if err != nil {
			return data, errors.New(helper.UsernameErr)
		}
		ex["username"] = uName
		ex["uid"] = mb.Uid
	} else {
		members, err := MemberFindByProxyId(proxyId)
		if err != nil {
			return data, errors.New(helper.ProxyIdErr)
		}
		if len(members) > 0 {
			for _, val := range members {
				uids = append(uids, val.Uid)
			}
			ex["uid"] = uids
		}
	}
	t := dialect.From("tbl_promo_sign_record")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT(1)).Where(ex).ToSQL()
		fmt.Println(query)
		err := meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := t.Select(colsPromoSignRecord...).Where(ex).Offset(offset).Limit(pageSize).Order(g.C("vip").Asc()).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	data.S = pageSize
	return data, nil
}
