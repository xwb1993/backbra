package model

import (
	"admin/contrib/helper"
	"errors"
)

type CSForm struct {
	Field string `json:"field"`
	Value string `json:"value"`
}

func CsList() (map[string]string, error) {

	val := map[string]string{
		"onlinecs": "",
		"telegram": "",
		"facebook": "",
	}

	res := meta.MerchantRedis.HMGet(ctx, "cs", "telegram", "facebook", "onlinecs").Val()
	if len(res) > 0 {
		if v, ok := res[0].(string); ok {
			val["telegram"] = v
		}
		if v, ok := res[1].(string); ok {
			val["facebook"] = v
		}
		if v, ok := res[2].(string); ok {
			val["onlinecs"] = v
		}
	}

	return val, nil
}

func CsUpdate(data CSForm) error {

	val := map[string]bool{
		"telegram": true,
		"facebook": true,
		"onlinecs": true,
	}

	pipe := meta.MerchantRedis.Pipeline()
	defer pipe.Close()

	if _, ok := val[data.Field]; !ok {
		return errors.New(helper.ParamErr)
	}
	pipe.HDel(ctx, "cs", data.Field)
	pipe.HSet(ctx, "cs", data.Field, data.Value)

	pipe.Exec(ctx)
	return nil
}
