package model

import (
	"admin/contrib/helper"
	"admin/contrib/validator"
	"database/sql"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/shopspring/decimal"
	"github.com/valyala/fasthttp"
	"strconv"
	"time"
)

type withdrawTotal struct {
	T   sql.NullInt64   `json:"t"`
	Agg sql.NullFloat64 `json:"agg"`
}

// tblWithdraw 出款
type tblWithdraw struct {
	ID             string  `db:"id" json:"id" cbor:"id"`                                        //
	OID            string  `db:"oid" json:"oid" cbor:"oid"`                                     //转账前的金额
	UID            string  `db:"uid" json:"uid" cbor:"uid"`                                     //用户ID
	Username       string  `db:"username" json:"username" cbor:"username"`                      //用户名
	TopId          string  `db:"top_id" json:"top_id" cbor:"top_id"`                            //总代uid
	TopName        string  `db:"top_name" json:"top_name" cbor:"top_name"`                      //总代代理
	ParentId       string  `db:"parent_id" json:"parent_id" cbor:"parent_id"`                   //上级uid
	ParentName     string  `db:"parent_name" json:"parent_name" cbor:"parent_name"`             //上级代理
	FID            string  `db:"fid" json:"fid" cbor:"fid"`                                     //通道id
	Fname          string  `db:"fname" json:"fname" cbor:"fname"`                               //通道名称
	Amount         float64 `db:"amount" json:"amount" cbor:"amount"`                            //金额
	Fee            float64 `db:"fee" json:"fee" cbor:"fee"`                                     //手续费
	State          int     `db:"state" json:"state" cbor:"state"`                               //0:待确认:1存款成功2:已取消
	PixAccount     string  `db:"pix_account" json:"pix_account" cbor:"pix_account"`             //银行名
	RealName       string  `db:"real_name" json:"real_name" cbor:"real_name"`                   //持卡人姓名
	PixId          string  `db:"pix_id" json:"pix_id" cbor:"pix_id"`                            //银行卡号
	CreatedAt      int64   `db:"created_at" json:"created_at" cbor:"created_at"`                //
	ConfirmAt      int64   `db:"confirm_at" json:"confirm_at" cbor:"confirm_at"`                //确认时间
	ConfirmUID     string  `db:"confirm_uid" json:"confirm_uid" cbor:"confirm_uid"`             //确认人uid
	ConfirmName    string  `db:"confirm_name" json:"confirm_name" cbor:"confirm_name"`          //确认人名
	ReviewRemark   string  `db:"review_remark" json:"review_remark" cbor:"review_remark"`       //确认人名
	WithdrawAt     int64   `db:"withdraw_at" json:"withdraw_at" cbor:"withdraw_at"`             //三方场馆ID
	WithdrawRemark string  `db:"withdraw_remark" json:"withdraw_remark" cbor:"withdraw_remark"` //确认人名
	WithdrawUID    string  `db:"withdraw_uid" json:"withdraw_uid" cbor:"withdraw_uid"`          //确认人uid
	WithdrawName   string  `db:"withdraw_name" json:"withdraw_name" cbor:"withdraw_name"`       //确认人名
	HangUpUID      string  `db:"hang_up_uid" json:"hang_up_uid" cbor:"hang_up_uid"`             // 挂起人uid
	HangUpRemark   string  `db:"hang_up_remark" json:"hang_up_remark" cbor:"hang_up_remark"`    // 挂起备注
	HangUpName     string  `db:"hang_up_name" json:"hang_up_name" cbor:"hang_up_name"`          //  挂起人名字
	HangUpAt       int     `db:"hang_up_at" json:"hang_up_at" cbor:"hang_up_at"`                //  挂起时间
	ReceiveAt      int64   `db:"receive_at" json:"receive_at" cbor:"receive_at"`                //领取时间
	Balance        string  `db:"balance" json:"balance" cbor:"balance"`                         //提款前余额
	Flag           string  `db:"flag" json:"flag" cbor:"flag"`                                  //1 cpf 2 phone 3 email
	Level          int     `db:"level" json:"level" cbor:"level"`                               //会员等级
}

// FWithdrawData 取款数据
type FWithdrawData struct {
	T   int64         `json:"t"`
	D   []tblWithdraw `json:"d"`
	Agg tblWithdraw   `json:"agg"`
}

// WithdrawList 提款记录
func WithdrawList(ex g.Ex, ty uint8, startTime, endTime string, page, pageSize uint) (FWithdrawData, error) {

	if startTime != "" && endTime != "" {

		startAt, err := helper.TimeToLoc(startTime, loc)
		if err != nil {
			return FWithdrawData{}, errors.New(helper.DateTimeErr)
		}

		endAt, err := helper.TimeToLoc(endTime, loc)
		if err != nil {
			return FWithdrawData{}, errors.New(helper.DateTimeErr)
		}

		if ty == 1 {
			ex["created_at"] = g.Op{"between": exp.NewRangeVal(startAt, endAt)}
		} else {
			ex["withdraw_at"] = g.Op{"between": exp.NewRangeVal(startAt, endAt)}
		}
	}
	// 待派单特殊操作 只显示一天的数据
	if ty == 3 {
		now := time.Now().Unix()
		ex["created_at"] = g.Op{"between": exp.NewRangeVal(now-172800, now)}
	}

	var data FWithdrawData
	if page == 1 {
		var total withdrawTotal
		query, _, _ := dialect.From("tbl_withdraw").Select(g.COUNT(1).As("t"), g.SUM("amount").As("agg")).Where(ex).ToSQL()
		fmt.Println(query)
		err := meta.MerchantDB.Get(&total, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		data.T = total.T.Int64
		data.Agg = tblWithdraw{
			Amount: total.Agg.Float64,
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := dialect.From("tbl_withdraw").
		Select(colsWithdraw...).Where(ex).Order(g.C("created_at").Desc()).Offset(offset).Limit(pageSize).ToSQL()
	//fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}

// WithdrawFind 查找单条提款记录, 订单不存在返回错误: OrderNotExist
func WithdrawFind(id string) (tblWithdraw, error) {

	w := tblWithdraw{}
	query, _, _ := dialect.From("tbl_withdraw").Select(colsWithdraw...).Where(g.Ex{"id": id}).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&w, query)
	if err == sql.ErrNoRows {
		return w, errors.New(helper.OrderNotExist)
	}

	if err != nil {
		return w, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return w, nil
}

func WithdrawUpdateInfo(id string, record g.Record) error {

	query, _, _ := dialect.Update("tbl_withdraw").Where(g.Ex{"id": id}).Set(record).ToSQL()
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func WithdrawRiskReview(id string, state int, record g.Record, withdraw tblWithdraw) error {

	//判断状态是否合法
	allow := map[int]bool{
		WithdrawReviewReject:  true,
		WithdrawDealing:       true,
		WithdrawSuccess:       true,
		WithdrawFailed:        true,
		WithdrawAutoPayFailed: true,
	}
	if _, ok := allow[state]; !ok {
		return errors.New(helper.StateParamErr)
	}

	//1、判断订单是否存在
	var order tblWithdraw
	ex := g.Ex{"id": id}
	query, _, _ := dialect.From("tbl_withdraw").Select(colsWithdraw...).Where(ex).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&order, query)
	if err != nil || len(order.Username) < 1 {
		return errors.New(helper.IDErr)
	}

	query, _, _ = dialect.Update("tbl_withdraw").Set(record).Where(ex).ToSQL()
	switch order.State {
	case WithdrawReviewing:
		// 审核中(风控待领取)的订单只能流向分配, 上层业务处理
		return errors.New(helper.OrderStateErr)
	case WithdrawDealing:
		// 出款处理中可以是自动代付失败(WithdrawAutoPayFailed) 提款成功(WithdrawSuccess) 提款失败(WithdrawFailed)
		// 自动代付失败和提款成功是调用三方代付才会有的状态
		// 提款失败通过提款管理的[拒绝]操作进行流转(同时出款类型必须是手动出款)
		if state == WithdrawAutoPayFailed {
			_, err = meta.MerchantDB.Exec(query)
			if err != nil {
				return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
			}

			return nil
		}

		if state != WithdrawSuccess && (state != WithdrawFailed) {
			return errors.New(helper.OrderStateErr)
		}

	case WithdrawAutoPayFailed:
		// 代付失败可以通过手动代付将状态流转至出款中
		if state == WithdrawDealing {
			_, err = meta.MerchantDB.Exec(query)
			if err != nil {
				return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
			}

			return nil
		}

		// 代付失败的订单也可以通过手动出款直接将状态流转至出款成功
		// 代付失败的订单还可以通过拒绝直接将状态流转至出款失败
		if state != WithdrawFailed && state != WithdrawSuccess {
			return errors.New(helper.OrderStateErr)
		}

	case WithdrawHangup:
		// 挂起的订单只能领取(该状态流转上传业务已经处理), 该状态只能流转至审核中(WithdrawReviewing)
		return errors.New(helper.OrderStateErr)

	case WithdrawDispatched:
		// 派单状态可流转状态为 挂起(WithdrawHangup) 通过(WithdrawDealing) 拒绝(WithdrawReviewReject)
		// 其中流转至挂起状态由上层业务处理
		if state == WithdrawDealing {
			_, err = meta.MerchantDB.Exec(query)
			if err != nil {
				return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
			}

			return nil
		}

		if state != WithdrawReviewReject {
			return errors.New(helper.OrderStateErr)
		}

	default:
		// 审核拒绝, 提款成功, 出款失败三个状态为终态 不能进行其他处理
		return errors.New(helper.OrderStateErr)
	}

	// 3 如果是出款成功,修改订单状态为提款成功,扣除锁定钱包中的钱,发送通知
	if WithdrawSuccess == state {
		return withdrawOrderSuccess(query, order)
	}

	order.ReviewRemark = record["review_remark"].(string)
	order.WithdrawRemark = record["withdraw_remark"].(string)
	// 出款失败
	return withdrawOrderFailed(query, order)
}

// 检查锁定钱包余额是否充足
func lockBalanceIsEnough(uid string, amount decimal.Decimal) (decimal.Decimal, error) {

	balance, err := MemberBalanceFindOne(uid)
	if err != nil {
		return decimal.NewFromString(balance.LockAmount)
	}
	brl, _ := decimal.NewFromString(balance.LockAmount)
	if brl.Sub(amount).IsNegative() {
		return brl, errors.New(helper.LackOfBalance)
	}

	return brl, nil
}

// 提款成功
func withdrawOrderSuccess(query string, order tblWithdraw) error {

	money := decimal.NewFromFloat(order.Amount)

	// 判断锁定余额是否充足
	_, err := lockBalanceIsEnough(order.UID, money)
	if err != nil {
		return err
	}

	//开启事务
	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	// 更新提款订单状态
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	// 锁定钱包下分
	ex := g.Ex{
		"uid": order.UID,
	}
	gr := g.Record{
		"last_withdraw_at": order.CreatedAt,
		"lock_amount":      g.L(fmt.Sprintf("lock_amount-%s", money.String())),
	}
	query, _, _ = dialect.Update("tbl_member_balance").Set(gr).Where(ex).ToSQL()
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	err = tx.Commit()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	//ryrpc.MemberFlushByUid(order.UID)

	// TODO 修改会员提款限制
	//date := time.Unix(order.CreatedAt, 0).Format("20060102")
	//_ = withDrawDailyLimitUpdate(money, date, order.Username)

	return nil
}

func withdrawOrderFailed(query string, order tblWithdraw) error {

	money := decimal.NewFromFloat(order.Amount)
	moneyFee := decimal.NewFromFloat(order.Fee)

	//4、查询用户额度
	balance, err := MemberBalanceFindOne(order.UID)
	if err != nil {
		return err
	}
	blr, _ := decimal.NewFromString(balance.Brl)
	balanceAfter := blr.Add(money)
	fmt.Println("修改提现状态开始", order)

	//开启事务
	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	//5、更新订单状态
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	//6、更新余额
	ex := g.Ex{
		"uid": order.UID,
	}
	balanceRecord := g.Record{
		"brl":         g.L(fmt.Sprintf("brl+%s", money.Add(moneyFee).String())),
		"lock_amount": g.L(fmt.Sprintf("lock_amount-%s", money.Add(moneyFee).String())),
	}
	query, _, _ = dialect.Update("tbl_member_balance").Set(balanceRecord).Where(ex).ToSQL()
	fmt.Println("更新余额SQL:", query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	//7、新增账变记录
	mbTrans := MemberTransaction{
		AfterAmount:  balanceAfter.String(),
		Amount:       money.String(),
		BeforeAmount: balance.Brl,
		BillNo:       order.ID,
		CreatedAt:    time.Now().UnixMilli(),
		ID:           helper.GenId(),
		CashType:     helper.TransactionWithDrawFail,
		UID:          order.UID,
		Username:     order.Username,
	}

	query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(mbTrans).ToSQL()
	fmt.Println("新增账变SQL:", query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	//8、新增账变记录 手续费退回
	mbTransFee := MemberTransaction{
		AfterAmount:  balanceAfter.Add(moneyFee).String(),
		Amount:       moneyFee.String(),
		BeforeAmount: balance.Brl,
		BillNo:       order.ID,
		CreatedAt:    time.Now().UnixMilli(),
		ID:           helper.GenId(),
		CashType:     helper.TransactionWithdrawalFeeRem,
		UID:          order.UID,
		Username:     order.Username,
	}

	query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(mbTransFee).ToSQL()
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(err, helper.DBErr)
	}

	err = tx.Commit()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	//ryrpc.MemberFlushByUid(order.UID)
	fmt.Println("修改提现状态完成")

	return nil
}

// WithdrawLock 锁定提款订单
// 订单因为外部因素(接口)导致的状态流转应该加锁
func WithdrawLock(id string) error {

	key := fmt.Sprintf(withdrawOrderLockKey, id)
	err := Lock(key)
	if err != nil {
		return err
	}

	return nil
}

// WithdrawUnLock 解锁提款订单
func WithdrawUnLock(id string) {
	Unlock(fmt.Sprintf(withdrawOrderLockKey, id))
}

func Lock(id string) error {

	val := fmt.Sprintf("%s%s", defaultRedisKeyPrefix, id)
	ok, err := meta.MerchantRedis.SetNX(ctx, val, "1", LockTimeout).Result()
	if err != nil {
		return pushLog(err, helper.RedisErr)
	}
	if !ok {
		return errors.New(helper.RequestBusy)
	}

	return nil
}

func Unlock(id string) {

	val := fmt.Sprintf("%s%s", defaultRedisKeyPrefix, id)
	res, err := meta.MerchantRedis.Unlink(ctx, val).Result()
	if err != nil || res != 1 {
		fmt.Println("Unlock res = ", res)
		fmt.Println("Unlock err = ", err)
	}
}

// 提款历史记录
func WithdrawHistoryList(ex g.Ex, ty, startTime, endTime, isAsc, sortField string, page, pageSize uint) (FWithdrawData, error) {

	ex["tester"] = []int{1, 3}
	orderField := g.L("created_at")
	if sortField != "" {
		orderField = g.L(sortField)
	}

	orderBy := orderField.Desc()
	if isAsc == "1" {
		orderBy = orderField.Asc()
	}
	if startTime != "" && endTime != "" {

		startAt, err := helper.TimeToLoc(startTime, loc)
		if err != nil {
			return FWithdrawData{}, errors.New(helper.DateTimeErr)
		}

		endAt, err := helper.TimeToLoc(endTime, loc)
		if err != nil {
			return FWithdrawData{}, errors.New(helper.DateTimeErr)
		}

		if startAt >= endAt {
			return FWithdrawData{}, errors.New(helper.QueryTimeRangeErr)
		}

		if ty == "1" {
			ex["created_at"] = g.Op{"between": exp.NewRangeVal(startAt, endAt)}
		} else {
			ex["withdraw_at"] = g.Op{"between": exp.NewRangeVal(startAt, endAt)}
		}
	}

	data := FWithdrawData{}
	if page == 1 {
		query, _, _ := dialect.From("tbl_withdraw").Select(g.COUNT("id")).Where(ex).ToSQL()
		err := meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}

		query, _, _ = dialect.From("tbl_withdraw").Select(g.SUM("amount").As("amount")).Where(ex).ToSQL()
		err = meta.MerchantDB.Get(&data.Agg, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}
	}
	offset := (page - 1) * pageSize
	query, _, _ := dialect.From("tbl_withdraw").Select(colsWithdraw...).Where(ex).
		Offset(offset).Limit(pageSize).Order(orderBy, g.C("id").Desc()).ToSQL()
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}

// 财务拒绝提款订单
func WithdrawReject(id string, d g.Record) error {
	return withdrawDownPoint(id, "", WithdrawFailed, d)
}

// 取款下分
func withdrawDownPoint(did, bankcard string, state int, record g.Record) error {

	//判断状态是否合法
	allow := map[int]bool{
		WithdrawReviewReject:  true,
		WithdrawDealing:       true,
		WithdrawSuccess:       true,
		WithdrawFailed:        true,
		WithdrawAutoPayFailed: true,
	}
	if _, ok := allow[state]; !ok {
		return errors.New(helper.StateParamErr)
	}

	//1、判断订单是否存在
	var order tblWithdraw
	ex := g.Ex{"id": did}
	query, _, _ := dialect.From("tbl_withdraw").Select(colsWithdraw...).Where(ex).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&order, query)
	fmt.Println("判断订单是否存在", state, query, order)
	if err != nil || len(order.Username) < 1 {
		return errors.New(helper.IDErr)
	}

	query, _, _ = dialect.Update("tbl_withdraw").Set(record).Where(ex).ToSQL()
	switch order.State {
	case WithdrawReviewing:
		// 审核中(风控待领取)的订单只能流向分配, 上层业务处理
		return errors.New(helper.OrderStateErr)
	case WithdrawDealing:
		// 出款处理中可以是自动代付失败(WithdrawAutoPayFailed) 提款成功(WithdrawSuccess) 提款失败(WithdrawFailed)
		// 自动代付失败和提款成功是调用三方代付才会有的状态
		// 提款失败通过提款管理的[拒绝]操作进行流转(同时出款类型必须是手动出款)
		if state == WithdrawAutoPayFailed {
			_, err = meta.MerchantDB.Exec(query)
			if err != nil {
				return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
			}

			return nil
		}

		if state != WithdrawSuccess && (state != WithdrawFailed) {
			return errors.New(helper.OrderStateErr)
		}

	case WithdrawAutoPayFailed:
		// 代付失败可以通过手动代付将状态流转至出款中
		if state == WithdrawDealing {
			_, err = meta.MerchantDB.Exec(query)
			if err != nil {
				return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
			}

			return nil
		}

		// 代付失败的订单也可以通过手动出款直接将状态流转至出款成功
		// 代付失败的订单还可以通过拒绝直接将状态流转至出款失败
		if state != WithdrawFailed && state != WithdrawSuccess {
			return errors.New(helper.OrderStateErr)
		}

	case WithdrawHangup:
		// 挂起的订单只能领取(该状态流转上传业务已经处理), 该状态只能流转至审核中(WithdrawReviewing)
		return errors.New(helper.OrderStateErr)

	case WithdrawDispatched:
		// 派单状态可流转状态为 挂起(WithdrawHangup) 通过(WithdrawDealing) 拒绝(WithdrawReviewReject)
		// 其中流转至挂起状态由上层业务处理
		if state == WithdrawDealing {
			_, err = meta.MerchantDB.Exec(query)
			if err != nil {
				return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
			}

			return nil
		}

		if state != WithdrawReviewReject {
			return errors.New(helper.OrderStateErr)
		}
	case WithdrawAutoPaying:
		// 三方出款失败，修改出款失败状态
		fmt.Println("三方出款失败，修改出款失败状态")
	default:
		// 审核拒绝, 提款成功, 出款失败三个状态为终态 不能进行其他处理
		return errors.New(helper.OrderStateErr)
	}

	// 3 如果是出款成功,修改订单状态为提款成功,扣除锁定钱包中的钱,发送通知
	if WithdrawSuccess == state {
		return withdrawOrderSuccess(query, order)
	}

	//order.ReviewRemark = record["review_remark"].(string)
	//order.WithdrawRemark = record["withdraw_remark"].(string)
	// 出款失败
	fmt.Println("设置出款失败", query, order)
	return withdrawOrderFailed(query, order)
}

// WithdrawHandToAuto 手动代付
func WithdrawHandToAuto(fctx *fasthttp.RequestCtx, uid, username, id, pid string, amount float64, withdraw tblWithdraw, t time.Time) error {

	p, err := CachePayFactory(pid)
	if err != nil {
		return err
	}

	if len(p.Fid) == 0 || p.State == "0" {
		return errors.New(helper.CateNotExist)
	}

	user, err := MemberFindByUsername(username)
	if err != nil {
		return err
	}

	as := strconv.FormatFloat(amount, 'f', -1, 64)
	// check amount range, continue the for loop if amount out of range
	_, ok := validator.CheckFloatScope(as, p.Fmin, p.Fmax)
	if !ok {
		return errors.New(helper.AmountOutRange)
	}
	payment, ok := thirdFuncCb[p.Fid]
	if !ok {
		return errors.New(helper.NoPayChannel)
	}
	data, err := payment.Withdraw(fctx, time.Now(), user.Uid, fmt.Sprintf(`%f`, amount), id, withdraw.PixAccount, withdraw.RealName, withdraw.PixId, withdraw.Flag, p)
	fmt.Println("Pay  payment.Pay err = ", err)
	if err != nil {
		return err
	}
	err = withdrawAutoUpdate(id, data.Oid, p.Fid, WithdrawAutoPaying)
	if err != nil {
		fmt.Println("withdrawHandToAuto failed 2:", id, err)
	}
	return nil
}

func withdrawAutoUpdate(id, oid, fid string, state int) error {

	r := g.Record{"state": state, "automatic": "1"}
	if oid != "" {
		r["oid"] = oid
	}
	if fid != "" {
		r["fid"] = fid
	}

	query, _, _ := dialect.Update("tbl_withdraw").Set(r).Where(g.Ex{"id": id}).ToSQL()
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return err
}

// WithdrawAutoPaySetFailed 将订单状态从出款中修改为代付失败
func WithdrawAutoPaySetFailed(id string, confirmAt int64, confirmUid, confirmName string) error {

	order, err := WithdrawFind(id)
	if err != nil {
		return err
	}

	// 只能将出款中(三方处理中, 即自动代付)的订单状态流转为代付失败
	if order.State != WithdrawDealing && order.State != WithdrawAutoPaying {
		return errors.New(helper.OrderStateErr)
	}

	// 将automatic设为1是为了确保状态为代付失败的订单一定为自动出款(automatic=1)
	record := g.Record{
		"state":     WithdrawAutoPayFailed,
		"automatic": "1",
	}

	return withdrawDownPoint(id, "", WithdrawAutoPayFailed, record)
}
