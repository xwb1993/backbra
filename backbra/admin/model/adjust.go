package model

import (
	"admin/contrib/helper"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/shopspring/decimal"
	"time"
)

type TblMemberAdjust struct {
	Id           string `json:"id" cbor:"id" db:"id"`
	Uid          string `json:"uid" cbor:"uid" db:"uid"`
	Username     string `json:"username" cbor:"username" db:"username"`
	Amount       string `json:"amount" cbor:"amount" db:"amount"`
	AdjustMode   int    `json:"adjust_mode" cbor:"adjust_mode" db:"adjust_mode"`
	ApplyRemark  string `json:"apply_remark" cbor:"apply_remark" db:"apply_remark"`
	ReviewRemark string `json:"review_remark" cbor:"review_remark" db:"review_remark"`
	State        int    `json:"state" cbor:"state" db:"state"`
	ApplyAt      int64  `json:"apply_at" cbor:"apply_at" db:"apply_at"`
	ApplyUid     string `json:"apply_uid" cbor:"apply_uid" db:"apply_uid"`
	ApplyName    string `json:"apply_name" cbor:"apply_name" db:"apply_name"`
	ReviewAt     int64  `json:"review_at" cbor:"review_at" db:"review_at"`
	ReviewUid    string `json:"review_uid" cbor:"review_uid" db:"review_uid"`
	ReviewName   string `json:"review_name" cbor:"review_name" db:"review_name"`
	Tester       int    `json:"tester" cbor:"tester" db:"tester"`
}

type AdjustData struct {
	T int               `cbor:"t" json:"t"`
	D []TblMemberAdjust `cbor:"d" json:"d"`
}

func AdjustInsert(data TblMemberAdjust) error {

	query, _, _ := dialect.Insert("tbl_member_adjust").Rows(&data).ToSQL()
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

// AdjustList 账户调整列表
func AdjustList(startTime, endTime string, ex g.Ex, page, pageSize int) (AdjustData, error) {

	data := AdjustData{}

	startAt, err := helper.TimeToLoc(startTime, loc)
	if err != nil {
		return data, errors.New(helper.TimeTypeErr)
	}

	endAt, err := helper.TimeToLoc(endTime, loc)
	if err != nil {
		return data, errors.New(helper.TimeTypeErr)
	}

	if startAt >= endAt {
		return data, errors.New(helper.QueryTimeRangeErr)
	}

	ex["apply_at"] = g.Op{
		"between": exp.NewRangeVal(startAt, endAt),
	}

	t := dialect.From("tbl_member_adjust")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT("id")).Where(ex).ToSQL()
		fmt.Println(query)
		err = meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := t.Select(colsMemberAdjust...).Where(ex).Order(g.C("review_at").Desc(), g.C("apply_at").Desc()).Offset(uint(offset)).Limit(uint(pageSize)).ToSQL()
	fmt.Println(query)
	err = meta.MerchantDB.Select(&data.D, query)
	if err != nil {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}

// AdjustReview 账户调整-审核
func AdjustReview(state string, record g.Record) error {

	data := TblMemberAdjust{}

	query, _, _ := dialect.From("tbl_member_adjust").
		Select(colsMemberAdjust...).Where(g.Ex{"id": record["id"]}).Limit(1).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&data, query)
	if err != nil {
		return errors.New(helper.DBErr)
	}

	// 只有审核中状态的才能操作
	if fmt.Sprintf(`%d`, data.State) != AdjustReviewing {
		return errors.New(helper.StateParamErr)
	}
	record["apply_remark"] = data.ApplyRemark
	amount, _ := decimal.NewFromString(data.Amount)

	fmt.Println("state:", state)
	fmt.Println("data.State:", data.State)
	// 下分只有中心钱包
	if data.AdjustMode == AdjustDownMode {

		flag := DownPointApplyReject
		if state == AdjustReviewPass {
			flag = DownPointApplyPass
		}
		err = adjustUpDownPoint(data.Uid, data.Username, flag, amount.Abs(), record)
		if err != nil {
			return err
		}

		return nil
	}

	// 后面都是上分 先处理拒绝的业务逻辑 再分别处理场馆和中心钱包同意的逻辑
	if state == AdjustReviewReject {

		// 更新调整记录状态
		query, _, _ = dialect.Update("tbl_member_adjust").Set(record).Where(g.Ex{"id": record["id"]}).ToSQL()
		_, err = meta.MerchantDB.Exec(query)
		if err != nil {
			return errors.New(helper.DBErr)
		}

		return nil
	}

	// 中心钱包上分申请同意逻辑
	err = adjustUpDownPoint(data.Uid, data.Username, UpPointApplyPass, amount, record)
	if err != nil {
		return err
	}

	return nil
}

// 中心钱包 下分申请 下分申请通过 下分申请拒绝
// 上分申请通过 adjust 参数为更新调整记录状态
// 下分申请 adjust 参数为新增调整记录
// 下分申请通过 adjust 参数为更新调整记录状态
// 下分申请拒绝 adjust 参数为更新调整记录状态
func adjustUpDownPoint(uid, username string, flag int, money decimal.Decimal, adjust g.Record) error {

	var (
		balanceAfter decimal.Decimal
		balance      decimal.Decimal
	)
	//1、判断金额是否合法
	if money.Cmp(decimal.Zero) == -1 {
		return errors.New(helper.AmountErr)
	}

	flags := map[int]bool{
		DownPointApply:       true,
		DownPointApplyPass:   true,
		DownPointApplyReject: true,
		UpPointApplyPass:     true,
	}

	fmt.Println("flag:", flag)
	// 非中心钱包转锁定钱包且非锁定钱包转中心钱包
	if _, ok := flags[flag]; !ok {
		return errors.New(helper.ParamErr)
	}

	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	record := g.Record{}
	switch flag {
	case UpPointApplyPass:
		// 获取钱包余额
		mb, err := MemberBalanceFindOne(uid)
		if err != nil {
			return err
		}

		// 中心钱包余额
		balance, _ = decimal.NewFromString(mb.Brl)
		// 中心钱包转出
		balanceAfter = balance.Add(money)

		r := g.Record{
			"state":         AdjustReviewPass, //审核状态:1=审核中,2=审核通过,3=审核未通过
			"review_remark": adjust["review_remark"],
			"review_at":     adjust["review_at"],
			"review_uid":    adjust["review_uid"],
			"review_name":   adjust["review_name"],
		}
		// 更新调整记录状态
		query, _, _ := dialect.Update("tbl_member_adjust").Set(r).Where(g.Ex{"id": adjust["id"]}).ToSQL()
		_, err = tx.Exec(query)
		if err != nil {
			_ = tx.Rollback()
			return pushLog(err, helper.DBErr)
		}

		//4、新增账变记录
		trans := MemberTransaction{
			AfterAmount:  balanceAfter.String(),
			Amount:       money.String(),
			BeforeAmount: balance.String(),
			BillNo:       adjust["id"].(string),
			CreatedAt:    time.Now().UnixMilli(),
			ID:           helper.GenId(),
			CashType:     helper.TransactionUpPoint,
			UID:          uid,
			Username:     username,
		}

		query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		_, err = tx.Exec(query)
		if err != nil {
			_ = tx.Rollback()
			return pushLog(err, helper.DBErr)
		}

		// 中心钱包上分
		record["brl"] = g.L(fmt.Sprintf("brl+%s", money.String()))
	case DownPointApply:
		// 获取钱包余额
		mb, err := MemberBalanceFindOne(uid)
		if err != nil {
			return err
		}

		// 检查中心钱包余额
		balance, _ = decimal.NewFromString(mb.Brl)
		if balance.Sub(money).IsNegative() {
			return errors.New(helper.LackOfBalance)
		}

		// 中心钱包转入
		balanceAfter = balance.Sub(money)
		adjust["amount"] = "-" + adjust["amount"].(string)
		// 新增调整记录
		query, _, _ := dialect.Insert("tbl_member_adjust").Rows(adjust).ToSQL()
		_, err = tx.Exec(query)
		if err != nil {
			_ = tx.Rollback()
			return pushLog(err, helper.DBErr)
		}

	case DownPointApplyPass:
		// 获取钱包余额
		mb, err := MemberBalanceFindOne(uid)
		if err != nil {
			return err
		}

		// 检查锁定钱包余额
		balance, _ = decimal.NewFromString(mb.Brl)
		if balance.Sub(money).IsNegative() {
			return errors.New(helper.LackOfBalance)
		}
		// 中心钱包转出
		balanceAfter = balance.Sub(money)
		r := g.Record{
			"state":         AdjustReviewPass, //审核状态:1=审核中,2=审核通过,3=审核未通过
			"review_remark": adjust["review_remark"],
			"review_at":     adjust["review_at"],
			"review_uid":    adjust["review_uid"],
			"review_name":   adjust["review_name"],
		}
		// 更新调整记录状态
		query, _, _ := dialect.Update("tbl_member_adjust").Set(r).Where(g.Ex{"id": adjust["id"]}).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			_ = tx.Rollback()
			return pushLog(err, helper.DBErr)
		}

		//4、新增账变记录
		trans := MemberTransaction{
			AfterAmount:  balanceAfter.String(),
			Amount:       money.String(),
			BeforeAmount: balance.String(),
			BillNo:       adjust["id"].(string),
			CreatedAt:    time.Now().UnixMilli(),
			ID:           helper.GenId(),
			CashType:     helper.TransactionDownPoint,
			UID:          uid,
			Username:     username,
		}

		query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			_ = tx.Rollback()
			return pushLog(err, helper.DBErr)
		}

		// 中心钱包下分
		record["brl"] = g.L(fmt.Sprintf("brl-%s", money.String()))

	case DownPointApplyReject:
		// 获取钱包余额
		mb, err := MemberBalanceFindOne(uid)
		if err != nil {
			return err
		}

		// 检查锁定钱包余额
		balance, _ = decimal.NewFromString(mb.Brl)

		// 中心钱包转出
		balanceAfter = balance.Add(money)
		r := g.Record{
			"state":         AdjustReviewReject, //审核状态:1=审核中,2=审核通过,3=审核未通过
			"review_remark": adjust["review_remark"],
			"review_at":     adjust["review_at"],
			"review_uid":    adjust["review_uid"],
			"review_name":   adjust["review_name"],
		}
		// 更新调整记录状态
		query, _, _ := dialect.Update("tbl_member_adjust").Set(r).Where(g.Ex{"id": adjust["id"]}).ToSQL()
		_, err = tx.Exec(query)
		if err != nil {
			_ = tx.Rollback()
			return pushLog(err, helper.DBErr)
		}

		err = tx.Commit()
		if err != nil {
			return pushLog(err, helper.DBErr)
		}

		return nil
	}

	ex := g.Ex{
		"uid": uid,
	}
	query, _, _ := dialect.Update("tbl_member_balance").Set(record).Where(ex).ToSQL()
	fmt.Println(query)
	res, err := tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(err, helper.DBErr)
	}

	if r, _ := res.RowsAffected(); r == 0 {
		_ = tx.Rollback()
		return pushLog(err, helper.DBErr)
	}

	err = tx.Commit()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	return nil
}
