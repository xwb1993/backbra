package model

import (
	"admin/contrib/helper"
	ryrpc "admin/rpc"
	"database/sql"
	"errors"
	"fmt"
	"strings"
	"time"

	g "github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/speps/go-hashids"
)

// 查询用户单条数据
func MemberFindByUsername(username string) (ryrpc.TblMemberBase, error) {

	m := ryrpc.TblMemberBase{}

	ex := g.Ex{}
	if username != "" {
		ex["username"] = username
	}

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMember...).Where(ex).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&m, query)
	if err != nil && err != sql.ErrNoRows {
		return m, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return m, errors.New(helper.UsernameErr)
	}

	return m, nil
}

// 查询渠道用户单条数据
func MemberFindByUnameAndOpeId(username, operatorId string) (ryrpc.TblMemberBase, error) {

	m := ryrpc.TblMemberBase{}

	ex := g.Ex{
		"username":    username,
		"operator_id": operatorId,
	}

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMember...).Where(ex).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&m, query)
	if err != nil && err != sql.ErrNoRows {
		return m, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return m, errors.New(helper.UsernameErr)
	}

	return m, nil
}

// 查询业务员用户单条数据
func MemberFindByUnameAndProId(username, proxyId string) (ryrpc.TblMemberBase, error) {

	m := ryrpc.TblMemberBase{}

	ex := g.Ex{
		"username": username,
		"proxy_id": proxyId,
	}

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMember...).Where(ex).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&m, query)
	if err != nil && err != sql.ErrNoRows {
		return m, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return m, errors.New(helper.UsernameErr)
	}

	return m, nil
}

func MemberFindByOperatorId(operatorId string) ([]ryrpc.TblMemberBase, error) {

	var m []ryrpc.TblMemberBase

	ex := g.Ex{}
	if operatorId != "" {
		ex["operator_id"] = operatorId
	}

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMember...).Where(ex).ToSQL()
	err := meta.MerchantDB.Select(&m, query)
	if err != nil && err != sql.ErrNoRows {
		return m, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return m, errors.New(helper.OperatorIdErr)
	}

	return m, nil
}

func MemberFindByProxyId(proxyId string) ([]ryrpc.TblMemberBase, error) {

	var m []ryrpc.TblMemberBase

	ex := g.Ex{}
	if proxyId != "" {
		ex["proxy_id"] = proxyId
	}

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMember...).Where(ex).ToSQL()
	err := meta.MerchantDB.Select(&m, query)
	if err != nil && err != sql.ErrNoRows {
		return m, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return m, errors.New(helper.ProxyIdErr)
	}

	return m, nil
}

func MemberFindByUid(uid string) (ryrpc.TblMemberBase, error) {

	m := ryrpc.TblMemberBase{}

	ex := g.Ex{}
	if uid != "" {
		ex["uid"] = uid
	}

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMember...).Where(ex).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&m, query)
	if err != nil && err != sql.ErrNoRows {
		return m, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return m, errors.New(helper.UsernameErr)
	}

	return m, nil
}

// 查询用户单条数据
func MemberFindByUidAndOpeId(uid, operatorId string) (ryrpc.TblMemberBase, error) {

	m := ryrpc.TblMemberBase{}

	ex := g.Ex{
		"uid":         uid,
		"operator_id": operatorId,
	}

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMember...).Where(ex).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&m, query)
	if err != nil && err != sql.ErrNoRows {
		return m, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return m, errors.New(helper.OperatorIdErr)
	}

	return m, nil
}

// 查询用户单条数据
func MemberFindByUidAndProId(uid, proxyId string) (ryrpc.TblMemberBase, error) {

	m := ryrpc.TblMemberBase{}

	ex := g.Ex{
		"uid":      uid,
		"proxy_id": proxyId,
	}

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMember...).Where(ex).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&m, query)
	if err != nil && err != sql.ErrNoRows {
		return m, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return m, errors.New(helper.OperatorIdErr)
	}

	return m, nil
}

func MemberBalanceFindOne(uid string) (MemberBalance, error) {

	balance := MemberBalance{}
	query, _, _ := dialect.From("tbl_member_balance").Select(colsMemberBalance...).Where(g.Ex{"uid": uid}).Limit(1).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&balance, query)
	if err != nil {
		return balance, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return balance, err
}

type MemberData struct {
	ryrpc.TblMemberBase
	MemberBalance
}

type MemberPageData struct {
	T  int64             `json:"t"`
	D  []MemberData      `json:"d"`
	S  uint              `json:"s"`
	Ip map[string]string `json:"ip"`
}

func MemberList(page, pageSize int, startTime, endTime, agent, adminId string, ex g.Ex) (MemberPageData, error) {

	data := MemberPageData{}
	var err error
	if startTime != "" && endTime != "" {

		startAt, err := helper.TimeToLoc(startTime, loc)
		if err != nil {
			return data, errors.New(helper.TimeTypeErr)
		}

		endAt, err := helper.TimeToLoc(endTime, loc)
		if err != nil {
			return data, errors.New(helper.TimeTypeErr)
		}
		if startAt >= endAt {
			return data, errors.New(helper.QueryTimeRangeErr)
		}

		ex["tm.created_at"] = g.Op{"between": exp.NewRangeVal(startAt, endAt)}
	}

	//验证是否是渠道登录，是的话查询该渠道的会员列表
	agentData := AgentChannelConfigData{}
	query, _, _ := dialect.From("tbl_agent_channel_config").Select(g.COUNT("id")).Where(g.Ex{"id": adminId}).ToSQL()
	fmt.Println(query)
	err = meta.MerchantDB.Get(&agentData.T, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}
	if agentData.T == 1 {
		ex["tm.operator_id"] = adminId
	} else {
		//验证是否是业务员登录，是的话查询该业务员的会员列表
		proxyData := ProxyInfoData{}
		query, _, _ = dialect.From("tbl_proxy_info").Select(g.COUNT("id")).Where(g.Ex{"id": adminId}).ToSQL()
		fmt.Println(query)
		err = meta.MerchantDB.Get(&proxyData.T, query)
		if err != nil && err != sql.ErrNoRows {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}
		if proxyData.T == 1 {
			ex["tm.proxy_id"] = adminId
		}
	}

	t := dialect.From(g.T("tbl_member_base").As("tm"))
	if page == 1 {
		query, _, _ = t.Select(g.COUNT("uid")).Where(ex).ToSQL()
		err = meta.MerchantDB.Get(&data.T, query)
		if err != nil && err != sql.ErrNoRows {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}
	offset := (page - 1) * pageSize
	var d []ryrpc.TblMemberBase
	query, _, _ = t.Select(colsMember...).Where(ex).Order(g.I("tm.created_at").Desc()).Offset(uint(offset)).Limit(uint(pageSize)).ToSQL()
	err = meta.MerchantDB.Select(&d, query)
	if err != nil {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if len(d) == 0 {
		return data, nil
	}

	for _, v := range d {
		mbl, _ := MemberBalanceFindOne(v.Uid)
		val := MemberData{v, mbl}
		data.D = append(data.D[0:], val)
	}
	data.S = uint(pageSize)

	return data, nil
}

func MemberFlushMulti(uids []string) error {

	var data []ryrpc.TblMemberBase

	ex := g.Ex{
		"uid": uids,
	}

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMember...).Where(ex).ToSQL()
	err := meta.MerchantDB.Select(&data, query)
	if err != nil {
		fmt.Println("MemberFlush err = ", err.Error())
		return err
	}

	b, err := helper.JsonMarshal(data)
	if err != nil {
		fmt.Println("MemberFlush JsonMarshal err = ", err.Error())
		return err
	}

	index := meta.Meili.Index("members")
	index.DeleteDocuments(uids)
	_, err = index.AddDocuments(b, "uid")
	if err != nil {
		fmt.Println("MemberFlush AddDocuments err = ", err.Error())
		return err
	}
	return nil
}

func MemberFlushAll() {

	var data []ryrpc.TblMemberBase

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMember...).ToSQL()
	err := meta.MerchantDB.Select(&data, query)
	if err != nil {
		fmt.Println("MemberFlushAll err = ", err.Error())
		return
	}

	fmt.Println("MemberFlushAll len(data) = ", len(data))
	filterable := []string{
		"username",
		"phone",
		"email",
	}
	sortable := []string{
		"created_at",
	}
	searchable := []string{
		"username",
	}

	b, err := helper.JsonMarshal(data)
	if err != nil {
		fmt.Println("MemberFlushAll JsonMarshal err = ", err.Error())
		return
	}

	meta.Meili.DeleteIndex("members")
	index := meta.Meili.Index("members")
	index.UpdateFilterableAttributes(&filterable)
	index.UpdateSortableAttributes(&sortable)
	index.UpdateSearchableAttributes(&searchable)

	_, err = index.AddDocuments(b, "uid")
	if err != nil {
		fmt.Println("MemberFlushAll AddDocuments err = ", err.Error())
		return
	}
	fmt.Println("MemberFlushAll ok")
}

func MemberInsert(username, password, parentId, operatorId, proxyId string, tester int, prefix string) error {

	if MemberExist(username) {
		return errors.New(helper.UsernameExist)
	}
	var phone, email string

	if strings.Contains(username, "@") {
		email = username
	}
	if helper.CtypeDigit(username) {
		phone = username
	}

	sid, err := meta.MerchantRedis.Incr(ctx, "id").Result()
	if err != nil {
		return pushLog(err, helper.RedisErr)
	}

	uid := fmt.Sprintf("%d", sid)
	myCode, err := shortURLGen(uid)
	if err != nil {
		return errors.New(helper.ServerErr)
	}

	ex := g.Ex{}
	if phone != "" {
		ex["phone"] = phone
		if MemberBindCheck(ex) {
			return errors.New(helper.PhoneExist)
		}
	}
	if email != "" {
		ex["email"] = email
		if MemberBindCheck(ex) {
			return errors.New(helper.EmailExist)
		}
	}

	parent := ryrpc.TblMemberBase{
		ParentID: "0",
		GrandID:  "0",
		Uid:      "0",
		TopID:    "0",
	}
	if parentId != "" {
		parent, err = MemberFindByUid(parentId)
		if err != nil {
			return errors.New(helper.UIDErr)
		}
	}
	createdAt := uint32(time.Now().Unix())
	paypwd := fmt.Sprintf("%d", pwdMurmurHash("123456", createdAt))

	pwd := fmt.Sprintf("%d", pwdMurmurHash(password, createdAt))
	m := ryrpc.TblMemberBase{
		Prefix:           prefix,
		Uid:              uid,
		Username:         username,
		Phone:            phone,
		Email:            email,
		TopID:            parent.TopID,
		TopName:          parent.TopName,
		GreatGrandID:     parent.GrandID,
		GreatGrandName:   parent.GrandName,
		GrandID:          parent.ParentID,
		GrandName:        parent.ParentName,
		ParentID:         parent.Uid,
		ParentName:       parent.Username,
		Password:         pwd,
		CreatedIp:        "",
		Vip:              0,
		Score:            0,
		CreatedAt:        createdAt,
		InviteCode:       myCode,
		Tester:           tester,
		PayPassword:      paypwd,
		Telegram:         "",
		CanBonus:         1,
		CanRebate:        1,
		Invited:          0,
		OperatorId:       "0",
		ProxyId:          "0",
		WinFlowMultiple:  "0",
		LoseFlowMultiple: "0",
	}
	if operatorId != "" {
		m.OperatorId = operatorId
	}
	if proxyId != "" {
		m.ProxyId = proxyId
	}
	if phone != "" {
		m.PhoneVerify = 1
		m.EmailVerify = 0
	}
	if email != "" {
		m.PhoneVerify = 0
		m.EmailVerify = 1
	}
	tx, err := meta.MerchantDB.Begin() // 开启事务
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	query, _, _ := dialect.Insert("tbl_member_base").Rows(&m).ToSQL()
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	b := TblMemberBalance{
		Prefix:            prefix,
		Uid:               uid,
		Brl:               0,
		LockAmount:        0,
		UnlockAmount:      0,
		AgencyAmount:      0,
		DepositLockAmount: 0,
		AgencyLockAmount:  0,
	}
	query, _, _ = dialect.Insert("tbl_member_balance").Rows(&b).ToSQL()
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	sign := PromoSignRecord{
		UID:       uid,
		Username:  username,
		Vip:       0,
		Sign1:     "0",
		Sign2:     "0",
		Sign3:     "0",
		Sign4:     "0",
		Sign5:     "0",
		Sign6:     "0",
		Sign7:     "0",
		Sign8:     "0",
		Sign9:     "0",
		Sign10:    "0",
		Sign11:    "0",
		Sign12:    "0",
		Sign13:    "0",
		Sign14:    "0",
		Sign15:    "0",
		Sign16:    "0",
		Sign17:    "0",
		Sign18:    "0",
		Sign19:    "0",
		Sign20:    "0",
		Sign21:    "0",
		Sign22:    "0",
		Sign23:    "0",
		Sign24:    "0",
		Sign25:    "0",
		Sign26:    "0",
		Sign27:    "0",
		Sign28:    "0",
		Sign29:    "0",
		Sign30:    "0",
		SignWeek1: "0",
		SignWeek2: "0",
		SignWeek3: "0",
		SignWeek4: "0",
		SignWeek5: "0",
		SignWeek6: "0",
		SignWeek7: "0",
	}
	query, _, _ = dialect.Insert("tbl_promo_sign_record").Rows(&sign).ToSQL()
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	query = MemberClosureInsert(m.Uid, m.ParentID)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if parent.Uid != "" && parent.Uid != "0" {

		inviteRecord := tblPromoInviteRecord{
			Id:             helper.GenId(),
			Uid:            parent.Uid,
			Username:       parent.Username,
			Lvl:            1,
			ChildUid:       uid,
			ChildUsername:  username,
			FirstDepositAt: 0,
			DepositAmount:  0,
			BonusAmount:    0,
			CreatedAt:      createdAt,
			State:          1,
		}
		query, _, _ = dialect.Insert("tbl_promo_invite_record").Rows(&inviteRecord).ToSQL()

		_, err = tx.Exec(query)
		if err != nil {
			_ = tx.Rollback()
			return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}
		pt := g.Record{
			"invite_register_num": g.L("invite_register_num+1"),
		}
		query, _, _ = dialect.Update("tbl_member_base").Set(pt).Where(g.Ex{"uid": parent.Uid}).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			_ = tx.Rollback()
			return pushLog(err, helper.DBErr)
		}

		ut := g.Record{
			"invited": 1,
		}
		update, _, _ := dialect.Update("tbl_member_base").Set(ut).Where(g.Ex{"uid": uid}).ToSQL()
		fmt.Println(update)
		_, err = tx.Exec(update)
		if err != nil {
			_ = tx.Rollback()
			return pushLog(err, helper.DBErr)
		}
	}

	err = tx.Commit()

	err = BankcardInsert(username, uid, "PIX")
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

// 检测会员账号是否已存在
func MemberExist(username string) bool {

	key := "member:" + username
	exists := meta.MerchantRedis.Exists(ctx, key).Val()
	if exists == 0 {
		return false
	}

	return true
}

// shortURLGen uri 原始url,返回短url
func shortURLGen(uri string) (string, error) {

	//fmt.Println("uri:", uri)
	for i := 0; i < 100; i++ {

		shortCode := shortUrlGenCode()
		key := fmt.Sprintf("shortcode:%s", shortCode)

		err := meta.MerchantRedis.SetNX(ctx, key, uri, 0).Err()
		if err != nil {
			fmt.Println(err)
			continue
			//return "", errors.New(helper.RedisErr)
		}

		return shortCode, nil
	}
	return "", errors.New(helper.RedisErr)
}

// shortUrlGenCode 传入ID生成短码
func shortUrlGenCode() string {

	hd := hashids.NewData()
	hd.Salt = helper.GenId()
	hd.MinLength = 6
	h, _ := hashids.NewWithData(hd)
	shortCode, _ := h.EncodeInt64([]int64{time.Now().UnixMilli()})
	return shortCode
}

func MemberBindCheck(ex g.Ex) bool {

	var id string

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select("uid").Where(ex).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&id, query)
	if err == sql.ErrNoRows {
		return false
	}

	return true
}

func MemberClosureInsert(nodeID, targetID string) string {

	if targetID == "0" {
		targetID = nodeID
	}
	t := "SELECT ancestor, " + nodeID + ",lvl+1 FROM tbl_members_tree WHERE descendant = " + targetID + " UNION SELECT " + nodeID + "," + nodeID + ",0"
	query := "INSERT INTO tbl_members_tree (ancestor, descendant,lvl) (" + t + ")"

	return query
}

func MemberUpdateBonus(uid string, canBonus int) error {

	record := g.Record{
		"can_bonus": canBonus,
	}
	query, _, _ := dialect.Update("tbl_member_base").Set(&record).Where(g.Ex{"uid": uid}).ToSQL()
	fmt.Println(query)
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func MemberUpdateRebate(uid string, canRebate int) error {

	record := g.Record{
		"can_rebate": canRebate,
	}
	query, _, _ := dialect.Update("tbl_member_base").Set(&record).Where(g.Ex{"uid": uid}).ToSQL()
	fmt.Println(query)
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func MemberUpdateState(uid string, state int, remake string) error {

	record := g.Record{
		"state":  state,
		"remake": remake,
	}
	query, _, _ := dialect.Update("tbl_member_base").Set(&record).Where(g.Ex{"uid": uid}).ToSQL()
	fmt.Println(query)
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func MemberUpdateAgentChannel(uid, cid string) error {

	member, err := MemberFindByUid(uid)
	if err != nil {
		return err
	}
	if member.Cid != "0" {
		return errors.New(helper.IsAgentUserAlready)
	}

	ac := TblAgentChannelConfig{}

	t := dialect.From("tbl_agent_channel_config")
	query, _, _ := t.Select(colsAgentChannel...).Where(g.Ex{"id": cid}).Limit(1).ToSQL()
	err = meta.MerchantDB.Get(&ac, query)
	if ac.State != "0" {
		return errors.New(helper.ParentTenantNotExistErr)
	}

	record := g.Record{
		"cid": cid,
	}
	query, _, _ = dialect.Update("tbl_member_base").Set(&record).Where(g.Ex{"uid": uid}).ToSQL()
	fmt.Println(query)
	_, err = meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func MemberUpdateFlowMultiple(uid, winFlowMultiple, loseFlowMultiple string) error {

	record := g.Record{
		"win_flow_multiple":  winFlowMultiple,
		"lose_flow_multiple": loseFlowMultiple,
	}
	query, _, _ := dialect.Update("tbl_member_base").Set(&record).Where(g.Ex{"uid": uid}).ToSQL()
	fmt.Println(query)
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}
