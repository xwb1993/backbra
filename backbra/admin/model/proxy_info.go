package model

import (
	"admin/contrib/helper"
	"admin/contrib/session"
	"database/sql"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"time"
)

// BusinessLoginResp 业务员登录返回数据结构体
type BusinessLoginResp struct {
	Id           string `json:"id" cbor:"id"`                       // id
	Token        string `json:"token" cbor:"token"`                 // 用户token
	LoginAccount string `json:"login_account" cbor:"login_account"` // 登录账号
}

func ProxyInfoList(page, pageSize uint, ex g.Ex) (ProxyInfoData, error) {

	data := ProxyInfoData{}
	t := dialect.From("tbl_proxy_info")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT(1)).Where(ex).ToSQL()
		fmt.Println(query)
		err := meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := t.Select(colsProxyInfo...).Where(ex).Offset(offset).Limit(pageSize).Order(g.C("created_at").Asc()).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	data.S = pageSize
	return data, nil
}

func ProxyInfoInsert(data TblProxyInfo) error {

	record := TblProxyInfo{
		Id:           helper.GenId(),
		AccountName:  data.AccountName,
		LoginAccount: data.LoginAccount,
		Password:     data.Password,
		//InviteUrl:    data.InviteUrl,
		OperatorId: data.OperatorId,
		Type:       data.Type,
		CreatedAt:  time.Now().Unix(),
		UpdatedAt:  time.Now().Unix(),
	}

	record.InviteUrl = meta.Callback + "?ic=" + record.Id
	if data.Type == helper.OrdinarySalesperson {
		record.ParentId = data.ParentId
		record.ParentName = data.ParentName
	} else {
		record.ParentId = "0"
		record.ParentName = ""
	}

	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	query, _, _ := dialect.Insert("tbl_proxy_info").Rows(record).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	err = tx.Commit()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}
	return err
}

func ProxyInfoUpdate(data TblProxyInfo) error {

	record := g.Record{
		"account_name":  data.AccountName,
		"login_account": data.LoginAccount,
		"password":      data.Password,
		"type":          data.Type,
		"updated_at":    time.Now().Unix(),
	}
	if data.Type == helper.OrdinarySalesperson {
		record["parent_id"] = data.ParentId
		record["parent_name"] = data.ParentName
	} else {
		record["parent_id"] = "0"
		record["parent_name"] = ""
	}

	query, _, _ := dialect.Update("tbl_proxy_info").Set(&record).Where(g.Ex{"id": data.Id}).ToSQL()
	fmt.Println(query)
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func BusinessLogin(deviceNo, loginAccount, password string) (BusinessLoginResp, error) {

	rsp := BusinessLoginResp{}
	data := TblProxyInfo{}
	t := dialect.From("tbl_proxy_info")
	query, _, _ := t.Select(colsProxyInfo...).Where(g.Ex{"login_account": loginAccount}).Limit(1).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&data, query)
	if err != nil && err != sql.ErrNoRows {
		return rsp, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	// 账号不存在提示
	if err == sql.ErrNoRows {
		return rsp, errors.New(helper.UserNotExist)
	}

	if password != data.Password {
		return rsp, errors.New(helper.UsernameOrPasswordErr)
	}

	token := fmt.Sprintf("admin:token:%s", data.Id)
	b, _ := helper.JsonMarshal(data)
	sid, err := session.AdminSet(b, token, deviceNo)
	if err != nil {
		fmt.Println("AdminSet = ", err)
		return rsp, errors.New(helper.SessionErr)
	}

	rsp.Token = sid
	rsp.LoginAccount = loginAccount
	rsp.Id = data.Id

	return rsp, nil
}
