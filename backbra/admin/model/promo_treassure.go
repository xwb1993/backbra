package model

import (
	"admin/contrib/helper"
	"admin/contrib/validator"
	"database/sql"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"net/url"
)

func PromoTreasureConfigInsert(data PromoTreasureConfig) error {

	data.ID = helper.GenId()
	query, _, _ := dialect.Insert("tbl_promo_treasure_config").Rows(&data).ToSQL()
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func PromoTreasureConfigUpdate(id string, data PromoTreasureConfig) error {

	query, _, _ := dialect.Update("tbl_promo_treasure_config").Set(&data).Where(g.Ex{"id": id}).ToSQL()
	fmt.Println(query)
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func PromoTreasureConfigList(page, pageSize uint) (PromoTreasureConfigData, error) {

	data := PromoTreasureConfigData{}
	t := dialect.From("tbl_promo_treasure_config")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT(1)).Where(g.Ex{}).ToSQL()
		fmt.Println(query)
		err := meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := t.Select(colsPromoTreasureConfig...).Where(g.Ex{}).Offset(offset).Limit(pageSize).Order(g.C("invite_num").Asc()).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	data.S = pageSize
	return data, nil
}

func PromoTreasureRecordList(page, pageSize uint, ex g.Ex) (PromoTreasureRecordData, error) {

	data := PromoTreasureRecordData{}
	t := dialect.From("tbl_promo_treasure_record")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT(1)).Where(ex).ToSQL()
		fmt.Println(query)
		err := meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := t.Select(colsPromoTreasureRecord...).Where(ex).Offset(offset).Limit(pageSize).Order(g.C("invite_num").Asc()).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	data.S = pageSize
	return data, nil
}

func MerchantPromoTreasureRecordList(page, pageSize uint, username, operatorId string) (PromoTreasureRecordData, error) {

	data := PromoTreasureRecordData{}

	ex := g.Ex{}
	uids := make([]string, 0)
	if username != "" { // 用户名校验
		uName, _ := url.PathUnescape(username)
		if !validator.CheckUName(uName, 5, 50) {
			return data, errors.New(helper.UsernameErr)
		}
		mb, err := MemberFindByUnameAndOpeId(uName, operatorId)
		if err != nil {
			return data, errors.New(helper.UsernameErr)
		}
		ex["username"] = uName
		ex["uid"] = mb.Uid
	} else {
		members, err := MemberFindByOperatorId(operatorId)
		if err != nil {
			return data, errors.New(helper.OperatorIdErr)
		}
		if len(members) > 0 {
			for _, val := range members {
				uids = append(uids, val.Uid)
			}
			ex["uid"] = uids
		}
	}

	t := dialect.From("tbl_promo_treasure_record")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT(1)).Where(ex).ToSQL()
		fmt.Println(query)
		err := meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := t.Select(colsPromoTreasureRecord...).Where(ex).Offset(offset).Limit(pageSize).Order(g.C("invite_num").Asc()).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	data.S = pageSize
	return data, nil
}

func BusinessPromoTreasureRecordList(page, pageSize uint, username, proxyId string) (PromoTreasureRecordData, error) {

	data := PromoTreasureRecordData{}

	ex := g.Ex{}
	uids := make([]string, 0)
	if username != "" { // 用户名校验
		uName, _ := url.PathUnescape(username)
		if !validator.CheckUName(uName, 5, 50) {
			return data, errors.New(helper.UsernameErr)
		}
		mb, err := MemberFindByUnameAndProId(uName, proxyId)
		if err != nil {
			return data, errors.New(helper.UsernameErr)
		}
		ex["username"] = uName
		ex["uid"] = mb.Uid
	} else {
		members, err := MemberFindByProxyId(proxyId)
		if err != nil {
			return data, errors.New(helper.ProxyIdErr)
		}
		if len(members) > 0 {
			for _, val := range members {
				uids = append(uids, val.Uid)
			}
			ex["uid"] = uids
		}
	}

	t := dialect.From("tbl_promo_treasure_record")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT(1)).Where(ex).ToSQL()
		fmt.Println(query)
		err := meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := t.Select(colsPromoTreasureRecord...).Where(ex).Offset(offset).Limit(pageSize).Order(g.C("invite_num").Asc()).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	data.S = pageSize
	return data, nil
}
