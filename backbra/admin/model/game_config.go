package model

import (
	"admin/contrib/helper"
	"database/sql"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
)

func GameConfigUpdate(id string, data TblGameConfig) error {

	record := g.Record{
		"cfg_type":    data.CfgType,
		"cfg_value":   data.CfgValue,
		"description": data.Description,
		"type_id":     data.TypeId,
	}
	query, _, _ := dialect.Update("tbl_game_config").Set(&record).Where(g.Ex{"id": id}).ToSQL()
	fmt.Println(query)
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func GameConfigList(page, pageSize uint, ex g.Ex) (GameConfigData, error) {

	data := GameConfigData{}
	t := dialect.From("tbl_game_config")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT(1)).Where(ex).ToSQL()
		fmt.Println(query)
		err := meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := t.Select(colsGameConfig...).Where(ex).Offset(offset).Limit(pageSize).Order(g.C("id").Asc()).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	data.S = pageSize
	return data, nil
}
