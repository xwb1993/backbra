package model

import (
	"admin/contrib/helper"
	"encoding/json"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"time"
)

type TransTypeResp struct {
	Status bool        `cbor:"status" json:"status"`
	Data   []TransType `cbor:"data" json:"data"`
}

// 游戏列表 分页查询
func LoadTransTypes() error {

	resp := TransTypeResp{
		Status: true,
	}
	query, _, _ := dialect.From("tbl_transaction_types").Select(colsTransType...).Where(g.Ex{}).ToSQL()
	err := meta.MerchantDB.Select(&resp.Data, query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	rec, err := json.Marshal(&resp)
	if err != nil {
		return pushLog(err, helper.FormatErr)
	}

	pipe := meta.MerchantRedis.Pipeline()
	defer pipe.Close()

	pipe.Del(ctx, "transTypes")
	pipe.Set(ctx, "transTypes", rec, 100*time.Hour)
	pipe.Persist(ctx, "transTypes")

	_, _ = pipe.Exec(ctx)

	return nil
}

func TransTypes() ([]byte, error) {
	return meta.MerchantRedis.Get(ctx, "transTypes").Bytes()
}
