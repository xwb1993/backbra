package model

import (
	"admin/contrib/helper"
	"encoding/json"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"strings"
	"time"
)

type Group struct {
	CreateAt   int64  `db:"create_at" json:"create_at" cbor:"create_at"`    //创建时间
	Gid        string `db:"gid" json:"gid" cbor:"gid"`                      //
	Gname      string `db:"gname" json:"gname" cbor:"gname"`                //组名
	Lft        int64  `db:"lft" json:"lft" cbor:"lft"`                      //节点左值
	Lvl        int64  `db:"lvl" json:"lvl" cbor:"lvl"`                      //
	Noted      string `db:"noted" json:"noted" cbor:"noted"`                //备注信息
	Permission string `db:"permission" json:"permission" cbor:"permission"` //权限模块ID
	Rgt        int64  `db:"rgt" json:"rgt" cbor:"rgt"`                      //节点右值
	Pid        string `db:"pid" json:"pid" cbor:"pid"`                      //父节点
	State      int    `db:"state" json:"state" cbor:"state"`                //0:关闭1:开启
}

type GroupInsertParam struct {
	Gid        string `json:"gid" cbor:"gid"`
	Pid        string `json:"pid" cbor:"pid"`
	Gname      string `json:"gname" cbor:"gname"`
	Noted      string `json:"noted" cbor:"noted"`
	Permission string `json:"permission" cbor:"permission"`
	State      int    `json:"state" cbor:"state"`
}

type GroupUpdateParam struct {
	ID         string `json:"id" cbor:"id"`
	Pid        string `json:"pid" cbor:"pid"`
	Gid        string `json:"gid" cbor:"gid"`
	Gname      string `json:"gname" cbor:"gname"`
	Noted      string `json:"noted" cbor:"noted"`
	Permission string `json:"permission" cbor:"permission"`
	State      int    `json:"state" cbor:"state"`
}

func GroupUpdate(gid, adminGid, adminName string, data GroupUpdateParam) error {

	// 所修改分组的权限map
	gPrivMap := make(map[string]bool)
	permissions := strings.Split(data.Permission, ",")
	// 检查新增分组的权限是否大于上级分组
	for _, v := range permissions {
		key := fmt.Sprintf("priv:GM%s", data.Pid)
		exists := meta.MerchantRedis.HExists(ctx, key, v).Val()
		if !exists {
			return errors.New(helper.MethodNoPermission)
		}

		gPrivMap[v] = true
	}

	// 检查当前后台账号是否有权限增加当前分组
	ok, err := groupSubCheck(gid, adminGid)
	if err != nil {
		return err
	}

	if !ok {
		return errors.New(helper.MethodNoPermission)
	}

	// 检查当前分组名是否已存在
	ex := g.Ex{
		"gid":   g.Op{"neq": gid},
		"gname": data.Gname,
	}
	ok, err = groupExistCheck(ex)
	if err != nil {
		return err
	}

	if ok {
		return errors.New(helper.RecordExistErr)
	}

	var parent Group
	ex = g.Ex{
		"gid": data.Pid,
	}
	query, _, _ := dialect.From("tbl_admin_group").Select(colsGroup...).Order(g.C("lvl").Asc()).Where(ex).ToSQL()
	err = meta.MerchantDB.Get(&parent, query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	parentPrivs := strings.Split(parent.Permission, ",")

	//fmt.Println(len(permissions), len(parentPrivs))
	//fmt.Println(permissions, parentPrivs)
	if len(permissions) >= len(parentPrivs) {
		return errors.New(helper.SubPermissionEqualErr)
	}

	// 获取当前分组的下级分组
	subGids, err := groupSubs(gid)
	if err != nil {
		return err
	}

	var subs []Group
	if len(subGids) > 0 {
		ex := g.Ex{
			"gid": subGids,
		}
		query, _, _ = dialect.From("tbl_admin_group").
			Select(colsGroup...).Where(ex).Order(g.C("lvl").Asc()).ToSQL()
		err = meta.MerchantDB.Select(&subs, query)
		if err != nil {
			return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}
	}

	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	if len(subs) > 0 {
		fmt.Println(subs)
		for _, v := range subs {
			privs := ""
			for _, vv := range strings.Split(v.Permission, ",") {
				// 下级权限在分组权限调整后的范围内保留，不在则删除
				if _, ok = gPrivMap[vv]; ok {
					if privs != "" {
						privs += ","
					}
					privs += vv
				}
			}

			record := g.Record{
				"permission": privs,
			}
			query, _, _ := dialect.Update("tbl_admin_group").Set(record).Where(g.Ex{"gid": v.Gid}).ToSQL()
			fmt.Println(query)
			_, err = tx.Exec(query)
			if err != nil {
				return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
			}
		}
	}

	record := g.Record{
		"gname":      data.Gname,
		"noted":      data.Noted,
		"state":      data.State,
		"permission": data.Permission,
	}
	query, _, _ = dialect.Update("tbl_admin_group").Set(record).Where(g.Ex{"gid": gid}).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	_ = tx.Commit()

	//log := fmt.Sprintf("系统管理/角色权限/配置 后台账号[%s],分组名[%s],状态[%s],备注[%s] 配置",
	//	adminName, data["gname"], data["state"], data["noted"])
	//fmt.Println(adminLogInsert("system", log, "update", adminName))

	return LoadGroups()
}

func groupSubs(pid string) ([]string, error) {

	var descendants []string
	ex := g.Ex{
		"ancestor":   pid,
		"descendant": g.Op{"neq": pid},
	}
	query, _, _ := dialect.From("tbl_admin_group_tree").
		Select("descendant").Where(ex).Order(g.C("lvl").Asc()).ToSQL()
	err := meta.MerchantDB.Select(&descendants, query)
	if err != nil {
		return descendants, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return descendants, nil
}

func GroupInsert(adminGid, adminName string, data GroupInsertParam) error {

	gid := helper.GenId()
	record := Group{
		CreateAt:   time.Now().Unix(), //创建时间
		Gid:        gid,               //
		Gname:      data.Gname,        //组名
		Lft:        0,                 //节点左值
		Lvl:        0,                 //
		Noted:      data.Noted,        //备注信息
		Permission: data.Permission,   //权限模块ID
		Rgt:        0,                 //节点右值
		Pid:        data.Pid,          //父节点
		State:      data.State,        //0:关闭1:开启
	}
	privs := strings.Split(data.Permission, ",")
	// 检查新增分组的权限是否大于上级分组
	for _, v := range privs {
		key := fmt.Sprintf("priv:GM%s", data.Pid)
		exists := meta.MerchantRedis.HExists(ctx, key, v).Val()
		if !exists {
			return errors.New(helper.MethodNoPermission)
		}
	}

	// 检查当前后台账号是否有权限增加当前分组
	ok, err := groupSubCheck(data.Pid, adminGid)
	if err != nil {
		return err
	}

	if !ok {
		return errors.New(helper.MethodNoPermission)
	}

	// 检查当前分组名是否已存在
	ex := g.Ex{
		"gname": data.Gname,
	}
	ok, err = groupExistCheck(ex)
	if err != nil {
		return err
	}

	if ok {
		return errors.New(helper.RecordExistErr)
	}

	var parent Group
	ex = g.Ex{
		"gid": data.Pid,
	}
	query, _, _ := dialect.From("tbl_admin_group").Select(colsGroup...).Where(ex).ToSQL()
	err = meta.MerchantDB.Get(&parent, query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	parentPrivs := strings.Split(parent.Permission, ",")
	fmt.Println(len(privs), len(parentPrivs))
	fmt.Println(privs, parentPrivs)
	if len(privs) >= len(parentPrivs) {
		return errors.New(helper.SubPermissionEqualErr)
	}

	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	record.Lvl = parent.Lvl + 1
	query, _, _ = dialect.Insert("tbl_admin_group").Rows(record).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	query = GroupClosureInsert(gid, record.Pid)
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	err = tx.Commit()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	//log := fmt.Sprintf("系统管理/角色权限/新增 后台账号[%s],分组名[%s],状态[%s],备注[%s] 新增",
	//	adminName, data["gname"], data["state"], data["noted"])
	//fmt.Println(adminLogInsert("system", log, "insert", adminName))

	return LoadGroups()
}

func groupExistCheck(ex g.Ex) (bool, error) {

	var count int

	query, _, _ := dialect.From("tbl_admin_group").Select(g.COUNT("gid")).Where(ex).ToSQL()
	err := meta.MerchantDB.Get(&count, query)
	if err != nil {
		return false, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if count == 0 {
		return false, nil
	}

	return true, nil
}

// 检查当前后台账号所属分组是不是所操作分组的上级
func groupSubCheck(gid, parentGid string) (bool, error) {

	var count int
	ex := g.Ex{
		"ancestor":   parentGid,
		"descendant": gid,
	}
	query, _, _ := dialect.From("tbl_admin_group_tree").Select(g.COUNT("descendant")).Where(ex).ToSQL()
	err := meta.MerchantDB.Get(&count, query)
	if err != nil {
		return false, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if count == 0 {
		return false, nil
	}

	return true, nil
}

// 查询当前代理的分组和下级分组gid
func groupSubList(gid string) ([]string, map[string]bool, error) {

	var gids []string
	gidMap := make(map[string]bool)
	ex := g.Ex{
		"ancestor": gid,
	}
	query, _, _ := dialect.From("tbl_admin_group_tree").Select("descendant").Where(ex).ToSQL()
	err := meta.MerchantDB.Select(&gids, query)
	if err != nil {
		return nil, nil, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	for _, v := range gids {
		gidMap[v] = true
	}

	return gids, gidMap, nil
}

type groupListRes struct {
	Status bool    `json:"status" cbor:"status"`
	Data   []Group `json:"data" cbor:"data"`
}

func GroupList(gid, adminGid string) (string, error) {

	if gid != "" {
		ok, err := groupSubCheck(gid, adminGid)
		if err != nil {
			return "[]", err
		}

		if !ok {
			return "[]", errors.New(helper.MethodNoPermission)
		}
	} else {
		gid = adminGid
	}

	gids, _, err := groupSubList(gid)
	if err != nil {
		return "[]", err
	}

	if len(gids) == 0 {
		return "[]", nil
	}

	var groups []Group
	ex := g.Ex{
		"gid": gids,
	}
	query, _, _ := dialect.From("tbl_admin_group").Select(colsGroup...).Where(ex).Order(g.C("create_at").Asc()).ToSQL()
	fmt.Println(query)
	err = meta.MerchantDB.Select(&groups, query)
	if err != nil {
		return "[]", pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	res := groupListRes{
		Status: true,
		Data:   groups,
	}
	recs, err := json.Marshal(res)
	if err != nil {
		return "[]", errors.New(helper.FormatErr)
	}

	//key := fmt.Sprintf("%s:priv:GroupAll", meta.Prefix)
	//val, err := meta.MerchantRedis.Get(ctx, key).Result()
	//if err != nil && err != redis.Nil {
	//	return val, pushLog(err, helper.RedisErr)
	//}

	return string(recs), nil
}

func GroupClosureInsert(nodeID, targetID string) string {

	t := "SELECT ancestor, " + nodeID + ", lvl+1 FROM tbl_admin_group_tree WHERE descendant = " + targetID + " UNION SELECT " + nodeID + "," + nodeID + ",0"
	query := "INSERT INTO tbl_admin_group_tree (ancestor, descendant,lvl) (" + t + ")"
	return query
}

type privListRes struct {
	Status bool   `json:"status" cbor:"status"`
	Data   []Priv `json:"data" cbor:"data"`
}

func LoadGroups() error {

	groupsRes := groupListRes{
		Status: true,
	}
	privsRes := privListRes{
		Status: true,
	}
	cols := []interface{}{"noted", "gid", "gname", "permission", "create_at", "state", "lft", "rgt", "lvl", "pid"}
	ex := g.Ex{}

	query, _, _ := dialect.From("tbl_admin_group").Select(cols...).Where(ex).ToSQL()
	query = "/* master */ " + query
	err := meta.MerchantDB.Select(&groupsRes.Data, query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	query, _, _ = dialect.From("tbl_admin_priv").
		Select("pid", "state", "id", "name", "sortlevel", "module").Order(g.C("sortlevel").Asc()).ToSQL()
	query = "/* master */ " + query
	err = meta.MerchantDB.Select(&privsRes.Data, query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if len(groupsRes.Data) == 0 || len(privsRes.Data) == 0 {
		return nil
	}

	privMap := make(map[string]Priv)
	permission := ""
	for _, v := range privsRes.Data {
		privMap[v.ID] = v
		if permission != "" {
			permission += ","
		}
		permission += v.ID
	}

	record := g.Ex{
		"permission": permission,
	}
	query, _, _ = dialect.Update("tbl_admin_group").Set(record).Where(g.Ex{"gid": 2}).ToSQL()
	query = "/* master */ " + query
	fmt.Println(query)
	_, err = meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	recs, err := json.Marshal(groupsRes)
	if err != nil {
		return errors.New(helper.FormatErr)
	}

	pipe := meta.MerchantRedis.Pipeline()
	defer pipe.Close()

	key := "priv:GroupAll"
	pipe.Unlink(ctx, key)
	pipe.Set(ctx, key, string(recs), 100*time.Hour)
	pipe.Persist(ctx, key)

	for _, val := range groupsRes.Data {

		id := fmt.Sprintf("priv:GM%s", val.Gid)
		pipe.Unlink(ctx, id)
		// 只保存开启状态的分组
		if val.State == 1 {
			gKey := fmt.Sprintf("priv:list:GM%s", val.Gid)
			pipe.Unlink(ctx, gKey)
			if val.Gid != "2" {
				privsRes := privListRes{
					Status: true,
				}
				for _, v := range strings.Split(val.Permission, ",") {
					pipe.HSet(ctx, id, v, "1")
					privsRes.Data = append(privsRes.Data, privMap[v])
				}
				gRecs, _ := json.Marshal(privsRes)
				pipe.Set(ctx, gKey, string(gRecs), 100*time.Hour)
			} else {
				for _, v := range privsRes.Data {
					pipe.HSet(ctx, id, v.ID, "1")
				}
				gRecs, _ := json.Marshal(privsRes)
				pipe.Set(ctx, gKey, string(gRecs), 100*time.Hour)
			}
			pipe.Persist(ctx, id)
			pipe.Persist(ctx, gKey)
		}
	}
	_, err = pipe.Exec(ctx)
	if err != nil {
		return pushLog(err, helper.RedisErr)
	}

	return nil
}
