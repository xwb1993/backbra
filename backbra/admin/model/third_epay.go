package model

import (
	"admin/contrib/helper"
	"admin/contrib/validator"
	"errors"
	"fmt"
	"github.com/shopspring/decimal"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"strconv"
	"strings"
	"time"
)

type thirdEpay struct {
}
type EpayCallBack struct {
	MchId      int    `json:"mch_id"`
	Code       int    `json:"code"`
	OrderNo    string `json:"order_no"`
	DisOrderNo string `json:"dis_order_no"`
	OrderPrice int    `json:"order_price"`
	RealPrice  int    `json:"real_price"`
	OrderCost  int    `json:"order_cost"`
	NtiTime    int    `json:"nti_time"`
	Sign       string `json:"sign"`
}

type EpayWithdrawCallBack struct {
	OrderNo    string `json:"order_no"`
	MchOrderNo string `json:"mch_order_no"`
	Amount     int    `json:"amount"`
	Status     int    `json:"status"`
	Fee        int    `json:"fee"`
	Sign       int64  `json:"sign"`
}

func (that *thirdEpay) DepositCallBack(fctx *fasthttp.RequestCtx) (map[string]string, error) {

	m := map[string]string{}
	params := EpayCallBack{}
	fmt.Println(string(fctx.PostBody()))
	if err := helper.JsonUnmarshal(fctx.PostBody(), &params); err != nil {
		fmt.Println("epay param format err:", err.Error())
		return m, errors.New(helper.ParamErr)
	}
	fmt.Println(params)
	m["order_id"] = params.OrderNo
	m["amount"] = fmt.Sprintf(`%d`, params.OrderPrice)
	m["success_time"] = fmt.Sprintf(`%d`, params.NtiTime)
	if params.Code != 1 {
		m["state"] = fmt.Sprintf(`%d`, DepositCancelled)
	} else {
		m["state"] = fmt.Sprintf(`%d`, DepositSuccess)
	}

	return m, nil
}

func (that *thirdEpay) Withdraw(fctx *fasthttp.RequestCtx, ts time.Time, uid, amount, orderNo, accountNo, accountName, pixId, flag string, p TblPayFactory) (paymentWithdrawResp, error) {

	var (
		iamount     int64
		accountType = int64(0)
		pixType     string
	)
	if flag == "1" {
		pixType = "CPF"
		accountType = 1007
	} else if flag == "2" {
		pixType = "PHONE"
		accountNo = "+55" + accountNo
		accountType = 1003
	} else if flag == "3" {
		pixType = "EMAIL"
		accountType = 1003
	}
	//accountType = 1012

	da, err := decimal.NewFromString(amount)
	if err != nil {
		fmt.Println("err:", err)
		return paymentWithdrawResp{}, errors.New(helper.AmountErr)
	}
	iamount = da.IntPart()

	dp, err := withdrawEpayOrder(accountType, iamount*10000, p.Mchid, accountNo, accountName, pixId, pixType, p.Url, p.AppKey, orderNo, meta.Callback+"ew")
	if err != nil {
		fmt.Println("err:", err)
		return dp, err
	}
	return dp, nil
}

func (that *thirdEpay) WithdrawCallBack(fctx *fasthttp.RequestCtx) (map[string]string, error) {

	m := map[string]string{}
	params := EpayWithdrawCallBack{}
	if err := helper.JsonUnmarshal(fctx.PostBody(), &params); err != nil {
		fmt.Println("niki param format err:", err.Error())
		return m, errors.New(helper.ParamErr)
	}

	m["order_id"] = params.OrderNo
	m["amount"] = fmt.Sprintf(`%d`, params.Amount)
	if params.Status != 2 {
		m["state"] = fmt.Sprintf(`%d`, WithdrawFailed)
	} else {
		m["state"] = fmt.Sprintf(`%d`, WithdrawSuccess)
	}

	return m, nil
}

func (that *thirdEpay) Deposit(fctx *fasthttp.RequestCtx, ts time.Time, uid, amount, orderNo string, p TblPayFactory) (paymentDepositResp, error) {

	ip := helper.FromRequest(fctx)

	var (
		mid     int64
		appid   int64
		paycode int64
		iamount int64
	)
	if validator.CtypeDigit(p.Mchid) {
		mid, _ = strconv.ParseInt(p.Mchid, 10, 64)
	}
	if validator.CtypeDigit(p.AppId) {
		appid, _ = strconv.ParseInt(p.AppId, 10, 64)
	}
	if validator.CtypeDigit(p.PayCode) {
		paycode, _ = strconv.ParseInt(p.PayCode, 10, 64)
	}
	if validator.CtypeDigit(amount) {
		iamount, _ = strconv.ParseInt(amount, 10, 64)
	}

	return createEpayOrder(mid, appid, paycode, iamount*100, p.Url, p.AppKey, orderNo, ip, uid, meta.Callback+"epay", meta.WebUrl)
}

func createEpayOrder(mchid, appId, payCode, amount int64, url, appKey, orderId, ip, uid, payNoticeUrl, payJumpUrl string) (paymentDepositResp, error) {

	resp := paymentDepositResp{
		OrderID: orderId,
	}
	ts := time.Now().Unix()
	content := strings.ToUpper(fmt.Sprintf(`app_id=%d&mch_id=%d&order_no=%s&pay_code=%d&pay_jump_url=%s&pay_notice_url=%s&price=%d&time=%d&key=%s`, appId, mchid, orderId, payCode, payJumpUrl, payNoticeUrl, amount, ts, appKey))
	fmt.Println(content)
	sign := strings.ToLower(helper.MD5Hash(content))
	recs := map[string]interface{}{
		"mch_id":         mchid,
		"pay_code":       payCode,
		"order_no":       orderId,
		"price":          amount,
		"app_id":         appId,
		"user_ip":        ip,
		"user_id":        uid,
		"pay_notice_url": payNoticeUrl,
		"pay_jump_url":   payJumpUrl,
		"time":           fmt.Sprintf(`%d`, ts),
		"sign":           sign,
	}
	requestBody, err := helper.JsonMarshal(recs)
	if err != nil {
		return resp, errors.New(helper.FormatErr)
	}
	fmt.Println(string(requestBody))
	header := map[string]string{
		"Content-Type": "application/json",
	}
	requestURI := fmt.Sprintf("%s/CreateOrder", url)
	fmt.Println(requestURI)
	body, statusCode, err := httpDoTimeout(requestBody, "POST", requestURI, header, time.Second*8)
	if err != nil || statusCode != 200 {
		fmt.Println("statusCode:", statusCode)
		fmt.Println("response:", string(body))
		fmt.Println("err:", err)
		return resp, errors.New(helper.PayServerErr)
	}
	fmt.Println(string(body))

	var p fastjson.Parser
	v, err := p.ParseBytes(body)
	if err != nil {
		return resp, fmt.Errorf(helper.PayServerErr)
	}

	if v.GetInt("code") != 0 {
		return resp, fmt.Errorf(helper.PayServerErr)
	}
	d := v.Get("data")
	resp.Addr = string(d.GetStringBytes("pay_url"))
	resp.Oid = string(d.GetStringBytes("Dis_order_no"))

	return resp, nil
}

func withdrawEpayOrder(accountType, amount int64, mchid, accountNo, accountName, pixId, pixType, url, appKey, orderId, payNoticeUrl string) (paymentWithdrawResp, error) {

	resp := paymentWithdrawResp{
		OrderID: orderId,
	}
	content := fmt.Sprintf(`account_name=%s&account_no=%s&account_type=%d&amount=%d&bank_city=%s&bank_code=PIX&bank_name=%s&bank_province=%s&call_back_url=%s&mch_account=%s&order_no=%s&pay_type=1&key=%s`,
		accountName, accountNo, accountType, amount, pixType, pixType, pixId, payNoticeUrl, mchid, orderId, appKey)
	fmt.Println(content)
	sign := strings.ToUpper(helper.MD5Hash(content))
	recs := map[string]interface{}{
		"order_no":      orderId,
		"mch_account":   mchid,
		"amount":        amount,
		"account_type":  accountType,
		"account_no":    accountNo,
		"account_name":  accountName,
		"bank_code":     "PIX",
		"bank_province": pixId,
		"bank_city":     pixType,
		"bank_name":     pixType,
		"call_back_url": payNoticeUrl,
		"pay_type":      "1",
		"sign":          sign,
	}
	requestBody, err := helper.JsonMarshal(recs)
	if err != nil {
		return resp, errors.New(helper.FormatErr)
	}
	fmt.Println(string(requestBody))
	header := map[string]string{
		"Content-Type": "application/json",
	}
	requestURI := fmt.Sprintf("%s", url)
	fmt.Println(requestURI)
	body, statusCode, err := httpDoTimeout(requestBody, "POST", requestURI, header, time.Second*8)
	if err != nil || statusCode != 200 {
		fmt.Println("statusCode:", statusCode)
		fmt.Println("response:", string(body))
		fmt.Println("err:", err)
		return resp, errors.New(helper.PayServerErr)
	}
	fmt.Println(string(body))

	var p fastjson.Parser
	v, err := p.ParseBytes(body)
	if err != nil {
		return resp, fmt.Errorf(helper.PayServerErr)
	}

	if v.GetInt("ret") != 200 {
		return resp, fmt.Errorf(helper.PayServerErr)
	}
	d := v.Get("data")
	resp.Oid = string(d.GetStringBytes("mch_order_no"))

	return resp, nil
}
