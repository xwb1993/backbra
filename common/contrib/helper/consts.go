package helper

// 帐变类型
const (
	// 存取款相关
	TransactionDeposit            = 1  //存款
	TransactionWithDraw           = 2  //提现
	TransactionOfflineDeposit     = 3  //线下转卡存款
	TransactionUSDTOfflineDeposit = 4  //USDT线下存款
	TransactionDepositFee         = 5  //存款手续费
	TransactionDepositBonus       = 6  //存款优惠
	TransactionFinanceDownPoint   = 7  //财务下分
	TransactionWithDrawFail       = 8  //提现失败
	TransactionWithdrawalFee      = 9  //提现手续费
	TransactionWithdrawalFeeRem   = 10 //提现手续费返还
	// 后台上下分相关
	TransactionUpPoint        = 101 //后台上分
	TransactionDownPoint      = 102 //后台下分
	TransactionDownPointBack  = 103 //后台下分回退
	TransactionSetBalanceZero = 104 //中心钱包余额冲正
	TransactionDividend       = 105 //中心钱包红利派发
	// 会员返水佣金相关
	TransactionRebate         = 201 //会员返水
	TransactionCommissionDraw = 202 //佣金提取
	TransactionSubRebateLv1   = 203 //下级返水（1级）
	TransactionSubRebateLv2   = 204 //下级返水（2级）
	TransactionSubRebateLv3   = 205 //下级返水（3级）
	// 会员活动相关
	TransactionVIPUpgrade           = 301 //vip晋级礼金
	TransactionVIPMonthly           = 302 //vip月红包
	TransactionVIpBirthday          = 303 //vip生日礼金
	TransactionFirstDepositDividend = 304 //首存活动红利
	TransactionInviteDividend       = 305 //邀请好友红利
	TransactionSignDividend         = 306 //天天签到活动红利
	TransactionTreasureDividend     = 307 //宝箱活动红利
	TransactionWeeklyDividend       = 308 //周投注活动红利
	TransactionInvitationBonus      = 309 //邀请奖金
	TransactionFlowBonus            = 310 //流水奖金
	// 场馆游戏相关
	TransactionBet                  = 401 //投注
	TransactionBetCancel            = 402 //投注取消
	TransactionPayout               = 403 //派彩
	TransactionResettlePlus         = 404 //重新结算加币
	TransactionResettleDeduction    = 405 //重新结算减币
	TransactionCancelPayout         = 406 //取消派彩
	TransactionSettledBetCancel     = 407 //投注取消(已结算注单)
	TransactionCancelledBetRollback = 408 //已取消注单回滚
	TransactionBetNSettleWin        = 409 //电子投付赢
	TransactionBetNSettleLose       = 410 //电子投付输
	TransactionBetNSettleWinCancel  = 411 //投付赢取消
	TransactionBetNSettleLoseCancel = 412 //投付输取消
	TransactionAdjustPlus           = 413 //场馆调整加
	TransactionAdjustDiv            = 414 //场馆调整减
	TransactionPromoPayout          = 415 //场馆活动派彩
	TransactionEVOPrize             = 416 //游戏奖金(EVO)
	TransactionEVOPromote           = 417 //推广(EVO)
	TransactionEVOJackpot           = 418 //头奖(EVO)
	TransactionReserve              = 419 //预投注(BTI)
	TransactionDebitReserve         = 420 //注单资讯(BTI)
	TransactionCommitReserve        = 421 //确认投注(BTI)
	TransactionCancelReserve        = 422 //取消投注(BTI)
	TransactionCreditCustomer       = 423 //派彩(BTI)
	TransactionDebitCustomer        = 424 //重新结算(BTI)
	TransactionBonusWin             = 425 //免费旋转奖励(PP)
	TransactionJackPotWin           = 426 //累积奖金赢奖(PP)
	TransactionPromoWin             = 427 //促销赢奖(PP)
	OrdinarySalesperson             = "普通业务员"
)

var CashTypes = map[int]bool{
	// 存取款相关
	TransactionDeposit:            true, //存款
	TransactionWithDraw:           true, //提现
	TransactionOfflineDeposit:     true, //线下转卡存款
	TransactionUSDTOfflineDeposit: true, //USDT线下存款
	TransactionDepositFee:         true, //存款手续费
	TransactionDepositBonus:       true, //存款优惠
	TransactionFinanceDownPoint:   true, //财务下分
	TransactionWithDrawFail:       true, //提现失败
	TransactionWithdrawalFee:      true,
	TransactionWithdrawalFeeRem:   true,
	// 后台上下分相关
	TransactionUpPoint:        true, //后台上分
	TransactionDownPoint:      true, //后台下分
	TransactionDownPointBack:  true, //后台下分回退
	TransactionSetBalanceZero: true, //中心钱包余额冲正
	TransactionDividend:       true, //中心钱包红利派发
	// 会员返水佣金相关
	TransactionRebate:         true, //会员返水
	TransactionCommissionDraw: true, //佣金提取
	TransactionSubRebateLv1:   true, //下级返水（1级）
	TransactionSubRebateLv2:   true, //下级返水（2级）
	TransactionSubRebateLv3:   true, //下级返水（3级）
	// 会员活动相关
	TransactionVIPUpgrade:           true, //vip晋级礼金
	TransactionVIPMonthly:           true, //vip月红包
	TransactionVIpBirthday:          true, //vip生日礼金
	TransactionFirstDepositDividend: true, //首存活动红利
	TransactionInviteDividend:       true, //邀请好友红利
	TransactionSignDividend:         true, //天天签到活动红利
	TransactionTreasureDividend:     true, //宝箱活动红利
	TransactionWeeklyDividend:       true, //周投注活动红利
	// 场馆游戏相关
	TransactionBet:                  true, //投注
	TransactionBetCancel:            true, //投注取消
	TransactionPayout:               true, //派彩
	TransactionResettlePlus:         true, //重新结算加币
	TransactionResettleDeduction:    true, //重新结算减币
	TransactionCancelPayout:         true, //取消派彩
	TransactionSettledBetCancel:     true, //投注取消(已结算注单)
	TransactionCancelledBetRollback: true, //已取消注单回滚
	TransactionBetNSettleWin:        true, //电子投付赢
	TransactionBetNSettleLose:       true, //电子投付输
	TransactionBetNSettleWinCancel:  true, //投付赢取消
	TransactionBetNSettleLoseCancel: true, //投付输取消
	TransactionAdjustPlus:           true, //场馆调整加
	TransactionAdjustDiv:            true, //场馆调整减
	TransactionPromoPayout:          true, //场馆活动派彩
	TransactionEVOPrize:             true, //游戏奖金(EVO)
	TransactionEVOPromote:           true, //推广(EVO)
	TransactionEVOJackpot:           true, //头奖(EVO)
	TransactionReserve:              true, //预投注(BTI)
	TransactionDebitReserve:         true, //注单资讯(BTI)
	TransactionCommitReserve:        true, //确认投注(BTI)
	TransactionCancelReserve:        true, //取消投注(BTI)
	TransactionCreditCustomer:       true, //派彩(BTI)
	TransactionDebitCustomer:        true, //重新结算(BTI)
	TransactionBonusWin:             true, //免费旋转奖励(PP)
	TransactionJackPotWin:           true, //累积奖金赢奖(PP)
	TransactionPromoWin:             true, //促销赢奖(PP)
}

const (
	// 真人
	EVO      = "16595015200101" //EVO真人
	VIVO     = "16595015200102" //VIVO真人
	PPCasino = "16595015200103" //PP真人
	// 捕鱼
	JILIFISH = "16595015200201" //JILI捕鱼
	JDBFISH  = "16595015200202" //JILI捕鱼
	// 电子
	JILI      = "16595015200301" //JILI电子
	PP        = "16595015200302" //PP电子
	PG        = "16595015200303" //PG电子
	JDBSpribe = "16595015200304" //JDB SPRIBE
	JDBSlot   = "16595015200305" //JDB SPRIBE
	OnePG     = "16595015200306" //ONE PG
	OnePP     = "16595015200307" //ONE PP
	OneJILI   = "16595015200308" //ONE JILI
	// 体育
	IBCSB = "16595015200401" //SABA体育
	VIVER = "16595015200402" //VIVER体育
	PPSB  = "16595015200403" //PP虚拟体育
	BTI   = "16595015200404" //BTI体育
	PB    = "16595015200405" //平博体育
	// 棋牌
	JILIPoker = "16595015200501" //JILI棋牌
	// 电竞
	IBCES = "16595015200601" //SABA电竞
)

var (
	BRPlatforms = map[string]bool{
		// 真人
		EVO:      true, //EVO真人
		VIVO:     true, //VIVO真人
		PPCasino: true, //PP真人
		// 捕鱼
		JILIFISH: true, //JILI捕鱼
		JDBFISH:  true, //JDB捕鱼
		// 电子
		JILI: true, //JILI电子
		PP:   true, //PP电子
		PG:   true, //PG电子
		// 小游戏
		JDBSpribe: true, //JDB SPRIBE
		JDBSlot:   true, //JDB电子
		OnePG:     true, //ONE PG电子
		OnePP:     true, //ONE PP电子
		OneJILI:   true, //ONE JILI
		// 体育
		IBCSB: true, //SABA体育
		VIVER: true, //VIVER体育
		PPSB:  true, //PP虚拟体育
		BTI:   true, //BTI体育
		PB:    true, //平博体育
	}
	BRPlatformNames = map[string]string{
		// 真人
		EVO:      "16595015200101", //EVO真人
		VIVO:     "16595015200102", //VIVO真人
		PPCasino: "16595015200103", //PP真人
		// 捕鱼
		JILIFISH: "16595015200201", //JILI捕鱼
		// 电子
		JILI: "16595015200301", //JILI电子
		PP:   "16595015200302", //PP电子
		PG:   "16595015200303", //PG电子
		// 小游戏
		JDBSpribe: "16595015200304", //JDB SPRIBE
		JDBSlot:   "16595015200305", //JDB电子
		OnePG:     "16595015200306", //ONE PG电子
		OnePP:     "16595015200307", //ONE PP
		OneJILI:   "16595015200308", //ONE JILI
		// 体育
		IBCSB: "16595015200401", //SABA体育
		VIVER: "16595015200402", //VIVER体育
		PPSB:  "16595015200403", //PP虚拟体育
		BTI:   "16595015200404", //BTI体育
		PB:    "16595015200405", //平博体育
		// 棋牌
		JILIPoker: "16595015200501", //JILI棋牌
	}
	BRCasinoPlatforms = map[string]bool{
		EVO:      true, //EVO真人
		VIVO:     true, //VIVO真人
		PPCasino: true, //PP真人
	}
	BRSportPlatforms = map[string]bool{
		IBCSB: true, //SABA体育
		VIVER: true, //VIVER体育
		PPSB:  true, //PP虚拟体育
		BTI:   true, //BTI体育
		PB:    true, //平博体育
	}
	BRFishingPlatforms = map[string]bool{
		JILIFISH: true, //JILI捕鱼
		JDBFISH:  true, //JDB捕鱼
	}
	BRLottPlatforms = map[string]bool{
		JILI:      true, //JILI电子
		PP:        true, //PP电子
		PG:        true, //PG电子
		JDBSpribe: true, //JDB SPRIBE
		JDBSlot:   true, //JDB电子
	}
	BRPokerPlatforms = map[string]bool{
		JILIPoker: true, //JILI捕鱼
	}
)
